/*
 Navicat Premium Data Transfer

 Source Server         : sqlserver
 Source Server Type    : SQL Server
 Source Server Version : 15002000
 Source Host           : localhost:1433
 Source Catalog        : jbolt
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 15002000
 File Encoding         : 65001

 Date: 23/06/2021 12:04:14
*/


-- ----------------------------
-- Table structure for jb_application
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_application]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_application]
GO

CREATE TABLE [dbo].[jb_application] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [brief_info] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [app_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [app_secret] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL,
  [user_id] int  NULL,
  [update_user_id] int  NULL,
  [type] int  NOT NULL,
  [need_check_sign] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [link_target_id] int  NULL
)
GO

ALTER TABLE [dbo].[jb_application] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'应用名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'应用简介',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'brief_info'
GO

EXEC sp_addextendedproperty
'MS_Description', N'应用ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'app_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'应用密钥',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'app_secret'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'app类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否需要接口校验SIGN',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'need_check_sign'
GO

EXEC sp_addextendedproperty
'MS_Description', N'关联目标ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_application',
'COLUMN', N'link_target_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'API应用中心的应用APP',
'SCHEMA', N'dbo',
'TABLE', N'jb_application'
GO


-- ----------------------------
-- Table structure for jb_brand
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_brand]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_brand]
GO

CREATE TABLE [dbo].[jb_brand] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [sort_rank] int  NOT NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [logo] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [remark] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [english_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_brand] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'品牌名',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'网址',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand',
'COLUMN', N'url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'logo地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand',
'COLUMN', N'logo'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注描述',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand',
'COLUMN', N'remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'英文名',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand',
'COLUMN', N'english_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'品牌',
'SCHEMA', N'dbo',
'TABLE', N'jb_brand'
GO


-- ----------------------------
-- Table structure for jb_change_log
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_change_log]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_change_log]
GO

CREATE TABLE [dbo].[jb_change_log] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [content] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime  NULL,
  [jbolt_version_id] int  NULL
)
GO

ALTER TABLE [dbo].[jb_change_log] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_change_log',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'日志内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_change_log',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_change_log',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jbolt版本ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_change_log',
'COLUMN', N'jbolt_version_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'changeLog',
'SCHEMA', N'dbo',
'TABLE', N'jb_change_log'
GO


-- ----------------------------
-- Table structure for jb_demotable
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_demotable]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_demotable]
GO

CREATE TABLE [dbo].[jb_demotable] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [demo_date] date  NULL,
  [demo_time] time(7)  NULL,
  [demo_date_time] datetime  NULL,
  [demo_week] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [demo_month] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [time] time(7)  NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [pid] int  NULL,
  [sort_rank] int  NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [age] int  NULL,
  [brief_info] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_system_admin] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [price] decimal(10,2)  NULL,
  [amount] decimal(10,2)  NULL,
  [goods_unit] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [total] decimal(10,2)  NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[jb_demotable] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'demo日期',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'demo_date'
GO

EXEC sp_addextendedproperty
'MS_Description', N'demo时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'demo_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'demo日期与时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'demo_date_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'demo周',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'demo_week'
GO

EXEC sp_addextendedproperty
'MS_Description', N'demo月',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'demo_month'
GO

EXEC sp_addextendedproperty
'MS_Description', N'demo纯时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'父级ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'pid'
GO

EXEC sp_addextendedproperty
'MS_Description', N'序号',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'姓名',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'年龄',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'age'
GO

EXEC sp_addextendedproperty
'MS_Description', N'简介',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'brief_info'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否为管理',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'is_system_admin'
GO

EXEC sp_addextendedproperty
'MS_Description', N'单价',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'price'
GO

EXEC sp_addextendedproperty
'MS_Description', N'数量',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'amount'
GO

EXEC sp_addextendedproperty
'MS_Description', N'单位',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'goods_unit'
GO

EXEC sp_addextendedproperty
'MS_Description', N'总额',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'total'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_demotable',
'COLUMN', N'update_time'
GO


-- ----------------------------
-- Table structure for jb_dept
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_dept]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_dept]
GO

CREATE TABLE [dbo].[jb_dept] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [full_name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [pid] int  NOT NULL,
  [type] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [leader] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [phone] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [email] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [address] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [zipcode] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [remark] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [sn] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sort_rank] int  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '1' NOT NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[jb_dept] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'全称',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'full_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'父级ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'pid'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'负责人',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'leader'
GO

EXEC sp_addextendedproperty
'MS_Description', N'手机号',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'phone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'电子邮箱',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'email'
GO

EXEC sp_addextendedproperty
'MS_Description', N'地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'address'
GO

EXEC sp_addextendedproperty
'MS_Description', N'邮政编码',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'zipcode'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'编码',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'sn'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门管理',
'SCHEMA', N'dbo',
'TABLE', N'jb_dept'
GO


-- ----------------------------
-- Table structure for jb_dictionary
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_dictionary]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_dictionary]
GO

CREATE TABLE [dbo].[jb_dictionary] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [type_id] int  NULL,
  [pid] int  NULL,
  [sort_rank] int  NULL,
  [sn] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [type_key] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_dictionary] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典ID主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典类型ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary',
'COLUMN', N'type_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'父类ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary',
'COLUMN', N'pid'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'编号编码',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary',
'COLUMN', N'sn'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典类型KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary',
'COLUMN', N'type_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典表',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary'
GO


-- ----------------------------
-- Table structure for jb_dictionary_type
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_dictionary_type]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_dictionary_type]
GO

CREATE TABLE [dbo].[jb_dictionary_type] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [mode_level] int  NULL,
  [type_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_dictionary_type] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary_type',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary_type',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'模式层级',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary_type',
'COLUMN', N'mode_level'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标识KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary_type',
'COLUMN', N'type_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'字典类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_dictionary_type'
GO


-- ----------------------------
-- Table structure for jb_download_log
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_download_log]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_download_log]
GO

CREATE TABLE [dbo].[jb_download_log] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [ipaddress] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [download_type] int  NULL,
  [download_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[jb_download_log] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'下载地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_download_log',
'COLUMN', N'ipaddress'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下载类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_download_log',
'COLUMN', N'download_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下载时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_download_log',
'COLUMN', N'download_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下载log',
'SCHEMA', N'dbo',
'TABLE', N'jb_download_log'
GO


-- ----------------------------
-- Table structure for jb_global_config
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_global_config]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_global_config]
GO

CREATE TABLE [dbo].[jb_global_config] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [config_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [config_value] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime  NOT NULL,
  [user_id] int  NULL,
  [update_time] datetime  NULL,
  [update_user_id] int  NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [value_type] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [type_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [type_id] int  NULL,
  [built_in] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL
)
GO

ALTER TABLE [dbo].[jb_global_config] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'参数KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'config_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'参数值',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'config_value'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'参数名',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'值类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'value_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'参数类型KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'type_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'参数类型ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'type_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'系统内置',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config',
'COLUMN', N'built_in'
GO

EXEC sp_addextendedproperty
'MS_Description', N'全局参数配置',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config'
GO


-- ----------------------------
-- Table structure for jb_global_config_type
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_global_config_type]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_global_config_type]
GO

CREATE TABLE [dbo].[jb_global_config_type] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [sort_rank] int  NULL,
  [type_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [built_in] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL
)
GO

ALTER TABLE [dbo].[jb_global_config_type] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config_type',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config_type',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config_type',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'参数类型KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config_type',
'COLUMN', N'type_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'系统内置',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config_type',
'COLUMN', N'built_in'
GO

EXEC sp_addextendedproperty
'MS_Description', N'全局参数类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_global_config_type'
GO


-- ----------------------------
-- Table structure for jb_goods
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_goods]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_goods]
GO

CREATE TABLE [dbo].[jb_goods] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [price] decimal(10,2)  NOT NULL,
  [original_price] decimal(10,2)  NULL,
  [main_image] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [extra_images] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [content_type] int  NULL,
  [groups] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [stock_count] decimal(10,2)  NULL,
  [sub_title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_multispec] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [limit_count] decimal(10,2)  NULL,
  [location_label] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_provide_invoices] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [is_guarantee] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [on_sale] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [under_time] datetime  NULL,
  [on_sale_user_id] int  NULL,
  [on_sale_time] datetime  NULL,
  [create_user_id] int  NULL,
  [create_time] datetime  NULL,
  [update_time] datetime  NULL,
  [update_user_id] int  NULL,
  [goods_unit] int  NULL,
  [real_sale_count] decimal(10,2)  NULL,
  [show_sale_count] decimal(10,2)  NULL,
  [type_id] int  NULL,
  [brand_id] int  NULL,
  [is_hot] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [is_recommend] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [fcategory_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [bcategory_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [bcategory_id] int  NULL,
  [fcategory_id] int  NULL,
  [under_user_id] int  NULL,
  [is_delete] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [goods_no] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_goods] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'单价',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'price'
GO

EXEC sp_addextendedproperty
'MS_Description', N'原价',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'original_price'
GO

EXEC sp_addextendedproperty
'MS_Description', N'主图',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'main_image'
GO

EXEC sp_addextendedproperty
'MS_Description', N'附图',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'extra_images'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品描述类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'content_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品组ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'groups'
GO

EXEC sp_addextendedproperty
'MS_Description', N'库存量',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'stock_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'二级标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'sub_title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否是多规格',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'is_multispec'
GO

EXEC sp_addextendedproperty
'MS_Description', N'限购数量 0=不限制',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'limit_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'所在地',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'location_label'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否提供发票',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'is_provide_invoices'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否保修',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'is_guarantee'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否上架',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'on_sale'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下架时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'under_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'上架处理人',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'on_sale_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'上架时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'on_sale_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'create_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新人',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品单位',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'goods_unit'
GO

EXEC sp_addextendedproperty
'MS_Description', N'真实销量',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'real_sale_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'展示营销销量',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'show_sale_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'type_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品品牌',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'brand_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'热销',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'is_hot'
GO

EXEC sp_addextendedproperty
'MS_Description', N'推荐',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'is_recommend'
GO

EXEC sp_addextendedproperty
'MS_Description', N'前台分类KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'fcategory_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'后台分类KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'bcategory_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'后端分类ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'bcategory_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'前端分类ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'fcategory_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下架处理人',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'under_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否已删除',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'is_delete'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品编号',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods',
'COLUMN', N'goods_no'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods'
GO


-- ----------------------------
-- Table structure for jb_goods_attr
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_goods_attr]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_goods_attr]
GO

CREATE TABLE [dbo].[jb_goods_attr] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [value] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [goods_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_goods_attr] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_attr',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'属性名',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_attr',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'属性值',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_attr',
'COLUMN', N'value'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_attr',
'COLUMN', N'goods_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品自定义属性',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_attr'
GO


-- ----------------------------
-- Table structure for jb_goods_back_category
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_goods_back_category]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_goods_back_category]
GO

CREATE TABLE [dbo].[jb_goods_back_category] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [pid] int  NULL,
  [type_id] int  NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [category_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sort_rank] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_goods_back_category] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_back_category',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分类名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_back_category',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'父级ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_back_category',
'COLUMN', N'pid'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_back_category',
'COLUMN', N'type_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'启用 禁用',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_back_category',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'所有上级和自身ID串联起来',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_back_category',
'COLUMN', N'category_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_back_category',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品后台类目表 无限',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_back_category'
GO


-- ----------------------------
-- Table structure for jb_goods_element_content
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_goods_element_content]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_goods_element_content]
GO

CREATE TABLE [dbo].[jb_goods_element_content] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [goods_id] int  NOT NULL,
  [type] int  NOT NULL,
  [content] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sort_rank] int  NOT NULL,
  [sku_id] int  NULL
)
GO

ALTER TABLE [dbo].[jb_goods_element_content] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'关联商品ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_element_content',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_element_content',
'COLUMN', N'goods_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_element_content',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_element_content',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_element_content',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'SKUID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_element_content',
'COLUMN', N'sku_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品关联的单条图片或者问题分类的content列表',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_element_content'
GO


-- ----------------------------
-- Table structure for jb_goods_group
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_goods_group]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_goods_group]
GO

CREATE TABLE [dbo].[jb_goods_group] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sort_rank] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [icon] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [goods_count] int  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL
)
GO

ALTER TABLE [dbo].[jb_goods_group] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_group',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_group',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_group',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'图标',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_group',
'COLUMN', N'icon'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品数量',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_group',
'COLUMN', N'goods_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_group',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品分组',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_group'
GO


-- ----------------------------
-- Table structure for jb_goods_html_content
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_goods_html_content]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_goods_html_content]
GO

CREATE TABLE [dbo].[jb_goods_html_content] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [goods_id] int  NOT NULL,
  [content] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sku_id] int  NULL,
  [update_user_id] int  NULL,
  [update_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[jb_goods_html_content] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_html_content',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_html_content',
'COLUMN', N'goods_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'详情内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_html_content',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'SKUID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_html_content',
'COLUMN', N'sku_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新人',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_html_content',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_html_content',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品关联的html富文本详情',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_html_content'
GO


-- ----------------------------
-- Table structure for jb_goods_type
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_goods_type]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_goods_type]
GO

CREATE TABLE [dbo].[jb_goods_type] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [sort_rank] int  NOT NULL,
  [spec_count] int  NULL,
  [attr_count] int  NULL,
  [brand_count] int  NULL,
  [pinyin] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [remark] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_goods_type] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规格数量',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type',
'COLUMN', N'spec_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'属性数量',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type',
'COLUMN', N'attr_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'品牌数量',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type',
'COLUMN', N'brand_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'拼音',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type',
'COLUMN', N'pinyin'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注信息',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type',
'COLUMN', N'remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type'
GO


-- ----------------------------
-- Table structure for jb_goods_type_brand
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_goods_type_brand]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_goods_type_brand]
GO

CREATE TABLE [dbo].[jb_goods_type_brand] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [goods_type_id] int  NOT NULL,
  [brand_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_goods_type_brand] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'关联商品类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type_brand',
'COLUMN', N'goods_type_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'关联商品品牌',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type_brand',
'COLUMN', N'brand_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品类型-品牌中间表',
'SCHEMA', N'dbo',
'TABLE', N'jb_goods_type_brand'
GO


-- ----------------------------
-- Table structure for jb_jbolt_file
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_jbolt_file]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_jbolt_file]
GO

CREATE TABLE [dbo].[jb_jbolt_file] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [local_path] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [local_url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [cdn_url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime  NULL,
  [user_id] int  NULL,
  [file_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [file_type] int  NULL,
  [file_size] int  NULL,
  [file_suffix] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_jbolt_file] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'保存物理地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'local_path'
GO

EXEC sp_addextendedproperty
'MS_Description', N'本地可访问URL地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'local_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'外部CDN地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'cdn_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'文件名',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'file_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'文件类型 图片 附件 视频 音频',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'file_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'文件大小',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'file_size'
GO

EXEC sp_addextendedproperty
'MS_Description', N'后缀名',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file',
'COLUMN', N'file_suffix'
GO

EXEC sp_addextendedproperty
'MS_Description', N'文件库',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_file'
GO


-- ----------------------------
-- Table structure for jb_jbolt_version
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_jbolt_version]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_jbolt_version]
GO

CREATE TABLE [dbo].[jb_jbolt_version] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [version] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [publish_time] datetime  NOT NULL,
  [is_new] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [create_time] datetime  NOT NULL,
  [user_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_jbolt_version] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'版本号',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version',
'COLUMN', N'version'
GO

EXEC sp_addextendedproperty
'MS_Description', N'发布时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version',
'COLUMN', N'publish_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否最新版',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version',
'COLUMN', N'is_new'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'jbolt版本升级',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version'
GO


-- ----------------------------
-- Table structure for jb_jbolt_version_file
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_jbolt_version_file]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_jbolt_version_file]
GO

CREATE TABLE [dbo].[jb_jbolt_version_file] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [jbolt_version_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_jbolt_version_file] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'jbolt版本ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version_file',
'COLUMN', N'jbolt_version_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'JBolt下载版本管理',
'SCHEMA', N'dbo',
'TABLE', N'jb_jbolt_version_file'
GO


-- ----------------------------
-- Table structure for jb_login_log
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_login_log]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_login_log]
GO

CREATE TABLE [dbo].[jb_login_log] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [user_id] int,
  [username] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [login_ip] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_time] datetime  NOT NULL,
  [login_state] int  NOT NULL,
  [is_browser] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [browser_version] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [browser_name] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [os_name] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_city] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_address] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_city_code] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_province] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_country] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_mobile] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [os_platform_name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [browser_engine_name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [browser_engine_version] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_address_latitude] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_address_longitude] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_remote_login] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [create_time] datetime  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_login_log] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户名',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'username'
GO

EXEC sp_addextendedproperty
'MS_Description', N'IP地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_ip'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录状态',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_state'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否是浏览器访问',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'is_browser'
GO

EXEC sp_addextendedproperty
'MS_Description', N'浏览器版本号',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'browser_version'
GO

EXEC sp_addextendedproperty
'MS_Description', N'浏览器',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'browser_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'操作系统',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'os_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录城市',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_city'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录位置详情',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_address'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录城市代码',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_city_code'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录省份',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_province'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录国家',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_country'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否移动端',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'is_mobile'
GO

EXEC sp_addextendedproperty
'MS_Description', N'平台名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'os_platform_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'浏览器引擎名',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'browser_engine_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'浏览器引擎版本',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'browser_engine_version'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录地横坐标',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_address_latitude'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录地纵坐标',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'login_address_longitude'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否为异地异常登录',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'is_remote_login'
GO

EXEC sp_addextendedproperty
'MS_Description', N'记录创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录日志',
'SCHEMA', N'dbo',
'TABLE', N'jb_login_log'
GO


-- ----------------------------
-- Table structure for jb_mall
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_mall]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_mall]
GO

CREATE TABLE [dbo].[jb_mall] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime  NOT NULL,
  [create_user_id] int  NOT NULL,
  [update_time] datetime  NULL,
  [update_user_id] int  NULL,
  [state] int  NOT NULL,
  [leader_id] int  NULL,
  [first_publish_time] datetime  NULL,
  [last_publish_time] datetime  NULL,
  [first_close_time] datetime  NULL,
  [last_close_time] datetime  NULL,
  [first_publish_user_id] int  NULL,
  [last_publish_user_id] int  NULL,
  [first_close_user_id] int  NULL,
  [last_close_user_id] int  NULL
)
GO

ALTER TABLE [dbo].[jb_mall] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商城名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'create_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新操作人',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'状态 1筹备中 2 已上线 3 关闭',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'state'
GO

EXEC sp_addextendedproperty
'MS_Description', N'负责人',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'leader_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'首次上线发布时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'first_publish_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次上线发布操作时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'last_publish_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'首次关闭时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'first_close_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次关闭时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'last_close_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'首次发布人',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'first_publish_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次发布人',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'last_publish_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'首次关闭商城操作人',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'first_close_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次关闭操作人',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall',
'COLUMN', N'last_close_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商城表一条数据代表一个商城',
'SCHEMA', N'dbo',
'TABLE', N'jb_mall'
GO


-- ----------------------------
-- Table structure for jb_online_user
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_online_user]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_online_user]
GO

CREATE TABLE [dbo].[jb_online_user] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [session_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [user_id] int  NOT NULL,
  [login_log_id] int  NOT NULL,
  [login_time] datetime  NOT NULL,
  [update_time] datetime  NOT NULL,
  [expiration_time] datetime  NOT NULL,
  [screen_locked] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [online_state] int  NOT NULL,
  [offline_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[jb_online_user] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'会话ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'session_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录日志ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'login_log_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'login_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'过期时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'expiration_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否锁屏',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'screen_locked'
GO

EXEC sp_addextendedproperty
'MS_Description', N'在线状态',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'online_state'
GO

EXEC sp_addextendedproperty
'MS_Description', N'离线时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user',
'COLUMN', N'offline_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'在线用户',
'SCHEMA', N'dbo',
'TABLE', N'jb_online_user'
GO


-- ----------------------------
-- Table structure for jb_order
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_order]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_order]
GO

CREATE TABLE [dbo].[jb_order] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [order_no] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime  NOT NULL,
  [buyer_id] int  NOT NULL,
  [buyer_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [update_time] datetime  NULL,
  [payment_time] datetime  NULL,
  [consign_time] datetime  NULL,
  [finish_time] datetime  NULL,
  [close_time] datetime  NULL,
  [buyer_message] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [buyer_rate] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [state] int  NULL,
  [goods_amount] decimal(10,2)  NULL,
  [post_fee] decimal(10,2)  NULL,
  [payment_amount] decimal(10,2)  NULL,
  [payment_type] int  NULL,
  [online_payment_type] int  NULL,
  [close_type] int  NULL,
  [close_uesr_id] int  NULL
)
GO

ALTER TABLE [dbo].[jb_order] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单编号',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'order_no'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下单时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'买家ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'buyer_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'冗余买家名字',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'buyer_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'付款时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'payment_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'发货时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'consign_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'交易完成时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'finish_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单关闭时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'close_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'卖家留言',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'buyer_message'
GO

EXEC sp_addextendedproperty
'MS_Description', N'买家是否已经评价',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'buyer_rate'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单状态 待付款 已付款 已发货 订单完成 订单关闭',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'state'
GO

EXEC sp_addextendedproperty
'MS_Description', N'总额',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'goods_amount'
GO

EXEC sp_addextendedproperty
'MS_Description', N'运费',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'post_fee'
GO

EXEC sp_addextendedproperty
'MS_Description', N'应付总额',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'payment_amount'
GO

EXEC sp_addextendedproperty
'MS_Description', N'支付类型 在线支付 货到付款',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'payment_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'在线支付选择了谁',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'online_payment_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'通过什么方式关闭 后台管理 or 客户自身',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'close_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'关闭订单用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order',
'COLUMN', N'close_uesr_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商城订单',
'SCHEMA', N'dbo',
'TABLE', N'jb_order'
GO


-- ----------------------------
-- Table structure for jb_order_item
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_order_item]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_order_item]
GO

CREATE TABLE [dbo].[jb_order_item] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [order_id] int  NOT NULL,
  [goods_id] int  NOT NULL,
  [goods_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [goods_sub_title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [price] decimal(10,2)  NOT NULL,
  [goods_count] int  NOT NULL,
  [goods_amount] decimal(10,2)  NOT NULL,
  [goods_image] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [order_no] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [original_price] decimal(10,2)  NULL,
  [save_price] decimal(10,2)  NULL,
  [save_amount] decimal(10,2)  NULL
)
GO

ALTER TABLE [dbo].[jb_order_item] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'order_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'goods_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'goods_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'二级促销标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'goods_sub_title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'单价',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'price'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品数量',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'goods_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'总额',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'goods_amount'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品图',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'goods_image'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单编号',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'order_no'
GO

EXEC sp_addextendedproperty
'MS_Description', N'原价',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'original_price'
GO

EXEC sp_addextendedproperty
'MS_Description', N'单个节省价格',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'save_price'
GO

EXEC sp_addextendedproperty
'MS_Description', N'一共节省多少钱',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item',
'COLUMN', N'save_amount'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单详情表',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_item'
GO


-- ----------------------------
-- Table structure for jb_order_shipping
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_order_shipping]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_order_shipping]
GO

CREATE TABLE [dbo].[jb_order_shipping] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [order_id] int  NOT NULL,
  [name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [phone] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [mobile] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [buyer_nickname] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [province] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [city] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [county] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [address] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [zipcode] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime  NOT NULL,
  [update_time] datetime  NULL,
  [update_user_id] int  NULL,
  [order_no] nvarchar(100) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [buyer_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_order_shipping] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'order_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'收件人姓名',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'收件人固话',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'phone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'移动电话',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'mobile'
GO

EXEC sp_addextendedproperty
'MS_Description', N'买方昵称',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'buyer_nickname'
GO

EXEC sp_addextendedproperty
'MS_Description', N'省份',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'province'
GO

EXEC sp_addextendedproperty
'MS_Description', N'城市',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'city'
GO

EXEC sp_addextendedproperty
'MS_Description', N'区县',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'county'
GO

EXEC sp_addextendedproperty
'MS_Description', N'收件具体地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'address'
GO

EXEC sp_addextendedproperty
'MS_Description', N'邮政编码',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'zipcode'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新人ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单编号',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'order_no'
GO

EXEC sp_addextendedproperty
'MS_Description', N'买方用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping',
'COLUMN', N'buyer_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单物流信息',
'SCHEMA', N'dbo',
'TABLE', N'jb_order_shipping'
GO


-- ----------------------------
-- Table structure for jb_permission
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_permission]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_permission]
GO

CREATE TABLE [dbo].[jb_permission] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [title] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [pid] int  NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [icons] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [sort_rank] int  NOT NULL,
  [permission_level] int  NULL,
  [permission_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [is_menu] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [is_target_blank] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [is_system_admin_default] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [open_type] int  NOT NULL,
  [open_option] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_permission] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'上级ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'pid'
GO

EXEC sp_addextendedproperty
'MS_Description', N'地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'图标',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'icons'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'层级',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'permission_level'
GO

EXEC sp_addextendedproperty
'MS_Description', N'权限资源KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'permission_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否是菜单',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'is_menu'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否新窗口打开',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'is_target_blank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否系统超级管理员默认拥有的权限',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'is_system_admin_default'
GO

EXEC sp_addextendedproperty
'MS_Description', N'打开类型 1 默认 2 iframe 3 dialog',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'open_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'组件属性json',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission',
'COLUMN', N'open_option'
GO

EXEC sp_addextendedproperty
'MS_Description', N'function定义',
'SCHEMA', N'dbo',
'TABLE', N'jb_permission'
GO


-- ----------------------------
-- Table structure for jb_post
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_post]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_post]
GO

CREATE TABLE [dbo].[jb_post] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [remark] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [sn] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sort_rank] int  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '1' NOT NULL
)
GO

ALTER TABLE [dbo].[jb_post] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_post',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_post',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_post',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'jb_post',
'COLUMN', N'remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'编码',
'SCHEMA', N'dbo',
'TABLE', N'jb_post',
'COLUMN', N'sn'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_post',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_post',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'岗位管理',
'SCHEMA', N'dbo',
'TABLE', N'jb_post'
GO


-- ----------------------------
-- Table structure for jb_private_message
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_private_message]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_private_message]
GO

CREATE TABLE [dbo].[jb_private_message] (
  [id] int IDENTITY(1,1) NOT NULL,
  [content] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime  NOT NULL,
  [from_user_id] int  NOT NULL,
  [to_user_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_private_message] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_private_message',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'私信内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_private_message',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'发送时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_private_message',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'发信人',
'SCHEMA', N'dbo',
'TABLE', N'jb_private_message',
'COLUMN', N'from_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'收信人',
'SCHEMA', N'dbo',
'TABLE', N'jb_private_message',
'COLUMN', N'to_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'内部私信',
'SCHEMA', N'dbo',
'TABLE', N'jb_private_message'
GO


-- ----------------------------
-- Table structure for jb_remote_login_log
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_remote_login_log]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_remote_login_log]
GO

CREATE TABLE [dbo].[jb_remote_login_log] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [last_login_country] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [last_login_province] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [last_login_city] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [last_login_time] datetime  NOT NULL,
  [login_country] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_province] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_city] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_time] datetime  NULL,
  [user_id] int  NOT NULL,
  [username] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [is_new] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [last_login_ip] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [login_ip] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_remote_login_log] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次登录国家',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'last_login_country'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次登录省份',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'last_login_province'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次登录城市',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'last_login_city'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次登录时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'last_login_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'新登录国家',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'login_country'
GO

EXEC sp_addextendedproperty
'MS_Description', N'新登录省份',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'login_province'
GO

EXEC sp_addextendedproperty
'MS_Description', N'新登录城市',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'login_city'
GO

EXEC sp_addextendedproperty
'MS_Description', N'新登录时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'login_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'登录用户名',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'username'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否为最新一次',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'is_new'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最近一次登录IP',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'last_login_ip'
GO

EXEC sp_addextendedproperty
'MS_Description', N'新登录IP',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'login_ip'
GO

EXEC sp_addextendedproperty
'MS_Description', N'记录创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'异地登录日志记录',
'SCHEMA', N'dbo',
'TABLE', N'jb_remote_login_log'
GO


-- ----------------------------
-- Table structure for jb_role
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_role]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_role]
GO

CREATE TABLE [dbo].[jb_role] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(20) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sn] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [pid] int  NULL
)
GO

ALTER TABLE [dbo].[jb_role] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_role',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_role',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'编码',
'SCHEMA', N'dbo',
'TABLE', N'jb_role',
'COLUMN', N'sn'
GO

EXEC sp_addextendedproperty
'MS_Description', N'父级角色ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_role',
'COLUMN', N'pid'
GO

EXEC sp_addextendedproperty
'MS_Description', N'角色表',
'SCHEMA', N'dbo',
'TABLE', N'jb_role'
GO


-- ----------------------------
-- Table structure for jb_role_permission
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_role_permission]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_role_permission]
GO

CREATE TABLE [dbo].[jb_role_permission] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [role_id] int  NOT NULL,
  [permission_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_role_permission] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_role_permission',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'角色ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_role_permission',
'COLUMN', N'role_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'权限ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_role_permission',
'COLUMN', N'permission_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'角色功能列表',
'SCHEMA', N'dbo',
'TABLE', N'jb_role_permission'
GO


-- ----------------------------
-- Table structure for jb_shelf
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_shelf]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_shelf]
GO

CREATE TABLE [dbo].[jb_shelf] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [create_time] datetime  NOT NULL,
  [create_user_id] int  NOT NULL,
  [publish_user_id] int  NULL,
  [publish_time] datetime  NULL,
  [share_image] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [share_title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [update_time] datetime  NULL,
  [update_user_id] int  NULL
)
GO

ALTER TABLE [dbo].[jb_shelf] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'create_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'上线发布人',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'publish_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'发布时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'publish_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分享图',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'share_image'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分享标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'share_title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新人',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'电商货架-小程序首页风格',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf'
GO


-- ----------------------------
-- Table structure for jb_shelf_activity
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_shelf_activity]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_shelf_activity]
GO

CREATE TABLE [dbo].[jb_shelf_activity] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [poster_image] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [open_url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [open_type] int  NOT NULL,
  [shelf_element_id] int  NOT NULL,
  [shelf_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_shelf_activity] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_activity',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'海报地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_activity',
'COLUMN', N'poster_image'
GO

EXEC sp_addextendedproperty
'MS_Description', N'打开地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_activity',
'COLUMN', N'open_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'打开方式 网页还是商品内页 还是分类 还是',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_activity',
'COLUMN', N'open_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架元素ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_activity',
'COLUMN', N'shelf_element_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_activity',
'COLUMN', N'shelf_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架元素_活动',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_activity'
GO


-- ----------------------------
-- Table structure for jb_shelf_carousel
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_shelf_carousel]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_shelf_carousel]
GO

CREATE TABLE [dbo].[jb_shelf_carousel] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [imgurl] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [open_type] int  NULL,
  [open_url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [shelf_id] int  NOT NULL,
  [shelf_element_id] int  NOT NULL,
  [sort_rank] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_shelf_carousel] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_carousel',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'图片地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_carousel',
'COLUMN', N'imgurl'
GO

EXEC sp_addextendedproperty
'MS_Description', N'打开模式 内置资源地址，网页地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_carousel',
'COLUMN', N'open_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'打开地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_carousel',
'COLUMN', N'open_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_carousel',
'COLUMN', N'shelf_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架元素ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_carousel',
'COLUMN', N'shelf_element_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_carousel',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架元素_轮播图',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_carousel'
GO


-- ----------------------------
-- Table structure for jb_shelf_element
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_shelf_element]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_shelf_element]
GO

CREATE TABLE [dbo].[jb_shelf_element] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [type] int  NOT NULL,
  [width] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [height] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sort_rank] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_shelf_element] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_element',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型 轮播 楼层 分类 活动等',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_element',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'宽度',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_element',
'COLUMN', N'width'
GO

EXEC sp_addextendedproperty
'MS_Description', N'高度',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_element',
'COLUMN', N'height'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_element',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架从上到下的元素',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_element'
GO


-- ----------------------------
-- Table structure for jb_shelf_goods_floor
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_shelf_goods_floor]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_shelf_goods_floor]
GO

CREATE TABLE [dbo].[jb_shelf_goods_floor] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [group_id] int  NOT NULL,
  [title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [open_url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [goods_count] int  NOT NULL,
  [column_count] int  NOT NULL,
  [sort_rank] int  NOT NULL,
  [shelf_id] int  NOT NULL,
  [shelf_element_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_shelf_goods_floor] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品分组ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'group_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'显示标题 默认是商品组的 可以改',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'打开地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'open_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'显示商品数量',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'goods_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'显示几列布局',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'column_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'shelf_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架元素ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor',
'COLUMN', N'shelf_element_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架元素_商品楼层',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_floor'
GO


-- ----------------------------
-- Table structure for jb_shelf_goods_group
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_shelf_goods_group]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_shelf_goods_group]
GO

CREATE TABLE [dbo].[jb_shelf_goods_group] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [icon] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [imgurl] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [sub_title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [sort_rank] int  NOT NULL,
  [shelf_id] int  NOT NULL,
  [shelf_element_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_shelf_goods_group] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_group',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'图标',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_group',
'COLUMN', N'icon'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分类下的顶部修饰图',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_group',
'COLUMN', N'imgurl'
GO

EXEC sp_addextendedproperty
'MS_Description', N'二级标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_group',
'COLUMN', N'sub_title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_group',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_group',
'COLUMN', N'shelf_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架元素ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_group',
'COLUMN', N'shelf_element_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'货架元素_商品组',
'SCHEMA', N'dbo',
'TABLE', N'jb_shelf_goods_group'
GO


-- ----------------------------
-- Table structure for jb_sku
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_sku]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_sku]
GO

CREATE TABLE [dbo].[jb_sku] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [sku_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [sku_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [goods_id] int  NOT NULL,
  [goods_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type_id] int  NOT NULL,
  [price] decimal(10,2)  NOT NULL,
  [original_price] decimal(10,2)  NULL,
  [stock_count] decimal(10,2)  NULL,
  [is_multispec] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NULL,
  [limit_count] decimal(10,2)  NULL,
  [sub_title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [on_sale] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [sellout] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [real_sale_count] decimal(10,2)  NULL,
  [show_sale_count] decimal(10,2)  NULL,
  [main_image] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [extra_images] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_hot] char(1) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [is_recommend] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [need_content] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [content_type] int  NOT NULL,
  [under_time] datetime  NULL,
  [onSale_time] datetime  NULL,
  [create_user_id] int  NOT NULL,
  [create_time] datetime  NOT NULL,
  [update_time] datetime  NULL,
  [update_user_id] int  NULL
)
GO

ALTER TABLE [dbo].[jb_sku] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规格编码',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'sku_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规格名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'sku_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'goods_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'goods_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'type_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'单价',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'price'
GO

EXEC sp_addextendedproperty
'MS_Description', N'原价',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'original_price'
GO

EXEC sp_addextendedproperty
'MS_Description', N'库存量',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'stock_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否多规格',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'is_multispec'
GO

EXEC sp_addextendedproperty
'MS_Description', N'限购量',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'limit_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'营销语',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'sub_title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否上架',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'on_sale'
GO

EXEC sp_addextendedproperty
'MS_Description', N'售罄',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'sellout'
GO

EXEC sp_addextendedproperty
'MS_Description', N'真实销量',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'real_sale_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'展示营销销量',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'show_sale_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'主图',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'main_image'
GO

EXEC sp_addextendedproperty
'MS_Description', N'附图',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'extra_images'
GO

EXEC sp_addextendedproperty
'MS_Description', N'热销',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'is_hot'
GO

EXEC sp_addextendedproperty
'MS_Description', N'推荐',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'is_recommend'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否需要详情描述',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'need_content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'描述类型 是富文本还是分开的图片 文本段数据',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'content_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下架时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'under_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'上架时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'onSale_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'create_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后更新人',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'SKU',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku'
GO


-- ----------------------------
-- Table structure for jb_sku_item
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_sku_item]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_sku_item]
GO

CREATE TABLE [dbo].[jb_sku_item] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [sku_id] int  NOT NULL,
  [goods_id] int  NOT NULL,
  [type_id] int  NOT NULL,
  [spec_id] int  NOT NULL,
  [spec_item_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_sku_item] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku_item',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'SKU ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku_item',
'COLUMN', N'sku_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku_item',
'COLUMN', N'goods_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'商品分类',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku_item',
'COLUMN', N'type_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规格 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku_item',
'COLUMN', N'spec_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规格项 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku_item',
'COLUMN', N'spec_item_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'sku详情',
'SCHEMA', N'dbo',
'TABLE', N'jb_sku_item'
GO


-- ----------------------------
-- Table structure for jb_spec
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_spec]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_spec]
GO

CREATE TABLE [dbo].[jb_spec] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_spec] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_spec',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规格名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_spec',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'系统规格表',
'SCHEMA', N'dbo',
'TABLE', N'jb_spec'
GO


-- ----------------------------
-- Table structure for jb_spec_item
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_spec_item]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_spec_item]
GO

CREATE TABLE [dbo].[jb_spec_item] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [spec_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_spec_item] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_spec_item',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'选项名',
'SCHEMA', N'dbo',
'TABLE', N'jb_spec_item',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'所属规格',
'SCHEMA', N'dbo',
'TABLE', N'jb_spec_item',
'COLUMN', N'spec_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规格选项表',
'SCHEMA', N'dbo',
'TABLE', N'jb_spec_item'
GO


-- ----------------------------
-- Table structure for jb_sys_notice
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_sys_notice]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_sys_notice]
GO

CREATE TABLE [dbo].[jb_sys_notice] (
  [id] int IDENTITY(1,1) NOT NULL,
  [title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [content] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type] int  NOT NULL,
  [priority_level] int  NOT NULL,
  [read_count] int  NULL,
  [create_time] datetime  NOT NULL,
  [update_time] datetime  NULL,
  [create_user_id] int  NULL,
  [update_user_id] int  NULL,
  [receiver_type] int  NULL,
  [receiver_value] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [files] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [del_flag] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL
)
GO

ALTER TABLE [dbo].[jb_sys_notice] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'消息内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'通知类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'优先级',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'priority_level'
GO

EXEC sp_addextendedproperty
'MS_Description', N'已读人数',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'read_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'create_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新人',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'接收人类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'receiver_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'接收人值',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'receiver_value'
GO

EXEC sp_addextendedproperty
'MS_Description', N'附件',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'files'
GO

EXEC sp_addextendedproperty
'MS_Description', N'删除标志',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice',
'COLUMN', N'del_flag'
GO

EXEC sp_addextendedproperty
'MS_Description', N'系统通知',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice'
GO


-- ----------------------------
-- Table structure for jb_sys_notice_reader
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_sys_notice_reader]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_sys_notice_reader]
GO

CREATE TABLE [dbo].[jb_sys_notice_reader] (
  [id] int IDENTITY(1,1) NOT NULL,
  [sys_notice_id] int  NOT NULL,
  [user_id] int  NOT NULL,
  [create_time] datetime  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_sys_notice_reader] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice_reader',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'通知ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice_reader',
'COLUMN', N'sys_notice_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice_reader',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice_reader',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'通知阅读用户关系表',
'SCHEMA', N'dbo',
'TABLE', N'jb_sys_notice_reader'
GO


-- ----------------------------
-- Table structure for jb_system_log
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_system_log]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_system_log]
GO

CREATE TABLE [dbo].[jb_system_log] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type] int  NOT NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [user_id] int  NOT NULL,
  [user_name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [target_type] int  NOT NULL,
  [create_time] datetime  NOT NULL,
  [target_id] int  NOT NULL,
  [open_type] int  NULL
)
GO

ALTER TABLE [dbo].[jb_system_log] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'操作类型 删除 更新 新增',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'连接对象详情地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'操作人ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'操作人姓名',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'user_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'操作对象类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'target_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'记录创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'操作对象ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'target_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'打开类型URL还是Dialog',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log',
'COLUMN', N'open_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'系统操作日志表',
'SCHEMA', N'dbo',
'TABLE', N'jb_system_log'
GO


-- ----------------------------
-- Table structure for jb_todo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_todo]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_todo]
GO

CREATE TABLE [dbo].[jb_todo] (
  [id] int IDENTITY(1,1) NOT NULL,
  [title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [content] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [user_id] int  NOT NULL,
  [state] int  NOT NULL,
  [specified_finish_time] datetime  NOT NULL,
  [type] int  NOT NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [priority_level] int  NOT NULL,
  [create_time] datetime  NOT NULL,
  [update_time] datetime  NOT NULL,
  [create_user_id] int  NOT NULL,
  [update_user_id] int  NULL,
  [real_finish_time] datetime  NULL
)
GO

ALTER TABLE [dbo].[jb_todo] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'详情内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'所属用户',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'状态',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'state'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规定完成时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'specified_finish_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型 链接还是内容 还是都有',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'链接',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'优先级',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'priority_level'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'create_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新人ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'完成时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo',
'COLUMN', N'real_finish_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'待办事项',
'SCHEMA', N'dbo',
'TABLE', N'jb_todo'
GO


-- ----------------------------
-- Table structure for jb_topnav
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_topnav]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_topnav]
GO

CREATE TABLE [dbo].[jb_topnav] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [icon] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [sort_rank] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_topnav] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'图标',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav',
'COLUMN', N'icon'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'顶部导航',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav'
GO


-- ----------------------------
-- Table structure for jb_topnav_menu
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_topnav_menu]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_topnav_menu]
GO

CREATE TABLE [dbo].[jb_topnav_menu] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [topnav_id] int  NOT NULL,
  [permission_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_topnav_menu] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav_menu',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'顶部导航ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav_menu',
'COLUMN', N'topnav_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'菜单资源ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav_menu',
'COLUMN', N'permission_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'顶部菜单对应左侧一级导航中间表',
'SCHEMA', N'dbo',
'TABLE', N'jb_topnav_menu'
GO


-- ----------------------------
-- Table structure for jb_update_libs
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_update_libs]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_update_libs]
GO

CREATE TABLE [dbo].[jb_update_libs] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [target] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [delete_all] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [is_must] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL
)
GO

ALTER TABLE [dbo].[jb_update_libs] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_update_libs',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_update_libs',
'COLUMN', N'url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'存储位置',
'SCHEMA', N'dbo',
'TABLE', N'jb_update_libs',
'COLUMN', N'target'
GO

EXEC sp_addextendedproperty
'MS_Description', N'清空文件夹',
'SCHEMA', N'dbo',
'TABLE', N'jb_update_libs',
'COLUMN', N'delete_all'
GO

EXEC sp_addextendedproperty
'MS_Description', N'强制',
'SCHEMA', N'dbo',
'TABLE', N'jb_update_libs',
'COLUMN', N'is_must'
GO

EXEC sp_addextendedproperty
'MS_Description', N'下载的libs',
'SCHEMA', N'dbo',
'TABLE', N'jb_update_libs'
GO


-- ----------------------------
-- Table structure for jb_user
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_user]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_user]
GO

CREATE TABLE [dbo].[jb_user] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [username] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [password] nvarchar(128) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [name] nvarchar(20) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [avatar] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime  NULL,
  [phone] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [sex] int  NULL,
  [pinyin] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [roles] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_system_admin] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [create_user_id] int  NULL,
  [pwd_salt] nvarchar(128) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_ip] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_time] datetime  NULL,
  [login_city] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_province] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [login_country] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_remote_login] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [dept_id] int  NULL,
  [posts] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_user] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户名',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'username'
GO

EXEC sp_addextendedproperty
'MS_Description', N'密码',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'password'
GO

EXEC sp_addextendedproperty
'MS_Description', N'姓名',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'头像',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'avatar'
GO

EXEC sp_addextendedproperty
'MS_Description', N'记录创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'手机号',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'phone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'性别',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'sex'
GO

EXEC sp_addextendedproperty
'MS_Description', N'拼音码',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'pinyin'
GO

EXEC sp_addextendedproperty
'MS_Description', N'角色 一对多',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'roles'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否系统超级管理员',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'is_system_admin'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建人',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'create_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'密码盐值',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'pwd_salt'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后登录IP',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'login_ip'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后登录时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'login_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后登录城市',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'login_city'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后登录省份',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'login_province'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后登录国家',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'login_country'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否异地登录',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'is_remote_login'
GO

EXEC sp_addextendedproperty
'MS_Description', N'部门ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'dept_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'岗位IDS',
'SCHEMA', N'dbo',
'TABLE', N'jb_user',
'COLUMN', N'posts'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户登录账户表',
'SCHEMA', N'dbo',
'TABLE', N'jb_user'
GO


-- ----------------------------
-- Table structure for jb_user_config
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_user_config]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_user_config]
GO

CREATE TABLE [dbo].[jb_user_config] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [config_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [config_value] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [user_id] int  NOT NULL,
  [create_time] datetime  NOT NULL,
  [update_time] datetime  NULL,
  [value_type] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_user_config] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'配置名',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'配置KEY',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config',
'COLUMN', N'config_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'配置值',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config',
'COLUMN', N'config_value'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'取值类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config',
'COLUMN', N'value_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户系统样式自定义设置表',
'SCHEMA', N'dbo',
'TABLE', N'jb_user_config'
GO


-- ----------------------------
-- Table structure for jb_wechat_article
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_article]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_article]
GO

CREATE TABLE [dbo].[jb_wechat_article] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [content] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [create_time] datetime  NOT NULL,
  [user_id] int  NOT NULL,
  [update_time] datetime  NULL,
  [update_user_id] int  NULL,
  [brief_info] nvarchar(120) COLLATE Chinese_PRC_CI_AS  NULL,
  [poster] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [view_count] int  NOT NULL,
  [media_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [mp_id] int  NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [type] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_article] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新用户 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'文章摘要',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'brief_info'
GO

EXEC sp_addextendedproperty
'MS_Description', N'海报',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'poster'
GO

EXEC sp_addextendedproperty
'MS_Description', N'阅读数',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'view_count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信素材 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'media_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'mp_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'图文链接地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'本地图文 公众号图文素材 外部图文',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信图文信息',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_article'
GO


-- ----------------------------
-- Table structure for jb_wechat_autoreply
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_autoreply]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_autoreply]
GO

CREATE TABLE [dbo].[jb_wechat_autoreply] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [mp_id] int  NOT NULL,
  [type] int  NOT NULL,
  [name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [reply_type] int  NOT NULL,
  [create_time] datetime  NOT NULL,
  [user_id] int  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_autoreply] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_autoreply',
'COLUMN', N'mp_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型 关注回复 无内容时回复 关键词回复',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_autoreply',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'规则名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_autoreply',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'回复类型 全部还是随机一条',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_autoreply',
'COLUMN', N'reply_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'记录创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_autoreply',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_autoreply',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_autoreply',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信公众号自动回复规则',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_autoreply'
GO


-- ----------------------------
-- Table structure for jb_wechat_config
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_config]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_config]
GO

CREATE TABLE [dbo].[jb_wechat_config] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [mp_id] int  NOT NULL,
  [config_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [config_value] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_config] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_config',
'COLUMN', N'mp_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'配置key',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_config',
'COLUMN', N'config_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'配置值',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_config',
'COLUMN', N'config_value'
GO

EXEC sp_addextendedproperty
'MS_Description', N'配置项名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_config',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'配置类型',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_config',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信公众号配置表',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_config'
GO


-- ----------------------------
-- Table structure for jb_wechat_keywords
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_keywords]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_keywords]
GO

CREATE TABLE [dbo].[jb_wechat_keywords] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [mp_id] int  NOT NULL,
  [name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [type] int  NOT NULL,
  [auto_reply_id] int  NOT NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_keywords] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_keywords',
'COLUMN', N'mp_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'模糊 全匹配',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_keywords',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'回复规则ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_keywords',
'COLUMN', N'auto_reply_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信关键词回复中的关键词定义',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_keywords'
GO


-- ----------------------------
-- Table structure for jb_wechat_media
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_media]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_media]
GO

CREATE TABLE [dbo].[jb_wechat_media] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [title] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [digest] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [type] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [mp_id] int  NOT NULL,
  [media_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [thumb_media_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [content_source_url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [author] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [update_time] datetime  NULL,
  [server_url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_media] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'描述',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'digest'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型 image video voice news',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'mp_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信素材ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'media_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'封面图ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'thumb_media_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'原文地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'content_source_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'访问地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'图文作者',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'author'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'存服务器URL',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media',
'COLUMN', N'server_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信公众平台素材库同步管理',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_media'
GO


-- ----------------------------
-- Table structure for jb_wechat_menu
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_menu]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_menu]
GO

CREATE TABLE [dbo].[jb_wechat_menu] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [mp_id] int  NOT NULL,
  [type] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [name] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [pid] int  NULL,
  [sort_rank] int  NOT NULL,
  [value] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [app_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [page_path] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_menu] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_menu',
'COLUMN', N'mp_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_menu',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信小程序APPID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_menu',
'COLUMN', N'app_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信小程序页面地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_menu',
'COLUMN', N'page_path'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信菜单',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_menu'
GO


-- ----------------------------
-- Table structure for jb_wechat_mpinfo
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_mpinfo]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_mpinfo]
GO

CREATE TABLE [dbo].[jb_wechat_mpinfo] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [name] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [logo] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [create_time] datetime  NOT NULL,
  [user_id] int  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [update_time] datetime  NULL,
  [update_user_id] int  NULL,
  [brief_info] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [remark] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [type] int  NOT NULL,
  [is_authenticated] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [subject_type] int  NOT NULL,
  [wechat_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [qrcode] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [link_app_id] int  NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_mpinfo] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'主键',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'平台名称',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'头像LOGO',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'logo'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新人ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'update_user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'简介',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'brief_info'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型 订阅号、服务号、小程序、企业号',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否已认证',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'is_authenticated'
GO

EXEC sp_addextendedproperty
'MS_Description', N'申请认证主体的类型 个人还是企业',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'subject_type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信号',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'wechat_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'二维码',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'qrcode'
GO

EXEC sp_addextendedproperty
'MS_Description', N'关联应用ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo',
'COLUMN', N'link_app_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信公众号与小程序',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_mpinfo'
GO


-- ----------------------------
-- Table structure for jb_wechat_reply_content
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_reply_content]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_reply_content]
GO

CREATE TABLE [dbo].[jb_wechat_reply_content] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [type] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [title] nvarchar(64) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [content] nvarchar(max) COLLATE Chinese_PRC_CI_AS  NULL,
  [poster] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [auto_reply_id] int  NOT NULL,
  [create_time] datetime  NOT NULL,
  [user_id] int  NOT NULL,
  [media_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [mp_id] int  NOT NULL,
  [sort_rank] int  NOT NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_reply_content] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型 图文 文字 图片 音频 视频',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标题',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'title'
GO

EXEC sp_addextendedproperty
'MS_Description', N'内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'content'
GO

EXEC sp_addextendedproperty
'MS_Description', N'海报',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'poster'
GO

EXEC sp_addextendedproperty
'MS_Description', N'地址',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'回复规则ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'auto_reply_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'关联数据',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'media_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信 ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'mp_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'排序',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'sort_rank'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否启用',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'自动回复内容',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_reply_content'
GO


-- ----------------------------
-- Table structure for jb_wechat_user
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[jb_wechat_user]') AND type IN ('U'))
	DROP TABLE [dbo].[jb_wechat_user]
GO

CREATE TABLE [dbo].[jb_wechat_user] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [nickname] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [open_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NOT NULL,
  [union_id] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [sex] int  NOT NULL,
  [language] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [subscibe] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [country] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [province] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [city] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [head_img_url] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [subscribe_time] datetime  NULL,
  [remark] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [group_id] int  NULL,
  [tag_ids] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [subscribe_scene] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [qr_scene] int  NULL,
  [qr_scene_str] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [realname] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [phone] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [phone_country_code] nvarchar(40) COLLATE Chinese_PRC_CI_AS  NULL,
  [check_code] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [is_checked] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [source] int  NULL,
  [session_key] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [enable] char(1) COLLATE Chinese_PRC_CI_AS DEFAULT '0' NOT NULL,
  [create_time] datetime  NOT NULL,
  [checked_time] datetime  NULL,
  [mp_id] int  NULL,
  [weixin] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL,
  [update_time] datetime  NULL,
  [last_login_time] datetime  NULL,
  [first_auth_time] datetime  NULL,
  [last_auth_time] datetime  NULL,
  [first_login_time] datetime  NULL,
  [bind_user] nvarchar(255) COLLATE Chinese_PRC_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[jb_wechat_user] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户昵称',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'nickname'
GO

EXEC sp_addextendedproperty
'MS_Description', N'openId',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'open_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'unionID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'union_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'性别 1男 2女 0 未知',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'sex'
GO

EXEC sp_addextendedproperty
'MS_Description', N'语言',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'language'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否已关注',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'subscibe'
GO

EXEC sp_addextendedproperty
'MS_Description', N'国家',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'country'
GO

EXEC sp_addextendedproperty
'MS_Description', N'省',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'province'
GO

EXEC sp_addextendedproperty
'MS_Description', N'城市',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'city'
GO

EXEC sp_addextendedproperty
'MS_Description', N'头像',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'head_img_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'关注时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'subscribe_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'备注',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'remark'
GO

EXEC sp_addextendedproperty
'MS_Description', N'分组',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'group_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'标签',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'tag_ids'
GO

EXEC sp_addextendedproperty
'MS_Description', N'关注渠道',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'subscribe_scene'
GO

EXEC sp_addextendedproperty
'MS_Description', N'二维码场景-开发者自定义',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'qr_scene'
GO

EXEC sp_addextendedproperty
'MS_Description', N'二维码扫码场景描述-开发者自定义',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'qr_scene_str'
GO

EXEC sp_addextendedproperty
'MS_Description', N'真实姓名',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'realname'
GO

EXEC sp_addextendedproperty
'MS_Description', N'手机号',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'phone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'手机号国家代码',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'phone_country_code'
GO

EXEC sp_addextendedproperty
'MS_Description', N'手机验证码',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'check_code'
GO

EXEC sp_addextendedproperty
'MS_Description', N'是否已验证',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'is_checked'
GO

EXEC sp_addextendedproperty
'MS_Description', N'来源 小程序还是公众平台',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'source'
GO

EXEC sp_addextendedproperty
'MS_Description', N'小程序登录SessionKey',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'session_key'
GO

EXEC sp_addextendedproperty
'MS_Description', N'禁用访问',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'enable'
GO

EXEC sp_addextendedproperty
'MS_Description', N'创建时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'create_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'验证绑定手机号时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'checked_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'所属公众平台ID',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'mp_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信号',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'weixin'
GO

EXEC sp_addextendedproperty
'MS_Description', N'更新时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'update_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后登录时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'last_login_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'首次授权时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'first_auth_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'最后一次更新授权时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'last_auth_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'首次登录时间',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'first_login_time'
GO

EXEC sp_addextendedproperty
'MS_Description', N'绑定其他用户信息',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user',
'COLUMN', N'bind_user'
GO

EXEC sp_addextendedproperty
'MS_Description', N'微信用户信息_模板表',
'SCHEMA', N'dbo',
'TABLE', N'jb_wechat_user'
GO


-- ----------------------------
-- Auto increment value for jb_application
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_application]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_application
-- ----------------------------
ALTER TABLE [dbo].[jb_application] ADD CONSTRAINT [PK_jb_application] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_brand
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_brand]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_brand
-- ----------------------------
ALTER TABLE [dbo].[jb_brand] ADD CONSTRAINT [PK_jb_brand] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_change_log
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_change_log]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_change_log
-- ----------------------------
ALTER TABLE [dbo].[jb_change_log] ADD CONSTRAINT [PK__jb_chang__3213E83F8E663F5D] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_demotable
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_demotable]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_demotable
-- ----------------------------
ALTER TABLE [dbo].[jb_demotable] ADD CONSTRAINT [PK__jb_demot__3213E83F72A24FD2] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_dept
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_dept]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_dept
-- ----------------------------
ALTER TABLE [dbo].[jb_dept] ADD CONSTRAINT [PK__jb_dept__3213E83FF640853E] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_dictionary
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_dictionary]', RESEED, 28)
GO


-- ----------------------------
-- Primary Key structure for table jb_dictionary
-- ----------------------------
ALTER TABLE [dbo].[jb_dictionary] ADD CONSTRAINT [PK__jb_dicti__3213E83F788D191C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_dictionary_type
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_dictionary_type]', RESEED, 6)
GO


-- ----------------------------
-- Primary Key structure for table jb_dictionary_type
-- ----------------------------
ALTER TABLE [dbo].[jb_dictionary_type] ADD CONSTRAINT [PK__jb_dicti__3213E83FC7E3221C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_download_log
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_download_log]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_download_log
-- ----------------------------
ALTER TABLE [dbo].[jb_download_log] ADD CONSTRAINT [PK__jb_downl__3213E83F4B494D92] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_global_config
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_global_config]', RESEED, 29)
GO


-- ----------------------------
-- Primary Key structure for table jb_global_config
-- ----------------------------
ALTER TABLE [dbo].[jb_global_config] ADD CONSTRAINT [PK__jb_globa__3213E83FEDB63D2E] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_global_config_type
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_global_config_type]', RESEED, 4)
GO


-- ----------------------------
-- Primary Key structure for table jb_global_config_type
-- ----------------------------
ALTER TABLE [dbo].[jb_global_config_type] ADD CONSTRAINT [PK__jb_globa__3213E83F46DA7936] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_goods
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_goods]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_goods
-- ----------------------------
ALTER TABLE [dbo].[jb_goods] ADD CONSTRAINT [PK__jb_goods__3213E83FABD65FA0] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_goods_attr
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_goods_attr]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_goods_attr
-- ----------------------------
ALTER TABLE [dbo].[jb_goods_attr] ADD CONSTRAINT [PK__jb_goods__3213E83FE7EA580C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_goods_back_category
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_goods_back_category]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_goods_back_category
-- ----------------------------
ALTER TABLE [dbo].[jb_goods_back_category] ADD CONSTRAINT [PK__jb_goods__3213E83F8DA740E7] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_goods_element_content
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_goods_element_content]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_goods_element_content
-- ----------------------------
ALTER TABLE [dbo].[jb_goods_element_content] ADD CONSTRAINT [PK__jb_goods__3213E83F428D4653] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_goods_group
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_goods_group]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_goods_group
-- ----------------------------
ALTER TABLE [dbo].[jb_goods_group] ADD CONSTRAINT [PK__jb_goods__3213E83F33AC994C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_goods_html_content
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_goods_html_content]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_goods_html_content
-- ----------------------------
ALTER TABLE [dbo].[jb_goods_html_content] ADD CONSTRAINT [PK__jb_goods__3213E83F3E67546A] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_goods_type
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_goods_type]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_goods_type
-- ----------------------------
ALTER TABLE [dbo].[jb_goods_type] ADD CONSTRAINT [PK__jb_goods__3213E83F21C9319C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_goods_type_brand
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_goods_type_brand]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_goods_type_brand
-- ----------------------------
ALTER TABLE [dbo].[jb_goods_type_brand] ADD CONSTRAINT [PK__jb_goods__3213E83F053A6754] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_jbolt_file
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_jbolt_file]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_jbolt_file
-- ----------------------------
ALTER TABLE [dbo].[jb_jbolt_file] ADD CONSTRAINT [PK__jb_jbolt__3213E83FF615902C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_jbolt_version
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_jbolt_version]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_jbolt_version
-- ----------------------------
ALTER TABLE [dbo].[jb_jbolt_version] ADD CONSTRAINT [PK__jb_jbolt__3213E83F7AD489F1] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_jbolt_version_file
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_jbolt_version_file]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_jbolt_version_file
-- ----------------------------
ALTER TABLE [dbo].[jb_jbolt_version_file] ADD CONSTRAINT [PK__jb_jbolt__3213E83FD468BEA8] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_login_log
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_login_log]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_login_log
-- ----------------------------
ALTER TABLE [dbo].[jb_login_log] ADD CONSTRAINT [PK__jb_login__3213E83F1549E8B9] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_mall
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_mall]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_mall
-- ----------------------------
ALTER TABLE [dbo].[jb_mall] ADD CONSTRAINT [PK__jb_mall__3213E83FCB57A466] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_online_user
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_online_user]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_online_user
-- ----------------------------
ALTER TABLE [dbo].[jb_online_user] ADD CONSTRAINT [PK__jb_onlin__3213E83F26B1097E] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_order
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_order]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_order
-- ----------------------------
ALTER TABLE [dbo].[jb_order] ADD CONSTRAINT [PK__jb_order__3213E83F90AE42A3] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_order_item
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_order_item]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_order_item
-- ----------------------------
ALTER TABLE [dbo].[jb_order_item] ADD CONSTRAINT [PK__jb_order__3213E83FAC0404F4] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_order_shipping
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_order_shipping]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_order_shipping
-- ----------------------------
ALTER TABLE [dbo].[jb_order_shipping] ADD CONSTRAINT [PK__jb_order__3213E83F8394DDE4] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_permission
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_permission]', RESEED, 42)
GO


-- ----------------------------
-- Primary Key structure for table jb_permission
-- ----------------------------
ALTER TABLE [dbo].[jb_permission] ADD CONSTRAINT [PK__jb_permi__3213E83F06812DC9] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_post
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_post]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_post
-- ----------------------------
ALTER TABLE [dbo].[jb_post] ADD CONSTRAINT [PK__jb_post__3213E83F9676FB87] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table jb_private_message
-- ----------------------------
ALTER TABLE [dbo].[jb_private_message] ADD CONSTRAINT [PK__jb_priva__3213E83F7BF530B1] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_remote_login_log
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_remote_login_log]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_remote_login_log
-- ----------------------------
ALTER TABLE [dbo].[jb_remote_login_log] ADD CONSTRAINT [PK__jb_remot__3213E83FB841B522] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_role
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_role]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_role
-- ----------------------------
ALTER TABLE [dbo].[jb_role] ADD CONSTRAINT [PK__jb_role__3213E83F26BD740D] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_role_permission
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_role_permission]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_role_permission
-- ----------------------------
ALTER TABLE [dbo].[jb_role_permission] ADD CONSTRAINT [PK__jb_role___3213E83FB94C60D2] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_shelf
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_shelf]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_shelf
-- ----------------------------
ALTER TABLE [dbo].[jb_shelf] ADD CONSTRAINT [PK__jb_shelf__3213E83F28DE6E75] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_shelf_activity
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_shelf_activity]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_shelf_activity
-- ----------------------------
ALTER TABLE [dbo].[jb_shelf_activity] ADD CONSTRAINT [PK__jb_shelf__3213E83FDD64E33D] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_shelf_carousel
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_shelf_carousel]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_shelf_carousel
-- ----------------------------
ALTER TABLE [dbo].[jb_shelf_carousel] ADD CONSTRAINT [PK__jb_shelf__3213E83FF8CBA325] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_shelf_element
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_shelf_element]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_shelf_element
-- ----------------------------
ALTER TABLE [dbo].[jb_shelf_element] ADD CONSTRAINT [PK__jb_shelf__3213E83F576649A8] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_shelf_goods_floor
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_shelf_goods_floor]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_shelf_goods_floor
-- ----------------------------
ALTER TABLE [dbo].[jb_shelf_goods_floor] ADD CONSTRAINT [PK__jb_shelf__3213E83F8809297B] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_shelf_goods_group
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_shelf_goods_group]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_shelf_goods_group
-- ----------------------------
ALTER TABLE [dbo].[jb_shelf_goods_group] ADD CONSTRAINT [PK__jb_shelf__3213E83F185596C2] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_sku
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_sku]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_sku
-- ----------------------------
ALTER TABLE [dbo].[jb_sku] ADD CONSTRAINT [PK__jb_sku__3213E83FC403FEF7] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_sku_item
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_sku_item]', RESEED, 1)
GO


-- ----------------------------
-- Auto increment value for jb_spec
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_spec]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_spec
-- ----------------------------
ALTER TABLE [dbo].[jb_spec] ADD CONSTRAINT [PK__jb_spec__3213E83F57D888B6] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_spec_item
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_spec_item]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_spec_item
-- ----------------------------
ALTER TABLE [dbo].[jb_spec_item] ADD CONSTRAINT [PK__jb_spec___3213E83F960666AE] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table jb_sys_notice
-- ----------------------------
ALTER TABLE [dbo].[jb_sys_notice] ADD CONSTRAINT [PK__jb_sys_n__3213E83F6490E8BB] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table jb_sys_notice_reader
-- ----------------------------
ALTER TABLE [dbo].[jb_sys_notice_reader] ADD CONSTRAINT [PK__jb_sys_n__3213E83F3DFFEAF4] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_system_log
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_system_log]', RESEED, 4)
GO


-- ----------------------------
-- Primary Key structure for table jb_system_log
-- ----------------------------
ALTER TABLE [dbo].[jb_system_log] ADD CONSTRAINT [PK__jb_syste__3213E83F4F487E98] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table jb_todo
-- ----------------------------
ALTER TABLE [dbo].[jb_todo] ADD CONSTRAINT [PK__jb_todo__3213E83FD2130B83] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_topnav
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_topnav]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_topnav
-- ----------------------------
ALTER TABLE [dbo].[jb_topnav] ADD CONSTRAINT [PK__jb_topna__3213E83F52022EC5] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_topnav_menu
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_topnav_menu]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_topnav_menu
-- ----------------------------
ALTER TABLE [dbo].[jb_topnav_menu] ADD CONSTRAINT [PK__jb_topna__3213E83F6D0FD097] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_update_libs
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_update_libs]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_update_libs
-- ----------------------------
ALTER TABLE [dbo].[jb_update_libs] ADD CONSTRAINT [PK__jb_updat__3213E83FECE55546] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_user
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_user]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_user
-- ----------------------------
ALTER TABLE [dbo].[jb_user] ADD CONSTRAINT [PK__jb_user__3213E83F0BA94935] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_user_config
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_user_config]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_user_config
-- ----------------------------
ALTER TABLE [dbo].[jb_user_config] ADD CONSTRAINT [PK__jb_user___3213E83FA5BCDA3A] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_article
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_article]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_article
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_article] ADD CONSTRAINT [PK__jb_wecha__3213E83FB4768489] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_autoreply
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_autoreply]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_autoreply
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_autoreply] ADD CONSTRAINT [PK__jb_wecha__3213E83F5B88643F] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_config
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_config]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_config
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_config] ADD CONSTRAINT [PK__jb_wecha__3213E83F4659EEE9] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_keywords
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_keywords]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_keywords
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_keywords] ADD CONSTRAINT [PK__jb_wecha__3213E83F39AD793E] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_media
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_media]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_media
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_media] ADD CONSTRAINT [PK__jb_wecha__3213E83FC304125E] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_menu
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_menu]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_menu
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_menu] ADD CONSTRAINT [PK__jb_wecha__3213E83FF9F8571A] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_mpinfo
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_mpinfo]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_mpinfo
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_mpinfo] ADD CONSTRAINT [PK__jb_wecha__3213E83F1B6C1BE9] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_reply_content
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_reply_content]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_reply_content
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_reply_content] ADD CONSTRAINT [PK__jb_wecha__3213E83F4517476C] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for jb_wechat_user
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[jb_wechat_user]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table jb_wechat_user
-- ----------------------------
ALTER TABLE [dbo].[jb_wechat_user] ADD CONSTRAINT [PK__jb_wecha__3213E83F8B776A0F] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

