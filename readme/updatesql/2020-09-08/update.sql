CREATE TABLE `jb_online_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `session_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '会话ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `login_log_id` int(11) NOT NULL COMMENT '登录日志ID',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '登录时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE `jb_topnav` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(40) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '名称',
  `icon` varchar(40) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图标',
  `enable` char(1) COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '是否启用',
  `sort_rank` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='顶部导航';

CREATE TABLE `jb_topnav_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `topnav_id` int(11) DEFAULT NULL COMMENT '顶部导航ID',
  `permission_id` int(11) DEFAULT NULL COMMENT '菜单资源ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='顶部菜单对应左侧一级导航中间表';