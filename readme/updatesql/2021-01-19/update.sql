alter table jb_global_config add column `type_key` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型key';
alter table jb_global_config add column `type_id` int(11) DEFAULT NULL COMMENT '类型ID';
alter table jb_global_config add column `built_in` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '内置参数';

CREATE TABLE `jb_global_config_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(40) COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型名称',
  `sort_rank` int(11) DEFAULT NULL COMMENT '序号',
  `type_key` varchar(40) COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型KEY',
  `built_in` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '内置',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='全局参数类型分组';