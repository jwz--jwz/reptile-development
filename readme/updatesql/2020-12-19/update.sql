alter table jb_demotable add column `price` decimal(10,2) DEFAULT NULL COMMENT '单价';
alter table jb_demotable add column `amount` decimal(10,2) DEFAULT NULL COMMENT '数量';
alter table jb_demotable add column `total` decimal(10,2) DEFAULT NULL COMMENT '总额';
alter table jb_demotable add column `create_time` datetime DEFAULT NULL COMMENT '创建时间';
alter table jb_demotable add column `update_time` datetime DEFAULT NULL COMMENT '更新时间';
alter table jb_demotable rename column `breif_info` to `brief_info`;