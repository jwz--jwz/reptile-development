/*
 Navicat Premium Data Transfer

 Source Server         : pgsql
 Source Server Type    : PostgreSQL
 Source Server Version : 130003
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130003
 File Encoding         : 65001

 Date: 21/06/2021 21:29:57
*/


-- ----------------------------
-- Sequence structure for jb_application_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_application_idsq";
CREATE SEQUENCE "public"."jb_application_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_brand_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_brand_idsq";
CREATE SEQUENCE "public"."jb_brand_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_change_log_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_change_log_idsq";
CREATE SEQUENCE "public"."jb_change_log_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_demotable_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_demotable_idsq";
CREATE SEQUENCE "public"."jb_demotable_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_dept_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_dept_idsq";
CREATE SEQUENCE "public"."jb_dept_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_dictionary_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_dictionary_idsq";
CREATE SEQUENCE "public"."jb_dictionary_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_dictionary_type_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_dictionary_type_idsq";
CREATE SEQUENCE "public"."jb_dictionary_type_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_download_log_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_download_log_idsq";
CREATE SEQUENCE "public"."jb_download_log_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_global_config_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_global_config_idsq";
CREATE SEQUENCE "public"."jb_global_config_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_global_config_type_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_global_config_type_idsq";
CREATE SEQUENCE "public"."jb_global_config_type_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_goods_attr_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_goods_attr_idsq";
CREATE SEQUENCE "public"."jb_goods_attr_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_goods_back_category_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_goods_back_category_idsq";
CREATE SEQUENCE "public"."jb_goods_back_category_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_goods_element_content_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_goods_element_content_idsq";
CREATE SEQUENCE "public"."jb_goods_element_content_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_goods_group_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_goods_group_idsq";
CREATE SEQUENCE "public"."jb_goods_group_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_goods_html_content_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_goods_html_content_idsq";
CREATE SEQUENCE "public"."jb_goods_html_content_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_goods_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_goods_idsq";
CREATE SEQUENCE "public"."jb_goods_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_goods_type_brand_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_goods_type_brand_idsq";
CREATE SEQUENCE "public"."jb_goods_type_brand_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_goods_type_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_goods_type_idsq";
CREATE SEQUENCE "public"."jb_goods_type_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_jbolt_file_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_jbolt_file_idsq";
CREATE SEQUENCE "public"."jb_jbolt_file_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_jbolt_version_file_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_jbolt_version_file_idsq";
CREATE SEQUENCE "public"."jb_jbolt_version_file_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_jbolt_version_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_jbolt_version_idsq";
CREATE SEQUENCE "public"."jb_jbolt_version_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_login_log_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_login_log_idsq";
CREATE SEQUENCE "public"."jb_login_log_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_mall_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_mall_idsq";
CREATE SEQUENCE "public"."jb_mall_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_online_user_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_online_user_idsq";
CREATE SEQUENCE "public"."jb_online_user_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_order_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_order_idsq";
CREATE SEQUENCE "public"."jb_order_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_order_item_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_order_item_idsq";
CREATE SEQUENCE "public"."jb_order_item_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_order_shipping_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_order_shipping_idsq";
CREATE SEQUENCE "public"."jb_order_shipping_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_permission_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_permission_idsq";
CREATE SEQUENCE "public"."jb_permission_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_post_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_post_idsq";
CREATE SEQUENCE "public"."jb_post_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_private_message_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_private_message_idsq";
CREATE SEQUENCE "public"."jb_private_message_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_remote_login_log_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_remote_login_log_idsq";
CREATE SEQUENCE "public"."jb_remote_login_log_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_role_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_role_idsq";
CREATE SEQUENCE "public"."jb_role_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_role_permission_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_role_permission_idsq";
CREATE SEQUENCE "public"."jb_role_permission_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_shelf_activity_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_shelf_activity_idsq";
CREATE SEQUENCE "public"."jb_shelf_activity_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_shelf_carousel_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_shelf_carousel_idsq";
CREATE SEQUENCE "public"."jb_shelf_carousel_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_shelf_element_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_shelf_element_idsq";
CREATE SEQUENCE "public"."jb_shelf_element_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_shelf_goods_floor_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_shelf_goods_floor_idsq";
CREATE SEQUENCE "public"."jb_shelf_goods_floor_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_shelf_goods_group_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_shelf_goods_group_idsq";
CREATE SEQUENCE "public"."jb_shelf_goods_group_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_shelf_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_shelf_idsq";
CREATE SEQUENCE "public"."jb_shelf_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_sku_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_sku_idsq";
CREATE SEQUENCE "public"."jb_sku_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_sku_item_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_sku_item_idsq";
CREATE SEQUENCE "public"."jb_sku_item_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_spec_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_spec_idsq";
CREATE SEQUENCE "public"."jb_spec_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_spec_item_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_spec_item_idsq";
CREATE SEQUENCE "public"."jb_spec_item_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_sys_notice_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_sys_notice_idsq";
CREATE SEQUENCE "public"."jb_sys_notice_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_sys_notice_reader_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_sys_notice_reader_idsq";
CREATE SEQUENCE "public"."jb_sys_notice_reader_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_system_log_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_system_log_idsq";
CREATE SEQUENCE "public"."jb_system_log_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_todo_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_todo_idsq";
CREATE SEQUENCE "public"."jb_todo_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_topnav_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_topnav_idsq";
CREATE SEQUENCE "public"."jb_topnav_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_topnav_menu_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_topnav_menu_idsq";
CREATE SEQUENCE "public"."jb_topnav_menu_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_update_libs_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_update_libs_idsq";
CREATE SEQUENCE "public"."jb_update_libs_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_user_config_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_user_config_idsq";
CREATE SEQUENCE "public"."jb_user_config_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_user_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_user_idsq";
CREATE SEQUENCE "public"."jb_user_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_article_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_article_idsq";
CREATE SEQUENCE "public"."jb_wechat_article_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_autoreply_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_autoreply_idsq";
CREATE SEQUENCE "public"."jb_wechat_autoreply_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_config_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_config_idsq";
CREATE SEQUENCE "public"."jb_wechat_config_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_keywords_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_keywords_idsq";
CREATE SEQUENCE "public"."jb_wechat_keywords_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_media_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_media_idsq";
CREATE SEQUENCE "public"."jb_wechat_media_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_menu_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_menu_idsq";
CREATE SEQUENCE "public"."jb_wechat_menu_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_mpinfo_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_mpinfo_idsq";
CREATE SEQUENCE "public"."jb_wechat_mpinfo_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_reply_content_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_reply_content_idsq";
CREATE SEQUENCE "public"."jb_wechat_reply_content_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Sequence structure for jb_wechat_user_idsq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."jb_wechat_user_idsq";
CREATE SEQUENCE "public"."jb_wechat_user_idsq" 
INCREMENT 1
MINVALUE  100001
MAXVALUE 9223372036854775807
START 100001
CACHE 50;

-- ----------------------------
-- Table structure for jb_application
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_application";
CREATE TABLE "public"."jb_application" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "brief_info" varchar(255) COLLATE "pg_catalog"."default",
  "app_id" varchar(255) COLLATE "pg_catalog"."default",
  "app_secret" varchar(255) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "create_time" timestamp(6),
  "update_time" timestamp(6),
  "user_id" int4,
  "update_user_id" int4,
  "type" int4 NOT NULL,
  "need_check_sign" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "link_target_id" int4
)
;
COMMENT ON COLUMN "public"."jb_application"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_application"."name" IS '应用名称';
COMMENT ON COLUMN "public"."jb_application"."brief_info" IS '应用简介';
COMMENT ON COLUMN "public"."jb_application"."app_id" IS '应用ID';
COMMENT ON COLUMN "public"."jb_application"."app_secret" IS '应用密钥';
COMMENT ON COLUMN "public"."jb_application"."enable" IS '是否启用';
COMMENT ON COLUMN "public"."jb_application"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_application"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_application"."user_id" IS '创建用户ID';
COMMENT ON COLUMN "public"."jb_application"."update_user_id" IS '更新用户ID';
COMMENT ON COLUMN "public"."jb_application"."type" IS 'app类型';
COMMENT ON COLUMN "public"."jb_application"."need_check_sign" IS '是否需要接口校验SIGN';
COMMENT ON COLUMN "public"."jb_application"."link_target_id" IS '关联目标ID';
COMMENT ON TABLE "public"."jb_application" IS 'API应用中心的应用APP';

-- ----------------------------
-- Table structure for jb_brand
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_brand";
CREATE TABLE "public"."jb_brand" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "sort_rank" int4,
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "logo" varchar(255) COLLATE "pg_catalog"."default",
  "remark" varchar(255) COLLATE "pg_catalog"."default",
  "english_name" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_brand"."id" IS '主键';
COMMENT ON COLUMN "public"."jb_brand"."name" IS '品牌名';
COMMENT ON COLUMN "public"."jb_brand"."enable" IS '是否启用';
COMMENT ON COLUMN "public"."jb_brand"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_brand"."url" IS '网址';
COMMENT ON COLUMN "public"."jb_brand"."logo" IS 'logo地址';
COMMENT ON COLUMN "public"."jb_brand"."remark" IS '备注描述';
COMMENT ON COLUMN "public"."jb_brand"."english_name" IS '英文名';
COMMENT ON TABLE "public"."jb_brand" IS '品牌';

-- ----------------------------
-- Table structure for jb_change_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_change_log";
CREATE TABLE "public"."jb_change_log" (
  "id" int4 NOT NULL,
  "content" text COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "jbolt_version_id" int4
)
;
COMMENT ON COLUMN "public"."jb_change_log"."id" IS '主键';
COMMENT ON COLUMN "public"."jb_change_log"."content" IS '日志内容';
COMMENT ON COLUMN "public"."jb_change_log"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_change_log"."jbolt_version_id" IS 'jbolt版本ID';
COMMENT ON TABLE "public"."jb_change_log" IS 'changeLog';

-- ----------------------------
-- Table structure for jb_demotable
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_demotable";
CREATE TABLE "public"."jb_demotable" (
  "id" int4 NOT NULL,
  "demo_date" date,
  "demo_time" time(6),
  "demo_date_time" timestamp(6),
  "demo_week" varchar(255) COLLATE "pg_catalog"."default",
  "demo_month" varchar(255) COLLATE "pg_catalog"."default",
  "time" time(6),
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "pid" int4,
  "sort_rank" int4,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "age" int4,
  "brief_info" varchar(255) COLLATE "pg_catalog"."default",
  "is_system_admin" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "price" numeric(10,2),
  "amount" numeric(10,2),
  "goods_unit" varchar(40) COLLATE "pg_catalog"."default",
  "total" numeric(10,2),
  "create_time" timestamp(6),
  "update_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."jb_demotable"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_demotable"."demo_date" IS 'demo日期';
COMMENT ON COLUMN "public"."jb_demotable"."demo_time" IS 'demo时间';
COMMENT ON COLUMN "public"."jb_demotable"."demo_date_time" IS 'demo日期与时间';
COMMENT ON COLUMN "public"."jb_demotable"."demo_week" IS 'demo周';
COMMENT ON COLUMN "public"."jb_demotable"."demo_month" IS 'demo月';
COMMENT ON COLUMN "public"."jb_demotable"."time" IS 'demo纯时间';
COMMENT ON COLUMN "public"."jb_demotable"."enable" IS '启用';
COMMENT ON COLUMN "public"."jb_demotable"."pid" IS '父级ID';
COMMENT ON COLUMN "public"."jb_demotable"."sort_rank" IS '序号';
COMMENT ON COLUMN "public"."jb_demotable"."name" IS '姓名';
COMMENT ON COLUMN "public"."jb_demotable"."age" IS '年龄';
COMMENT ON COLUMN "public"."jb_demotable"."brief_info" IS '简介';
COMMENT ON COLUMN "public"."jb_demotable"."is_system_admin" IS '是否为管理';
COMMENT ON COLUMN "public"."jb_demotable"."price" IS '单价';
COMMENT ON COLUMN "public"."jb_demotable"."amount" IS '数量';
COMMENT ON COLUMN "public"."jb_demotable"."goods_unit" IS '单位';
COMMENT ON COLUMN "public"."jb_demotable"."total" IS '总额';
COMMENT ON COLUMN "public"."jb_demotable"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_demotable"."update_time" IS '更新时间';
COMMENT ON TABLE "public"."jb_demotable" IS 'demo表';

-- ----------------------------
-- Table structure for jb_dept
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_dept";
CREATE TABLE "public"."jb_dept" (
  "id" int4 NOT NULL,
  "name" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "full_name" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "pid" int4 NOT NULL,
  "type" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "leader" varchar(40) COLLATE "pg_catalog"."default",
  "phone" varchar(40) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default",
  "address" varchar(100) COLLATE "pg_catalog"."default",
  "zipcode" varchar(40) COLLATE "pg_catalog"."default",
  "sn" varchar(40) COLLATE "pg_catalog"."default",
  "sort_rank" int4,
  "remark" varchar(255) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '1'::bpchar NOT NULL,
  "create_time" timestamp(6),
  "update_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."jb_dept"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_dept"."name" IS '部门名称';
COMMENT ON COLUMN "public"."jb_dept"."full_name" IS '全称';
COMMENT ON COLUMN "public"."jb_dept"."type" IS '部门类型';
COMMENT ON COLUMN "public"."jb_dept"."leader" IS '负责人';
COMMENT ON COLUMN "public"."jb_dept"."phone" IS '手机号';
COMMENT ON COLUMN "public"."jb_dept"."email" IS '电子邮件';
COMMENT ON COLUMN "public"."jb_dept"."address" IS '地址';
COMMENT ON COLUMN "public"."jb_dept"."zipcode" IS '邮政编码';
COMMENT ON COLUMN "public"."jb_dept"."sn" IS '编码';
COMMENT ON COLUMN "public"."jb_dept"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_dept"."remark" IS '备注';
COMMENT ON COLUMN "public"."jb_dept"."enable" IS '启用';
COMMENT ON COLUMN "public"."jb_dept"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_dept"."update_time" IS '更新时间';
COMMENT ON TABLE "public"."jb_dept" IS '部门管理';

-- ----------------------------
-- Table structure for jb_dictionary
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_dictionary";
CREATE TABLE "public"."jb_dictionary" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "type_id" int4,
  "pid" int4,
  "sort_rank" int4,
  "sn" varchar(255) COLLATE "pg_catalog"."default",
  "type_key" varchar(40) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_dictionary"."id" IS '字典ID主键';
COMMENT ON COLUMN "public"."jb_dictionary"."name" IS '名称';
COMMENT ON COLUMN "public"."jb_dictionary"."type_id" IS '字典类型ID';
COMMENT ON COLUMN "public"."jb_dictionary"."pid" IS '父类ID';
COMMENT ON COLUMN "public"."jb_dictionary"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_dictionary"."sn" IS '编号编码';
COMMENT ON COLUMN "public"."jb_dictionary"."type_key" IS '字典类型KEY';
COMMENT ON TABLE "public"."jb_dictionary" IS '字典表';

-- ----------------------------
-- Table structure for jb_dictionary_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_dictionary_type";
CREATE TABLE "public"."jb_dictionary_type" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "mode_level" int4,
  "type_key" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_dictionary_type"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_dictionary_type"."name" IS '类型名称';
COMMENT ON COLUMN "public"."jb_dictionary_type"."mode_level" IS '模式层级';
COMMENT ON COLUMN "public"."jb_dictionary_type"."type_key" IS '标识KEY';
COMMENT ON TABLE "public"."jb_dictionary_type" IS '字典类型';

-- ----------------------------
-- Table structure for jb_download_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_download_log";
CREATE TABLE "public"."jb_download_log" (
  "id" int4 NOT NULL,
  "ipaddress" varchar(255) COLLATE "pg_catalog"."default",
  "download_type" int4,
  "download_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."jb_download_log"."ipaddress" IS '下载地址';
COMMENT ON COLUMN "public"."jb_download_log"."download_type" IS '下载类型';
COMMENT ON COLUMN "public"."jb_download_log"."download_time" IS '下载时间';
COMMENT ON TABLE "public"."jb_download_log" IS '下载log';

-- ----------------------------
-- Table structure for jb_global_config
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_global_config";
CREATE TABLE "public"."jb_global_config" (
  "id" int4 NOT NULL,
  "config_key" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "config_value" varchar(255) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6) NOT NULL,
  "user_id" int4,
  "update_time" timestamp(6),
  "update_user_id" int4,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "value_type" varchar(40) COLLATE "pg_catalog"."default",
  "type_key" varchar(255) COLLATE "pg_catalog"."default",
  "type_id" int4,
  "built_in" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_global_config"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_global_config"."config_key" IS '参数KEY';
COMMENT ON COLUMN "public"."jb_global_config"."config_value" IS '参数值';
COMMENT ON COLUMN "public"."jb_global_config"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_global_config"."user_id" IS '创建用户ID';
COMMENT ON COLUMN "public"."jb_global_config"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_global_config"."update_user_id" IS '更新用户ID';
COMMENT ON COLUMN "public"."jb_global_config"."name" IS '参名称';
COMMENT ON COLUMN "public"."jb_global_config"."value_type" IS '值类型';
COMMENT ON COLUMN "public"."jb_global_config"."type_key" IS '参数类型';
COMMENT ON COLUMN "public"."jb_global_config"."type_id" IS '参数类型ID';
COMMENT ON COLUMN "public"."jb_global_config"."built_in" IS '系统内置';
COMMENT ON TABLE "public"."jb_global_config" IS '全局参数配置';

-- ----------------------------
-- Table structure for jb_global_config_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_global_config_type";
CREATE TABLE "public"."jb_global_config_type" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "sort_rank" int4,
  "type_key" varchar(255) COLLATE "pg_catalog"."default",
  "built_in" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_global_config_type"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_global_config_type"."name" IS '类型名称';
COMMENT ON COLUMN "public"."jb_global_config_type"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_global_config_type"."type_key" IS '类型KEY';
COMMENT ON COLUMN "public"."jb_global_config_type"."built_in" IS '系统内置';
COMMENT ON TABLE "public"."jb_global_config_type" IS '全局参数类型';

-- ----------------------------
-- Table structure for jb_goods
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_goods";
CREATE TABLE "public"."jb_goods" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "price" numeric(10,2),
  "original_price" numeric(10,2),
  "main_image" varchar(255) COLLATE "pg_catalog"."default",
  "extra_images" text COLLATE "pg_catalog"."default",
  "content_type" int4,
  "groups" varchar(255) COLLATE "pg_catalog"."default",
  "stock_count" numeric(10,2),
  "sub_title" varchar(255) COLLATE "pg_catalog"."default",
  "is_multispec" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "limit_count" numeric(10,2),
  "location_label" varchar(255) COLLATE "pg_catalog"."default",
  "is_provide_invoices" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "is_guarantee" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "on_sale" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "under_time" timestamp(6),
  "on_sale_user_id" int4,
  "on_sale_time" timestamp(6),
  "create_user_id" int4,
  "create_time" timestamp(6),
  "update_time" timestamp(6),
  "update_user_id" int4,
  "goods_unit" int4,
  "real_sale_count" numeric(10,2),
  "show_sale_count" numeric(10,2),
  "type_id" int4,
  "brand_id" int4,
  "is_hot" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "is_recommend" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "fcategory_key" varchar(255) COLLATE "pg_catalog"."default",
  "bcategory_key" varchar(255) COLLATE "pg_catalog"."default",
  "bcategory_id" int4,
  "fcategory_id" int4,
  "under_user_id" int4,
  "is_delete" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "goods_no" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_goods"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_goods"."price" IS '单价';
COMMENT ON COLUMN "public"."jb_goods"."original_price" IS '原价';
COMMENT ON COLUMN "public"."jb_goods"."main_image" IS '主图';
COMMENT ON COLUMN "public"."jb_goods"."extra_images" IS '附图';
COMMENT ON COLUMN "public"."jb_goods"."content_type" IS '商品描述类型';
COMMENT ON COLUMN "public"."jb_goods"."groups" IS '商品组ID';
COMMENT ON COLUMN "public"."jb_goods"."stock_count" IS '库存量';
COMMENT ON COLUMN "public"."jb_goods"."sub_title" IS '二级标题';
COMMENT ON COLUMN "public"."jb_goods"."is_multispec" IS '是否是多规格';
COMMENT ON COLUMN "public"."jb_goods"."limit_count" IS '限购数量 0=不限制';
COMMENT ON COLUMN "public"."jb_goods"."location_label" IS '所在地';
COMMENT ON COLUMN "public"."jb_goods"."is_provide_invoices" IS '是否提供发票';
COMMENT ON COLUMN "public"."jb_goods"."is_guarantee" IS '是否保修';
COMMENT ON COLUMN "public"."jb_goods"."on_sale" IS '是否上架';
COMMENT ON COLUMN "public"."jb_goods"."under_time" IS '下架时间';
COMMENT ON COLUMN "public"."jb_goods"."on_sale_user_id" IS '上架处理人';
COMMENT ON COLUMN "public"."jb_goods"."on_sale_time" IS '上架时间';
COMMENT ON COLUMN "public"."jb_goods"."create_user_id" IS '创建人';
COMMENT ON COLUMN "public"."jb_goods"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_goods"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_goods"."update_user_id" IS '更新人';
COMMENT ON COLUMN "public"."jb_goods"."goods_unit" IS '商品单位';
COMMENT ON COLUMN "public"."jb_goods"."real_sale_count" IS '真实销量';
COMMENT ON COLUMN "public"."jb_goods"."show_sale_count" IS '展示营销销量';
COMMENT ON COLUMN "public"."jb_goods"."type_id" IS '商品类型';
COMMENT ON COLUMN "public"."jb_goods"."brand_id" IS '商品品牌';
COMMENT ON COLUMN "public"."jb_goods"."is_hot" IS '热销';
COMMENT ON COLUMN "public"."jb_goods"."is_recommend" IS '推荐';
COMMENT ON COLUMN "public"."jb_goods"."fcategory_key" IS '前台分类KEY';
COMMENT ON COLUMN "public"."jb_goods"."bcategory_key" IS '后台分类KEY';
COMMENT ON COLUMN "public"."jb_goods"."bcategory_id" IS '后端分类ID';
COMMENT ON COLUMN "public"."jb_goods"."fcategory_id" IS '前端分类ID';
COMMENT ON COLUMN "public"."jb_goods"."under_user_id" IS '下架处理人';
COMMENT ON COLUMN "public"."jb_goods"."is_delete" IS '是否已删除';
COMMENT ON COLUMN "public"."jb_goods"."goods_no" IS '商品编号';
COMMENT ON TABLE "public"."jb_goods" IS '商品';

-- ----------------------------
-- Table structure for jb_goods_attr
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_goods_attr";
CREATE TABLE "public"."jb_goods_attr" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "value" varchar(255) COLLATE "pg_catalog"."default",
  "goods_id" int4
)
;
COMMENT ON COLUMN "public"."jb_goods_attr"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_goods_attr"."name" IS '属性名';
COMMENT ON COLUMN "public"."jb_goods_attr"."value" IS '属性值';
COMMENT ON COLUMN "public"."jb_goods_attr"."goods_id" IS '商品ID';
COMMENT ON TABLE "public"."jb_goods_attr" IS '商品自定义属性';

-- ----------------------------
-- Table structure for jb_goods_back_category
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_goods_back_category";
CREATE TABLE "public"."jb_goods_back_category" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "pid" int4,
  "type_id" int4,
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "category_key" varchar(255) COLLATE "pg_catalog"."default",
  "sort_rank" int4
)
;
COMMENT ON COLUMN "public"."jb_goods_back_category"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_goods_back_category"."name" IS '分类名称';
COMMENT ON COLUMN "public"."jb_goods_back_category"."pid" IS '父级ID';
COMMENT ON COLUMN "public"."jb_goods_back_category"."type_id" IS '商品类型';
COMMENT ON COLUMN "public"."jb_goods_back_category"."enable" IS '启用 禁用';
COMMENT ON COLUMN "public"."jb_goods_back_category"."category_key" IS '所有上级和自身ID串联起来';
COMMENT ON COLUMN "public"."jb_goods_back_category"."sort_rank" IS '排序';
COMMENT ON TABLE "public"."jb_goods_back_category" IS '商品后台类目表 无限';

-- ----------------------------
-- Table structure for jb_goods_element_content
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_goods_element_content";
CREATE TABLE "public"."jb_goods_element_content" (
  "id" int4 NOT NULL,
  "goods_id" int4,
  "type" int4,
  "content" varchar(255) COLLATE "pg_catalog"."default",
  "sort_rank" int4,
  "sku_id" int4
)
;
COMMENT ON COLUMN "public"."jb_goods_element_content"."id" IS '关联商品ID';
COMMENT ON COLUMN "public"."jb_goods_element_content"."goods_id" IS '商品ID';
COMMENT ON COLUMN "public"."jb_goods_element_content"."content" IS '内容';
COMMENT ON COLUMN "public"."jb_goods_element_content"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_goods_element_content"."sku_id" IS 'SKUID';
COMMENT ON TABLE "public"."jb_goods_element_content" IS '商品关联的单条图片或者问题分类的content列表';

-- ----------------------------
-- Table structure for jb_goods_group
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_goods_group";
CREATE TABLE "public"."jb_goods_group" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "sort_rank" varchar(255) COLLATE "pg_catalog"."default",
  "icon" varchar(255) COLLATE "pg_catalog"."default",
  "goods_count" int4,
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_goods_group"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_goods_group"."name" IS '名称';
COMMENT ON COLUMN "public"."jb_goods_group"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_goods_group"."icon" IS '图标';
COMMENT ON COLUMN "public"."jb_goods_group"."goods_count" IS '商品数量';
COMMENT ON COLUMN "public"."jb_goods_group"."enable" IS '是否启用';
COMMENT ON TABLE "public"."jb_goods_group" IS '商品分组';

-- ----------------------------
-- Table structure for jb_goods_html_content
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_goods_html_content";
CREATE TABLE "public"."jb_goods_html_content" (
  "id" int4 NOT NULL,
  "goods_id" int4,
  "content" text COLLATE "pg_catalog"."default",
  "sku_id" int4,
  "update_user_id" int4,
  "update_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."jb_goods_html_content"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_goods_html_content"."goods_id" IS '商品ID';
COMMENT ON COLUMN "public"."jb_goods_html_content"."content" IS '详情内容';
COMMENT ON COLUMN "public"."jb_goods_html_content"."sku_id" IS 'SKUID';
COMMENT ON COLUMN "public"."jb_goods_html_content"."update_user_id" IS '更新人';
COMMENT ON COLUMN "public"."jb_goods_html_content"."update_time" IS '更新时间';
COMMENT ON TABLE "public"."jb_goods_html_content" IS '商品关联的html富文本详情';

-- ----------------------------
-- Table structure for jb_goods_type
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_goods_type";
CREATE TABLE "public"."jb_goods_type" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "sort_rank" int4,
  "spec_count" int4,
  "attr_count" int4,
  "brand_count" int4,
  "pinyin" varchar(255) COLLATE "pg_catalog"."default",
  "remark" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_goods_type"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_goods_type"."enable" IS '是否启用';
COMMENT ON COLUMN "public"."jb_goods_type"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_goods_type"."spec_count" IS '规格数量';
COMMENT ON COLUMN "public"."jb_goods_type"."attr_count" IS '属性数量';
COMMENT ON COLUMN "public"."jb_goods_type"."brand_count" IS '品牌数量';
COMMENT ON COLUMN "public"."jb_goods_type"."pinyin" IS '拼音';
COMMENT ON COLUMN "public"."jb_goods_type"."remark" IS '备注信息';
COMMENT ON TABLE "public"."jb_goods_type" IS '商品类型';

-- ----------------------------
-- Table structure for jb_goods_type_brand
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_goods_type_brand";
CREATE TABLE "public"."jb_goods_type_brand" (
  "id" int4 NOT NULL,
  "goods_type_id" int4,
  "brand_id" int4
)
;
COMMENT ON COLUMN "public"."jb_goods_type_brand"."goods_type_id" IS '关联商品类型';
COMMENT ON COLUMN "public"."jb_goods_type_brand"."brand_id" IS '关联商品品牌';
COMMENT ON TABLE "public"."jb_goods_type_brand" IS '商品类型-品牌中间表';

-- ----------------------------
-- Table structure for jb_jbolt_file
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_jbolt_file";
CREATE TABLE "public"."jb_jbolt_file" (
  "id" int4 NOT NULL,
  "local_path" varchar(255) COLLATE "pg_catalog"."default",
  "local_url" varchar(255) COLLATE "pg_catalog"."default",
  "cdn_url" varchar(255) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "user_id" int4,
  "file_name" varchar(255) COLLATE "pg_catalog"."default",
  "file_type" int4,
  "file_size" int4 DEFAULT 0,
  "file_suffix" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_jbolt_file"."local_path" IS '保存物理地址';
COMMENT ON COLUMN "public"."jb_jbolt_file"."local_url" IS '本地可访问URL地址';
COMMENT ON COLUMN "public"."jb_jbolt_file"."cdn_url" IS '外部CDN地址';
COMMENT ON COLUMN "public"."jb_jbolt_file"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_jbolt_file"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."jb_jbolt_file"."file_name" IS '文件名';
COMMENT ON COLUMN "public"."jb_jbolt_file"."file_type" IS '文件类型 图片 附件 视频 音频';
COMMENT ON COLUMN "public"."jb_jbolt_file"."file_size" IS '文件大小';
COMMENT ON COLUMN "public"."jb_jbolt_file"."file_suffix" IS '后缀名';
COMMENT ON TABLE "public"."jb_jbolt_file" IS '文件库';

-- ----------------------------
-- Table structure for jb_jbolt_version
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_jbolt_version";
CREATE TABLE "public"."jb_jbolt_version" (
  "id" int4 NOT NULL,
  "version" varchar(255) COLLATE "pg_catalog"."default",
  "publish_time" timestamp(6),
  "is_new" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "create_time" timestamp(6),
  "user_id" int4
)
;
COMMENT ON COLUMN "public"."jb_jbolt_version"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_jbolt_version"."version" IS '版本号';
COMMENT ON COLUMN "public"."jb_jbolt_version"."publish_time" IS '发布时间';
COMMENT ON COLUMN "public"."jb_jbolt_version"."is_new" IS '是否最新版';
COMMENT ON COLUMN "public"."jb_jbolt_version"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_jbolt_version"."user_id" IS '用户ID';
COMMENT ON TABLE "public"."jb_jbolt_version" IS 'jbolt版本升级';

-- ----------------------------
-- Table structure for jb_jbolt_version_file
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_jbolt_version_file";
CREATE TABLE "public"."jb_jbolt_version_file" (
  "id" int4 NOT NULL,
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "jbolt_version_id" int4
)
;
COMMENT ON COLUMN "public"."jb_jbolt_version_file"."jbolt_version_id" IS 'jbolt版本ID';
COMMENT ON TABLE "public"."jb_jbolt_version_file" IS 'JBolt下载版本管理';

-- ----------------------------
-- Table structure for jb_login_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_login_log";
CREATE TABLE "public"."jb_login_log" (
  "id" int4 NOT NULL,
  "user_id" int4,
  "username" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "login_ip" varchar(40) COLLATE "pg_catalog"."default",
  "login_time" timestamp(6) NOT NULL,
  "login_state" int4 NOT NULL,
  "is_browser" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "browser_version" varchar(40) COLLATE "pg_catalog"."default",
  "browser_name" varchar(100) COLLATE "pg_catalog"."default",
  "os_name" varchar(100) COLLATE "pg_catalog"."default",
  "login_city" varchar(40) COLLATE "pg_catalog"."default",
  "login_address" varchar(255) COLLATE "pg_catalog"."default",
  "login_city_code" varchar(40) COLLATE "pg_catalog"."default",
  "login_province" varchar(40) COLLATE "pg_catalog"."default",
  "login_country" varchar(40) COLLATE "pg_catalog"."default",
  "is_mobile" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "os_platform_name" varchar(40) COLLATE "pg_catalog"."default",
  "browser_engine_name" varchar(40) COLLATE "pg_catalog"."default",
  "browser_engine_version" varchar(40) COLLATE "pg_catalog"."default",
  "login_address_latitude" varchar(40) COLLATE "pg_catalog"."default",
  "login_address_longitude" varchar(40) COLLATE "pg_catalog"."default",
  "is_remote_login" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "create_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."jb_login_log"."id" IS 'ID';
COMMENT ON COLUMN "public"."jb_login_log"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."jb_login_log"."username" IS '用户名';
COMMENT ON COLUMN "public"."jb_login_log"."login_ip" IS 'IP地址';
COMMENT ON COLUMN "public"."jb_login_log"."login_time" IS '登录时间';
COMMENT ON COLUMN "public"."jb_login_log"."login_state" IS '登录状态';
COMMENT ON COLUMN "public"."jb_login_log"."is_browser" IS '是否是浏览器访问';
COMMENT ON COLUMN "public"."jb_login_log"."browser_version" IS '浏览器版本号';
COMMENT ON COLUMN "public"."jb_login_log"."browser_name" IS '浏览器';
COMMENT ON COLUMN "public"."jb_login_log"."os_name" IS '操作系统';
COMMENT ON COLUMN "public"."jb_login_log"."login_city" IS '登录城市';
COMMENT ON COLUMN "public"."jb_login_log"."login_address" IS '登录位置详情';
COMMENT ON COLUMN "public"."jb_login_log"."login_city_code" IS '登录城市代码';
COMMENT ON COLUMN "public"."jb_login_log"."login_province" IS '登录省份';
COMMENT ON COLUMN "public"."jb_login_log"."login_country" IS '登录国家';
COMMENT ON COLUMN "public"."jb_login_log"."is_mobile" IS '是否移动端';
COMMENT ON COLUMN "public"."jb_login_log"."os_platform_name" IS '平台名称';
COMMENT ON COLUMN "public"."jb_login_log"."browser_engine_name" IS '浏览器引擎名';
COMMENT ON COLUMN "public"."jb_login_log"."browser_engine_version" IS '浏览器引擎版本';
COMMENT ON COLUMN "public"."jb_login_log"."login_address_latitude" IS '登录地横坐标';
COMMENT ON COLUMN "public"."jb_login_log"."login_address_longitude" IS '登录地纵坐标';
COMMENT ON COLUMN "public"."jb_login_log"."is_remote_login" IS '是否为异地异常登录';
COMMENT ON COLUMN "public"."jb_login_log"."create_time" IS '记录创建时间';
COMMENT ON TABLE "public"."jb_login_log" IS '登录日志';

-- ----------------------------
-- Table structure for jb_mall
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_mall";
CREATE TABLE "public"."jb_mall" (
  "id" int4 NOT NULL,
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "create_time" timestamp(6) NOT NULL,
  "create_user_id" int4 NOT NULL,
  "update_time" timestamp(6),
  "update_user_id" int4,
  "state" int4 NOT NULL,
  "leader_id" int4,
  "first_publish_time" timestamp(6),
  "last_publish_time" timestamp(6),
  "first_close_time" timestamp(6),
  "last_close_time" timestamp(6),
  "first_publish_user_id" int4,
  "last_publish_user_id" int4,
  "first_close_user_id" int4,
  "last_close_user_id" int4
)
;
COMMENT ON COLUMN "public"."jb_mall"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_mall"."name" IS '商城名称';
COMMENT ON COLUMN "public"."jb_mall"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_mall"."create_user_id" IS '创建人';
COMMENT ON COLUMN "public"."jb_mall"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_mall"."update_user_id" IS '更新操作人';
COMMENT ON COLUMN "public"."jb_mall"."state" IS '状态 1筹备中 2 已上线 3 关闭';
COMMENT ON COLUMN "public"."jb_mall"."leader_id" IS '负责人';
COMMENT ON COLUMN "public"."jb_mall"."first_publish_time" IS '首次上线发布时间';
COMMENT ON COLUMN "public"."jb_mall"."last_publish_time" IS '最近一次上线发布操作时间';
COMMENT ON COLUMN "public"."jb_mall"."first_close_time" IS '首次关闭时间';
COMMENT ON COLUMN "public"."jb_mall"."last_close_time" IS '最近一次关闭时间';
COMMENT ON COLUMN "public"."jb_mall"."first_publish_user_id" IS '首次发布人';
COMMENT ON COLUMN "public"."jb_mall"."last_publish_user_id" IS '最近一次发布人';
COMMENT ON COLUMN "public"."jb_mall"."first_close_user_id" IS '首次关闭商城操作人';
COMMENT ON COLUMN "public"."jb_mall"."last_close_user_id" IS '最近一次关闭操作人';
COMMENT ON TABLE "public"."jb_mall" IS '商城表一条数据代表一个商城';

-- ----------------------------
-- Table structure for jb_online_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_online_user";
CREATE TABLE "public"."jb_online_user" (
  "id" int4 NOT NULL,
  "session_id" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "user_id" int4 NOT NULL,
  "login_log_id" int4 NOT NULL,
  "login_time" timestamp(6) NOT NULL,
  "update_time" timestamp(6) NOT NULL,
  "expiration_time" timestamp(6) NOT NULL,
  "screen_locked" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "online_state" int4 NOT NULL,
  "offline_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."jb_online_user"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_online_user"."session_id" IS '会话ID';
COMMENT ON COLUMN "public"."jb_online_user"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."jb_online_user"."login_log_id" IS '登录日志ID';
COMMENT ON COLUMN "public"."jb_online_user"."login_time" IS '登录时间';
COMMENT ON COLUMN "public"."jb_online_user"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_online_user"."expiration_time" IS '过期时间';
COMMENT ON COLUMN "public"."jb_online_user"."screen_locked" IS '是否锁屏';
COMMENT ON COLUMN "public"."jb_online_user"."online_state" IS '在线状态';
COMMENT ON COLUMN "public"."jb_online_user"."offline_time" IS '离线时间';

-- ----------------------------
-- Table structure for jb_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_order";
CREATE TABLE "public"."jb_order" (
  "id" int4 NOT NULL,
  "order_no" varchar(255) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "buyer_id" int4,
  "buyer_name" varchar(255) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "payment_time" timestamp(6),
  "consign_time" timestamp(6),
  "finish_time" timestamp(6),
  "close_time" timestamp(6),
  "buyer_message" varchar(255) COLLATE "pg_catalog"."default",
  "buyer_rate" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "state" int4,
  "goods_amount" numeric(10,2),
  "post_fee" numeric(10,2),
  "payment_amount" numeric(10,2),
  "payment_type" int4,
  "online_payment_type" int4,
  "close_type" int4,
  "close_uesr_id" int4
)
;
COMMENT ON COLUMN "public"."jb_order"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_order"."order_no" IS '订单编号';
COMMENT ON COLUMN "public"."jb_order"."create_time" IS '下单时间';
COMMENT ON COLUMN "public"."jb_order"."buyer_id" IS '买家ID';
COMMENT ON COLUMN "public"."jb_order"."buyer_name" IS '冗余买家名字';
COMMENT ON COLUMN "public"."jb_order"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_order"."payment_time" IS '付款时间';
COMMENT ON COLUMN "public"."jb_order"."consign_time" IS '发货时间';
COMMENT ON COLUMN "public"."jb_order"."finish_time" IS '交易完成时间';
COMMENT ON COLUMN "public"."jb_order"."close_time" IS '订单关闭时间';
COMMENT ON COLUMN "public"."jb_order"."buyer_message" IS '卖家留言';
COMMENT ON COLUMN "public"."jb_order"."buyer_rate" IS '买家是否已经评价';
COMMENT ON COLUMN "public"."jb_order"."state" IS '订单状态 待付款 已付款 已发货 订单完成 订单关闭';
COMMENT ON COLUMN "public"."jb_order"."goods_amount" IS '总额';
COMMENT ON COLUMN "public"."jb_order"."post_fee" IS '运费';
COMMENT ON COLUMN "public"."jb_order"."payment_amount" IS '应付总额';
COMMENT ON COLUMN "public"."jb_order"."payment_type" IS '支付类型 在线支付 货到付款';
COMMENT ON COLUMN "public"."jb_order"."online_payment_type" IS '在线支付选择了谁';
COMMENT ON COLUMN "public"."jb_order"."close_type" IS '通过什么方式关闭 后台管理 or 客户自身';
COMMENT ON COLUMN "public"."jb_order"."close_uesr_id" IS '关闭订单用户ID';
COMMENT ON TABLE "public"."jb_order" IS '商城订单';

-- ----------------------------
-- Table structure for jb_order_item
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_order_item";
CREATE TABLE "public"."jb_order_item" (
  "id" int4 NOT NULL,
  "order_id" int4 NOT NULL,
  "goods_id" int4 NOT NULL,
  "goods_name" varchar(255) COLLATE "pg_catalog"."default",
  "goods_sub_title" varchar(255) COLLATE "pg_catalog"."default",
  "price" numeric(10,2),
  "goods_count" int4,
  "goods_amount" numeric(10,2),
  "goods_image" varchar(255) COLLATE "pg_catalog"."default",
  "order_no" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "original_price" numeric(10,2),
  "save_price" numeric(10,2),
  "save_amount" numeric(10,2)
)
;
COMMENT ON COLUMN "public"."jb_order_item"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_order_item"."order_id" IS '订单ID';
COMMENT ON COLUMN "public"."jb_order_item"."goods_id" IS '商品ID';
COMMENT ON COLUMN "public"."jb_order_item"."goods_name" IS '商品名称';
COMMENT ON COLUMN "public"."jb_order_item"."goods_sub_title" IS '二级促销标题';
COMMENT ON COLUMN "public"."jb_order_item"."price" IS '单价';
COMMENT ON COLUMN "public"."jb_order_item"."goods_count" IS '商品数量';
COMMENT ON COLUMN "public"."jb_order_item"."goods_amount" IS '总额';
COMMENT ON COLUMN "public"."jb_order_item"."goods_image" IS '商品图';
COMMENT ON COLUMN "public"."jb_order_item"."order_no" IS '订单编号';
COMMENT ON COLUMN "public"."jb_order_item"."original_price" IS '原价';
COMMENT ON COLUMN "public"."jb_order_item"."save_price" IS '单个节省价格';
COMMENT ON COLUMN "public"."jb_order_item"."save_amount" IS '一共节省多少钱';
COMMENT ON TABLE "public"."jb_order_item" IS '订单详情表';

-- ----------------------------
-- Table structure for jb_order_shipping
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_order_shipping";
CREATE TABLE "public"."jb_order_shipping" (
  "id" int4 NOT NULL,
  "order_id" int4 NOT NULL,
  "name" varchar(40) COLLATE "pg_catalog"."default",
  "phone" varchar(40) COLLATE "pg_catalog"."default",
  "mobile" varchar(40) COLLATE "pg_catalog"."default",
  "buyer_nickname" varchar(40) COLLATE "pg_catalog"."default",
  "province" varchar(40) COLLATE "pg_catalog"."default",
  "city" varchar(40) COLLATE "pg_catalog"."default",
  "county" varchar(40) COLLATE "pg_catalog"."default",
  "address" varchar(100) COLLATE "pg_catalog"."default",
  "zipcode" varchar(40) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "update_time" timestamp(6),
  "update_user_id" int4,
  "order_no" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "buyer_id" int4 NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_order_shipping"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_order_shipping"."order_id" IS '订单ID';
COMMENT ON COLUMN "public"."jb_order_shipping"."name" IS '收件人姓名';
COMMENT ON COLUMN "public"."jb_order_shipping"."phone" IS '收件人固话';
COMMENT ON COLUMN "public"."jb_order_shipping"."mobile" IS '移动电话';
COMMENT ON COLUMN "public"."jb_order_shipping"."buyer_nickname" IS '买方昵称';
COMMENT ON COLUMN "public"."jb_order_shipping"."province" IS '省份';
COMMENT ON COLUMN "public"."jb_order_shipping"."city" IS '城市';
COMMENT ON COLUMN "public"."jb_order_shipping"."county" IS '区县';
COMMENT ON COLUMN "public"."jb_order_shipping"."address" IS '收件具体地址';
COMMENT ON COLUMN "public"."jb_order_shipping"."zipcode" IS '邮政编码';
COMMENT ON COLUMN "public"."jb_order_shipping"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_order_shipping"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_order_shipping"."update_user_id" IS '更新人ID';
COMMENT ON COLUMN "public"."jb_order_shipping"."order_no" IS '订单编号';
COMMENT ON COLUMN "public"."jb_order_shipping"."buyer_id" IS '买方用户ID';
COMMENT ON TABLE "public"."jb_order_shipping" IS '订单物流信息';

-- ----------------------------
-- Table structure for jb_permission
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_permission";
CREATE TABLE "public"."jb_permission" (
  "id" int4 NOT NULL,
  "title" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "pid" int4,
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "icons" varchar(40) COLLATE "pg_catalog"."default",
  "sort_rank" int4,
  "permission_level" int4,
  "permission_key" varchar(255) COLLATE "pg_catalog"."default",
  "is_menu" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "is_target_blank" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "is_system_admin_default" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "open_type" int4,
  "open_option" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_permission"."id" IS '主键';
COMMENT ON COLUMN "public"."jb_permission"."url" IS '地址';
COMMENT ON COLUMN "public"."jb_permission"."icons" IS '图标';
COMMENT ON COLUMN "public"."jb_permission"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_permission"."permission_level" IS '层级';
COMMENT ON COLUMN "public"."jb_permission"."permission_key" IS '权限资源KEY';
COMMENT ON COLUMN "public"."jb_permission"."is_menu" IS '是否是菜单';
COMMENT ON COLUMN "public"."jb_permission"."is_target_blank" IS '是否新窗口打开';
COMMENT ON COLUMN "public"."jb_permission"."is_system_admin_default" IS '是否系统超级管理员默认拥有的权限';
COMMENT ON COLUMN "public"."jb_permission"."open_type" IS '打开类型 1 默认 2 iframe 3 dialog';
COMMENT ON COLUMN "public"."jb_permission"."open_option" IS '组件属性json';
COMMENT ON TABLE "public"."jb_permission" IS 'function定义';

-- ----------------------------
-- Table structure for jb_post
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_post";
CREATE TABLE "public"."jb_post" (
  "id" int4 NOT NULL,
  "name" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "type" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "sn" varchar(40) COLLATE "pg_catalog"."default",
  "sort_rank" int4,
  "remark" varchar(255) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '1'::bpchar NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_post"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_post"."name" IS '部门名称';
COMMENT ON COLUMN "public"."jb_post"."type" IS '部门类型';
COMMENT ON COLUMN "public"."jb_post"."sn" IS '编码';
COMMENT ON COLUMN "public"."jb_post"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_post"."remark" IS '备注';
COMMENT ON COLUMN "public"."jb_post"."enable" IS '启用';
COMMENT ON TABLE "public"."jb_post" IS '岗位管理';

-- ----------------------------
-- Table structure for jb_private_message
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_private_message";
CREATE TABLE "public"."jb_private_message" (
  "id" int4 NOT NULL,
  "content" text COLLATE "pg_catalog"."default" NOT NULL,
  "create_time" timestamp(6) NOT NULL,
  "from_user_id" int4 NOT NULL,
  "to_user_id" int4 NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_private_message"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_private_message"."content" IS '私信内容';
COMMENT ON COLUMN "public"."jb_private_message"."create_time" IS '发送时间';
COMMENT ON COLUMN "public"."jb_private_message"."from_user_id" IS '发信人';
COMMENT ON COLUMN "public"."jb_private_message"."to_user_id" IS '收信人';
COMMENT ON TABLE "public"."jb_private_message" IS '内部私信';

-- ----------------------------
-- Table structure for jb_remote_login_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_remote_login_log";
CREATE TABLE "public"."jb_remote_login_log" (
  "id" int4 NOT NULL,
  "last_login_country" varchar(40) COLLATE "pg_catalog"."default",
  "last_login_province" varchar(40) COLLATE "pg_catalog"."default",
  "last_login_city" varchar(40) COLLATE "pg_catalog"."default",
  "last_login_time" timestamp(6),
  "login_country" varchar(40) COLLATE "pg_catalog"."default",
  "login_province" varchar(40) COLLATE "pg_catalog"."default",
  "login_city" varchar(40) COLLATE "pg_catalog"."default",
  "login_time" timestamp(6),
  "user_id" int4,
  "username" varchar(40) COLLATE "pg_catalog"."default",
  "is_new" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "last_login_ip" varchar(40) COLLATE "pg_catalog"."default",
  "login_ip" varchar(40) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."jb_remote_login_log"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_remote_login_log"."last_login_country" IS '最近一次登录国家';
COMMENT ON COLUMN "public"."jb_remote_login_log"."last_login_province" IS '最近一次登录省份';
COMMENT ON COLUMN "public"."jb_remote_login_log"."last_login_city" IS '最近一次登录城市';
COMMENT ON COLUMN "public"."jb_remote_login_log"."last_login_time" IS '最近一次登录时间';
COMMENT ON COLUMN "public"."jb_remote_login_log"."login_country" IS '新登录国家';
COMMENT ON COLUMN "public"."jb_remote_login_log"."login_province" IS '新登录省份';
COMMENT ON COLUMN "public"."jb_remote_login_log"."login_city" IS '新登录城市';
COMMENT ON COLUMN "public"."jb_remote_login_log"."login_time" IS '新登录时间';
COMMENT ON COLUMN "public"."jb_remote_login_log"."user_id" IS '登录用户ID';
COMMENT ON COLUMN "public"."jb_remote_login_log"."username" IS '登录用户名';
COMMENT ON COLUMN "public"."jb_remote_login_log"."is_new" IS '是否为最新一次';
COMMENT ON COLUMN "public"."jb_remote_login_log"."last_login_ip" IS '最近一次登录IP';
COMMENT ON COLUMN "public"."jb_remote_login_log"."login_ip" IS '新登录IP';
COMMENT ON COLUMN "public"."jb_remote_login_log"."create_time" IS '记录创建时间';
COMMENT ON TABLE "public"."jb_remote_login_log" IS '异地登录日志记录';

-- ----------------------------
-- Table structure for jb_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_role";
CREATE TABLE "public"."jb_role" (
  "id" int4 NOT NULL,
  "name" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "sn" varchar(40) COLLATE "pg_catalog"."default",
  "pid" int4
)
;
COMMENT ON COLUMN "public"."jb_role"."id" IS '主键';
COMMENT ON COLUMN "public"."jb_role"."name" IS '名称';
COMMENT ON COLUMN "public"."jb_role"."sn" IS '编码';
COMMENT ON COLUMN "public"."jb_role"."pid" IS '父级角色ID';
COMMENT ON TABLE "public"."jb_role" IS '角色表';

-- ----------------------------
-- Table structure for jb_role_permission
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_role_permission";
CREATE TABLE "public"."jb_role_permission" (
  "id" int4 NOT NULL,
  "role_id" int4 NOT NULL,
  "permission_id" int4 NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_role_permission"."id" IS '主键';
COMMENT ON COLUMN "public"."jb_role_permission"."role_id" IS '角色ID';
COMMENT ON COLUMN "public"."jb_role_permission"."permission_id" IS '权限ID';
COMMENT ON TABLE "public"."jb_role_permission" IS '角色功能列表';

-- ----------------------------
-- Table structure for jb_shelf
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_shelf";
CREATE TABLE "public"."jb_shelf" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "create_time" timestamp(6),
  "create_user_id" int4,
  "publish_user_id" int4,
  "publish_time" timestamp(6),
  "share_image" varchar(255) COLLATE "pg_catalog"."default",
  "share_title" varchar(255) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "update_user_id" int4
)
;
COMMENT ON COLUMN "public"."jb_shelf"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_shelf"."name" IS '名称';
COMMENT ON COLUMN "public"."jb_shelf"."enable" IS '是否启用';
COMMENT ON COLUMN "public"."jb_shelf"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_shelf"."create_user_id" IS '创建人ID';
COMMENT ON COLUMN "public"."jb_shelf"."publish_user_id" IS '上线发布人';
COMMENT ON COLUMN "public"."jb_shelf"."publish_time" IS '发布时间';
COMMENT ON COLUMN "public"."jb_shelf"."share_image" IS '分享图';
COMMENT ON COLUMN "public"."jb_shelf"."share_title" IS '分享标题';
COMMENT ON COLUMN "public"."jb_shelf"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_shelf"."update_user_id" IS '更新人';
COMMENT ON TABLE "public"."jb_shelf" IS '电商货架-小程序首页风格';

-- ----------------------------
-- Table structure for jb_shelf_activity
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_shelf_activity";
CREATE TABLE "public"."jb_shelf_activity" (
  "id" int4 NOT NULL,
  "poster_image" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "open_url" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "open_type" int4 NOT NULL,
  "shelf_element_id" int4 NOT NULL,
  "shelf_id" int4 NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_shelf_activity"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_shelf_activity"."poster_image" IS '海报地址';
COMMENT ON COLUMN "public"."jb_shelf_activity"."open_url" IS '打开地址';
COMMENT ON COLUMN "public"."jb_shelf_activity"."open_type" IS '打开方式 网页还是商品内页 还是分类 还是';
COMMENT ON COLUMN "public"."jb_shelf_activity"."shelf_element_id" IS '货架元素ID';
COMMENT ON COLUMN "public"."jb_shelf_activity"."shelf_id" IS '货架ID';
COMMENT ON TABLE "public"."jb_shelf_activity" IS '货架元素_活动';

-- ----------------------------
-- Table structure for jb_shelf_carousel
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_shelf_carousel";
CREATE TABLE "public"."jb_shelf_carousel" (
  "id" int4 NOT NULL,
  "imgurl" varchar(255) COLLATE "pg_catalog"."default",
  "open_type" int4,
  "open_url" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "shelf_id" int4 NOT NULL,
  "shelf_element_id" int4 NOT NULL,
  "sort_rank" int4
)
;
COMMENT ON COLUMN "public"."jb_shelf_carousel"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_shelf_carousel"."imgurl" IS '图片地址';
COMMENT ON COLUMN "public"."jb_shelf_carousel"."open_type" IS '打开模式 内置资源地址，网页地址';
COMMENT ON COLUMN "public"."jb_shelf_carousel"."open_url" IS '打开地址';
COMMENT ON COLUMN "public"."jb_shelf_carousel"."shelf_id" IS '货架ID';
COMMENT ON COLUMN "public"."jb_shelf_carousel"."shelf_element_id" IS '货架元素ID';
COMMENT ON COLUMN "public"."jb_shelf_carousel"."sort_rank" IS '排序';
COMMENT ON TABLE "public"."jb_shelf_carousel" IS '货架元素_轮播图';

-- ----------------------------
-- Table structure for jb_shelf_element
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_shelf_element";
CREATE TABLE "public"."jb_shelf_element" (
  "id" int4 NOT NULL,
  "type" int4,
  "width" varchar(255) COLLATE "pg_catalog"."default",
  "height" varchar(255) COLLATE "pg_catalog"."default",
  "sort_rank" int4
)
;
COMMENT ON COLUMN "public"."jb_shelf_element"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_shelf_element"."type" IS '类型 轮播 楼层 分类 活动等';
COMMENT ON COLUMN "public"."jb_shelf_element"."width" IS '宽度';
COMMENT ON COLUMN "public"."jb_shelf_element"."height" IS '高度';
COMMENT ON COLUMN "public"."jb_shelf_element"."sort_rank" IS '排序';
COMMENT ON TABLE "public"."jb_shelf_element" IS '货架从上到下的元素';

-- ----------------------------
-- Table structure for jb_shelf_goods_floor
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_shelf_goods_floor";
CREATE TABLE "public"."jb_shelf_goods_floor" (
  "id" int4 NOT NULL,
  "group_id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "open_url" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "goods_count" int4 NOT NULL,
  "column_count" int4 NOT NULL,
  "sort_rank" int4,
  "shelf_id" int4 NOT NULL,
  "shelf_element_id" int4 NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."group_id" IS '商品分组ID';
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."title" IS '显示标题 默认是商品组的 可以改';
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."open_url" IS '打开地址';
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."goods_count" IS '显示商品数量';
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."column_count" IS '显示几列布局';
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."shelf_id" IS '货架ID';
COMMENT ON COLUMN "public"."jb_shelf_goods_floor"."shelf_element_id" IS '货架元素ID';
COMMENT ON TABLE "public"."jb_shelf_goods_floor" IS '货架元素_商品楼层';

-- ----------------------------
-- Table structure for jb_shelf_goods_group
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_shelf_goods_group";
CREATE TABLE "public"."jb_shelf_goods_group" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "icon" varchar(255) COLLATE "pg_catalog"."default",
  "imgurl" varchar(255) COLLATE "pg_catalog"."default",
  "sub_title" varchar(255) COLLATE "pg_catalog"."default",
  "sort_rank" int4,
  "shelf_id" int4 NOT NULL,
  "shelf_element_id" int4 NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_shelf_goods_group"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_shelf_goods_group"."icon" IS '图标';
COMMENT ON COLUMN "public"."jb_shelf_goods_group"."imgurl" IS '分类下的顶部修饰图';
COMMENT ON COLUMN "public"."jb_shelf_goods_group"."sub_title" IS '二级标题';
COMMENT ON COLUMN "public"."jb_shelf_goods_group"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_shelf_goods_group"."shelf_id" IS '货架ID';
COMMENT ON COLUMN "public"."jb_shelf_goods_group"."shelf_element_id" IS '货架元素ID';
COMMENT ON TABLE "public"."jb_shelf_goods_group" IS '货架元素_商品组';

-- ----------------------------
-- Table structure for jb_sku
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_sku";
CREATE TABLE "public"."jb_sku" (
  "id" int4 NOT NULL,
  "sku_key" varchar(255) COLLATE "pg_catalog"."default",
  "sku_name" varchar(255) COLLATE "pg_catalog"."default",
  "goods_id" int4,
  "goods_name" varchar(255) COLLATE "pg_catalog"."default",
  "type_id" int4,
  "price" numeric(10,2),
  "original_price" numeric(10,2),
  "stock_count" numeric(10,2),
  "limit_count" numeric(10,2),
  "sub_title" varchar(255) COLLATE "pg_catalog"."default",
  "on_sale" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "sellout" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "real_sale_count" int4,
  "show_sale_count" int4,
  "main_image" varchar(255) COLLATE "pg_catalog"."default",
  "extra_images" text COLLATE "pg_catalog"."default",
  "is_hot" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "is_recommend" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "need_content" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "content_type" int4,
  "under_time" timestamp(6),
  "onSale_time" timestamp(6),
  "create_user_id" int4,
  "create_time" timestamp(6),
  "update_time" timestamp(6),
  "update_user_id" int4
)
;
COMMENT ON COLUMN "public"."jb_sku"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_sku"."sku_key" IS '规格编码';
COMMENT ON COLUMN "public"."jb_sku"."sku_name" IS '规格名称';
COMMENT ON COLUMN "public"."jb_sku"."goods_id" IS '商品ID';
COMMENT ON COLUMN "public"."jb_sku"."goods_name" IS '商品名称';
COMMENT ON COLUMN "public"."jb_sku"."type_id" IS '类型ID';
COMMENT ON COLUMN "public"."jb_sku"."price" IS '单价';
COMMENT ON COLUMN "public"."jb_sku"."original_price" IS '原价';
COMMENT ON COLUMN "public"."jb_sku"."stock_count" IS '库存量';
COMMENT ON COLUMN "public"."jb_sku"."sub_title" IS '营销语';
COMMENT ON COLUMN "public"."jb_sku"."on_sale" IS '是否上架';
COMMENT ON COLUMN "public"."jb_sku"."sellout" IS '售罄';
COMMENT ON COLUMN "public"."jb_sku"."real_sale_count" IS '真实销量';
COMMENT ON COLUMN "public"."jb_sku"."show_sale_count" IS '展示营销销量';
COMMENT ON COLUMN "public"."jb_sku"."main_image" IS '主图';
COMMENT ON COLUMN "public"."jb_sku"."extra_images" IS '附图';
COMMENT ON COLUMN "public"."jb_sku"."is_hot" IS '热销';
COMMENT ON COLUMN "public"."jb_sku"."is_recommend" IS '推荐';
COMMENT ON COLUMN "public"."jb_sku"."need_content" IS '是否需要详情描述';
COMMENT ON COLUMN "public"."jb_sku"."content_type" IS '描述类型 是富文本还是分开的图片 文本段数据';
COMMENT ON COLUMN "public"."jb_sku"."under_time" IS '下架时间';
COMMENT ON COLUMN "public"."jb_sku"."onSale_time" IS '上架时间';
COMMENT ON COLUMN "public"."jb_sku"."create_user_id" IS '创建人';
COMMENT ON COLUMN "public"."jb_sku"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_sku"."update_time" IS '最后更新时间';
COMMENT ON COLUMN "public"."jb_sku"."update_user_id" IS '最后更新人';
COMMENT ON TABLE "public"."jb_sku" IS 'SKU';

-- ----------------------------
-- Table structure for jb_sku_item
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_sku_item";
CREATE TABLE "public"."jb_sku_item" (
  "id" int4 NOT NULL,
  "sku_id" int4 NOT NULL,
  "goods_id" int4 NOT NULL,
  "type_id" int4 NOT NULL,
  "spec_id" int4 NOT NULL,
  "spec_item_id" int4 NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_sku_item"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_sku_item"."sku_id" IS 'SKU ID';
COMMENT ON COLUMN "public"."jb_sku_item"."goods_id" IS '商品 ID';
COMMENT ON COLUMN "public"."jb_sku_item"."type_id" IS '商品分类';
COMMENT ON COLUMN "public"."jb_sku_item"."spec_id" IS '规格 ID';
COMMENT ON COLUMN "public"."jb_sku_item"."spec_item_id" IS '规格项 ID';
COMMENT ON TABLE "public"."jb_sku_item" IS 'sku详情';

-- ----------------------------
-- Table structure for jb_spec
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_spec";
CREATE TABLE "public"."jb_spec" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON TABLE "public"."jb_spec" IS '系统规格表';

-- ----------------------------
-- Table structure for jb_spec_item
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_spec_item";
CREATE TABLE "public"."jb_spec_item" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "spec_id" int4
)
;
COMMENT ON COLUMN "public"."jb_spec_item"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_spec_item"."name" IS '选项名';
COMMENT ON COLUMN "public"."jb_spec_item"."spec_id" IS '所属规格';
COMMENT ON TABLE "public"."jb_spec_item" IS '规格选项表';

-- ----------------------------
-- Table structure for jb_sys_notice
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_sys_notice";
CREATE TABLE "public"."jb_sys_notice" (
  "id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "content" text COLLATE "pg_catalog"."default" NOT NULL,
  "type" int4 NOT NULL,
  "priority_level" int4 NOT NULL,
  "read_count" int4,
  "create_time" timestamp(6) NOT NULL,
  "update_time" timestamp(6),
  "create_user_id" int4,
  "update_user_id" int4,
  "receiver_type" int4,
  "receiver_value" text COLLATE "pg_catalog"."default",
  "files" varchar(255) COLLATE "pg_catalog"."default",
  "del_flag" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_sys_notice"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_sys_notice"."title" IS '标题';
COMMENT ON COLUMN "public"."jb_sys_notice"."content" IS '消息内容';
COMMENT ON COLUMN "public"."jb_sys_notice"."type" IS '通知类型';
COMMENT ON COLUMN "public"."jb_sys_notice"."priority_level" IS '优先级';
COMMENT ON COLUMN "public"."jb_sys_notice"."read_count" IS '已读人数';
COMMENT ON COLUMN "public"."jb_sys_notice"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_sys_notice"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_sys_notice"."create_user_id" IS '创建人';
COMMENT ON COLUMN "public"."jb_sys_notice"."update_user_id" IS '更新人';
COMMENT ON COLUMN "public"."jb_sys_notice"."receiver_type" IS '接收人类型';
COMMENT ON COLUMN "public"."jb_sys_notice"."receiver_value" IS '接收人值';
COMMENT ON COLUMN "public"."jb_sys_notice"."files" IS '附件';
COMMENT ON COLUMN "public"."jb_sys_notice"."del_flag" IS '删除标志';
COMMENT ON TABLE "public"."jb_sys_notice" IS '系统通知';

-- ----------------------------
-- Table structure for jb_sys_notice_reader
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_sys_notice_reader";
CREATE TABLE "public"."jb_sys_notice_reader" (
  "id" int4 NOT NULL,
  "sys_notice_id" int4 NOT NULL,
  "user_id" int4 NOT NULL,
  "create_time" timestamp(6) NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_sys_notice_reader"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_sys_notice_reader"."sys_notice_id" IS '通知ID';
COMMENT ON COLUMN "public"."jb_sys_notice_reader"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."jb_sys_notice_reader"."create_time" IS '创建时间';
COMMENT ON TABLE "public"."jb_sys_notice_reader" IS '通知阅读用户关系表';

-- ----------------------------
-- Table structure for jb_system_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_system_log";
CREATE TABLE "public"."jb_system_log" (
  "id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "type" int4,
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "user_id" int4,
  "user_name" varchar(255) COLLATE "pg_catalog"."default",
  "target_type" int4,
  "create_time" timestamp(6),
  "target_id" int4,
  "open_type" int4
)
;
COMMENT ON COLUMN "public"."jb_system_log"."id" IS '主键';
COMMENT ON COLUMN "public"."jb_system_log"."title" IS '标题';
COMMENT ON COLUMN "public"."jb_system_log"."type" IS '操作类型 删除 更新 新增';
COMMENT ON COLUMN "public"."jb_system_log"."url" IS '连接对象详情地址';
COMMENT ON COLUMN "public"."jb_system_log"."user_id" IS '操作人ID';
COMMENT ON COLUMN "public"."jb_system_log"."user_name" IS '操作人姓名';
COMMENT ON COLUMN "public"."jb_system_log"."target_type" IS '操作对象类型';
COMMENT ON COLUMN "public"."jb_system_log"."create_time" IS '记录创建时间';
COMMENT ON COLUMN "public"."jb_system_log"."target_id" IS '操作对象ID';
COMMENT ON COLUMN "public"."jb_system_log"."open_type" IS '打开类型URL还是Dialog';
COMMENT ON TABLE "public"."jb_system_log" IS '系统操作日志表';

-- ----------------------------
-- Table structure for jb_todo
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_todo";
CREATE TABLE "public"."jb_todo" (
  "id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "content" text COLLATE "pg_catalog"."default",
  "user_id" int4 NOT NULL,
  "state" int4 NOT NULL,
  "specified_finish_time" timestamp(6) NOT NULL,
  "type" int4 NOT NULL,
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "priority_level" int4 NOT NULL,
  "create_time" timestamp(6) NOT NULL,
  "update_time" timestamp(6) NOT NULL,
  "create_user_id" int4 NOT NULL,
  "update_user_id" int4,
  "real_finish_time" timestamp(6)
)
;
COMMENT ON COLUMN "public"."jb_todo"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_todo"."title" IS '标题';
COMMENT ON COLUMN "public"."jb_todo"."content" IS '详情内容';
COMMENT ON COLUMN "public"."jb_todo"."user_id" IS '所属用户';
COMMENT ON COLUMN "public"."jb_todo"."state" IS '状态';
COMMENT ON COLUMN "public"."jb_todo"."specified_finish_time" IS '规定完成时间';
COMMENT ON COLUMN "public"."jb_todo"."type" IS '类型 链接还是内容 还是都有';
COMMENT ON COLUMN "public"."jb_todo"."url" IS '链接';
COMMENT ON COLUMN "public"."jb_todo"."priority_level" IS '优先级';
COMMENT ON COLUMN "public"."jb_todo"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_todo"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_todo"."create_user_id" IS '创建人ID';
COMMENT ON COLUMN "public"."jb_todo"."update_user_id" IS '更新人ID';
COMMENT ON COLUMN "public"."jb_todo"."real_finish_time" IS '完成时间';
COMMENT ON TABLE "public"."jb_todo" IS '待办事项';

-- ----------------------------
-- Table structure for jb_topnav
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_topnav";
CREATE TABLE "public"."jb_topnav" (
  "id" int4 NOT NULL,
  "name" varchar(40) COLLATE "pg_catalog"."default",
  "icon" varchar(40) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "sort_rank" int4
)
;
COMMENT ON COLUMN "public"."jb_topnav"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_topnav"."name" IS '名称';
COMMENT ON COLUMN "public"."jb_topnav"."icon" IS '图标';
COMMENT ON COLUMN "public"."jb_topnav"."enable" IS '是否启用';
COMMENT ON COLUMN "public"."jb_topnav"."sort_rank" IS '排序';
COMMENT ON TABLE "public"."jb_topnav" IS '顶部导航';

-- ----------------------------
-- Table structure for jb_topnav_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_topnav_menu";
CREATE TABLE "public"."jb_topnav_menu" (
  "id" int4 NOT NULL,
  "topnav_id" int4,
  "permission_id" int4
)
;
COMMENT ON COLUMN "public"."jb_topnav_menu"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_topnav_menu"."topnav_id" IS '顶部导航ID';
COMMENT ON COLUMN "public"."jb_topnav_menu"."permission_id" IS '菜单资源ID';
COMMENT ON TABLE "public"."jb_topnav_menu" IS '顶部菜单对应左侧一级导航中间表';

-- ----------------------------
-- Table structure for jb_update_libs
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_update_libs";
CREATE TABLE "public"."jb_update_libs" (
  "id" int4 NOT NULL,
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "target" varchar(255) COLLATE "pg_catalog"."default",
  "delete_all" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "is_must" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_update_libs"."delete_all" IS '清空文件夹';
COMMENT ON COLUMN "public"."jb_update_libs"."is_must" IS '强制';
COMMENT ON TABLE "public"."jb_update_libs" IS '下载的libs';

-- ----------------------------
-- Table structure for jb_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_user";
CREATE TABLE "public"."jb_user" (
  "id" int4 NOT NULL,
  "username" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "avatar" varchar(255) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "phone" varchar(40) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "sex" int4,
  "pinyin" varchar(255) COLLATE "pg_catalog"."default",
  "roles" varchar(255) COLLATE "pg_catalog"."default",
  "is_system_admin" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "create_user_id" int4,
  "pwd_salt" varchar(128) COLLATE "pg_catalog"."default",
  "login_ip" varchar(255) COLLATE "pg_catalog"."default",
  "login_time" timestamp(6),
  "login_city" varchar(40) COLLATE "pg_catalog"."default",
  "login_province" varchar(40) COLLATE "pg_catalog"."default",
  "login_country" varchar(40) COLLATE "pg_catalog"."default",
  "is_remote_login" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "dept_id" int4,
  "posts" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_user"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_user"."username" IS '用户名';
COMMENT ON COLUMN "public"."jb_user"."password" IS '密码';
COMMENT ON COLUMN "public"."jb_user"."name" IS '姓名';
COMMENT ON COLUMN "public"."jb_user"."avatar" IS '头像';
COMMENT ON COLUMN "public"."jb_user"."create_time" IS '记录创建时间';
COMMENT ON COLUMN "public"."jb_user"."phone" IS '手机号';
COMMENT ON COLUMN "public"."jb_user"."enable" IS '启用';
COMMENT ON COLUMN "public"."jb_user"."sex" IS '性别';
COMMENT ON COLUMN "public"."jb_user"."pinyin" IS '拼音码';
COMMENT ON COLUMN "public"."jb_user"."roles" IS '角色 一对多';
COMMENT ON COLUMN "public"."jb_user"."is_system_admin" IS '是否系统超级管理员';
COMMENT ON COLUMN "public"."jb_user"."create_user_id" IS '创建人';
COMMENT ON COLUMN "public"."jb_user"."pwd_salt" IS '密码盐值';
COMMENT ON COLUMN "public"."jb_user"."login_ip" IS '最后登录IP';
COMMENT ON COLUMN "public"."jb_user"."login_time" IS '最后登录时间';
COMMENT ON COLUMN "public"."jb_user"."login_city" IS '最后登录城市';
COMMENT ON COLUMN "public"."jb_user"."login_province" IS '最后登录省份';
COMMENT ON COLUMN "public"."jb_user"."login_country" IS '最后登录国家';
COMMENT ON COLUMN "public"."jb_user"."is_remote_login" IS '是否异地登录';
COMMENT ON COLUMN "public"."jb_user"."dept_id" IS '部门ID';
COMMENT ON COLUMN "public"."jb_user"."posts" IS '岗位IDS';
COMMENT ON TABLE "public"."jb_user" IS '用户登录账户表';

-- ----------------------------
-- Table structure for jb_user_config
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_user_config";
CREATE TABLE "public"."jb_user_config" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "config_key" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "config_value" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "user_id" int4 NOT NULL,
  "create_time" timestamp(6),
  "update_time" timestamp(6),
  "value_type" varchar(40) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_user_config"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_user_config"."name" IS '配置名';
COMMENT ON COLUMN "public"."jb_user_config"."config_key" IS '配置KEY';
COMMENT ON COLUMN "public"."jb_user_config"."config_value" IS '配置值';
COMMENT ON COLUMN "public"."jb_user_config"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."jb_user_config"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_user_config"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_user_config"."value_type" IS '取值类型';
COMMENT ON TABLE "public"."jb_user_config" IS '用户系统样式自定义设置表';

-- ----------------------------
-- Table structure for jb_wechat_article
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_article";
CREATE TABLE "public"."jb_wechat_article" (
  "id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "content" varchar(255) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "user_id" int4,
  "update_time" timestamp(6),
  "update_user_id" int4,
  "brief_info" varchar(120) COLLATE "pg_catalog"."default",
  "poster" varchar(255) COLLATE "pg_catalog"."default",
  "view_count" int4,
  "media_id" varchar(255) COLLATE "pg_catalog"."default",
  "mp_id" int4,
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "type" int4
)
;
COMMENT ON COLUMN "public"."jb_wechat_article"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_wechat_article"."title" IS '标题';
COMMENT ON COLUMN "public"."jb_wechat_article"."content" IS '内容';
COMMENT ON COLUMN "public"."jb_wechat_article"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_wechat_article"."user_id" IS '用户 ID';
COMMENT ON COLUMN "public"."jb_wechat_article"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_wechat_article"."update_user_id" IS '更新用户 ID';
COMMENT ON COLUMN "public"."jb_wechat_article"."brief_info" IS '文章摘要';
COMMENT ON COLUMN "public"."jb_wechat_article"."poster" IS '海报';
COMMENT ON COLUMN "public"."jb_wechat_article"."view_count" IS '阅读数';
COMMENT ON COLUMN "public"."jb_wechat_article"."media_id" IS '微信素材 ID';
COMMENT ON COLUMN "public"."jb_wechat_article"."mp_id" IS '微信 ID';
COMMENT ON COLUMN "public"."jb_wechat_article"."url" IS '图文链接地址';
COMMENT ON COLUMN "public"."jb_wechat_article"."type" IS '本地图文 公众号图文素材 外部图文';
COMMENT ON TABLE "public"."jb_wechat_article" IS '微信图文信息';

-- ----------------------------
-- Table structure for jb_wechat_autoreply
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_autoreply";
CREATE TABLE "public"."jb_wechat_autoreply" (
  "id" int4 NOT NULL,
  "mp_id" int4,
  "type" int4,
  "name" varchar(40) COLLATE "pg_catalog"."default",
  "reply_type" int4,
  "create_time" timestamp(6),
  "user_id" int4,
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_wechat_autoreply"."mp_id" IS '微信 ID';
COMMENT ON COLUMN "public"."jb_wechat_autoreply"."type" IS '类型 关注回复 无内容时回复 关键词回复';
COMMENT ON COLUMN "public"."jb_wechat_autoreply"."name" IS '规则名称';
COMMENT ON COLUMN "public"."jb_wechat_autoreply"."reply_type" IS '回复类型 全部还是随机一条';
COMMENT ON COLUMN "public"."jb_wechat_autoreply"."create_time" IS '记录创建时间';
COMMENT ON COLUMN "public"."jb_wechat_autoreply"."user_id" IS '用户 ID';
COMMENT ON TABLE "public"."jb_wechat_autoreply" IS '微信公众号自动回复规则';

-- ----------------------------
-- Table structure for jb_wechat_config
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_config";
CREATE TABLE "public"."jb_wechat_config" (
  "id" int4 NOT NULL,
  "mp_id" int4,
  "config_key" varchar(255) COLLATE "pg_catalog"."default",
  "config_value" varchar(255) COLLATE "pg_catalog"."default",
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "type" int4
)
;
COMMENT ON COLUMN "public"."jb_wechat_config"."mp_id" IS '微信 ID';
COMMENT ON COLUMN "public"."jb_wechat_config"."config_key" IS '配置key';
COMMENT ON COLUMN "public"."jb_wechat_config"."config_value" IS '配置值';
COMMENT ON COLUMN "public"."jb_wechat_config"."name" IS '配置项名称';
COMMENT ON COLUMN "public"."jb_wechat_config"."type" IS '配置类型';
COMMENT ON TABLE "public"."jb_wechat_config" IS '微信公众号配置表';

-- ----------------------------
-- Table structure for jb_wechat_keywords
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_keywords";
CREATE TABLE "public"."jb_wechat_keywords" (
  "id" int4 NOT NULL,
  "mp_id" int4,
  "name" varchar(40) COLLATE "pg_catalog"."default",
  "type" int4,
  "auto_reply_id" int4
)
;
COMMENT ON COLUMN "public"."jb_wechat_keywords"."mp_id" IS '微信 ID';
COMMENT ON COLUMN "public"."jb_wechat_keywords"."type" IS '模糊 全匹配';
COMMENT ON COLUMN "public"."jb_wechat_keywords"."auto_reply_id" IS '回复规则ID';
COMMENT ON TABLE "public"."jb_wechat_keywords" IS '微信关键词回复中的关键词定义';

-- ----------------------------
-- Table structure for jb_wechat_media
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_media";
CREATE TABLE "public"."jb_wechat_media" (
  "id" int4 NOT NULL,
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "digest" varchar(255) COLLATE "pg_catalog"."default",
  "type" varchar(255) COLLATE "pg_catalog"."default",
  "mp_id" int4,
  "media_id" varchar(255) COLLATE "pg_catalog"."default",
  "thumb_media_id" varchar(255) COLLATE "pg_catalog"."default",
  "content_source_url" varchar(255) COLLATE "pg_catalog"."default",
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "author" varchar(255) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "server_url" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_wechat_media"."id" IS '主键ID';
COMMENT ON COLUMN "public"."jb_wechat_media"."title" IS '标题';
COMMENT ON COLUMN "public"."jb_wechat_media"."digest" IS '描述';
COMMENT ON COLUMN "public"."jb_wechat_media"."type" IS '类型 image video voice news';
COMMENT ON COLUMN "public"."jb_wechat_media"."mp_id" IS '微信 ID';
COMMENT ON COLUMN "public"."jb_wechat_media"."media_id" IS '微信素材ID';
COMMENT ON COLUMN "public"."jb_wechat_media"."thumb_media_id" IS '封面图ID';
COMMENT ON COLUMN "public"."jb_wechat_media"."content_source_url" IS '原文地址';
COMMENT ON COLUMN "public"."jb_wechat_media"."url" IS '访问地址';
COMMENT ON COLUMN "public"."jb_wechat_media"."author" IS '图文作者';
COMMENT ON COLUMN "public"."jb_wechat_media"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_wechat_media"."server_url" IS '存服务器URL';
COMMENT ON TABLE "public"."jb_wechat_media" IS '微信公众平台素材库同步管理';

-- ----------------------------
-- Table structure for jb_wechat_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_menu";
CREATE TABLE "public"."jb_wechat_menu" (
  "id" int4 NOT NULL,
  "mp_id" int4,
  "type" varchar(40) COLLATE "pg_catalog"."default",
  "name" varchar(40) COLLATE "pg_catalog"."default",
  "pid" int4,
  "sort_rank" int4,
  "value" varchar(255) COLLATE "pg_catalog"."default",
  "app_id" varchar(255) COLLATE "pg_catalog"."default",
  "page_path" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_wechat_menu"."mp_id" IS '微信 ID';
COMMENT ON COLUMN "public"."jb_wechat_menu"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_wechat_menu"."app_id" IS '微信小程序APPID';
COMMENT ON COLUMN "public"."jb_wechat_menu"."page_path" IS '微信小程序页面地址';
COMMENT ON TABLE "public"."jb_wechat_menu" IS '微信菜单';

-- ----------------------------
-- Table structure for jb_wechat_mpinfo
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_mpinfo";
CREATE TABLE "public"."jb_wechat_mpinfo" (
  "id" int4 NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "logo" varchar(255) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6),
  "user_id" int4,
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "update_time" timestamp(6),
  "update_user_id" int4,
  "brief_info" varchar(255) COLLATE "pg_catalog"."default",
  "remark" varchar(255) COLLATE "pg_catalog"."default",
  "type" int4,
  "is_authenticated" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "subject_type" int4,
  "wechat_id" varchar(255) COLLATE "pg_catalog"."default",
  "qrcode" varchar(255) COLLATE "pg_catalog"."default",
  "link_app_id" int4
)
;
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."id" IS '主键';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."name" IS '平台名称';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."logo" IS '头像LOGO';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."user_id" IS '用户ID';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."enable" IS '是否启用';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."update_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."update_user_id" IS '更新人ID';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."brief_info" IS '简介';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."remark" IS '备注';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."type" IS '类型 订阅号、服务号、小程序、企业号';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."is_authenticated" IS '是否已认证';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."subject_type" IS '申请认证主体的类型 个人还是企业';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."wechat_id" IS '微信号';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."qrcode" IS '二维码';
COMMENT ON COLUMN "public"."jb_wechat_mpinfo"."link_app_id" IS '关联应用ID';
COMMENT ON TABLE "public"."jb_wechat_mpinfo" IS '微信公众号与小程序';

-- ----------------------------
-- Table structure for jb_wechat_reply_content
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_reply_content";
CREATE TABLE "public"."jb_wechat_reply_content" (
  "id" int4 NOT NULL,
  "type" varchar(40) COLLATE "pg_catalog"."default",
  "title" varchar(64) COLLATE "pg_catalog"."default",
  "content" text COLLATE "pg_catalog"."default",
  "poster" varchar(255) COLLATE "pg_catalog"."default",
  "url" varchar(255) COLLATE "pg_catalog"."default",
  "auto_reply_id" int4,
  "create_time" timestamp(6),
  "user_id" int4,
  "media_id" varchar(255) COLLATE "pg_catalog"."default",
  "mp_id" int4,
  "sort_rank" int4,
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL
)
;
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."type" IS '类型 图文 文字 图片 音频 视频';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."title" IS '标题';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."content" IS '内容';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."poster" IS '海报';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."url" IS '地址';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."auto_reply_id" IS '回复规则ID';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."user_id" IS '用户 ID';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."media_id" IS '关联数据';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."mp_id" IS '微信 ID';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."sort_rank" IS '排序';
COMMENT ON COLUMN "public"."jb_wechat_reply_content"."enable" IS '是否启用';
COMMENT ON TABLE "public"."jb_wechat_reply_content" IS '自动回复内容';

-- ----------------------------
-- Table structure for jb_wechat_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."jb_wechat_user";
CREATE TABLE "public"."jb_wechat_user" (
  "id" int4 NOT NULL,
  "nickname" varchar(255) COLLATE "pg_catalog"."default",
  "open_id" varchar(255) COLLATE "pg_catalog"."default",
  "union_id" varchar(255) COLLATE "pg_catalog"."default",
  "sex" int4,
  "language" varchar(255) COLLATE "pg_catalog"."default",
  "subscibe" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "country" varchar(255) COLLATE "pg_catalog"."default",
  "province" varchar(255) COLLATE "pg_catalog"."default",
  "city" varchar(255) COLLATE "pg_catalog"."default",
  "head_img_url" varchar(255) COLLATE "pg_catalog"."default",
  "subscribe_time" timestamp(6),
  "remark" varchar(255) COLLATE "pg_catalog"."default",
  "group_id" int4,
  "tag_ids" varchar(255) COLLATE "pg_catalog"."default",
  "subscribe_scene" varchar(255) COLLATE "pg_catalog"."default",
  "qr_scene" int4,
  "qr_scene_str" varchar(255) COLLATE "pg_catalog"."default",
  "realname" varchar(255) COLLATE "pg_catalog"."default",
  "phone" varchar(255) COLLATE "pg_catalog"."default",
  "phone_country_code" varchar(40) COLLATE "pg_catalog"."default",
  "check_code" varchar(255) COLLATE "pg_catalog"."default",
  "is_checked" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "source" int4,
  "session_key" varchar(255) COLLATE "pg_catalog"."default",
  "enable" char(1) COLLATE "pg_catalog"."default" DEFAULT '0'::bpchar NOT NULL,
  "create_time" timestamp(6),
  "checked_time" timestamp(6),
  "mp_id" int4,
  "weixin" varchar(255) COLLATE "pg_catalog"."default",
  "update_time" timestamp(6),
  "last_login_time" timestamp(6),
  "first_auth_time" timestamp(6),
  "last_auth_time" timestamp(6),
  "first_login_time" timestamp(6),
  "bind_user" varchar(255) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "public"."jb_wechat_user"."id" IS '用户ID';
COMMENT ON COLUMN "public"."jb_wechat_user"."nickname" IS '用户昵称';
COMMENT ON COLUMN "public"."jb_wechat_user"."open_id" IS 'openId';
COMMENT ON COLUMN "public"."jb_wechat_user"."union_id" IS 'unionID';
COMMENT ON COLUMN "public"."jb_wechat_user"."sex" IS '性别 1男 2女 0 未知';
COMMENT ON COLUMN "public"."jb_wechat_user"."language" IS '语言';
COMMENT ON COLUMN "public"."jb_wechat_user"."subscibe" IS '是否已关注';
COMMENT ON COLUMN "public"."jb_wechat_user"."country" IS '国家';
COMMENT ON COLUMN "public"."jb_wechat_user"."province" IS '省';
COMMENT ON COLUMN "public"."jb_wechat_user"."city" IS '城市';
COMMENT ON COLUMN "public"."jb_wechat_user"."head_img_url" IS '头像';
COMMENT ON COLUMN "public"."jb_wechat_user"."subscribe_time" IS '关注时间';
COMMENT ON COLUMN "public"."jb_wechat_user"."remark" IS '备注';
COMMENT ON COLUMN "public"."jb_wechat_user"."group_id" IS '分组';
COMMENT ON COLUMN "public"."jb_wechat_user"."tag_ids" IS '标签';
COMMENT ON COLUMN "public"."jb_wechat_user"."subscribe_scene" IS '关注渠道';
COMMENT ON COLUMN "public"."jb_wechat_user"."qr_scene" IS '二维码场景-开发者自定义';
COMMENT ON COLUMN "public"."jb_wechat_user"."qr_scene_str" IS '二维码扫码场景描述-开发者自定义';
COMMENT ON COLUMN "public"."jb_wechat_user"."realname" IS '真实姓名';
COMMENT ON COLUMN "public"."jb_wechat_user"."phone" IS '手机号';
COMMENT ON COLUMN "public"."jb_wechat_user"."phone_country_code" IS '手机号国家代码';
COMMENT ON COLUMN "public"."jb_wechat_user"."check_code" IS '手机验证码';
COMMENT ON COLUMN "public"."jb_wechat_user"."is_checked" IS '是否已验证';
COMMENT ON COLUMN "public"."jb_wechat_user"."source" IS '来源 小程序还是公众平台';
COMMENT ON COLUMN "public"."jb_wechat_user"."session_key" IS '小程序登录SessionKey';
COMMENT ON COLUMN "public"."jb_wechat_user"."enable" IS '禁用访问';
COMMENT ON COLUMN "public"."jb_wechat_user"."create_time" IS '创建时间';
COMMENT ON COLUMN "public"."jb_wechat_user"."checked_time" IS '验证绑定手机号时间';
COMMENT ON COLUMN "public"."jb_wechat_user"."mp_id" IS '所属公众平台ID';
COMMENT ON COLUMN "public"."jb_wechat_user"."weixin" IS '微信号';
COMMENT ON COLUMN "public"."jb_wechat_user"."update_time" IS '更新时间';
COMMENT ON COLUMN "public"."jb_wechat_user"."last_login_time" IS '最后登录时间';
COMMENT ON COLUMN "public"."jb_wechat_user"."first_auth_time" IS '首次授权时间';
COMMENT ON COLUMN "public"."jb_wechat_user"."last_auth_time" IS '最后一次更新授权时间';
COMMENT ON COLUMN "public"."jb_wechat_user"."first_login_time" IS '首次登录时间';
COMMENT ON COLUMN "public"."jb_wechat_user"."bind_user" IS '绑定其他用户信息';
COMMENT ON TABLE "public"."jb_wechat_user" IS '微信用户信息_模板表';

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_application_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_brand_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_change_log_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_demotable_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_dept_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_dictionary_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_dictionary_type_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_download_log_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_global_config_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_global_config_type_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_goods_attr_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_goods_back_category_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_goods_element_content_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_goods_group_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_goods_html_content_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_goods_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_goods_type_brand_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_goods_type_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_jbolt_file_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_jbolt_version_file_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_jbolt_version_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_login_log_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_mall_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_online_user_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_order_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_order_item_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_order_shipping_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_permission_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_post_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_private_message_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_remote_login_log_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_role_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_role_permission_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_shelf_activity_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_shelf_carousel_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_shelf_element_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_shelf_goods_floor_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_shelf_goods_group_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_shelf_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_sku_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_sku_item_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_spec_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_spec_item_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_sys_notice_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_sys_notice_reader_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_system_log_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_todo_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_topnav_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_topnav_menu_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_update_libs_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_user_config_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_user_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_article_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_autoreply_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_config_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_keywords_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_media_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_menu_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_mpinfo_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_reply_content_idsq"', 100002, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."jb_wechat_user_idsq"', 100002, false);

-- ----------------------------
-- Primary Key structure for table jb_application
-- ----------------------------
ALTER TABLE "public"."jb_application" ADD CONSTRAINT "jb_application_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_brand
-- ----------------------------
ALTER TABLE "public"."jb_brand" ADD CONSTRAINT "jb_brand_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_change_log
-- ----------------------------
ALTER TABLE "public"."jb_change_log" ADD CONSTRAINT "jb_change_log_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_demotable
-- ----------------------------
ALTER TABLE "public"."jb_demotable" ADD CONSTRAINT "jb_demotable_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_dept
-- ----------------------------
ALTER TABLE "public"."jb_dept" ADD CONSTRAINT "jb_dept_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_dictionary
-- ----------------------------
ALTER TABLE "public"."jb_dictionary" ADD CONSTRAINT "jb_dictionary_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_dictionary_type
-- ----------------------------
ALTER TABLE "public"."jb_dictionary_type" ADD CONSTRAINT "jb_dictionary_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_download_log
-- ----------------------------
ALTER TABLE "public"."jb_download_log" ADD CONSTRAINT "jb_download_log_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_global_config
-- ----------------------------
ALTER TABLE "public"."jb_global_config" ADD CONSTRAINT "jb_global_config_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_global_config_type
-- ----------------------------
ALTER TABLE "public"."jb_global_config_type" ADD CONSTRAINT "jb_global_config_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_goods
-- ----------------------------
ALTER TABLE "public"."jb_goods" ADD CONSTRAINT "jb_goods_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_goods_attr
-- ----------------------------
ALTER TABLE "public"."jb_goods_attr" ADD CONSTRAINT "jb_goods_attr_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_goods_back_category
-- ----------------------------
ALTER TABLE "public"."jb_goods_back_category" ADD CONSTRAINT "jb_goods_back_category_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_goods_element_content
-- ----------------------------
ALTER TABLE "public"."jb_goods_element_content" ADD CONSTRAINT "jb_goods_element_content_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_goods_group
-- ----------------------------
ALTER TABLE "public"."jb_goods_group" ADD CONSTRAINT "jb_goods_group_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_goods_html_content
-- ----------------------------
ALTER TABLE "public"."jb_goods_html_content" ADD CONSTRAINT "jb_goods_html_content_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_goods_type
-- ----------------------------
ALTER TABLE "public"."jb_goods_type" ADD CONSTRAINT "jb_goods_type_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_goods_type_brand
-- ----------------------------
ALTER TABLE "public"."jb_goods_type_brand" ADD CONSTRAINT "jb_goods_type_brand_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_jbolt_file
-- ----------------------------
ALTER TABLE "public"."jb_jbolt_file" ADD CONSTRAINT "jb_jbolt_file_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_jbolt_version
-- ----------------------------
ALTER TABLE "public"."jb_jbolt_version" ADD CONSTRAINT "jb_jbolt_version_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_jbolt_version_file
-- ----------------------------
ALTER TABLE "public"."jb_jbolt_version_file" ADD CONSTRAINT "jb_jbolt_version_file_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_login_log
-- ----------------------------
ALTER TABLE "public"."jb_login_log" ADD CONSTRAINT "jb_login_log_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_mall
-- ----------------------------
ALTER TABLE "public"."jb_mall" ADD CONSTRAINT "jb_mall_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_online_user
-- ----------------------------
ALTER TABLE "public"."jb_online_user" ADD CONSTRAINT "jb_online_user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_order
-- ----------------------------
ALTER TABLE "public"."jb_order" ADD CONSTRAINT "jb_order_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_order_item
-- ----------------------------
ALTER TABLE "public"."jb_order_item" ADD CONSTRAINT "jb_order_item_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_order_shipping
-- ----------------------------
ALTER TABLE "public"."jb_order_shipping" ADD CONSTRAINT "jb_order_shipping_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_permission
-- ----------------------------
ALTER TABLE "public"."jb_permission" ADD CONSTRAINT "jb_permission_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_post
-- ----------------------------
ALTER TABLE "public"."jb_post" ADD CONSTRAINT "jb_post_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_private_message
-- ----------------------------
ALTER TABLE "public"."jb_private_message" ADD CONSTRAINT "jb_private_message_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_remote_login_log
-- ----------------------------
ALTER TABLE "public"."jb_remote_login_log" ADD CONSTRAINT "jb_remote_login_log_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_role
-- ----------------------------
ALTER TABLE "public"."jb_role" ADD CONSTRAINT "jb_role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_role_permission
-- ----------------------------
ALTER TABLE "public"."jb_role_permission" ADD CONSTRAINT "jb_role_permission_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_shelf
-- ----------------------------
ALTER TABLE "public"."jb_shelf" ADD CONSTRAINT "jb_shelf_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_shelf_activity
-- ----------------------------
ALTER TABLE "public"."jb_shelf_activity" ADD CONSTRAINT "jb_shelf_activity_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_shelf_carousel
-- ----------------------------
ALTER TABLE "public"."jb_shelf_carousel" ADD CONSTRAINT "jb_shelf_carousel_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_shelf_element
-- ----------------------------
ALTER TABLE "public"."jb_shelf_element" ADD CONSTRAINT "jb_shelf_element_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_shelf_goods_floor
-- ----------------------------
ALTER TABLE "public"."jb_shelf_goods_floor" ADD CONSTRAINT "jb_shelf_goods_floor_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_shelf_goods_group
-- ----------------------------
ALTER TABLE "public"."jb_shelf_goods_group" ADD CONSTRAINT "jb_shelf_goods_group_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_sku
-- ----------------------------
ALTER TABLE "public"."jb_sku" ADD CONSTRAINT "jb_sku_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_sku_item
-- ----------------------------
ALTER TABLE "public"."jb_sku_item" ADD CONSTRAINT "jb_sku_item_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_spec
-- ----------------------------
ALTER TABLE "public"."jb_spec" ADD CONSTRAINT "jb_spec_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_spec_item
-- ----------------------------
ALTER TABLE "public"."jb_spec_item" ADD CONSTRAINT "jb_spec_item_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_sys_notice
-- ----------------------------
ALTER TABLE "public"."jb_sys_notice" ADD CONSTRAINT "jb_sys_notice_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_sys_notice_reader
-- ----------------------------
ALTER TABLE "public"."jb_sys_notice_reader" ADD CONSTRAINT "jb_sys_notice_reader_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_system_log
-- ----------------------------
ALTER TABLE "public"."jb_system_log" ADD CONSTRAINT "jb_system_log_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_todo
-- ----------------------------
ALTER TABLE "public"."jb_todo" ADD CONSTRAINT "jb_todo_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_topnav
-- ----------------------------
ALTER TABLE "public"."jb_topnav" ADD CONSTRAINT "jb_topnav_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_topnav_menu
-- ----------------------------
ALTER TABLE "public"."jb_topnav_menu" ADD CONSTRAINT "jb_topnav_menu_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_update_libs
-- ----------------------------
ALTER TABLE "public"."jb_update_libs" ADD CONSTRAINT "jb_update_libs_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_user
-- ----------------------------
ALTER TABLE "public"."jb_user" ADD CONSTRAINT "jb_user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_user_config
-- ----------------------------
ALTER TABLE "public"."jb_user_config" ADD CONSTRAINT "jb_user_config_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_article
-- ----------------------------
ALTER TABLE "public"."jb_wechat_article" ADD CONSTRAINT "jb_wechat_article_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_autoreply
-- ----------------------------
ALTER TABLE "public"."jb_wechat_autoreply" ADD CONSTRAINT "jb_wechat_autoreply_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_config
-- ----------------------------
ALTER TABLE "public"."jb_wechat_config" ADD CONSTRAINT "jb_wechat_config_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_keywords
-- ----------------------------
ALTER TABLE "public"."jb_wechat_keywords" ADD CONSTRAINT "jb_wechat_keywords_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_media
-- ----------------------------
ALTER TABLE "public"."jb_wechat_media" ADD CONSTRAINT "jb_wechat_media_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_menu
-- ----------------------------
ALTER TABLE "public"."jb_wechat_menu" ADD CONSTRAINT "jb_wechat_menu_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_mpinfo
-- ----------------------------
ALTER TABLE "public"."jb_wechat_mpinfo" ADD CONSTRAINT "jb_wechat_mpinfo_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_reply_content
-- ----------------------------
ALTER TABLE "public"."jb_wechat_reply_content" ADD CONSTRAINT "jb_wechat_reply_content_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table jb_wechat_user
-- ----------------------------
ALTER TABLE "public"."jb_wechat_user" ADD CONSTRAINT "jb_wechat_user_pkey" PRIMARY KEY ("id");
