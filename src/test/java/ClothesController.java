//package qj.controllers;
//
//import cn.hutool.json.JSONArray;
//import cn.hutool.json.JSONObject;
//import cn.hutool.json.JSONUtil;
//import cn.jbolt._admin.permission.CheckPermission;
//import cn.jbolt._admin.permission.PermissionKey;
//import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
//import cn.jbolt.base.JBoltBaseController;
//import cn.jbolt.common.config.Msg;
//import com.jfinal.aop.Inject;
//import com.jfinal.core.Path;
//import qj.pojo.Clothes;
//import qj.service.ClothesService;
//
//import java.util.List;
//
//
//@CheckPermission(PermissionKey.CLOTHES_MANAGE)
//@UnCheckIfSystemAdmin
//@Path(value = "/minip/qj/clothes", viewPath = "/qj/clothes")
//public class ClothesController extends JBoltBaseController {
//
//    @Inject
//    ClothesService clothesService;
//
//    public void index() {
//        render("index.html");
//    }
//
//    public void datas() {
//        renderJsonData(clothesService.datas(getPageNumber(), getPageSize(), getKeywords()));
//    }
//
//    /**
//     * 场景数据
//     */
//    public void occasionDatas() {
//        String id = get(0);
//        set("_id", id);
//        set("files", clothesService.occasionDatas(get(0)));
//        render("occasionDatas.html");
//    }
//
//    public void otherPathDatas() {
//        String id = get(0);
//        set("_id", id);
//        set("files", clothesService.otherPathDatas(get(0)));
//        render("otherPathDatas.html");
//    }
//
//    public void typesDatas() {
//        String id = get(0);
//        set("_id", id);
//        set("files", clothesService.typesDatas(get(0)));
//        render("typesDatas.html");
//    }
//
//    /**
//     * 通过 _id 删除数据
//     */
//    public void deleteById() {
//        renderJson(clothesService.deleteById(get(0)));
//    }
//
//    /**
//     * 切换状态
//     */
//    public void toggleEnable() {
//        renderJson(clothesService.toggleEnable(get(0), get(1)));
//    }
//
//    /**
//     * 编辑页
//     */
//    public void edit() {
//        JSONObject data = clothesService.recordData(get(0));
//        JSONArray jsonArray = JSONUtil.parseArray(data.get("data"));
//        if (jsonArray == null) {
//            renderDialogFail(Msg.DATA_NOT_EXIST);
//            return;
//        }
//        Clothes clothes = JSONUtil.toBean(JSONUtil.parseObj(jsonArray.get(0)), Clothes.class);
//        LOG.debug("toBean=========================== " + clothes);
//        set("clothes", clothes);
//        set("price", (int) (float) clothes.getPrice());
//        render("edit.html");
//    }
//
//    /**
//     * 更新
//     */
//    public void update() {
//        renderJson(clothesService.update(getBean(Clothes.class, "clothes"), getBean(Clothes.CloComplexion.class, "clothes.cloComplexion"), get("price")));
//    }
//
//    /**
//     * 批量删除
//     */
//    public void deleteByIds() {
//        renderJson(clothesService.deleteByBatchIds(getStrParaToArray("ids", ",")));
//    }
//
//    /**
//     * 新增
//     */
//    public void add() {
//        render("add.html");
//    }
//
//    /**
//     * 保存
//     */
//    public void save() {
//        renderJson(clothesService.save(getBean(Clothes.class, "clothes"), getBean(Clothes.CloComplexion.class, "clothes.cloComplexion"), get("price")));
//    }
//
//    /**
//     * 场景数据修改
//     */
//    public void occasionDatasEdit() {
//        set("_id", get(0));
//        set("occasion", clothesService.occasionDatasEdit(get(0), get(1)));
//        set("order", get(1));
//        render("occasionDatasEdit.html");
//    }
//
//    /**
//     * 场景数据更新
//     */
//    public void occasionDatasUpdate() {
//        renderJson(clothesService.occasionDatasUpdate(get("_id"), get("order"), get("occasion")));
//    }
//
//    /**
//     * 场景数据新增
//     */
//    public void occasionDatasAdd() {
//        set("_id", get(0));
//        render("occasionDatasAdd.html");
//    }
//
//    /**
//     * 场景数据保存
//     */
//    public void occasionDatasSave() {
//        renderJson(clothesService.occasionDatasSave(get("_id"), get("occasion")));
//    }
//
//    /**
//     * 场景数据删除
//     */
//    public void occasionDatasDelete() {
//        renderJson(clothesService.occasionDatasDelete(get(0), get(1)));
//    }
//
//    /**
//     * 其它路径数据修改
//     */
//    public void otherPathDatasEdit() {
//        set("_id", get(0));
//        set("otherPath", clothesService.otherPathDatasEdit(get(0), get(1)));
//        set("order", get(1));
//        render("otherPathDatasEdit.html");
//    }
//
//    /**
//     * 其它路径数据更新
//     */
//    public void otherPathDatasUpdate() {
//        renderJson(clothesService.otherPathDatasUpdate(get("_id"), get("order"), getBean(Clothes.OtherPath.class, "otherPath")));
//    }
//
//    /**
//     * 其它路径数据新增
//     */
//    public void otherPathDatasAdd() {
//        set("_id", get(0));
//        render("otherPathDatasAdd.html");
//    }
//
//    /**
//     * 其它路径数据保存
//     */
//    public void otherPathDatasSave() {
//        renderJson(clothesService.otherPathDatasSave(get("_id"), getBean(Clothes.OtherPath.class, "otherPath")));
//    }
//
//    /**
//     * 其它路径数据删除
//     */
//    public void otherPathDatasDelete() {
//        renderJson(clothesService.otherPathDatasDelete(get(0), get(1)));
//    }
//
//    /**
//     * 类型数据修改
//     */
//    public void typesDatasEdit() {
//        set("_id", get(0));
//        set("types", clothesService.typesDatasEdit(get(0), get(1)));
//        set("order", get(1));
//        render("typesDatasEdit.html");
//    }
//
//    /**
//     * 类型数据更新
//     */
//    public void typesDatasUpdate() {
//        renderJson(clothesService.typesDatasUpdate(get("_id"), get("order"), getBean(Clothes.Types.class, "types")));
//    }
//
//    /**
//     * 类型数据新增
//     */
//    public void typesDatasAdd() {
//        set("_id", get(0));
//        render("typesDatasAdd.html");
//    }
//
//    /**
//     * 类型数据保存
//     */
//    public void typesDatasSave() {
//        renderJson(clothesService.typesDatasSave(get("_id"), getBean(Clothes.Types.class, "types")));
//    }
//
//    /**
//     * 类型数据删除
//     */
//    public void typesDatasDelete() {
//        renderJson(clothesService.typesDatasDelete(get(0), get(1)));
//    }
//
//    /**
//     * 规格数据
//     */
//    public void specificationsDatas() {
//        String id = get(0);
//        set("_id", id);
//        List<List<String>> lists = clothesService.specificationsDatas(get(0));
//        set("clothesData", lists.get(0));
//        set("sizesData", lists.get(1));
//        render("specificationsDatas.html");
//    }
//
//    /**
//     * 规格数据修改页面
//     */
//    public void specificationsEdit() {
//        set("_id", get(0));
//        set("specification", clothesService.specificationsEdit(get(0), get(1), get(2)));
//        set("order", get(1));
//        set("column", get(2));
//        render("specificationsDatasEdit.html");
//    }
//
//    /**
//     * 规格数据更新
//     */
//    public void specificationsDatasUpdate() {
//        renderJson(clothesService.specificationsDatasUpdate(get("_id"), get("order"), get("specification"), get("column")));
//    }
//
//    /**
//     * 规格数据新增页面
//     */
//    public void specificationsDatasAdd() {
//        set("_id", get(0));
//        set("column", get(1));
//        render("specificationsDatasAdd.html");
//    }
//
//    /**
//     * 规格数据保存
//     */
//    public void specificationsDatasSave() {
//        renderJson(clothesService.specificationsDatasSave(get("_id"), get("specification"), get("column")));
//    }
//
//    /**
//     * 规格数据删除
//     */
//    public void specificationsDatasDelete() {
//        renderJson(clothesService.specificationsDatasDelete(get(0), get(1), get(2)));
//    }
//
//}
