package craw.wz.gen;

import java.io.Serializable;

public class Grow implements Serializable {

    String  href;
    String  name ;
    String  imgName;

    public String getHref() {

        return href;
    }

    public Grow setHref(String href) {
        this.href = href;
        return this;
    }

    public String getName() {
        return name;
    }

    public Grow setName(String name) {
        this.name = name;
        return this;
    }

    public String getImgName() {
        return imgName;
    }

    public Grow setImgName(String imgName) {
        this.imgName = imgName;
        return this;
    }

    @Override
    public String toString() {
        return "Grow{" +
                "href='" + href + '\'' +
                ", name='" + name + '\'' +
                ", imgName='" + imgName + '\'' +
                '}';
    }
}
