package craw.wz;

import com.alibaba.fastjson.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class HtmlParseUtils {
    public static void main(String[] args) throws IOException {
//        获取请求
        String url = "https://www.drlmeng.com/type/";
//       解析网页(返回浏览器document对象)
        Document document = Jsoup.parse(new URL(url), 30000);
//       获取景天科的类别

        Element first = document.getElementsByClass("sub-menu").first();

        Elements li = first.getElementsByTag("a");
//        System.out.println(li);
//        System.out.println(li);
        List<JSONObject> jsonObjects = new ArrayList<>();
        for (Element element :
                li) {
            String explain = element.attr("title");
            String href = element.attr("href");
            String name = element.text();
            JSONObject object = new JSONObject();
            object.put("explain", explain);
            object.put("href", href);
            object.put("name", name);
//            System.out.println(object.toString());
            jsonObjects.add(object);
        }
        List<Subject> collect = jsonObjects.stream().filter(e ->
                e.get("name").toString().endsWith("科")
        ).map(e -> {
            Subject subject = new Subject();
            subject = JSONObject.parseObject(e.toString(), Subject.class);
            subject.setIndex(jsonObjects.indexOf(e));
            return subject;
        }).collect(Collectors.toList());
        for (Subject s :
                collect) {
            System.out.println(s.getIndex());
        }
        List<Category> category = new ArrayList();
        List< List<Category>> listCategory = new ArrayList<>();
        Category category1 = null;
        int next =0;
        int start = 1;
         next++;
        int end = collect.get(next).getIndex();
        System.out.println("start = " + start);
        System.out.println("end = " + end);
        for (int k = start; k <= end; k++) {
            int z =next-1;
            if (next!=collect.size()&&k == collect.get(next).getIndex()) {
                if (category == null||category.size()==0){
                    category = Collections.emptyList();
                }
                collect.get(z).setCategories(category);

                start =end;
                System.out.println("start = " + start);
                next++;
                category = new ArrayList<>();
                if (k>jsonObjects.size()){
                    break;
                }
                if (next==9){
                    end =jsonObjects.size();
                    System.out.println("end = " + end);
                }else {
                    System.out.println(next);
                    end =collect.get(next).getIndex();
                    System.out.println("end = " + end);
                }
                continue;
            }
            if (k==jsonObjects.size()){
              break;
            }else {
                category1 = JSONObject.parseObject(jsonObjects.get(k).toString(),Category.class);
                category.add(category1);
                System.out.println("jsonObjects.get(" + k + ") = " + jsonObjects.get(k));
            }

//            System.out.println(collect.toString());
            System.out.println(JSONObject.toJSON(collect));
        }


    }


}
