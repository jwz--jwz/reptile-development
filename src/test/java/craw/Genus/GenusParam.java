package craw.Genus;

public class GenusParam {
    private  String genusHref;
    private  String genusName;

    public String getGenusHref() {
        return genusHref;
    }

    public GenusParam setGenusHref(String genusHref) {
        this.genusHref = genusHref;
        return this;
    }

    public String getGenusName() {
        return genusName;
    }

    public GenusParam setGenusName(String genusName) {
        this.genusName = genusName;
        return this;
    }
    @Override
    public String toString() {
        return "GenusParam{" +
                "genusHref='" + genusHref + '\'' +
                ", genusName='" + genusName + '\'' +
                '}';
    }
}
