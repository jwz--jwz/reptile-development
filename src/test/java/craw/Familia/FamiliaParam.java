package craw.Familia;

public class FamiliaParam {
    private  String href;
    private  String name;
    private  String alias;

    public String getHref() {
        return href;
    }

    public FamiliaParam setHref(String href) {
        this.href = href;
        return this;
    }

    public String getName() {
        return name;
    }

    public FamiliaParam setName(String name) {
        this.name = name;
        return this;
    }

    public String getAlias() {
        return alias;
    }

    public FamiliaParam setAlias(String alias) {
        this.alias = alias;
        return this;
    }

    @Override
    public String toString() {
        return "FamiliaParam{" +
                "href='" + href + '\'' +
                ", name='" + name + '\'' +
                ", alias='" + alias + '\'' +
                '}';
    }
}
