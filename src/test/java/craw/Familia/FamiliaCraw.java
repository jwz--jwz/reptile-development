package craw.Familia;

import craw.Genus.GenusParam;
import craw.utily;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.net.URL;
import java.util.ArrayList;

public class FamiliaCraw {

    @Test
    public void TestCrawFamilia(){
//        获取请求
        String url = "http://www.mengsang.com/duorou/";
//       解析网页(返回浏览器document对象)
        try {
            ArrayList<FamiliaParam> arrayList = new ArrayList<>();

            Document document = Jsoup.parse(new URL(url), 30000);
            Elements sideBarUl_borderTop = document.getElementsByClass("sideBarUl borderTop");
//            System.out.println(sideBarUl_borderTop);
            for (Element e:
                 sideBarUl_borderTop) {
                Elements a = e.getElementsByTag("a");
                for (Element element:
                     a) {
                    String href = element.attr("href");
                    String long_name = element.text().toString();
                    FamiliaParam familiaParam = new FamiliaParam();
                    utily.u.partition(long_name,familiaParam);
                    arrayList.add(familiaParam.setHref(href));
                }
            }
            arrayList.stream().forEach(e->{
                System.out.println(e.toString());
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public  void TestGenus(){
//        获取请求
        String url = "http://www.mengsang.com/duorou/jingtianke/qingsuolongshu/";
//       解析网页(返回浏览器document对象)
        try {
            ArrayList<GenusParam> arrayList = new ArrayList<>();
            Document document = Jsoup.parse(new URL(url), 30000);
            Elements sideBarUl_borderTop = document.getElementsByClass("sideBarUl borderTop");
//            System.out.println(sideBarUl_borderTop);
            for (Element e:
                    sideBarUl_borderTop) {
                Elements a = e.getElementsByTag("a");
                for (Element element:
                        a) {
                    String href = element.attr("href");
                    String long_name = element.text().toString();
                   GenusParam genusParam = new GenusParam();
                   genusParam.setGenusHref(href);
                   genusParam.setGenusName(long_name);
                    arrayList.add(genusParam);
                }
            }
            arrayList.stream().forEach(e->{
                System.out.println(e.toString());
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }



}
