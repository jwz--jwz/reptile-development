#sql ("plantDoinfoList")
select a.*
from   plant_do_info as a
where a.is_del = 0

#if(keywords??)
   and  concat(IFNULL(a.plant_name,''),IFNULL(a.genus,''),IFNULL(a.familia,'')) like '%#(keywords)%'
#end

#if(familia_name??)
   and a.connect_id in (select id from plant_do_familia_genus where  familia_id = #(familia_name) )
#end
#if(genus_name??)
   and a.connect_id in (select id from plant_do_familia_genus where  genus_id = #(genus_name) )
#end

order  by a.id desc
#end