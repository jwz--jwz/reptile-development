/**
 * JBolt web socket js
 */
var JBoltWebSocket = null;
/**
 * 初始化JBolt的webSocket
 */
function initJBoltWebSocket(){
	var startJBoltWebSocket=function(token){
        var host = document.location.host;
        var Protocol = window.location.protocol.split(':')[0];
        if (Protocol == 'https') {
        	JBoltWebSocket = new WebSocket('wss://' + host + '/websocket.ws/' + token);
        } else {
        	JBoltWebSocket = new WebSocket('ws://' + host + '/websocket.ws/' + token);
        }
        //处理事件
        processJBoltWebSocketEvent();
	}
    //判断当前浏览器是否支持WebSocket
    if (window.WebSocket) {
    	Ajax.get("admin/myToken",function(res){
    		startJBoltWebSocket(res.data);
    	});
    } else {
        LayerMsgBox.alert('当前浏览器不支持websocket',2);
    }
}

/**
 * 处理事件
 * @param websocket
 * @returns
 */
function processJBoltWebSocketEvent(){
	
	    //连接发生错误
		JBoltWebSocket.onerror = function () {
	        console.log('WebSocket连接发生错误');
	    };
	 
	    //连接成功
	    JBoltWebSocket.onopen = function () {
	    	console.log('WebSocket连接成功');
	    }
	 
	    //接收到消息
	    JBoltWebSocket.onmessage = function (event) {
	    	processJBoltWebSocketOnMessage(event.data);
	    }
	 
	    //连接关闭
	    JBoltWebSocket.onclose = function () {
	    	console.log('WebSocket连接关闭');
	    }
	 
	    //监听窗口关闭
	    JBoltWebSocket.onbeforeunload = function () {
	    	JBoltWebSocket.close();
	    }
	}


//处理接收到消息
function processJBoltWebSocketOnMessage(message) {
    //这里解析message，然后用js赋值给前端就行
	console.log("processOnMessage")
	console.log(message)
}

$(function(){
//	initJBoltWebSocket();
});

