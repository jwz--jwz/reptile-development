package duorou.util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

//实例化对象工具类
public class JedisUtil {
//        redis 服务器IP
    private  static final  String ADDR = "192.168.199.137";
//    端口
    private  static  final  Integer PORT =6379;
//    访问密码
    private  static  final String Auth ="root";
//  可用连接实例的最大数目，默认值为8
//  如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
    private  static final int MAX_ACTIVE = 1024;
//  控制一个pool最多有多少个状态为idle(空闲的) jedis 实例 ，默认值也是 8
    private  static  final int  MAX_WAIT = 10000;
//  等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException
    private  static final int TIMEOUT  = 10000;
    //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    private static  final  boolean TEST_ON_BORROW = true;

    private static JedisPool jedisPool = null;

//    初始化redis
     static {
         try {
             JedisPoolConfig  config =new JedisPoolConfig();
             config.setMaxTotal(MAX_ACTIVE);
             config.setMaxIdle(MAX_WAIT);
              config.setTestOnBorrow(TEST_ON_BORROW);
              jedisPool = new JedisPool(config,ADDR,PORT,TIMEOUT);
         }catch (Exception e){
             e.printStackTrace();
         }
     }
    /**
     * 获取redis实例
     */
    public synchronized  static  Jedis getJedis() {
        try {
            if (jedisPool != null)
                return jedisPool.getResource();
            else
                return  null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
        /**
         * 释放redis资源
         */
        public static void returnResource(final Jedis jedis){
            if (jedis != null){
                jedis.close();
            }
        }
    /**
     * 将Java对象转换为bytes数组 序列化过程
     */
    public static  byte[] serialize(Object object){
        ObjectOutputStream objectOutputStream = null;
        ByteArrayOutputStream baos = null;
        try {
            baos = new ByteArrayOutputStream();
            objectOutputStream = new ObjectOutputStream(baos);
            objectOutputStream.writeObject(object);
            byte [] bytes = baos.toByteArray();
            return  bytes;
        }catch (Exception e){
            e.printStackTrace();
        }
        return  null;
    }
    /**
     * 将byte数组数组转换为Java对象
     */
    public static  Object unserialize(byte [] bytes){
        if (bytes ==null) return  null;
        ByteArrayInputStream bais = null;
        try {
//            反序列化
            bais = new ByteArrayInputStream(bytes);
            ObjectInputStream   objectInputStream = new ObjectInputStream(bais);
            return  objectInputStream.readObject();
        }catch (Exception e){
            e.printStackTrace();
        }
        return  null;
    }







}
