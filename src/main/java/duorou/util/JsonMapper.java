package duorou.util;


import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;
import org.codehaus.jackson.type.TypeReference;

public class JsonMapper {

    private  static ObjectMapper objectMapper = new ObjectMapper();
//    单例模式
    static {
//        变量初始化
//        config
//    遇到不认识的字段怎末处理
    objectMapper.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
    objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS,false);
    objectMapper.setFilters(new SimpleFilterProvider().setFailOnUnknownId(false));
    objectMapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_EMPTY);
    }
//    将对象转换为字符串  Object-->String
        public static  <T> String obj2String(T src){
        if (src == null){
            return  null;
        }
        try {
//            判断src是否为String,如果是,强转为string  如果不是调用jackson方法转换
        return  src instanceof String ? (String) src: objectMapper.writeValueAsString(src);
        }catch (Exception e){
           return  null;
        }
        }
//      String --> Object
        public static <T> T string2Obj(String  src , TypeReference<T> typeReference){
//        将string转换为为我们所需要的对象
        if (src == null|| typeReference == null){
            return  null;
        }
        try {
            return  (T)(typeReference.getType().equals(String.class)? src:objectMapper.readValue(src,typeReference));
        }catch (Exception exception){
            exception.printStackTrace();
            return null;
        }
        }

}
