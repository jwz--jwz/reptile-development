package duorou.route;

import com.jfinal.config.Routes;
import duorou.controller.plant.PlantDoFamiliaController;
import duorou.controller.plant.PlantDoGenusController;
import duorou.controller.plant.PlantDoInfoController;
import duorou.model.PlantDoFamilia;

public class DoRoute extends Routes {

    @Override
    public void config() {
        this.setBaseViewPath("/duorou");
        this.add("/plant/species", PlantDoInfoController.class,"/plant/species");
        this.add("/plant/genus", PlantDoGenusController.class,"/plant/genus");
        this.add("/plant/familia", PlantDoFamiliaController.class,"/plant/familia");
    }
}
