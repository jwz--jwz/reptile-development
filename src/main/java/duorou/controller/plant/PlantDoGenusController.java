package duorou.controller.plant;

import com.jfinal.aop.Inject;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.common.config.Msg;
import duorou.model.PlantDoGenus;
import duorou.service.plant.PlantDoGenusService;

/**
 * 多肉基本属性管理 Controller
 * @ClassName: PlantDoGenusAdminController   
 * @author: wz
 * @date: 2021-08-31 22:20  
 */
@CheckPermission(PermissionKey.PLANT_GENUS)
@UnCheckIfSystemAdmin
public class PlantDoGenusController extends JBoltBaseController {

	@Inject
	private PlantDoGenusService service;
	
   /**
	* 首页
	*/
	public void index() {
		render("index.html");
	}
  	
  	/**
	* 数据源
	*/
	public void datas() {
		renderJsonData(service.paginateAdminDatas(getPageNumber(),getPageSize(),getKeywords()));
	}
	
   /**
	* 新增
	*/
	public void add() {
		render("add.html");
	}
	
   /**
	* 编辑
	*/
	public void edit() {
		PlantDoGenus plantDoGenus=service.findById(getInt(0)); 
		if(plantDoGenus == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		set("plantDoGenus",plantDoGenus);
		render("edit.html");
	}
	
  /**
	* 保存
	*/
	public void save() {
		renderJson(service.save(getModel(PlantDoGenus.class, "plantDoGenus")));
	}
	
   /**
	* 更新
	*/
	public void update() {
		renderJson(service.update(getModel(PlantDoGenus.class, "plantDoGenus")));
	}
	
   /**
	* 批量删除
	*/
	public void deleteByIds() {
		renderJson(service.deleteByBatchIds(get("ids")));
	}
	
   /**
	* 删除
	*/
	public void delete() {
		renderJson(service.delete(getInt(0)));
	}
	
	
}
