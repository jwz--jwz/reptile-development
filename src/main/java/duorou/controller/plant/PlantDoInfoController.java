package duorou.controller.plant;

import cn.jbolt.common.bean.OptionBean;
import com.jfinal.aop.Inject;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import com.jfinal.core.Path;
import cn.jbolt.common.config.Msg;
import com.jfinal.plugin.activerecord.Page;
import duorou.model.PlantDoFamilia;
import duorou.model.PlantDoGenus;
import duorou.model.PlantDoInfo;
import duorou.service.plant.PlantDoInfoService;

import java.util.ArrayList;
import java.util.List;

/**
 * 多肉基本信息管理 Controller
 * @ClassName: PlantDoInfoAdminController   
 * @author: wz
 * @date: 2021-08-30 20:35  
 */
@CheckPermission(PermissionKey.PLANT_SPECIES)
@UnCheckIfSystemAdmin
//@Path(value = "/plant/species", viewPath = "/duorou/plant/species")
public class PlantDoInfoController extends JBoltBaseController {

	@Inject
	private PlantDoInfoService service;
	
   /**
	* 首页
	*/
	public void index() {
		render("index.html");
	}
  	
  	/**
	* 数据源
	*/
	public void datas() {
		renderJsonData(service.paginateAdminDatas(getPageNumber(), getPageSize(),getKeywords(), get("familia_name"), get("genus_name")));
	}
	
   /**
	* 新增
	*/
	public void add() {
		render("add.html");
	}
	
   /**
	* 编辑
	*/
	public void edit() {
		PlantDoInfo plantDoInfo=service.findById(getInt(0)); 
		if(plantDoInfo == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		set("plantDoInfo",plantDoInfo);
		render("edit.html");
	}
	
  /**
	* 保存
	*/
	public void save() {
		renderJson(service.save(getModel(PlantDoInfo.class, "plantDoInfo")));
	}
	
   /**
	* 更新
	*/
	public void update() {
		renderJson(service.update(getModel(PlantDoInfo.class, "plantDoInfo")));
	}
	
   /**
	* 批量删除
	*/
	public void deleteByIds() {
		renderJson(service.deleteByBatchIds(get("ids")));
	}
	
   /**
	* 删除
	*/
	public void delete() {
		renderJson(service.delete(getInt(0)));
	}

	/**
	 * 找到科类点击信息
	 */
	public void  find_familia_type(){
		List<OptionBean> optionBeans = new ArrayList<OptionBean>();
		PlantDoFamilia plantDoFamilia = new PlantDoFamilia();
		plantDoFamilia.find("select * from plant_do_familia where is_del = 0").stream().forEach(e->{
			optionBeans.add(new OptionBean(e.getFamiliaName(),e.getId()));
		});
		renderJsonData(optionBeans);
	}

	/**
	 * 获取属性的信息
	 */

	public void find_genus_type(){
		List<OptionBean> optionBeans = new ArrayList<OptionBean>();
		PlantDoGenus plantDoGenus = new PlantDoGenus();
		plantDoGenus.find("select * from plant_do_genus where  is_del=0").stream().forEach(
				e->{
					optionBeans.add(new OptionBean(e.getGenusName(),e.getId()));
				}
		);
		renderJsonData(optionBeans);
	}
}
