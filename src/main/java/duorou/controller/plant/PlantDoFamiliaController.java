package duorou.controller.plant;

import com.jfinal.aop.Inject;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.common.config.Msg;
import duorou.model.PlantDoFamilia;
import duorou.service.plant.PlantDoFamiliaService;

/**
 * 多肉基本科类管理 Controller
 * @ClassName: PlantDoFamiliaAdminController   
 * @author: wz
 * @date: 2021-08-31 22:44  
 */
@CheckPermission(PermissionKey.PLANT_FAMiLIA)
@UnCheckIfSystemAdmin
public class PlantDoFamiliaController extends JBoltBaseController {

	@Inject
	private PlantDoFamiliaService service;
	
   /**
	* 首页
	*/
	public void index() {
		render("index.html");
	}
  	
  	/**
	* 数据源
	*/
	public void datas() {
		renderJsonData(service.paginateAdminDatas(getPageNumber(),getPageSize(),getKeywords()));
	}
	
   /**
	* 新增
	*/
	public void add() {
		render("add.html");
	}
	
   /**
	* 编辑
	*/
	public void edit() {
		PlantDoFamilia plantDoFamilia=service.findById(getInt(0)); 
		if(plantDoFamilia == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		set("plantDoFamilia",plantDoFamilia);
		render("edit.html");
	}
	
  /**
	* 保存
	*/
	public void save() {
		renderJson(service.save(getModel(PlantDoFamilia.class, "plantDoFamilia")));
	}
	
   /**
	* 更新
	*/
	public void update() {
		renderJson(service.update(getModel(PlantDoFamilia.class, "plantDoFamilia")));
	}
	
   /**
	* 批量删除
	*/
	public void deleteByIds() {
		renderJson(service.deleteByBatchIds(get("ids")));
	}
	
   /**
	* 删除
	*/
	public void delete() {
		renderJson(service.delete(getInt(0)));
	}
	
	
}
