package duorou.service.redis;

import com.google.common.base.Joiner;
import duorou.util.CacheKeyConstants;
import duorou.util.JedisUtil;
import redis.clients.jedis.Jedis;

import java.util.Set;


public class RedisService {

//    保存数据
    public void saveCache(String toSaveValue, int timeoutSeconds, CacheKeyConstants prefix, String ...keys){
        if (toSaveValue == null){
            return;
        }
        Jedis jedis = null;
        try {
            String  cacheKey = generateCacheKey(prefix,keys);
            jedis = JedisUtil.getJedis();
            jedis.select(3);
            jedis.setex(cacheKey,timeoutSeconds,toSaveValue);
        }catch (Exception e){
              e.printStackTrace();
//              log.error("save cache exception ,prefix:{} ,key:{}",prefix.name,)
        }finally {
            JedisUtil.returnResource(jedis);
        }
    }

    /**
     * 保存数据
     * @param toSaveValue
     * @param prefix
     * @param keys
     */
    public void saveCache(String toSaveValue,CacheKeyConstants prefix,String ...keys){
        if (toSaveValue == null){
            return;
        }
        Jedis jedis = null;
        try {
            String  cacheKey = generateCacheKey(prefix,keys);
            jedis = JedisUtil.getJedis();
            jedis.select(3);
            jedis.set(cacheKey,toSaveValue);
        }catch (Exception e){
            e.printStackTrace();
//              log.error("save cache exception ,prefix:{} ,key:{}",prefix.name,)
        }finally {
            JedisUtil.returnResource(jedis);
        }
    }

    /**
     * 获取对应的数据
     * @param prefix
     * @param keys
     * @return
     */
    public String getFromCache(CacheKeyConstants prefix, String...keys){
            Jedis jedis = null;
            String cacheKey = generateCacheKey(prefix,keys);
            try {
                jedis = JedisUtil.getJedis();
                jedis.select(3);
                String  value = jedis.get(cacheKey);
                System.out.println("value = " + value);
                return value;
            } catch (Exception e){
                e.printStackTrace();
//                遇到返回一个空值
                return null;
            }finally {
                JedisUtil.returnResource(jedis);
            }
    }
    /**
     * 获取对应的数据
     * @param prefix
     * @param keys
     * @return
     */
    public String getFromCache(String keys){
        Jedis jedis = null;
        try {
            jedis = JedisUtil.getJedis();
            jedis.select(3);
            String  value = jedis.get(keys);
            System.out.println("value = " + value);
            return value;
        } catch (Exception e){
            e.printStackTrace();
//                遇到返回一个空值
            return null;
        }finally {
            JedisUtil.returnResource(jedis);
        }
    }



    /**
     * 获取对应的数据
     * @param prefix
     * @param keys
     * @return
     */
    public Set getFromCacheKeys(CacheKeyConstants prefix){
        Jedis jedis = null;
        try {
            jedis = JedisUtil.getJedis();
            jedis.select(3);
            Set<String> set =jedis.keys(String.valueOf(prefix)+"*");
            return set;
        } catch (Exception e){
            e.printStackTrace();
//                遇到返回一个空值
            return null;
        }finally {
            JedisUtil.returnResource(jedis);
        }
    }

    /**
     * 自动拼接字符串
     * @param prefix
     * @param keys
     * @return
     */
    private  String generateCacheKey(CacheKeyConstants prefix,String ...keys){
        String key = prefix.name();
        if (keys !=null&&keys.length>0){
            key +="_"+ Joiner.on("_").join(keys);
        }
       return key;
    }
}
