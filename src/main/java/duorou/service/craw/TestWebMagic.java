package duorou.service.craw;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

/**
 * 实现页面分析的业务逻辑
 */
public class TestWebMagic implements PageProcessor {
    /**
     * 页面的分析
     * 下载的结果封装成Page对象
     * 可以从page中获取下载的结果
     * @param page
     */
    @Override
    public void process(Page page) {
        Html html =page.getHtml();
        String s = html.toString();
//        将结果输出到控制台,
//        ResultItems resultItems = page.getResultItems();
//        resultItems.put("html",html);
//        传到pipeline
//        page.putField("html",html);
//        使用css解析器进行解析
//        Selectable selectable = html.$("title", "text");
//        String s1 = selectable.get();
//        page.putField("s1",s1);
        Selectable xpath = html.xpath("/html/body/div[5]/div[2]/div[2]/div[2]/div[2]/table/tbody/tr/td/div[4]/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div/div[5]/div[2]/div[6]/div[2]/text()");
        String s1 = xpath.get();
        page.putField("addr",s1);
    }

    /**
     * site 就是站点的信息
     * @return
     */
    @Override
    public Site getSite() {
        return Site.me();
    }
    //      初始化爬虫
    public static void main(String[] args) {
        Spider.create(new TestWebMagic())
//                设置起始的url
                .addUrl("http://www.360doc.com/content/15/0427/16/6816362_466380753.shtml")
//               同步方法，在当前线程中执行
//                .run();
//                异步执行，重新开启一个新的线程
                .start();
    }
}
