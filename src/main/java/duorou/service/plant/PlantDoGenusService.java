package duorou.service.plant;

import com.jfinal.plugin.activerecord.Page;
import cn.jbolt.base.JBoltBaseService;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import cn.jbolt.common.config.Msg;
import duorou.model.PlantDoGenus;
/**
 * 多肉基本属性管理 Service
 * @ClassName: PlantDoGenusService   
 * @author: wz
 * @date: 2021-08-31 22:20  
 */
public class PlantDoGenusService extends JBoltBaseService<PlantDoGenus> {
	private PlantDoGenus dao=new PlantDoGenus().dao();
	@Override
	protected PlantDoGenus dao() {
		return dao;
	}
		
  /**
	 * 后台管理分页查询true
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @return
	 */
	public Page<PlantDoGenus> paginateAdminDatas(int pageNumber, int pageSize, String keywords) {
		return paginateByKeywords("id","desc", pageNumber, pageSize, keywords, "genus_name,genus_describe");
	}
	
  /**
	 * 保存
	 * @param userId
	 * @param plantDoGenus
	 * @return
	 */
	public Ret save(PlantDoGenus plantDoGenus) {
		if(plantDoGenus==null || isOk(plantDoGenus.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		//if(exists(columnName, value)) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		boolean success=plantDoGenus.save();
		if(success) {
			//添加日志
			//addSaveSystemLog(plantDoGenus.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOGENUS, plantDoGenus.getName());
		}
		return ret(success);
	}
	
   /**
	 * 更新
	 * @param plantDoGenus
	 * @return
	 */
	public Ret update(PlantDoGenus plantDoGenus) {
		if(plantDoGenus==null || notOk(plantDoGenus.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		//更新时需要判断数据存在
		PlantDoGenus dbPlantDoGenus=findById(plantDoGenus.getId());
		if(dbPlantDoGenus==null) {return fail(Msg.DATA_NOT_EXIST);}
		//if(exists(columnName, value, plantDoGenus.getId())) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		boolean success=plantDoGenus.update();
		if(success) {
			//添加日志
			//addUpdateSystemLog(plantDoGenus.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOGENUS, plantDoGenus.getName());
		}
		return ret(success);
	}
	
   /**
	  * 删除 指定多个ID
	 * @param ids
	 * @return
	 */
	public Ret deleteByBatchIds(String ids) {
		return deleteByIds(ids);
	}
	
   /**
	  * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		return deleteById(id,true);
	}
	
	/**
	  * 删除数据后执行的回调
	 * @param plantDoGenus 要删除的model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	protected String afterDelete(PlantDoGenus plantDoGenus, Kv kv) {
		//addDeleteSystemLog(plantDoGenus.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOGENUS, plantDoGenus.getName());
		return null;
	}
	
	/**
	  * 检测是否可以删除
	 * @param plantDoGenus 要删除的model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	public String checkCanDelete(PlantDoGenus plantDoGenus, Kv kv) {
		//如果检测被用了 返回信息 则阻止删除 如果返回null 则正常执行删除
		return checkInUse(plantDoGenus, kv);
	}
	
	/**
	  * 检测是否可以删除
	 * @param plantDoGenus model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	public String checkInUse(PlantDoGenus plantDoGenus, Kv kv) {
		//这里用来覆盖 检测PlantDoGenus是否被其它表引用
		return null;
	}
	
}