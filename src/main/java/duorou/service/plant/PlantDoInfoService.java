package duorou.service.plant;

import com.jfinal.plugin.activerecord.Page;
import cn.jbolt.base.JBoltBaseService;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import cn.jbolt.common.config.Msg;

import duorou.model.PlantDoInfo;



/**
 * 多肉基本信息管理 Service
 * @ClassName: PlantDoInfoService   
 * @author: wz
 * @date: 2021-08-30 20:35  
 */
public class PlantDoInfoService extends JBoltBaseService<PlantDoInfo> {
	private PlantDoInfo dao=new PlantDoInfo().dao();
	@Override
	protected PlantDoInfo dao() {
		return dao;
	}
		
  /**
	 * 后台管理分页查询true
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @return
	 */
	public Page<PlantDoInfo> 	paginateAdminDatas(int pageNumber, int pageSize, String keywords,String familia_name, String genus_name  ) {
		Kv kv =Kv.by("table",table());
		kv.setIfNotBlank("keywords",keywords);
		kv.setIfNotBlank("familia_name",familia_name);
		kv.setIfNotBlank("genus_name",genus_name);
		return dao().paginate(pageNumber,pageSize,Db.getSqlPara("duorou.do.plantDoinfoList",kv));
	}
	
  /**
	 * 保存
	 * @param plantDoInfo
	 * @return
	 */
	public Ret save(PlantDoInfo plantDoInfo) {
		if(plantDoInfo==null || isOk(plantDoInfo.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		//if(exists(columnName, value)) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		boolean success=plantDoInfo.save();
		if(success) {
			//添加日志
			//addSaveSystemLog(plantDoInfo.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOINFO, plantDoInfo.getName());
		}
		return ret(success);
	}
	
   /**
	 * 更新
	 * @param plantDoInfo
	 * @return
	 */
	public Ret update(PlantDoInfo plantDoInfo) {
		if(plantDoInfo==null || notOk(plantDoInfo.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		//更新时需要判断数据存在
		PlantDoInfo dbPlantDoInfo=findById(plantDoInfo.getId());
		if(dbPlantDoInfo==null) {return fail(Msg.DATA_NOT_EXIST);}
		//if(exists(columnName, value, plantDoInfo.getId())) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		boolean success=plantDoInfo.update();
		if(success) {
			//添加日志
			//addUpdateSystemLog(plantDoInfo.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOINFO, plantDoInfo.getName());
		}
		return ret(success);
	}
	
   /**
	  * 删除 指定多个ID
	 * @param ids
	 * @return
	 */
	public Ret deleteByBatchIds(String ids) {
		return deleteByIds(ids);
	}
	
   /**
	  * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		return deleteById(id,true);
	}
	
	/**
	  * 删除数据后执行的回调
	 * @param plantDoInfo 要删除的model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	protected String afterDelete(PlantDoInfo plantDoInfo, Kv kv) {
		//addDeleteSystemLog(plantDoInfo.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOINFO, plantDoInfo.getName());
		return null;
	}
	
	/**
	  * 检测是否可以删除
	 * @param plantDoInfo 要删除的model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	public String checkCanDelete(PlantDoInfo plantDoInfo, Kv kv) {
		//如果检测被用了 返回信息 则阻止删除 如果返回null 则正常执行删除
		return checkInUse(plantDoInfo, kv);
	}
	
	/**
	  * 检测是否可以删除
	 * @param plantDoInfo model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	public String checkInUse(PlantDoInfo plantDoInfo, Kv kv) {
		//这里用来覆盖 检测PlantDoInfo是否被其它表引用
		return null;
	}


}