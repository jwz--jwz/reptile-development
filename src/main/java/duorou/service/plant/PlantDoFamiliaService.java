package duorou.service.plant;

import com.jfinal.plugin.activerecord.Page;
import cn.jbolt.base.JBoltBaseService;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Db;
import cn.jbolt.common.config.Msg;
import duorou.model.PlantDoFamilia;
/**
 * 多肉基本科类管理 Service
 * @ClassName: PlantDoFamiliaService   
 * @author: wz
 * @date: 2021-08-31 22:44  
 */
public class PlantDoFamiliaService extends JBoltBaseService<PlantDoFamilia> {
	private PlantDoFamilia dao=new PlantDoFamilia().dao();
	@Override
	protected PlantDoFamilia dao() {
		return dao;
	}
		
  /**
	 * 后台管理分页查询true
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @return
	 */
	public Page<PlantDoFamilia> paginateAdminDatas(int pageNumber, int pageSize, String keywords) {
		return paginateByKeywords("id","desc", pageNumber, pageSize, keywords, "familia_name,familia_describe");
	}
	
  /**
	 * 保存
	 * @param userId
	 * @param plantDoFamilia
	 * @return
	 */
	public Ret save(PlantDoFamilia plantDoFamilia) {
		if(plantDoFamilia==null || isOk(plantDoFamilia.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		//if(exists(columnName, value)) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		boolean success=plantDoFamilia.save();
		if(success) {
			//添加日志
			//addSaveSystemLog(plantDoFamilia.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOFAMILIA, plantDoFamilia.getName());
		}
		return ret(success);
	}
	
   /**
	 * 更新
	 * @param plantDoFamilia
	 * @return
	 */
	public Ret update(PlantDoFamilia plantDoFamilia) {
		if(plantDoFamilia==null || notOk(plantDoFamilia.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		//更新时需要判断数据存在
		PlantDoFamilia dbPlantDoFamilia=findById(plantDoFamilia.getId());
		if(dbPlantDoFamilia==null) {return fail(Msg.DATA_NOT_EXIST);}
		//if(exists(columnName, value, plantDoFamilia.getId())) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		boolean success=plantDoFamilia.update();
		if(success) {
			//添加日志
			//addUpdateSystemLog(plantDoFamilia.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOFAMILIA, plantDoFamilia.getName());
		}
		return ret(success);
	}
	
   /**
	  * 删除 指定多个ID
	 * @param ids
	 * @return
	 */
	public Ret deleteByBatchIds(String ids) {
		return deleteByIds(ids);
	}
	
   /**
	  * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		return deleteById(id,true);
	}
	
	/**
	  * 删除数据后执行的回调
	 * @param plantDoFamilia 要删除的model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	protected String afterDelete(PlantDoFamilia plantDoFamilia, Kv kv) {
		//addDeleteSystemLog(plantDoFamilia.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PLANTDOFAMILIA, plantDoFamilia.getName());
		return null;
	}
	
	/**
	  * 检测是否可以删除
	 * @param plantDoFamilia 要删除的model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	public String checkCanDelete(PlantDoFamilia plantDoFamilia, Kv kv) {
		//如果检测被用了 返回信息 则阻止删除 如果返回null 则正常执行删除
		return checkInUse(plantDoFamilia, kv);
	}
	
	/**
	  * 检测是否可以删除
	 * @param plantDoFamilia model
	 * @param kv 携带额外参数一般用不上
	 * @return
	 */
	@Override
	public String checkInUse(PlantDoFamilia plantDoFamilia, Kv kv) {
		//这里用来覆盖 检测PlantDoFamilia是否被其它表引用
		return null;
	}
	
}