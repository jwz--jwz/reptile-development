package duorou.pojo.craw.plant;

public class Category {
    private String name;
    private String href;
    private  String  explain;

    public String getName() {
        return name;
    }

    public Category setName(String name) {
        this.name = name;
        return this;
    }

    public String getHref() {
        return href;
    }

    public Category setHref(String href) {
        this.href = href;
        return this;
    }

    public String getExplain() {
        return explain;
    }

    public Category setExplain(String explain) {
        this.explain = explain;
        return this;
    }


    @Override
    public String toString() {
        return "category{" +
                "name='" + name + '\'' +
                ", href='" + href + '\'' +
                ", explain='" + explain + '\'' +
                '}';
    }
}
