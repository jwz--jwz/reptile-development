package duorou.pojo.craw.plant;

import java.io.Serializable;
import java.util.Objects;

public class Grow implements Serializable {

    String  href;
    String  name ;
    String  imgName;

    public String getHref() {

        return href;
    }

    public Grow setHref(String href) {
        this.href = href;
        return this;
    }

    public String getName() {
        return name;
    }

    public Grow setName(String name) {
        this.name = name;
        return this;
    }

    public String getImgName() {
        return imgName;
    }

    public Grow setImgName(String imgName) {
        this.imgName = imgName;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Grow)) return false;
        Grow grow = (Grow) o;
        return Objects.equals(getHref(), grow.getHref()) && Objects.equals(getName(), grow.getName()) && Objects.equals(getImgName(), grow.getImgName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHref(), getName(), getImgName());
    }

    @Override
    public String toString() {
        return "Grow{" +
                "href='" + href + '\'' +
                ", name='" + name + '\'' +
                ", imgName='" + imgName + '\'' +
                '}';
    }
}
