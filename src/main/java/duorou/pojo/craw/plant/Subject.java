package duorou.pojo.craw.plant;

import java.util.Collections;
import java.util.List;

public class Subject {
   private String name;
   private String href;
   private  String  explain;
   private   Integer index;
   private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public Subject setCategories(List<Category> categories) {
        this.categories = categories;
        return this;
    }

    public Integer getIndex() {
        return index;
    }

    public Subject setIndex(Integer index) {
        this.index = index;
        return this;
    }

    public String getName() {
        return name;
    }

    public Subject setName(String name) {
        this.name = name;
        return this;
    }

    public String getHref() {
        return href;
    }

    public Subject setHref(String href) {
        this.href = href;
        return this;
    }

    public String getExplain() {
        return explain;
    }

    public Subject setExplain(String explain) {
        this.explain = explain;
        return this;
    }

    @Override
    public String toString() {
        if (categories==null ||categories.size()==0){
            return "Subject{" +
                    "name='" + name + '\'' +
                    ", href='" + href + '\'' +
                    ", explain='" + explain + '\'' +
                    ", index=" + index +
                    ", categories=" + Collections.emptyList() +
                    '}';
        }else
        return "Subject{" +
                "name='" + name + '\'' +
                ", href='" + href + '\'' +
                ", explain='" + explain + '\'' +
                ", index=" + index +
                ", categories=" + categories.toString() +
                '}';
    }
}
