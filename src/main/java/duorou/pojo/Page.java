package duorou.pojo;

import java.util.List;

public class Page<T>{

    private Integer pageNum; // 当前页  （前台传递的参数；如果前台未传递，则默认第一页）
    private Integer pageSize; // 每页显示的数量 （前台传递或后台设定）
    private long totalCount; // 总记录数 （后台数据库中查询得到；count()函数）
    private Integer totalPages; // 总页数 （总记录数/每页显示的数量；将参数转换为浮点型，执行除法操作，向上取整）

    private List<T> dataList; // 当前页的数据集合 （查询数据库中指定页的数据列表）

    public Page() {
    }

    public Page(Integer pageNum, Integer pageSize, long totalCount) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
        // 总页数 （总记录数/每页显示的数量；将参数转换为浮点型，执行除法操作，向上取整）
        this.totalPages = (int)Math.ceil(totalCount/(pageSize * 1.0));
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public Page<T> setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
        return this;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public Page<T> setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public Page<T> setTotalCount(long totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public Page<T> setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
        return this;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public Page<T> setDataList(List<T> dataList) {
        this.dataList = dataList;
        return this;
    }

    @Override
    public String toString() {
        if (dataList==null||dataList.size() ==0){
            return "Page{" +
                    "pageNum=" + pageNum +
                    ", pageSize=" + pageSize +
                    ", totalCount=" + totalCount +
                    ", totalPages=" + totalPages +
                    '}';
        }else
            return "Page{" +
                "pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", totalCount=" + totalCount +
                ", totalPages=" + totalPages +
                ", dataList=" + dataList.toString() +
                '}';
    }
}
