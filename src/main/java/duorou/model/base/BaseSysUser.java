package duorou.model.base;
import cn.jbolt.base.JBoltBaseModel;
import cn.jbolt.common.gen.JBoltField;

/**
 * 用户表
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseSysUser<M extends BaseSysUser<M>> extends JBoltBaseModel<M>{

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	@JBoltField(name="id" ,columnName="id",type="Integer", remark="ID", required=true, maxLength=10, fixed=0, order=1)
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 部门关系，默认值为 0
	 */
	public M setDeptId(java.lang.Integer deptId) {
		set("dept_id", deptId);
		return (M)this;
	}
	
	/**
	 * 部门关系，默认值为 0
	 */
	@JBoltField(name="deptId" ,columnName="dept_id",type="Integer", remark="部门关系，默认值为 0", required=true, maxLength=10, fixed=0, order=2)
	public java.lang.Integer getDeptId() {
		return getInt("dept_id");
	}

	/**
	 * 用户的姓名
	 */
	public M setUserName(java.lang.String userName) {
		set("user_name", userName);
		return (M)this;
	}
	
	/**
	 * 用户的姓名
	 */
	@JBoltField(name="userName" ,columnName="user_name",type="String", remark="用户的姓名", required=true, maxLength=20, fixed=0, order=3)
	public java.lang.String getUserName() {
		return getStr("user_name");
	}

	/**
	 * 用户加密之后的密码,40就够了
	 */
	public M setUserPwd(java.lang.String userPwd) {
		set("user_pwd", userPwd);
		return (M)this;
	}
	
	/**
	 * 用户加密之后的密码,40就够了
	 */
	@JBoltField(name="userPwd" ,columnName="user_pwd",type="String", remark="用户加密之后的密码,40就够了", required=true, maxLength=40, fixed=0, order=4)
	public java.lang.String getUserPwd() {
		return getStr("user_pwd");
	}

	/**
	 * 邮箱
	 */
	public M setEmail(java.lang.String email) {
		set("email", email);
		return (M)this;
	}
	
	/**
	 * 邮箱
	 */
	@JBoltField(name="email" ,columnName="email",type="String", remark="邮箱", required=true, maxLength=20, fixed=0, order=5)
	public java.lang.String getEmail() {
		return getStr("email");
	}

	/**
	 * 电话
	 */
	public M setPhone(java.lang.String phone) {
		set("phone", phone);
		return (M)this;
	}
	
	/**
	 * 电话
	 */
	@JBoltField(name="phone" ,columnName="phone",type="String", remark="电话", required=true, maxLength=13, fixed=0, order=6)
	public java.lang.String getPhone() {
		return getStr("phone");
	}

	/**
	 * 备注的信息
	 */
	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	/**
	 * 备注的信息
	 */
	@JBoltField(name="remark" ,columnName="remark",type="String", remark="备注的信息", required=true, maxLength=200, fixed=0, order=7)
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	/**
	 * 有效状态 默认值为 1 有效  0冻结  2 ：删除
	 */
	public M setStatus(java.lang.Integer status) {
		set("status", status);
		return (M)this;
	}
	
	/**
	 * 有效状态 默认值为 1 有效  0冻结  2 ：删除
	 */
	@JBoltField(name="status" ,columnName="status",type="Integer", remark="有效状态 默认值为 1 有效  0冻结  2 ：删除", required=true, maxLength=10, fixed=0, order=8)
	public java.lang.Integer getStatus() {
		return getInt("status");
	}

	/**
	 * 用户最后一次更新时间
	 */
	public M setOperateTime(java.util.Date operateTime) {
		set("operate_time", operateTime);
		return (M)this;
	}
	
	/**
	 * 用户最后一次更新时间
	 */
	@JBoltField(name="operateTime" ,columnName="operate_time",type="Date", remark="用户最后一次更新时间", required=true, maxLength=19, fixed=0, order=9)
	public java.util.Date getOperateTime() {
		return getDate("operate_time");
	}

	/**
	 * 操作人员操作的时间
	 */
	public M setOperator(java.lang.String operator) {
		set("operator", operator);
		return (M)this;
	}
	
	/**
	 * 操作人员操作的时间
	 */
	@JBoltField(name="operator" ,columnName="operator",type="String", remark="操作人员操作的时间", required=true, maxLength=20, fixed=0, order=10)
	public java.lang.String getOperator() {
		return getStr("operator");
	}

	/**
	 * 操作人员修改的ip
	 */
	public M setOperatorIp(java.lang.String operatorIp) {
		set("operator_ip", operatorIp);
		return (M)this;
	}
	
	/**
	 * 操作人员修改的ip
	 */
	@JBoltField(name="operatorIp" ,columnName="operator_ip",type="String", remark="操作人员修改的ip", required=true, maxLength=40, fixed=0, order=11)
	public java.lang.String getOperatorIp() {
		return getStr("operator_ip");
	}

}

