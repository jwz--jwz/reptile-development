package duorou.model;

import duorou.model.base.BasePlantDoFamiliaGenus;
import cn.jbolt.common.config.TableBind;
import cn.jbolt.base.JBoltIDGenMode;

/**
 * 多肉属性科类关联表
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
@TableBind(dataSource = "main" , table = "plant_do_familia_genus" , primaryKey = "id" , idGenMode = JBoltIDGenMode.AUTO)
public class PlantDoFamiliaGenus extends BasePlantDoFamiliaGenus<PlantDoFamiliaGenus> {

}

