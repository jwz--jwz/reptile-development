package duorou.model;

import duorou.model.base.BaseSharePlantReview;
import cn.jbolt.common.config.TableBind;
import cn.jbolt.base.JBoltIDGenMode;

/**
 * 
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
@TableBind(dataSource = "main" , table = "share_plant_review" , primaryKey = "id" , idGenMode = JBoltIDGenMode.AUTO)
public class SharePlantReview extends BaseSharePlantReview<SharePlantReview> {

}

