package duorou.model;

import duorou.model.base.BaseArtcle;
import cn.jbolt.common.config.TableBind;
import cn.jbolt.base.JBoltIDGenMode;

/**
 * 
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
@TableBind(dataSource = "main" , table = "artcle" , primaryKey = "id" , idGenMode = JBoltIDGenMode.AUTO)
public class Artcle extends BaseArtcle<Artcle> {

}

