package cn.jbolt.extend.util;

import com.jfinal.plugin.ehcache.CacheKit;

import cn.jbolt.common.util.CacheParaValidator;

/**
 * 全局CACHE操作工具类
 * 
 * @ClassName: CacheUtil
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2021年04月13日
 */
public class CacheUtil extends CacheParaValidator {
	public static final CacheUtil me = new CacheUtil();
	public static final String JBOLT_CACHE_NAME = "jbolt_cache";
	public static final String JBOLT_CACHE_DEFAULT_PREFIX = "jbc_";
	/**
	 * 拼接cacheKey
	 * @param pre
	 * @param value
	 * @return
	 */
	@SuppressWarnings("unused")
	private String buildCacheKey(String pre,Object value) {
		return JBOLT_CACHE_DEFAULT_PREFIX+pre+value.toString();
	}
	
	/**
	 * put
	 * @param key
	 * @param value
	 */
	public void put(String key, Object value) {
		put(JBOLT_CACHE_NAME, key, value);
	}
	/**
	 * put
	 * @param cacheName
	 * @param key
	 * @param value
	 */
	public void put(String cacheName,String key, Object value) {
		CacheKit.put(cacheName, key, value);
	}
	
	/**
	 * get
	 * @param <T>
	 * @param key
	 * @return
	 */
	public <T> T get(String key) {
		return get(JBOLT_CACHE_NAME, key);
	}
	/**
	 * get
	 * @param <T>
	 * @param cacheName
	 * @param key
	 * @return
	 */
	public <T> T get(String cacheName,String key) {
		return CacheKit.get(cacheName, key);
	}
	 

	/**
	 * 举例 删除removeRoleMenus
	 * 
	 * @param roleIds
	 */
//	public void removeRolesMenus(String roleIds) {
//		if (StrKit.notBlank(roleIds)) {
//			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("roles_menus_" ,roleIds));
//		}
//	}
	
	/**
	 * 举例  根据roleIds获取对应菜单
	 * @param roleIds
	 * @return
	 */
//	public List<Permission> getRoleMenus(String roleIds) {
//		if (StrKit.isBlank(roleIds)) {
//			return null;
//		}
//		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("roles_menus_" , roleIds), new IDataLoader() {
//			@Override
//			public Object load() {
//				//这里的service可以直接调用AOP获取 也可以拿到上面去定义出来
//				return Aop.get(PermissionService.class).getMenusByRoles(roleIds);
//			}
//		});
//
//	}
	
	
	/**
	 * 举例 获取岗位信息
	 * @param id
	 * @return
	 */
//	public Post getPost(Object id) {
//		//这里的service可以直接调用AOP获取 也可以拿到上面去定义出来
//		return Aop.get(PostService.class).findById(id);
//	} 
	
	/**
	 * 举例 获取岗位Name
	 * @param deptId
	 * @return
	 */
//	public String getPostName(Object id) {
//		Post post=getPost(id);
//		return post==null?"":post.getName();
//	}
	
	
	
	

	
	 
}
