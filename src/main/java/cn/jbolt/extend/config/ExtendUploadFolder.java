package cn.jbolt.extend.config;

import cn.jbolt.common.config.JBoltUploadFolder;

/**   
 *    扩展二开使用上传控制 定义的目录
 * @ClassName:  UploadFolder   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年3月24日 上午12:00:48   
 */
public class ExtendUploadFolder extends JBoltUploadFolder{
	//下方举例说明
	/**
	 * 举例说明这个是哪个地方使用的目录
	 */
	public static final String EXTEND_DEMO_EDITOR_IMAGE = "extend"+ SEPARATOR +"demo" + SEPARATOR + "editor";
	
}
