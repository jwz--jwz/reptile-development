package cn.jbolt.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.jfinal.kit.HandlerKit;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;

import cn.hutool.core.util.StrUtil;
import cn.jbolt.common.config.MainConfig;
import cn.jbolt.common.model.OnlineUser;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CACHE;
import cn.jbolt.common.util.CookieUtil;
/**
 * JBolt开发平台全局handler
 * @ClassName:  JBoltBaseHandler   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年12月10日   
 */
public class JBoltBaseHandler extends Handler {
	private String[] unlimiteds;
	
	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		request.setAttribute(JBoltConst.RQKEY_NEED_ALWAYS_HTTPS, MainConfig.NEED_ALWAYS_HTTPS);
		String basePath = processBasepath(request);
		request.setAttribute(JBoltConst.RQKEY_BASEPATH, basePath);
		
		if (target.endsWith(JBoltConst.HTML)&&checkNotInUnlimited(target)) {
			HandlerKit.renderError404(request, response, isHandled);
		} else {
			processJBoltCurrentUserInfo(request);
			processJBoltRequest(target,request);
			try {
				next.handle(target, request, response, isHandled);
			} finally {
				JBoltUserKit.clear();
			}
		}

	}
	
	/**
	 * 拦截处理用户userId
	 * @param request
	 */
	private void processJBoltCurrentUserInfo(HttpServletRequest request) {
		//拿到用户客户端的cookie里保存的sessionId 如果拿不到说明没登录或者过期清理了 需要跳转重新登录了 返回false即可
		String sessionId = CookieUtil.getValue(request, JBoltConst.JBOLT_SESSIONID_KEY);
		if(StrKit.notBlank(sessionId)) {
			//登录后拿到userId
			OnlineUser oneOnlineUser = CACHE.me.getOnlineUserBySessionId(sessionId);
			//判断有数据并且没有过期 就可以加入threadLocal 过期了就不设置 那么后面拦截器就会拦截到了
			if(oneOnlineUser != null && !oneOnlineUser.getIsExpiration()) {
				//设置当前现成UserId和sessionId
				JBoltUserKit.setUserId(oneOnlineUser.getUserId());
				JBoltUserKit.setUserSessionId(sessionId);
				if(oneOnlineUser.getScreenLocked()) {
					JBoltUserKit.setScreenIsLocked();
				}
			}
		}
	}
	/**
	 * 处理basePath 默认需要最后的斜杠
	 * @param request
	 * @return
	 */
	public static String processBasepath(HttpServletRequest request) {
		return processBasepath(request, true);
	}
	/**
	 * 处理basePath
	 * @param request
	 * @param needEndSlash 是否需要最后的斜杠
	 * @return
	 */
	public static String processBasepath(HttpServletRequest request,boolean needEndSlash) {
		return processBasepathStrBuilder(request, needEndSlash).toString();
	}
	/**
	 * 处理basePath 返回StringBUiler
	 * @param request
	 * @param needEndSlash 是否需要最后的斜杠
	 * @return
	 */
	public static StringBuilder processBasepathStrBuilder(HttpServletRequest request,boolean needEndSlash) {
		StringBuilder basePath=new StringBuilder();
		String scheme=request.getScheme();
		int port=request.getServerPort();
		if (MainConfig.NEED_ALWAYS_HTTPS) {
			scheme=JBoltConst.HTTPS;
		} else {
			boolean isHttps=HttpKit.isHttps(request);
			if(isHttps) {
				scheme=JBoltConst.HTTPS;
			}
		}
		basePath.append(scheme).append(JBoltConst.MHSP).append(request.getServerName());
		if (port!= 80) {
			basePath.append(JBoltConst.MH).append(port);
		}
		String contextPath=request.getContextPath();
		if(StrKit.isBlank(contextPath)){
			contextPath=JBoltConst.SLASH_STR;
		}else if(contextPath.endsWith(JBoltConst.SLASH_STR)==false){
			contextPath=contextPath+JBoltConst.SLASH_STR;
		}
		if(needEndSlash) {
			basePath.append(contextPath);
		}
		return basePath;
	}
	/**
	 * 处理请求类型
	 * @param target 
	 * @param request
	 */
	private void processJBoltRequest(String target, HttpServletRequest request) {
		if(isStaticResource(target)) {
			return;
		}
		
		request.setAttribute(JBoltConst.RQKEY_PMKEY, (target.length()>=2&&target.charAt(0)==JBoltConst.SLASH&&target.charAt(1)!=JBoltConst.SLASH)?target.substring(1):target);
		
//		System.out.println("请求地址:"+request.getRequestURL());
		request.setAttribute(JBoltConst.JBOLT_BASE_UPLOAD_PATH_PRE, MainConfig.BASE_UPLOAD_PATH_PRE);
		//判断是不是API SDK调用
		if(JBoltConst.TRUE.equalsIgnoreCase(request.getHeader(JBoltConst.RQ_HEAD_KEY_JBOLTAPI))) {
			request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.JBOLTAPI);
			return;
		}
		
		//判断是不是pjax
		if(JBoltConst.TRUE.equalsIgnoreCase(request.getHeader(JBoltConst.RQ_HEAD_KEY_XPJAX))) {
			request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.PJAX);
			return;
		}
		
		//判断是不是ajaxportal
		if(JBoltConst.TRUE.equalsIgnoreCase(request.getHeader(JBoltConst.RQ_HEAD_KEY_AJAXPORTAL))) {
			request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.AJAXPORTAL);
			return;
		}
		
		//判断是不是 ajax
		String xrequestedwith = request.getHeader(JBoltConst.RQ_HEAD_KEY_XREQUESTEDWITH);
		if((xrequestedwith != null && JBoltConst.XMLHTTPREQUEST.equalsIgnoreCase(xrequestedwith))) {
			request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.AJAX);
			return;
		}
		
		String rqParam=request.getParameter(JBoltConst.RQKEY_JB_RQTYPE);		
		//判断URL参数中带_jb_rqtype_=dialog
		if(JBoltConst.RQ_TYPE_DIALOG.equalsIgnoreCase(rqParam)) {
			request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.DIALOG);
			return;
		}
		//判断URL参数中带_jb_rqtype_=iframe
		if(JBoltConst.RQ_TYPE_IFRAME.equalsIgnoreCase(rqParam)) {
			request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.IFRAME);
			return;
		}
		
		//判断请求地址上带着_jb_rqtype_dialog 例如localhost/demo/table/1-2-_jb_rqtype_dialog
		String url=request.getRequestURI();
		if(StrKit.notBlank(url)) {
			if(url.endsWith(JBoltConst.RQ_TYPE_DIALOG_STR)) {
				request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.DIALOG);
				return;
			}
			if(url.endsWith(JBoltConst.RQ_TYPE_IFRAME_STR)) {
				request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.IFRAME);
				return;
			}
		}
		
		request.setAttribute(JBoltConst.RQKEY_JB_RQTYPE,JBoltRequestType.NORMAL);
	}
	
	
	/**
	 * 判断js css 图片等静态资源地址
	 * @param target
	 * @return
	 */
	private boolean isStaticResource(String target) {
		int index=target.lastIndexOf(JBoltConst.DOT);
		if(index==-1) {
			//没有就直接返回false 不是静态资源
			return false;
		}
		String suffixes=target.substring(index);
		return StrUtil.endWithAnyIgnoreCase(suffixes,JBoltConst.RESOURCE_SUFFIXES);
	}
	
	/**
	 * 检测是否属于忽略检测
	 * @param target
	 * @return
	 */
	private boolean checkNotInUnlimited(String target) {
		if(unlimiteds==null||unlimiteds.length==0) {
			return true;
		}
		boolean in=false;
		for(String l:unlimiteds) {
			if(target.equals(l)||target.indexOf(l)!=-1) {
				in=true;
				break;
			}
		}
		return !in;
	}
	/**
	 * 设置不检测的路径
	 * @param unlimiteds
	 * @return
	 */
	public JBoltBaseHandler unlimited(String... unlimiteds) {
		if(ArrayUtil.isEmpty(this.unlimiteds)) {
			this.unlimiteds=unlimiteds;
		}else {
			this.unlimiteds=ArrayUtil.merge(this.unlimiteds, unlimiteds);
		}
		return this;
	}
	

	 

}
