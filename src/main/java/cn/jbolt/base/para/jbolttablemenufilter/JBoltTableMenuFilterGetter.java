package cn.jbolt.base.para.jbolttablemenufilter;

import com.jfinal.core.Action;
import com.jfinal.core.Controller;
import com.jfinal.core.paragetter.ParaGetter;

import cn.jbolt.base.JBoltControllerKit;
/**
 * JBolt平台 参数获取器
 * JBolt官网 http://jbolt.cn
 * @ClassName:  JBoltParamGetter   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年3月18日   
 */
public class JBoltTableMenuFilterGetter extends ParaGetter<JBoltTableMenuFilter> {

	public JBoltTableMenuFilterGetter(String parameterName, String defaultValue) {
		super(parameterName, defaultValue);
	}
	
	@Override
	public JBoltTableMenuFilter get(Action action, Controller c) {
		return JBoltControllerKit.getJBoltTableMenuFilter(action, c);
	}


	@Override
	protected JBoltTableMenuFilter to(String v) {
		return null;
	}

}
