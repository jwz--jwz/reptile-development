package cn.jbolt.base.para.jbolttablemenufilter;

import java.util.List;
/**
 * 通过右键菜单进行表格数据查询过滤的封装过滤器
 * @ClassName:  JBoltTableMenuFilter   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年7月17日   
 */
public class JBoltTableMenuFilter{
	/**
	 * 关键词
	 */
	private String keywords;
	/**
	 * 是否包含查询
	 */
	private boolean include;
	/**
	 * 是否启用分页
	 */
	private boolean paging;
	/**
	 * 分页
	 */
	private int pageNumber;
	/**
	 * 分页单页数量
	 */
	private int pageSize;
	/**
	 * 排序列
	 */
	private String sortColumn;
	/**
	 * 排序方式
	 */
	private String sortType;
	/**
	 * 设置显示列
	 */
	private List<String> returnColumns;
	/**
	 * 过滤筛选条件
	 */
	private List<JBoltTableMenuFilterItem> items;
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public boolean getInclude() {
		return include;
	}
	public void setInclude(boolean include) {
		this.include = include;
	}
	public boolean getPaging() {
		return paging;
	}
	public void setPaging(boolean paging) {
		this.paging = paging;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public List<String> getReturnColumns() {
		return returnColumns;
	}
	public void setReturnColumns(List<String> returnColumns) {
		this.returnColumns = returnColumns;
	}
	public List<JBoltTableMenuFilterItem> getItems() {
		return items;
	}
	public void setItems(List<JBoltTableMenuFilterItem> items) {
		this.items = items;
	}
	public int getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	
	
}
