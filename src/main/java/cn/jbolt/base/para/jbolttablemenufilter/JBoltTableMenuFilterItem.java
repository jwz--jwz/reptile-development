package cn.jbolt.base.para.jbolttablemenufilter;
/**
 * 表格菜单多条件过滤item
 * @ClassName:  JBoltTableMenuFilterItem   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年7月17日   
 *    
 */
public class JBoltTableMenuFilterItem {
	public static final int TYPE_STRING = 1;
	public static final int TYPE_INT = 2;
	public static final int TYPE_BIGDECIMAL = 3;
	public static final int TYPE_DATE = 4;
	public static final int TYPE_DATETIME = 5;
	public static final int TYPE_TIME = 6;
	public static final int TYPE_BOOLEAN = 7;
	/**
	 * 字段名
	 */
	private String column;
	/**
	 * 对比类型
	 */
	private String comparison;
	/**
	 * 值
	 */
	private Object value;
	/**
	 * 类型
	 */
	private int type;
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public String getComparison() {
		return comparison;
	}
	public void setComparison(String comparison) {
		this.comparison = comparison;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
}
