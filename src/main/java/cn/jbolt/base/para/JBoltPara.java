package cn.jbolt.base.para;
import java.math.BigDecimal;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.json.Json;
import com.jfinal.kit.StrKit;

import cn.jbolt.common.util.ArrayUtil;
/**
 * JBolt平台封装的参数包装器
 * @ClassName:  JBoltParam   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年3月18日   
 */
@SuppressWarnings("serial")
public class JBoltPara extends JSONObject{
	private String rawData;
	public JBoltPara(String rawData) {
		if(StrKit.notBlank(rawData)) {
			this.rawData=rawData;
			JSONObject datas=JSON.parseObject(rawData);
			if(datas!=null) {
				putAll(datas);
			}
		}
	}
	
	public JBoltPara(Map<String, String[]> paraMap) {
		if(paraMap!=null && paraMap.isEmpty()==false) {
			for (Entry<String, String[]> entry : paraMap.entrySet()) {
				String[] values = entry.getValue();
				String value = (values != null && values.length > 0) ? values[0] : null;
				put(entry.getKey(), "".equals(value) ? null : value);
			}
		}
	}
	
	/**
	 * 得到原始rawdata
	 * @return
	 */
	public String getRawData() {
		if(StrKit.isBlank(rawData)&&isEmpty()==false) {
			this.rawData=Json.getJson().toJson(this);
		}
		return this.rawData;
	}
	
	/**
	 * 获取参数里组合字符串分割转数组 String
	 * @param key
	 * @param split
	 * @return
	 */
	public String[] getStringArray(String key,String split) {
		return ArrayUtil.from3(getString(key), split);
	}
	
	/**
	 * 获取参数里组合字符串分割转数组 默认逗号分隔符 String
	 * @param key
	 * @return
	 */
	public String[] getStringArray(String key) {
		return getStringArray(key, ",");
	}
	
	/**
	 * 获取参数里组合字符串分割转数组 Integer
	 * @param key
	 * @param split
	 * @return
	 */
	public Integer[] getIntegerArray(String key,String split) {
		return ArrayUtil.toInt(getString(key), split);
	}
	
	/**
	 * 获取参数里组合字符串分割转数组 默认逗号分隔符 Integer
	 * @param key
	 * @return
	 */
	public Integer[] getIntegerArray(String key) {
		return getIntegerArray(key, ",");
	}
	/**
	 * 获取参数里组合字符串分割转数组 Long
	 * @param key
	 * @param split
	 * @return
	 */
	public Long[] getLongArray(String key,String split) {
		return ArrayUtil.toLong(getString(key), split);
	}
	
	/**
	 * 获取参数里组合字符串分割转数组 默认逗号分隔符 Long
	 * @param key
	 * @return
	 */
	public Long[] getLongArray(String key) {
		return getLongArray(key, ",");
	}
	/**
	 * 获取参数里组合字符串分割转数组 BigDecimal
	 * @param key
	 * @param split
	 * @return
	 */
	public BigDecimal[] getBigDecimalArray(String key,String split) {
		return ArrayUtil.toBigDecimal(getString(key), split);
	}
	
	/**
	 * 获取参数里组合字符串分割转数组 默认逗号分隔符 BigDecimal
	 * @param key
	 * @return
	 */
	public BigDecimal[] getBigDecimalArray(String key) {
		return getBigDecimalArray(key, ",");
	}
}
