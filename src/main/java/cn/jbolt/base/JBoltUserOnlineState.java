package cn.jbolt.base;

import cn.jbolt.base.enumutil.JBoltEnum;
/**
 *   用户在线状态
 * @ClassName:  JBoltUserOnlineState   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月18日   
 */
public enum JBoltUserOnlineState {
	ONLINE("在线",1),
	OFFLINE("离线",2),
	FORCED_OFFLINE("强退下线",3),
	TERMINAL_OFFLINE("异端顶替",4);
	private String text;
	private int value;
	private JBoltUserOnlineState(String text,int value) {
		this.text = text;
		this.value = value;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	//枚举类加入到JBoltEnum管理器中
	static {
		JBoltEnum.addToTvBeanMap(JBoltUserOnlineState.class);
	}
}
