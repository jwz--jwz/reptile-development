package cn.jbolt.base;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;

import cn.hutool.setting.Setting;
import cn.jbolt.common.config.MainConfig;
import cn.jbolt.common.config.TableBind;
import cn.jbolt.common.db.sql.DBType;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.extend.config.ExtendDatabaseConfig;
/**
 * JBoltModel配置
 * @ClassName:  JBoltModelConfig   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月14日   
 *    
 */
@SuppressWarnings({"rawtypes"})
public enum JBoltModelConfig {
	me;
	protected static final Log LOG = Log.getLog(JBoltModelConfig.class);
	private static Map<Class<? extends Model>, TableBind> tableBindMap = new HashMap<Class<? extends Model>, TableBind>();
	private static Map<Class<? extends Model>, JBoltAutoCache> autoCacheMap = new HashMap<Class<? extends Model>, JBoltAutoCache>();
	private static Map<Class<? extends Model>, String> datasourceMap = new HashMap<Class<? extends Model>, String>();
	private static Map<Class<? extends Model>, String> dbTypeMap = new HashMap<Class<? extends Model>, String>();
	private static Map<Class<? extends Model>, String> tableNameMap = new HashMap<Class<? extends Model>, String>();
	private static Map<Class<? extends Model>, String> primaryKeyMap = new HashMap<Class<? extends Model>, String>();
	private static Map<Class<? extends Model>, String[]> compositePrimaryKeyMap = new HashMap<Class<? extends Model>, String[]>();
	private static Map<Class<? extends Model>, String> idGenModeMap = new HashMap<Class<? extends Model>, String>();
	private static Map<Class<? extends Model>, String> idSeqMap = new HashMap<Class<? extends Model>, String>();
	/**
	 *  添加配置
	 * @param modelClass
	 */
	public void addConfig(Class<? extends Model> modelClass) {
		addTableBind(modelClass);
		addAutoCache(modelClass);
	}
	/**
	 * 添加表绑定配置
	 * @param modelClass
	 */
	public void addTableBind(Class<? extends Model> modelClass) {
		if(!modelClass.isAnnotationPresent(TableBind.class)) {
			String msg = String.format("[%s]中未设置TableBind注解，无法初始化其配置", modelClass);
			LOG.error(msg);
			throw new RuntimeException(msg);
		}
		TableBind tableBind = modelClass.getAnnotation(TableBind.class);
		if(StrKit.isBlank(tableBind.table())) {
			String msg = String.format("[%s]中@TableBind未设置table属性，无法完成表与Model映射", modelClass);
			LOG.error(msg);
			throw new RuntimeException(msg);
		}
		
		//datasource 数据源配置
		addDatasource(modelClass, tableBind.dataSource());
		//table表名 配置数据库类型
		addTableName(modelClass, tableBind.table());
		//dbtype 配置数据库类型
		addDbType(modelClass, tableBind.dataSource());
		//primarykey 配置主键
		addPrimaryKey(modelClass, tableBind.primaryKey());
		//id策略
		addIdGenMode(modelClass, tableBind.idGenMode());
		//id seq str
		addIdSequenceStr(modelClass,tableBind.idSequence());
		//加入map
		tableBindMap.put(modelClass,tableBind);
	}
	/**
	 *   添加id主键 自增序列配置
	 * @param modelClass
	 * @param idSequence 
	 */
	private void addIdSequenceStr(Class<? extends Model> modelClass, String idSequence) {
		String dbType = dbTypeMap.get(modelClass);
		//如果不是空并且不是default 说明是自己指定的 直接赋值即可
		if(StrKit.notBlank(idSequence) && JBoltConst.JBOLT_ID_SEQUENCE_DEFAULT.equals(idSequence) == false) {
			switch (dbType) {
			case DBType.POSTGRESQL:
				idSeqMap.put(modelClass, "nextval('"+idSequence+"')");
				break;
			case DBType.ORACLE:
				idSeqMap.put(modelClass, idSequence+".nextval");
				break;
			case DBType.DM:
				idSeqMap.put(modelClass, idSequence+".nextval");
				break;
			}
			return;
		}
		//判断是不是序列类型ID 然后按照内置规则处理
		String idGenMode = idGenModeMap.get(modelClass);
		if (JBoltIDGenMode.SEQUENCE.equalsIgnoreCase(idGenMode) || JBoltIDGenMode.SEQUENCE_LONG.equalsIgnoreCase(idGenMode)) {
			String tableName = tableNameMap.get(modelClass);
			switch (dbType) {
			case DBType.POSTGRESQL:
				idSeqMap.put(modelClass, "nextval('"+tableName+"_idsq')");
				break;
			case DBType.ORACLE:
				idSeqMap.put(modelClass, tableName+"_idsq.nextval");
				break;
			case DBType.DM:
				idSeqMap.put(modelClass, tableName+"_idsq.nextval");
				break;
			}
		}
	}
	/**
	 * 添加自动缓存配置
	 * @param modelClass
	 */
	public void addAutoCache(Class<? extends Model> modelClass) {
		if(modelClass.isAnnotationPresent(JBoltAutoCache.class)) {
			autoCacheMap.put(modelClass,modelClass.getAnnotation(JBoltAutoCache.class));
		}
	}
	
	/**
	 * 配置主键和复合主键
	 * @param modelClass
	 * @param primaryKey
	 */
	private void addPrimaryKey(Class<? extends Model> modelClass, String primaryKey) {
		if(StrKit.isBlank(primaryKey)) {
			LOG.warn(String.format("[%s]中@TableBind未设置primaryKey,默认值 id", modelClass));
			primaryKeyMap.put(modelClass, JBoltConst.DEFAULT_ID_NAME);
			return;
		}
		if(primaryKey.indexOf(",") == -1) {
			primaryKeyMap.put(modelClass, primaryKey);
			return;
		}
		//复合多主键处理
		String[] compositePrimaryKeys = ArrayUtil.from3(primaryKey, ",");
		if (compositePrimaryKeys == null || compositePrimaryKeys.length == 0) {
			String msg = String.format("addPrimaryKey compositePrimaryKeys is null");
			LOG.error(msg);
			throw new RuntimeException(msg);
		}
		compositePrimaryKeyMap.put(modelClass, compositePrimaryKeys);
	}
	
	

	/**
	 * 获取id序列名称
	 * @param modelClass
	 * @return
	 */
	public String getIdSequenceStr(Class<? extends Model> modelClass) {
		return idSeqMap.get(modelClass);
	}
	
	
	/**
	 *  配置model映射的表的名字
	 * @param modelClass
	 * @param dataSource
	 */
	private void addTableName(Class<? extends Model> modelClass, String table) {
		tableNameMap.put(modelClass, table);
	}
	
	/**
	 *  配置数据源配置name
	 * @param modelClass
	 * @param dataSource
	 */
	private void addDatasource(Class<? extends Model> modelClass, String dataSource) {
		if(StrKit.isBlank(dataSource)) {
			LOG.warn(String.format("[%s]中@TableBind未设置datasource,默认使用了main主数据源", modelClass));
			datasourceMap.put(modelClass, DbKit.MAIN_CONFIG_NAME);
			return;
		}		
		datasourceMap.put(modelClass, dataSource);
	}
	
	/**
	 * 添加数据库类型配置
	 * @param modelClass
	 * @param dataSource
	 */
	private void addDbType(Class<? extends Model> modelClass,String dataSource) {
		if(StrKit.isBlank(dataSource) || DbKit.MAIN_CONFIG_NAME.equals(dataSource)) {
			dbTypeMap.put(modelClass, MainConfig.MAIN_DB_TYPE);
			return;
		}
		Setting setting = ExtendDatabaseConfig.me().getSetting();
		if(setting == null) {
			dbTypeMap.put(modelClass, MainConfig.MAIN_DB_TYPE);
			return;
		}
		
		dbTypeMap.put(modelClass, setting.getStr(JBoltConst.CONFIG_PROPERTIES_KEY_DB_TYPE,dataSource,MainConfig.MAIN_DB_TYPE));
	}
	
	/**
	 * 添加主键生成策略配置
	 * @param modelClass
	 * @param idGenMode
	 */
	private void addIdGenMode(Class<? extends Model> modelClass,String idGenMode) {
		if(StrKit.notBlank(MainConfig.FORCE_CAST_ALL_ID_GEN_MODE)) {
			idGenModeMap.put(modelClass, MainConfig.FORCE_CAST_ALL_ID_GEN_MODE);
			return;
		}
		if(StrKit.isBlank(idGenMode)) {
			idGenModeMap.put(modelClass, JBoltIDGenMode.AUTO);
			return;
		}
		idGenModeMap.put(modelClass, idGenMode);
	}
	
	/**
	 *  获取主键
	 * @param modelClass
	 * @return
	 */
	public String getPrimaryKey(Class<? extends Model> modelClass) {
		return primaryKeyMap.get(modelClass);
	}
	
	/**
	 *  获取符合主键
	 * @param modelClass
	 * @return
	 */
	public String[] getCompositePrimaryKeys(Class<? extends Model> modelClass) {
		return compositePrimaryKeyMap.get(modelClass);
	}
	/**
	 * 获取表名
	 * @param modelClass
	 * @return
	 */
	public String getTableName(Class<? extends Model> modelClass) {
		return tableNameMap.get(modelClass);
	}
	/**
	 * 获取数据源配置名
	 * @param modelClass
	 * @return
	 */
	public String getDataSourceConfigName(Class<? extends Model> modelClass) {
		return datasourceMap.get(modelClass);
	}
	/**
	 * 获取数据库类型
	 * @param modelClass
	 * @return
	 */
	public String getDbType(Class<? extends Model> modelClass) {
		return dbTypeMap.get(modelClass);
	}
	/**
	 * 获取id策略
	 * @param modelClass
	 * @return
	 */
	public String getIdGenMode(Class<? extends Model> modelClass) {
		return idGenModeMap.get(modelClass);
	}
	/**
	 * 获取autoCache配置
	 * @param modelClass
	 * @return
	 */
	public JBoltAutoCache getJBoltAutoCache(Class<? extends Model> modelClass) {
		return autoCacheMap.get(modelClass);
	}
	/**
	 * 获取tableBind配置
	 * @param modelClass
	 * @return
	 */
	public TableBind getTableBind(Class<? extends Model> modelClass) {
		return tableBindMap.get(modelClass);
	}
	/**
	 * 判断如果没有处理过配置就主动处理
	 * @param modelClass
	 */
	public void addConfigIfNotExist(Class<? extends Model> modelClass) {
		if(!tableBindMap.containsKey(modelClass)) {
			addConfig(modelClass);
		}
	}
}
