package cn.jbolt.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Record;
/**
 * JBoltTable表格整体提交数据JSON
 * @ClassName:  JBoltTable   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月23日   
 */
public class JBoltTable {
	public static final String PARAM_KEY="jboltTable";
	/**
	 * 需要执行删除的数据ids
	 */
	private Object[] delete;
	/**
	 * 需要执行更新的数据行
	 */
	private JSONArray update;
	/**
	 * 需要保存到数据库里的数据行
	 */
	private JSONArray save;
	/**
	 * 额外包含的表单数据 
	 */
	private JSONObject form;
	/**
	 * 额外的传递参数
	 */
	private JSONObject params;
	public JSONArray getUpdate() {
		return update;
	}
	public void setUpdate(JSONArray update) {
		this.update = update;
	}
	public JSONObject getForm() {
		return form;
	}
	public void setForm(JSONObject form) {
		this.form = form;
	}
	public JSONArray getSave() {
		return save;
	}
	public void setSave(JSONArray save) {
		this.save = save;
	}
	/**
	 * 转JSON
	 * @return
	 */
	public String toJson() {
		return JsonKit.toJson(this);
	}
	public Object[] getDelete() {
		return delete;
	}
	public void setDelete(Object[] delete) {
		this.delete = delete;
	}
	/**
	 * 判断提交数据全部为空
	 * @return
	 */
	public boolean isBlank() {
		return deleteIsBlank() && saveIsBlank() && updateIsBlank() && formIsBlank() && paramsIsBlank();
	}
	/**
	 * 判断提交数据是否不为空
	 * @return
	 */
	public boolean isNotBlank() {
		return deleteIsNotBlank() || saveIsNotBlank() || updateIsNotBlank() || formIsNotBlank() || paramsIsNotBlank();
	}
	/**
	 * 判断是否携带params数据提交 为空
	 * @return
	 */
	public boolean paramsIsBlank() {
		return params == null || params.isEmpty();
	}
	/**
	 * 判断是否携带params数据提交 不为空
	 * @return
	 */
	public boolean paramsIsNotBlank() {
		return params != null && params.isEmpty() == false;
	}
	/**
	 * 判断是否携带Form数据提交 为空
	 * @return
	 */
	public boolean formIsBlank() {
		return form == null || form.isEmpty();
	}
	/**
	 * 判断是否携带Form数据提交 不为空
	 * @return
	 */
	public boolean formIsNotBlank() {
		return form != null && form.isEmpty() == false;
	}
	/**
	 * 判断是否有待更新的数据 为空
	 * @return
	 */
	public boolean updateIsBlank() {
		return update == null || update.isEmpty();
	}
	/**
	 * 判断是否有待更新的数据 不为空
	 * @return
	 */
	public boolean updateIsNotBlank() {
		return update != null && update.isEmpty() == false;
	}
	/**
	 * 判断是否有待保存的数据 为空
	 * @return
	 */
	public boolean saveIsBlank() {
		return save == null || save.isEmpty();
	}
	/**
	 * 判断是否有待保存的数据 不为空
	 * @return
	 */
	public boolean saveIsNotBlank() {
		return save != null && save.isEmpty() == false;
	}
	/**
	 * 判断是否有待删除的数据 为空
	 * @return
	 */
	public boolean deleteIsBlank() {
		return delete == null || delete.length == 0;
	}
	/**
	 * 判断是否有待删除的数据 不为空
	 * @return
	 */
	public boolean deleteIsNotBlank() {
		return delete != null && delete.length > 0;
	}
	/**
	 * 拿到需要更新的ModelList
	 * @param <T>
	 * @param modelClass
	 * @return
	 */
	public <T> List<T> getUpdateModelList(Class<T> modelClass) {
		return getUpdateBeanList(modelClass);
	}
	/**
	 * 拿到需要更新的BeanList
	 * @param <T>
	 * @param beanClass
	 * @return
	 */
	public <T> List<T> getUpdateBeanList(Class<T> beanClass) {
		if(updateIsBlank()) {
			return null;
		}
		return update.toJavaList(beanClass);
	}
	/**
	 * 拿到需要更新的RecordList
	 * @param <T>
	 * @param beanClass
	 * @return
	 */
	public List<Record> getUpdateRecordList() {
		return getInnerRecordList(update);
	}
	
	
	/**
	 * 拿到需要保存的ModelList
	 * @param <T>
	 * @param modelClass
	 * @return
	 */
	public <T> List<T> getSaveModelList(Class<T> modelClass) {
		return getSaveBeanList(modelClass);
	}
	/**
	 * 拿到需要保存的BeanList
	 * @param <T>
	 * @param beanClass
	 * @return
	 */
	public <T> List<T> getSaveBeanList(Class<T> beanClass) {
		if(saveIsBlank()) {
			return null;
		}
		return save.toJavaList(beanClass);
	}
	/**
	 * 拿到需要保存的RecordList
	 * @param <T>
	 * @param beanClass
	 * @return
	 */
	public List<Record> getSaveRecordList() {
		return getInnerRecordList(save);
	}
	
	/**
	 * 内部底层实现recordList转换
	 * @param jsonArray
	 * @return
	 */
	private List<Record> getInnerRecordList(JSONArray jsonArray){
		if(jsonArray==null || jsonArray.isEmpty()) {
			return null;
		}
		List<Record> records=new ArrayList<Record>();
		int size=jsonArray.size();
		for(int i=0;i<size;i++) {
			records.add(new Record().setColumns(jsonArray.getJSONObject(i).getInnerMap()));
		}
		return records;
	}
	
	/**
	 * 从form中拿到指定Model
	 * @param <T>
	 * @param modelClass
	 * @return
	 */
	public <T> T getFormModel(Class<T> modelClass) {
		return getFormBean(modelClass);
	}
	/**
	 * 从form中拿到指定Model
	 * 模型驱动模式 带着modelName
	 * @param <T>
	 * @param modelClass
	 * @param modelName
	 * @return
	 */
	public <T> T getFormModel(Class<T> modelClass,String modelName) {
		if(StrKit.isBlank(modelName)) {
			return getFormModel(modelClass);
		}
		if(formIsBlank()) {
			return null;
		}
		return JBoltInjector.injectModel(modelClass, modelName, form.getInnerMap());
	}
	
	/**
	 * 从form中拿到指定bean
	 * @param <T>
	 * @param modelClass
	 * @return
	 */
	public <T> T getFormBean(Class<T> beanClass) {
		if(formIsBlank()) {
			return null;
		}
		return form.toJavaObject(beanClass);
	}
	/**
	 * 从form中拿到指定bean
	 * 模型驱动模式 带着beanName
	 * @param <T>
	 * @param beanClass
	 * @param beanName
	 * @return
	 */
	public <T> T getFormBean(Class<T> beanClass,String beanName) {
		if(StrKit.isBlank(beanName)) {
			return getFormBean(beanClass);
		}
		if(formIsBlank()) {
			return null;
		}
		return JBoltInjector.injectBean(beanClass, beanName, form.getInnerMap());
	}
	
	/**
	 * 从form中拿到Record
	 * @return
	 */
	public Record getFormRecord() {
		if(formIsBlank()) {
			return null;
		}
		return new Record().setColumns(form.getInnerMap());
	}
	
	public JSONObject getParams() {
		return params;
	}
	public void setParams(JSONObject params) {
		this.params = params;
	}
	
	/**
	 * 从params中拿到指定Model
	 * @param <T>
	 * @param modelClass
	 * @return
	 */
	public <T> T getParamsModel(Class<T> modelClass) {
		return getParamsBean(modelClass);
	}
	/**
	 * 从params中拿到指定Model
	 * 模型驱动模式 带着modelName
	 * @param <T>
	 * @param modelClass
	 * @param modelName
	 * @return
	 */
	public <T> T getParamsModel(Class<T> modelClass,String modelName) {
		if(StrKit.isBlank(modelName)) {
			return getParamsModel(modelClass);
		}
		if(paramsIsBlank()) {
			return null;
		}
		return JBoltInjector.injectModel(modelClass, modelName, params.getInnerMap());
	}
	
	/**
	 * 从params中拿到指定bean
	 * @param <T>
	 * @param modelClass
	 * @return
	 */
	public <T> T getParamsBean(Class<T> beanClass) {
		if(paramsIsBlank()) {
			return null;
		}
		return params.toJavaObject(beanClass);
	}
	/**
	 * 从params中拿到指定bean
	 * 模型驱动模式 带着beanName
	 * @param <T>
	 * @param beanClass
	 * @param beanName
	 * @return
	 */
	public <T> T getParamsBean(Class<T> beanClass,String beanName) {
		if(StrKit.isBlank(beanName)) {
			return getParamsBean(beanClass);
		}
		if(paramsIsBlank()) {
			return null;
		}
		return JBoltInjector.injectBean(beanClass, beanName, params.getInnerMap());
	}
	
	/**
	 * 从params中拿到Record
	 * @return
	 */
	public Record getParamRecord() {
		if(paramsIsBlank()) {
			return null;
		}
		return new Record().setColumns(params.getInnerMap());
	}
	
	public Integer getParamToInt(String paramName) {
		return paramsIsBlank()?null:params.getInteger(paramName);
	}
	public Long getParamToLong(String paramName) {
		return paramsIsBlank()?null:params.getLong(paramName);
	}
	public String getParamToStr(String paramName) {
		return paramsIsBlank()?null:params.getString(paramName);
	}
	public BigDecimal getParamToBigDecimal(String paramName) {
		return paramsIsBlank()?null:params.getBigDecimal(paramName);
	}
	public Boolean getParamToBool(String paramName) {
		return paramsIsBlank()?null:params.getBoolean(paramName);
	}
	public Double getParamToDouble(String paramName) {
		return paramsIsBlank()?null:params.getDouble(paramName);
	}
	public Date getParamToDate(String paramName) {
		return paramsIsBlank()?null:params.getDate(paramName);
	}
	public Object getParamToObject(String paramName) {
		return paramsIsBlank()?null:params.get(paramName);
	}
}
