package cn.jbolt.base.json;

import com.jfinal.json.IJsonFactory;
import com.jfinal.json.Json;
/**
 * JBolt自定义FastJson工厂
 * @ClassName:  JBoltFastJsonFactory   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年10月8日   
 */
public class JBoltFastJsonFactory implements IJsonFactory {
	
	private static final JBoltFastJsonFactory me = new JBoltFastJsonFactory();
	
	public static JBoltFastJsonFactory me() {
		return me;
	}
	
	public Json getJson() {
		return new JBoltFastJson();
	}
	
}
