package cn.jbolt.base.json;

import java.lang.reflect.Type;
import java.sql.Time;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.ParserConfig;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.jfinal.json.FastJsonRecordSerializer;
import com.jfinal.json.Json;
import com.jfinal.plugin.activerecord.Record;
/**
 * JBolt自定义FastJson
 * @ClassName:  JBoltFastJson   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年10月8日   
 */
public class JBoltFastJson extends Json {
	
	static {
		// 支持序列化 ActiveRecord 的 Record 类型
		addSerializer(Record.class, new FastJsonRecordSerializer());
		// Long转String 防止js 精度丢失
		addSerializer(Long.class, ToStringSerializer.instance);
		//time 反序列化处理
		addDeserializer(Time.class, JBoltSqlTimeDeserializer.instance);
		// 完全禁用 autoType，提升安全性
		try {
			ParserConfig.getGlobalInstance().setSafeMode(true);
		} catch (Exception e) {
			// 老版本 fastjson 无 setSafeMode(boolean) 方法
			com.jfinal.kit.LogKit.logNothing(e);
		}
	}
	@Override
	public String toJson(Object object) {
		// 优先使用对象级的属性 datePattern, 然后才是全局性的 defaultDatePattern
		String dp = datePattern != null ? datePattern : getDefaultDatePattern();
		if (dp == null) {
			return JSON.toJSONString(object,SerializerFeature.DisableCircularReferenceDetect);
		} else {
			return JSON.toJSONStringWithDateFormat(object, dp, SerializerFeature.WriteDateUseDateFormat,SerializerFeature.DisableCircularReferenceDetect);	// return JSON.toJSONString(object, SerializerFeature.WriteDateUseDateFormat);
		}
	}
	@Override
	public <T> T parse(String jsonString, Class<T> type) {
		return JSON.parseObject(jsonString, type);
	}
	
	/**
	 * 添加序列化配置
	 * @param type
	 * @param value
	 */
	public static void addSerializer(Type type, ObjectSerializer value) {
		SerializeConfig.getGlobalInstance().put(type, value);
	}
	/**
	 * 添加反序列化配置
	 * @param type
	 * @param value
	 */
	public static void addDeserializer(Type type, ObjectDeserializer value) {
		ParserConfig.getGlobalInstance().putDeserializer(type, value);
	}
}
