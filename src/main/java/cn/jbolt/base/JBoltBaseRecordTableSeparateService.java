package cn.jbolt.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbTemplate;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.jbolt.base.para.jbolttablemenufilter.JBoltTableMenuFilter;
import cn.jbolt.common.bean.JsTreeBean;
import cn.jbolt.common.config.MainConfig;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.DBType;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CamelCaseUtil;
import cn.jbolt.common.util.TreeCheckIsParentNode;

/**
 * 采用Db+record模式实现 分表版 需要母表版
 * @ClassName:  BaseRecordService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年7月20日   
 */
public abstract class JBoltBaseRecordTableSeparateService<M extends JBoltBaseModel<M>> extends JBoltCommonService{
	/**
	 * 抽象方法定义 dao 让调用者自己实现
	 * @return
	 */
	protected abstract M mainTableModelDao();

	/**
	 *  获取表名
	 * @return
	 */
	private String mainTableName(){
		if(tableName==null){
			tableName=mainTableModelDao()._getTableName();
		}
		return tableName;
	}
	@Override
	public String dataSourceConfigName() {
		if(dataSourceConfigName==null){
			dataSourceConfigName=mainTableModelDao()._getDataSourceConfigName();
		}
		return dataSourceConfigName;
	}

	@Override
	public String dbType() {
		if(dbType==null){
			dbType=mainTableModelDao()._getDbType();
		}
		return dbType;
	}
	@Override
	public String primaryKey() {
		if(primaryKey==null){
			primaryKey=mainTableModelDao()._getPrimaryKey();
		}
		return primaryKey;
	}
	

	@Override
	public String idGenMode() {
		if(idGenMode==null){
			idGenMode=mainTableModelDao()._getIdGenMode();
		}
		return idGenMode;
	}
	
	/**
	 * 获取序列名 默认就是表名
	 * @param _id
	 * @return
	 */
	public String idSequence(Object _id) {
		return table(_id);
	}
	 
	/**
	 *  获取Sql模板 Db模式
	 * @param _id 多表模式指定后缀ID
	 * @param key
	 * @param data
	 * @return
	 */
	public DbTemplate dbTemplate(Object _id,String key,Kv data) {
		data.setIfNotBlank("table", table(_id));
		return Db.use(dataSourceConfigName()).template(key, data);
	}

	/**
	 * 得到下拉列表数据
	 * @param _id 多表模式指定后缀ID
	 * @param textColumn
	 * @param valueColumn
	 * @param paras
	 * @return
	 */
	public List<Record> getOptionList(Object _id,String textColumn,String valueColumn,Kv paras){
		Kv conf=Kv.by("value",valueColumn).set("text",textColumn).set("myparas", paras).set("customCompare",false);
		return dbTemplate(_id,"common.optionlist", conf).find();
	}
	

	/**
	 * 得到下拉列表数据
	 * @param _id 多表模式指定后缀ID
	 * @param textColumn
	 * @param valueColumn
	 * @return
	 */
	public List<Record> getOptionList(Object _id,String textColumn,String valueColumn){
		Kv conf=Kv.by("value",valueColumn).set("text",textColumn).set("customCompare",false);
		return dbTemplate(_id,"common.optionlist", conf).find();
	}
	/**
	 * 得到下拉列表数据
	 * @param _id 多表模式指定后缀ID
	 * @return
	 */
	public List<Record> getOptionList(Object _id){
		Kv conf=Kv.by("value",primaryKey()).set("text","name").set("customCompare",false);
		return dbTemplate(_id,"common.optionlist", conf).find();
	}
	 
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 自定义参数compare
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public List<Record> getCommonList(Object _id,String columns,Kv paras,boolean customCompare){
		return dbTemplate(_id,"common.list", 
				Kv.by("customCompare",customCompare)
				.setIfNotNull("myparas", paras)
				.setIfNotBlank("columns",columns))
				.find();
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 可以自定义参数compare 默认=
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public List<Record> getCommonList(Object _id,Kv paras,boolean customCompare){
		return getCommonList(_id,"*",paras, customCompare);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 不能自定义参数compare 默认=
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @return
	 */
	public List<Record> getCommonList(Object _id,Kv paras){
		return getCommonList(_id,"*",paras, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 不能自定义参数compare 默认=
	 * @param _id 多表模式指定后缀ID
	 * @param columns 指定查询的列
	 * @param paras
	 * @return
	 */
	public List<Record> getCommonList(Object _id,String columns,Kv paras){
		return getCommonList(_id,columns,paras,false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 默认正序 
	 * 不能自定义参数compare 默认=
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param orderColums
	 * @return
	 */
	public List<Record> getCommonList(Object _id,Kv paras,String orderColums){
		int count=StrUtil.count(orderColums, ",");
		String orderTypes="";
		for(int i=0;i<=count;i++){
			if(i==0){
				orderTypes="asc";
			}else{
				orderTypes=orderTypes+","+"asc";
			}
		}
		return getCommonList(_id,"*",paras, orderColums, orderTypes, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @return
	 */
	public List<Record> getCommonList(Object _id,Kv paras,String orderColumns,String orderTypes){
		return getCommonList(_id,"*",paras, orderColumns, orderTypes, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param sort
	 * @param orderType
	 * @return
	 */
	public List<Record> getCommonList(Object _id,String columns,Kv paras,String orderColumns,String orderTypes){
		return getCommonList(_id,columns,paras, orderColumns, orderTypes, false);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param orderColumns
	 * @param orderType
	 * @return
	 */
	public List<Record> getCommonList(Object _id,String columns,String orderColumns,String orderTypes){
		return getCommonList(_id,columns,null, orderColumns, orderTypes, false);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 自定义参数compare
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param orderColumn
	 * @param orderType
	 * @param customCompare
	 * @return
	 */
	public List<Record> getCommonList(Object _id,String columns,Kv paras,String orderColumns,String orderTypes,boolean customCompare){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		if(isOk(columns)){
			conf.set("columns",columns);
		}
		if(isOk(orderColumns)){
			conf.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			conf.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return dbTemplate(_id,"common.list",conf).find();
	}
	
	/**
	 * 分页查询
	 * @param _id
	 * @param sql
	 * @param pageNumber
	 * @param pageSize
	 * @param columnToCamelCase 列名转驼峰
	 * @return
	 */
	public Page<Record> paginate(Object _id,Sql sql,int pageNumber,int pageSize,boolean columnToCamelCase){
		Sql totalCountSql=ObjectUtil.clone(sql);
		totalCountSql.count();
		sql.page(pageNumber, pageSize);
		return paginate(_id,sql, totalCountSql,columnToCamelCase);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 自定义参数compare
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @param or
	 * @return
	 */
	public Page<Record> paginate(Object _id,String columns,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare,boolean or){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		conf.set("or",or);
		conf.setIfNotBlank("columns",columns);
		if(isOk(orderColumns)){
			conf.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			conf.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return dbTemplate(_id,"common.list",conf).paginate(pageNumber, pageSize);
	}
	
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(Object _id,String columns,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate(_id,columns, paras, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @param or
	 * @return
	 */
	public Page<Record> paginate(Object _id,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare,boolean or){
		return paginate(_id,"*", paras, orderColumns, orderTypes, pageNumber, pageSize, customCompare,or);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param orderColumns
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Object _id,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare){
		return paginate(_id,"*", paras, orderColumns, orderTypes, pageNumber, pageSize, customCompare,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(Object _id,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate(_id,"*", paras, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(Object _id,String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate(_id,"*", null, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不尅一可以排序
	 * 条件自定义 customCompare
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Object _id,String columns,Kv paras,int pageNumber, int pageSize,boolean customCompare){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		if(isOk(columns)){
			conf.set("columns",columns);
		}
		return dbTemplate(_id,"common.list",conf).paginate(pageNumber, pageSize);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 条件自定义 customCompare
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Object _id,Kv paras,int pageNumber, int pageSize,boolean customCompare){
		return paginate(_id,"*", paras, pageNumber, pageSize, customCompare);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Object _id,Kv paras,int pageNumber, int pageSize){
		return paginate(_id,"*", paras, pageNumber, pageSize, false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Object _id,Kv paras,int pageSize,boolean customCompare){
		return paginate(_id,"*", paras, 1, pageSize, customCompare);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(Object _id,int pageNumber,int pageSize){
		return paginate(_id,"*", null, pageNumber, pageSize, false);
	}
	/**
	 * 按照sql模板分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param key
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginateBySqlTemplate(Object _id,String key,Kv data,int pageNumber,int pageSize){
		return dbTemplate(_id,key, data).paginate(pageNumber, pageSize);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(Object _id,Kv paras,int pageSize){
		return paginate(_id,"*", paras, 1, pageSize, false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param _id 多表模式指定后缀ID
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(Object _id,int pageSize){
		return paginate(_id,"*", null, 1, pageSize, false);
	}
	/**
	 * 根据ID 批量删除 指定分隔符版
	 * @param ids
	 * @param split 分隔符
	 * @return
	 */
	public Ret deleteByIds(Object _id,String ids,String split,boolean checkCanDelete) {
		return deleteByIds(_id,ArrayUtil.from3(ids, split),checkCanDelete);
	}
	/**
	 * 根据ID 批量删除 指定分隔符版
	 * @param ids
	 * @param split 分隔符
	 * @return
	 */
	public Ret deleteByIds(Object _id,String ids,String split) {
		return deleteByIds(_id,ids,split,false);
	}
	
	/**
	 * 根据ID 批量删除 默认分隔符是逗号 
	 * @param ids
	 * @return
	 */
	public Ret deleteByIds(Object _id,String ids) {
		return deleteByIds(_id,ids,false);
	}
	/**
	 * 根据ID 批量删除 默认分隔符是逗号
	 * @param ids
	 * @param checkCanDelete
	 * @return
	 */
	public Ret deleteByIds(Object _id,String ids,boolean checkCanDelete) {
		return deleteByIds(_id,ids,",",checkCanDelete);
	}
	
	/**
	 * 根据ID 批量删除
	 * @param ids
	 * @return
	 */
	public Ret deleteByIds(Object _id,Object[] ids) {
		return deleteByIds(_id,ids,false);
	}
	/**
	 * 根据ID 批量删除
	 * @param _id
	 * @param ids
	 * @param checkCanDelete
	 * @return
	 */
	public Ret deleteByIds(Object _id,Object[] ids,boolean checkCanDelete) {
		if(notOk(ids)) {return fail(Msg.PARAM_ERROR);}
		Ret ret;
		for(Object id:ids) {
			ret=deleteById(_id,id,checkCanDelete);
			if(ret.isFail()) {
				return ret;
			}
		}
		return SUCCESS;
	}
	
	/**
	 * 通用根据ID删除数据
	 * @param _id 多表模式指定后缀ID
	 * @param id
	 * @return
	 */
	public Ret deleteById(Object _id,Object id){
		return deleteById(_id,id,null);
	}
	/**
	 * 通用根据ID删除数据
	 * @param _id 多表模式指定后缀ID
	 * @param id
	 * @return
	 */
	public Ret deleteById(Object _id,Object id,boolean checkCanDelete){
		return deleteById(_id,id,checkCanDelete,null);
	}
	
	/**
	 * 通用根据ID删除数据
	 * @param _id 多表模式指定后缀ID
	 * @param id
	 * @param kv 额外数据
	 * @return
	 */
	public Ret deleteById(Object _id,Object id,Kv kv){
		return deleteById(_id,id, false,kv);
	}
	
	/**
	 * 通用根据ID删除数据
	 * @param _id 多表模式指定后缀ID
	 * @param id
	 * @param checkCanDelete
	 * @param kv 额外数据
	 * @return
	 */
	public Ret deleteById(Object _id,Object id,boolean checkCanDelete,Kv kv){
		return deleteById(_id,id, checkCanDelete,true,kv);
	}
	 
	
	/**
	 * 通用根据ID删除数据 需要先检查是否被其他地方使用
	 * @param _id 多表模式指定后缀ID
	 * @param id
	 * @param checkInUse
	 * @param checkCanDelete
	 * @param returnDeleteData
	 * @param kv 额外数据
	 * @return
	 */
	public Ret deleteById(Object _id,Object id,boolean checkCanDelete,boolean returnDeleteData,Kv kv){
		if(MainConfig.DEMO_MODE) {return fail(Msg.DEMO_MODE_CAN_NOT_DELETE);}
		if(notOk(id)){
			return fail(Msg.PARAM_ERROR);
		}
		Record record=findById(_id,id);
		if(record==null){
			return fail(Msg.DATA_NOT_EXIST);
		}
		if(checkCanDelete){
			String msg=checkCanDelete(_id,record,kv);
			if(StrKit.notBlank(msg)){return fail(msg);}
		}
		
		Ret ret=delete(_id, record);
		if(ret.isOk()) {
			String msg=afterDelete(_id,record,kv);
			if(isOk(msg)){return fail(msg);}
		}
		if(returnDeleteData) {
			return ret.isOk()?success(record,Msg.SUCCESS):ret;
		}
		return ret;
	}
	
	/**
	 * 删除成功后需要处理的事情
	 * @param _id 多表模式指定后缀ID
	 * @param record
	 * @param kv
	 * @return
	 */
	protected String afterDelete(Object _id,Record record,Kv kv){
		return null;
	}
	
	/**
	 * 检测数据是否可以删除
	 * @param _id 多表模式指定后缀ID
	 * @param record
	 * @param kv
	 * @return
	 */
	public String checkCanDelete(Object _id,Record record,Kv kv){
		return null;
	}
	
	/**
	 * 检测数据是否被其它数据外键引用
	 * @param _id 多表模式指定后缀ID
	 * @param record
	 * @param kv
	 * @return
	 */
	public String checkInUse(Object _id,Record record,Kv kv){
		return null;
	}
	/**
	 * 检测数据是否字段是否可以执行切换true false
	 * @param _id 多表模式指定后缀ID
	 * @param record
	 * @param column
	 * @param kv
	 * @return
	 */
	public String checkCanToggle(Object _id,Record record,String column,Kv kv){
		return null;
	}
	/**
	 * 额外需要处理toggle操作
	 * @param _id 多表模式指定后缀ID
	 * @param record
	 * @param column
	 * @param kv
	 * @return
	 */
	public String afterToggleBoolean(Object _id,Record record,String column,Kv kv){
		return null;
	}
	
	/**
	 * 判断字典名是否存在 排除指定ID
	 * @param _id 多表模式指定后缀ID
	 * @param name
	 * @param id
	 * @return
	 */
	public boolean existsName(Object _id,String name,Object id) {
		name = name.toLowerCase().trim();
		Sql sql=selectSql(_id).selectId().eqQM("name").idNoteqQM().first();
		Object existId = queryColumn(sql, name, id);
		return isOk(existId);
	}

	/**
	 * 判断是否存在
	 * @param _id 多表模式指定后缀ID
	 * @param columnName
	 * @param value
	 * @param id
	 * @return
	 */
	public boolean exists(Object _id,String columnName,Serializable value,Object id) {
		value = value.toString().trim();
		Sql sql=selectSql(_id).selectId().eqQM(columnName).idNoteqQM().first();
		Object existId = queryColumn(sql, value,id);
		return isOk(existId);
	}
	/**
	 * 判断是否存在
	 * @param _id 多表模式指定后缀ID
	 * @param columnName
	 * @param value
	 * @return
	 */
	public boolean exists(Object _id,String columnName,Serializable value) {
		return exists(_id,columnName, value, -1);
	}
	/**
	 * 判断字典名是否存在
	 * @param _id 多表模式指定后缀ID
	 * @param name
	 * @return
	 */
	public boolean existsName(Object _id,String name) {
		return existsName(_id,name, -1);
	}
	/**
	 * 根据ID获得一条数据
	 * @param _id 多表模式指定后缀ID
	 * @param id
	 * @return
	 */
	public Record findById(Object _id,Object id) {
		if(notOk(id)){return null;}
		return Db.use(dataSourceConfigName()).findById(table(_id),id);
	}
	/**
	 * 得到符合条件的第一个
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @return
	 */
	public Record findFirst(Object _id,Kv paras) {
		return findFirst(_id,paras,false);
	}
	/**
	 * 得到符合条件的第一个
	 * @param _id 多表模式指定后缀ID
	 * @return
	 */
	public Record findFirst(Object _id) {
		return findFirst(_id,null,false);
	}
	/**
	 * 得到符合条件的第一个
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public Record findFirst(Object _id,Kv paras,boolean customCompare) {
		return dbTemplate(_id,"common.first", Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras)).findFirst();
	}
	/**
	 * 随机得到符合条件的第一个
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @return
	 */
	public Record getRandomOne(Object _id,Kv paras) {
		return getRandomOne(_id,paras, false);
	}
	/**
	 * 随机得到符合条件的第一个
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public Record getRandomOne(Object _id,Kv paras,boolean customCompare) {
		Kv conf=Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras);
		conf.set("orderColumns",new String[] {"rand()"});
		conf.set("orderTypes",new String[] {"asc"});
		return dbTemplate(_id,"common.firstrand", conf).findFirst();
	}
	/**
	 * 根据条件删除数据
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public Ret deleteBy(Object _id,Kv paras,boolean customCompare) {
		if(MainConfig.DEMO_MODE) {return fail(Msg.DEMO_MODE_CAN_NOT_DELETE);}
		dbTemplate(_id,"common.delete", Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras)).delete();
		return SUCCESS;
	}
	/**
	 * 根据条件删除数据
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @return
	 */
	public Ret deleteBy(Object _id,Kv paras) {
		return deleteBy(_id,paras, false);
	}


	/**
	 * 切换Boolean类型字段
	 * @param _id 多表模式指定后缀ID
	 * @param id 需要切换的数据ID
	 * @param columns 需要切换的字段列表
	 * @return
	 */
	public Ret toggleBoolean(Object _id,Object id,String... columns) {
		return toggleBoolean(_id,null, id, columns);
	}
	/**
	 * 保存
	 * @param _id
	 * @param record
	 * @return
	 */
	public Ret save(Object _id,Record record) {
		return save(_id, record, primaryKey());
	}
	/**
	 * 保存
	 * @param _id
	 * @param record
	 * @param primaryKeyName
	 * @return
	 */
	public Ret save(Object _id,Record record,String primaryKeyName) {
		if(record==null||notOk(primaryKeyName)) {
			return fail(Msg.PARAM_ERROR);
		}
		Object idValue = record.getObject(primaryKeyName);
		String idGenMode = idGenMode();
		//如果不需要自己赋值 但是有值了 就报错
		if(!JBoltIDGenMode.isNeedAssign(idGenMode) && isOk(idValue)) {
			return fail(Msg.PARAM_ERROR);
		}
		//如果需要赋值 但是没有赋值
		if(JBoltIDGenMode.isNeedAssign(idGenMode) && notOk(idValue)) {
			//那就自动赋值
			if(StrKit.notBlank()) {
				switch (idGenMode) {
				case JBoltIDGenMode.SNOWFLAKE:
					record.set(primaryKeyName, JBoltSnowflakeKit.me.nextId());
					break;
				case JBoltIDGenMode.UUID:
					record.set(primaryKeyName, IdUtil.fastSimpleUUID());
					break;
				case JBoltIDGenMode.SEQUENCE:
					record.set(primaryKeyName, getIdSequenceStr(_id));
					break;
				case JBoltIDGenMode.SEQUENCE_LONG:
					record.set(primaryKeyName, getIdSequenceStr(_id));
					break;
				}
			}
		}
		boolean success=Db.use(dataSourceConfigName()).save(table(_id), record);
		return ret(success);
	}
	
	/**
	 * 获得id主键 自增序列配置
	 * @param _id
	 * @param idSequence 
	 */
	private String getIdSequenceStr(Object _id) {
		String dbType = dbType();
		String idSequence = idSequence(_id);
		String idSequenceStr=null;
		//如果不是空并且不是default 说明是自己指定的 直接赋值即可
		if(StrKit.notBlank(idSequence) && JBoltConst.JBOLT_ID_SEQUENCE_DEFAULT.equals(idSequence) == false) {
			switch (dbType) {
			case DBType.POSTGRESQL:
				idSequenceStr = "nextval('"+idSequence+"')";
				break;
			case DBType.ORACLE:
				idSequenceStr = idSequence+".nextval";
				break;
			case DBType.DM:
				idSequenceStr = idSequence+".nextval";
				break;
			}
			return idSequenceStr;
		}
		//判断是不是序列类型ID 然后按照内置规则处理
		String idGenMode = idGenMode();
		if (JBoltIDGenMode.SEQUENCE.equalsIgnoreCase(idGenMode) || JBoltIDGenMode.SEQUENCE_LONG.equalsIgnoreCase(idGenMode)) {
			String tableName = table(_id);
			switch (dbType) {
			case DBType.POSTGRESQL:
				idSequenceStr = "nextval('"+tableName+"_idsq')";
				break;
			case DBType.ORACLE:
				idSequenceStr =  tableName+"_idsq.nextval";
				break;
			case DBType.DM:
				idSequenceStr =  tableName+"_idsq.nextval";
				break;
			}
		}
		return idSequenceStr;
	}
	 
	/**
	 * 删除
	 * @param _id
	 * @param record
	 * @return
	 */
	protected Ret delete(Object _id,Record record) {
		return delete(_id, record, primaryKey());
	}
	
	/**
	 * 删除
	 * @param _id
	 * @param record
	 * @return
	 */
	protected Ret delete(Object _id,Record record,String primaryKeyName) {
		if(record==null||notOk(primaryKeyName)||notOk(record.getObject(primaryKeyName))) {
			return fail(Msg.PARAM_ERROR);
		}
		boolean success=Db.use(dataSourceConfigName()).delete(table(_id), record);
		return ret(success);
	}
	
	
	/**
	 * 更新
	 * @param _id
	 * @param record
	 * @return
	 */
	protected Ret update(Object _id,Record record) {
		return update(_id, record, primaryKey());
	}
	/**
	 * 更新
	 * @param _id
	 * @param record
	 * @return
	 */
	protected Ret update(Object _id,Record record,String primaryKeyName) {
		if(record==null||notOk(primaryKeyName)||notOk(record.getObject(primaryKeyName))) {
			return fail(Msg.PARAM_ERROR);
		}
		boolean success=Db.use(dataSourceConfigName()).update(table(_id), record);
		return ret(success);
	}
	
	
	/**
	 * 切换Boolean类型字段值
	 * @param _id 多表模式指定后缀ID
	 * @param kv 额外传入的参数 用于 toggleExtra里用
	 * @param id 需要切换的数据ID
	 * @param columns 需要切换的字段列表
	 * @return
	 */
	protected Ret toggleBoolean(Object _id,Kv kv,Object id,String... columns) {
		if(notOk(id)){
			return fail(Msg.PARAM_ERROR);
		}
		Record record=findById(_id,id);
		if(record==null){
			return fail(Msg.DATA_NOT_EXIST);
		}
		
			Boolean value;
			for(String column:columns){
				String msg=checkCanToggle(_id,record,column,kv);
				if(StrKit.notBlank(msg)){
					return fail(msg);
				}
				value=record.getBoolean(column);
				record.set(column, value==null?true:!value);
				//处理完指定这个字段 还需要额外处理什么？
				msg=afterToggleBoolean(_id,record,column,kv);
				if(StrKit.notBlank(msg)){
					return fail(msg);
				}
			}
			Ret ret=update(_id, record);
			return ret.isOk()?success(record,Msg.SUCCESS):ret;
		
	}
	
	/**
	 * 常用的得到列表数据数量
	 * 自定义参数compare
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	protected int getCount(Object _id,Kv paras,boolean customCompare){
		return dbTemplate(_id,"common.count",Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras)).queryInt();
	}
	/**
	 * 常用的得到列表数据数量
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @return
	 */
	protected int getCount(Object _id,Kv paras){
		return getCount(_id,paras, false);
	}
	

	/**
	 * 得到新数据的排序Rank值 默认从1开始 不带任何查询条件
	 * @param _id 多表模式指定后缀ID
	 * @return
	 */
	protected int getNextSortRank(Object _id){
		return getNextSortRank(_id,null, false);
	}
	/**
	 * 得到新数据的排序Rank值 从0开始 不带任何查询条件
	 * @param _id 多表模式指定后缀ID
	 * @return
	 */
	protected int getNextRankFromZero(Object _id){
		return getNextSortRank(_id,null, true);
	}
	/**
	 * 得到新数据的排序Rank值 从0开始 带查询条件
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @return
	 */
	protected int getNextRankFromZero(Object _id,Kv paras){
		return getNextSortRank(_id,paras, true);
	}
	/**
	 * 得到新数据的排序Rank值 自带简单条件查询默认从1开始
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @return
	 */
	protected int getNextSortRank(Object _id,Kv paras){
		return getNextSortRank(_id,paras, false);
	}
	/**
	 * 得到新数据的排序Rank值 自带简单条件查询 可以自定义是否从零开始
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param fromZero
	 * @return
	 */
	protected int getNextSortRank(Object _id,Kv paras,boolean  fromZero){
		int count=getCount(_id,paras);
		if(fromZero){
			return count;
		}
		return count+1;
	}
	

	/**
	 * 得到新数据的排序Rank值 自带简单条件查询 可以自定义是否从零开始
	 * 条件可定制版
	 * @param _id 多表模式指定后缀ID
	 * @param paras
	 * @param customCompare
	 * @param fromZero
	 * @return
	 */
	protected int getNextSortRank(Object _id,Kv paras,Boolean customCompare,boolean  fromZero){
		int count=getCount(_id,paras, customCompare);
		if(fromZero){
			return count;
		}
		return count+1;
	}
	
	/**
	 * 常用的得到列表数据数量
	 * @param _id 多表模式指定后缀ID
	 * @return
	 */
	protected int getCount(Object _id){
		return getCount(_id,null, false);
	}
	
	
	

	/**
	 * 创建一个新表 如果存在 直接返回TRUE
	 * @param _id 多表模式指定后缀ID
	 * @param idGenMode 
	 * @return
	 */
	public boolean createTable(Object _id, String idGenMode) {
		String newTableName=table(_id);
		boolean ret=tableExist(newTableName);
		if(!ret) {
			ret=createTheTable(newTableName,mainTableName());
			if(ret&&(idGenMode.equals(JBoltIDGenMode.SEQUENCE)||idGenMode.equals(JBoltIDGenMode.SEQUENCE_LONG))) {
				//如果判断ID使用序列自增就需要动态创建序列了
				if(MainConfig.isOracle()||MainConfig.isDM()||MainConfig.isPostgresql()) {
					createIdSequence(newTableName);
				}
			}
		}
		return ret;
	}
	
	public boolean tableExist(Object _id) {
		String newTableName=table(_id);
		return tableExist(newTableName);
	}
	/**
	 * 获取表名
	 * @param _id 多表模式指定后缀ID
	 * @return
	 */
	protected String table(Object _id){
		String tableName=mainTableName();
		if(notOk(_id)) {return tableName;}
		return tableName+"_"+_id;
	}
	
	/**
	 * 更新同级删除数据之后数据的排序
	 * @param _id 多表模式指定后缀ID
	 * @param sortRank
	 */
	protected void updateSortRankAfterDelete(Object _id,Integer sortRank) {
		updateSortRankAfterDelete(table(_id),sortRank);
	}
	
	/**
	 * 更新同级删除数据之后数据的排序
	 * @param _id 多表模式指定后缀ID
	 * @param params
	 * @param sortRank
	 */
	protected void updateSortRankAfterDelete(Object _id,Kv params, Integer sortRank) {
		updateSortRankAfterDelete(table(_id), params, sortRank);
	}
	/**
	 * 删除关联子数据
	 * @param _id 多表模式指定后缀ID
	 * @param pid
	 * @return
	 */
	protected Ret deleteByPid(Object _id,Object pid) {
		return deleteBy(_id,Kv.by("pid", pid));
	}
	
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * 关键词为空的时候 返回空list
	 * @param _id 多表模式指定后缀ID
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<Record> getAutocompleteList(Object _id,String keywords, Integer limitCount,String matchColumns) {
		return getAutocompleteList(_id,keywords, limitCount, false, Sql.KEY_STAR, matchColumns,null);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * 关键词为空的时候 返回空list
	 * @param _id 多表模式指定后缀ID
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @param whereSql 额外条件sql
	 * @return
	 */
	public List<Record> getAutocompleteList(Object _id,String keywords, Integer limitCount,String matchColumns,Sql whereSql) {
		return getAutocompleteList(_id,keywords, limitCount, false, Sql.KEY_STAR, matchColumns,whereSql);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * @param _id 多表模式指定后缀ID
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<Record> getAutocompleteList(Object _id,String keywords, Integer limitCount,Boolean always,String matchColumns) {
		return getAutocompleteList(_id,keywords, limitCount, always, Sql.KEY_STAR, matchColumns,null);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * @param _id 多表模式指定后缀ID
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @param whereSql 额外条件sql
	 * @return
	 */
	public List<Record> getAutocompleteList(Object _id,String keywords, Integer limitCount,Boolean always,String matchColumns,Sql whereSql) {
		return getAutocompleteList(_id,keywords, limitCount, always, Sql.KEY_STAR, matchColumns,whereSql);
	}
	/**
	 * 关键词查询指定返回个数的数据
	 * @param _id 多表模式指定后缀ID
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param returnColumns 查询字段 可以是* 也可以是 id,name这种逗号隔开
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<Record> getAutocompleteList(Object _id,String keywords, Integer limitCount,Boolean always,String returnColumns,String matchColumns) {
		return getAutocompleteList(_id, keywords, limitCount, always, returnColumns, matchColumns, null);
	}
	/**
	 * 关键词查询指定返回个数的数据
	 * @param _id 多表模式指定后缀ID
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param returnColumns 查询字段 可以是* 也可以是 id,name这种逗号隔开
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @param whereSql 额外条件Sql
	 * @return
	 */
	public List<Record> getAutocompleteList(Object _id,String keywords, Integer limitCount,Boolean always,String returnColumns,String matchColumns,Sql whereSql) {
		if((notOk(keywords)&&(always==null||always==false))||notOk(matchColumns)) {
			return Collections.emptyList();
		}
		if(whereSql == null) {
			whereSql=selectSql(_id);
		}
		whereSql.select(returnColumns).firstPage(limitCount);
		//如果关键词为空 默认是返回空数据
			//但是如果指定了关键词是空 就按照指定个数返回数据的话
		if(notOk(keywords)&&always!=null&&always==true) {
			return find(whereSql.toSql());
		}
		
		String[] columns=ArrayUtil.from(matchColumns, ",");
		if(columns==null||columns.length==0) {
			return Collections.emptyList();
		}
		int size=columns.length;
		whereSql.bracketLeft();
		keywords=keywords.trim();
		for(int i=0;i<size;i++) {
			whereSql.like(columns[i],keywords);
			if(i<size-1) {
				whereSql.or();
			}
		}
		whereSql.bracketRight();
		return find(whereSql.toSql());
	}
	/**
	 * 得到表中的字段字符拼接，除了指定的withoutColumns
	 * @param withoutColumns
	 * @return
	 */
	public String getTableSelectColumnsWithout(String... withoutColumns) {
		if(withoutColumns==null||withoutColumns.length==0) {return Sql.KEY_STAR;}
		Set<String> tableColumns=mainTableModelDao()._getTable().getColumnNameSet();
		List<String> selectColumns=new ArrayList<String>(tableColumns);
		for(String col:withoutColumns) {
			selectColumns.remove(col.trim().toLowerCase());
		}
		return selectColumns.size()==0?Sql.KEY_STAR:CollectionUtil.join(selectColumns, ",");
	}
	
	/**
	 * 得到不包含指定列的数据
	 * @param _id
	 * @param withoutColumns
	 * @return
	 */
	public List<Record> getCommonListWithoutColumns(Object _id,String withoutColumns) {
		return getCommonListWithoutColumns(_id,null, false,withoutColumns);
	}
	
	/**
	 * 得到不包含指定列的数据
	 * @param _id
	 * @param paras
	 * @param withoutColumns
	 * @param customCompare
	 * @return
	 */
	public List<Record> getCommonListWithoutColumns(Object _id,Kv paras,boolean customCompare,String... withoutColumns) {
		return dbTemplate(_id,"common.list", 
				Kv.by("customCompare",customCompare)
				.setIfNotNull("myparas", paras)
				.setIfNotBlank("columns",getTableSelectColumnsWithout(withoutColumns)))
				.find();
	}
	
	/**
	  *根据关键词分页查询 
	  * 默认倒序
	 * @param _id
	 * @param orderColumn
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param matchColumns
	 * @param otherParas
	 * @param columnToCamelCase
	 * @return
	 */
	public Page<Record> paginateByKeywords(Object _id,String orderColumn,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas,boolean columnToCamelCase){
		return paginateByKeywords(_id,orderColumn,"desc",pageNumber,pageSize,keywords,matchColumns,otherParas,columnToCamelCase);
	}
	/**
	   * 根据关键词分页查询 
	 * @param _id
	 * @param orderColumn
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param matchColumns
	 * @param otherParas
	 * @param columnToCamelCase
	 * @return
	 */
	public Page<Record> paginateByKeywords(Object _id,String orderColumn,String orderType,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas,boolean columnToCamelCase){
		return paginateByKeywords(_id,Sql.KEY_STAR,orderColumn,orderType,pageNumber,pageSize,keywords,matchColumns,otherParas,columnToCamelCase);
	}
	
	
	/**
	 * 根据关键词匹配分页查询
	 * @param _id
	 * @param returnColumns
	 * @param orderColumn
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param matchColumns
	 * @param otherParas
	 * @param columnToCamelCase
	 * @return
	 */
	public Page<Record> paginateByKeywords(Object _id,String returnColumns,String orderColumn,String orderType,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas,boolean columnToCamelCase){
		if(notOk(matchColumns)) {
			return EMPTY_PAGE;
		}
		Sql sql=selectSql(_id).select(returnColumns).page(pageNumber, pageSize);
		Sql totalCountSql=selectSql(_id).count();
		if(isOk(orderColumn)&&isOk(orderType)) {
			sql.orderBy(orderColumn,orderType.equals("desc"));
		}
		if(otherParas!=null&&otherParas.size()>0) {
			Set<String> keys=otherParas.keySet();
			sql.bracketLeft();
			totalCountSql.bracketLeft();
			for(String key:keys){
				sql.eq(key, otherParas.getAs(key));
				totalCountSql.eq(key, otherParas.getAs(key));
	        }
			sql.bracketRight();
			totalCountSql.bracketRight();
		}
		//如果没有给keywords字段
		if(notOk(keywords)) {
			return paginate(_id,sql,totalCountSql,columnToCamelCase);
		}
		//如果给了Keywords
		String[] columns=ArrayUtil.from(matchColumns, ",");
		if(columns==null||columns.length==0) {
			return EMPTY_PAGE;
		}
		int size=columns.length;
		sql.bracketLeft();
		totalCountSql.bracketLeft();
		keywords=keywords.trim();
		for(int i=0;i<size;i++) {
			sql.like(columns[i],keywords);
			totalCountSql.like(columns[i],keywords);
			if(i<size-1) {
				sql.or();
				totalCountSql.or();
			}
		}
		sql.bracketRight();
		totalCountSql.bracketRight();
		return paginate(_id,sql, totalCountSql,columnToCamelCase);
	}
	/**
	 * 分页查询
	 * @param _id
	 * @param sql
	 * @param totalCountSql
	 * @param columnToCamelCase
	 * @return
	 */
	public Page<Record> paginate(Object _id,Sql sql, Sql totalCountSql,boolean columnToCamelCase) {
		if(notOk(sql.getPageSize())) {
			throw new RuntimeException("Sql必须指定pageSize和pageNumber");
		}
		totalCountSql.from(table(_id));
		totalCountSql.clearOrderBy();
		Integer totalRow=queryInt(totalCountSql);
		if(totalRow==null||totalRow==0) {
			return EMPTY_PAGE;
		}
		sql.fromIfBlank(table(_id));
		List<Record> list=find(sql);
		if(columnToCamelCase) {
			CamelCaseUtil.keyToCamelCase(list);
		}
		int pageNumber = sql.getPageNumber();
		int pageSize = sql.getPageSize();
		int totalPage=(totalRow/pageSize)+(totalRow%pageSize>0?1:0);
		return new Page<Record>(list, pageNumber, pageSize, totalPage, totalRow);
	}
	
	/**
	 * 快速获取sql 默认是select sql
	 * @param _id
	 * @return
	 */
	protected Sql selectSql(Object _id) {
		return Sql.me(dbType()).select().from(table(_id));
	}
	
	/**
	 * 快速获取sql 使用jboltTableMenuFilter初始化
	 * @param _id
	 * @param jboltTableMenuFilter
	 * @param matchColumns
	 * @return
	 */
	public Sql selectSql(Object _id,JBoltTableMenuFilter jboltTableMenuFilter,String... matchColumns) {
		Sql sql = selectSql(_id);
		processJBoltTableMenuFilterSql(sql,jboltTableMenuFilter,matchColumns);
		return sql;
	}
	/**
	 * 快速获取sql 使用jboltTableMenuFilter初始化
	 * @param _id
	 * @param jboltTableMenuFilter
	 * @param mainTableAsName
	 * @param matchColumns
	 * @return
	 */
	public Sql selectSql(Object _id,JBoltTableMenuFilter jboltTableMenuFilter,String mainTableAsName,String... matchColumns) {
		Sql sql = selectSql(_id).from(table(_id),mainTableAsName);
		processJBoltTableMenuFilterSql(sql,jboltTableMenuFilter,matchColumns);
		return sql;
	}
	/**
	 * 快速获取update sql
	 * @param _id
	 * @return
	 */
	protected Sql updateSql(Object _id) {
		return Sql.me(dbType()).update(table(_id));
	}
	/**
	 * 快速获取delete sql
	 * @param _id
	 * @return
	 */
	protected Sql deleteSql(Object _id) {
		return Sql.me(dbType()).delete().from(table(_id));
	}
	
	
	/**
	 * 执行查询
	 * @param sql
	 * @return
	 */
	protected List<Record> find(Sql sql){
		return find(sql.toSql());
	}
	/**
	 * 执行查询 第一个符合条件的
	 * @param sql
	 * @return
	 */
	protected Record findFirst(Sql sql){
		return Db.use(dataSourceConfigName()).findFirst(sql.toSql());
	}
	/**
	 * 执行查询
	 * @param sql
	 * @param paras
	 * @return
	 */
	protected List<Record> find(Sql sql,Object... paras){
		return find(sql.toSql(),paras);
	}
	
	/**
	 * 执行查询
	 * @param sql
	 * @return
	 */
	protected List<Record> find(String sql){
		return Db.use(dataSourceConfigName()).find(sql);
	}
	/**
	 * 执行查询
	 * @param sql
	 * @param paras
	 * @return
	 */
	protected List<Record> find(String sql,Object... paras){
		return Db.use(dataSourceConfigName()).find(sql, paras);
	}
	
	
	/**
	 * 批量保存处理 
	 * @param _id
	 * @param records
	 * @param batchSize
	 */
	protected void batchSave(Object _id, List<Record> records, int batchSize) {
		if(isOk(records)) {
			Db.use(dataSourceConfigName()).batchSave(table(_id), records, batchSize);
		}
	}
	/**
	 * 批量保存处理 
	 * @param _id
	 * @param records
	 */
	protected void batchSave(Object _id, List<Record> records) {
		if(isOk(records)) {
			int batchSize=records.size();
			batchSave(_id,records, batchSize>500?500:batchSize);
		}
	}
	/**
	 * 批量更新处理 
	 * @param _id
	 * @param records
	 * @param batchSize
	 */
	protected void batchUpdate(Object _id, List<Record> records, int batchSize) {
		if(isOk(records)) {
			Db.use(dataSourceConfigName()).batchUpdate(table(_id), records, batchSize);
		}
	}
	/**
	 * 批量更新处理 
	 * @param _id
	 * @param records
	 */
	protected void batchUpdate(Object _id, List<Record> records) {
		if(isOk(records)) {
			int batchSize=records.size();
			batchUpdate(_id,records, batchSize>500?500:batchSize);
		}
	}
	

	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId,int openLevel) {
		return convertJsTree(_id,datas, selectedId, openLevel, null,false);
	}
	
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param needOriginData 是否需要保留原始Data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId,int openLevel,boolean needOriginData) {
		return convertJsTree(_id,datas, selectedId, openLevel, null,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId, int openLevel,String keyColumn) {
		return convertJsTree(_id,datas, selectedId, openLevel, keyColumn,null,false);
	}
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId, int openLevel,String keyColumn,boolean needOriginData) {
		return convertJsTree(_id,datas, selectedId, openLevel, keyColumn,null,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param rootName 根节点名字
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId, int openLevel,String keyColumn,String rootName) {
		return convertJsTree(_id,datas, selectedId, openLevel, keyColumn, "name",rootName,false);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId, int openLevel,String keyColumn,String rootName,boolean needOriginData) {
		return convertJsTree(_id,datas, selectedId, openLevel, keyColumn, "name",rootName,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param textColumn 显示文本字段
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId, int openLevel,String keyColumn,String textColumn,String rootName,boolean needOriginData) {
		return convertJsTree(_id,datas, selectedId, openLevel, keyColumn,textColumn,"enable",rootName,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param textColumn 显示文本字段
	 * @param enableColumn 启用禁用字段
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始Data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId, int openLevel,String keyColumn,String textColumn,String enableColumn,String rootName,boolean needOriginData) {
		return convertJsTree(_id,datas, selectedId, openLevel, keyColumn, "id", "pid",textColumn,enableColumn,rootName,needOriginData,null);
	}
	private String[] getTreeColumnKeys(Object _id,Record m,String idColumn,String pidColumn) {
		List<String> keys=new ArrayList<String>();
		keys.add(m.getStr(idColumn));
		Object pid=m.getStr(pidColumn);
		if(pid!=null&&pid.toString().equals("0")==false) {
			processTreeColumnKeys(_id,keys, pid, idColumn, pidColumn);
		}
		return keys.toArray(new String[keys.size()]);
	}
	private void processTreeColumnKeys(Object _id,List<String> keys,Object id,String idColumn,String pidColumn) {
		if(id==null || id.toString().equals("0")) {
			return;
		}
		Record m=findById(_id,id);
		if(m==null) {
			return;
		}
		//pid有值 需要加入keys
		keys.add(0, id.toString());
		Object pid=m.getStr(pidColumn);
		if(pid!=null&&pid.toString().equals("0")==false) {
			//pid有值 需要加入keys
			processTreeColumnKeys(_id,keys, pid, idColumn, pidColumn);
		}
	}

	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param idColumn 节点 标识字段
	 * @param pidColumn 节点父标识字段
	 * @param textColumn 显示文本字段
	 * @param enableColumn 启用禁用字段
	 * @param rootName 跟节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @param checkIsParentFunc 检测是否为parent的Func
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(Object _id,List<Record> datas,Object selectedId, int openLevel,String keyColumn,String idColumn,String pidColumn,String textColumn,String enableColumn,String rootName,boolean needOriginData,TreeCheckIsParentNode<Record> checkIsParentFunc) {
		List<JsTreeBean> treeBeans=new ArrayList<JsTreeBean>();
		boolean hasRoot=isOk(rootName);
		if(hasRoot) {
			treeBeans.add(new JsTreeBean(0, 0, rootName, true,"root_opened",true));
		}
		if(datas.size()>0){
			String[] keys=null;
			if(isOk(selectedId)){
				Record m=findById(_id,selectedId);
				if(m != null) {
					if(isOk(keyColumn)) {
						String key=m.get(keyColumn);
						if(isOk(key)) {
							keys=ArrayUtil.from(key, "_");
						}
					}else {
						keys=getTreeColumnKeys(_id,m,idColumn,pidColumn);
					}
				}
			}
			List<Record> jstreeMs=null;
			if(checkIsParentFunc!=null) {
				jstreeMs = convertToRecordTree(datas, idColumn, pidColumn, (p)->checkIsParentFunc.isParent(p));
			}else {
				jstreeMs = convertToRecordTree(datas, idColumn, pidColumn, (p)->{
					Object v=p.get(pidColumn);
					return v==null||v.toString().equals("0");
				});
			}
			processRecordJsTreeType(jstreeMs);
			
			processRecordJsTree(treeBeans,jstreeMs,openLevel,1,idColumn,pidColumn,textColumn,enableColumn,keys,selectedId,hasRoot,needOriginData);
		}
		return treeBeans;
	}
	
	
}
