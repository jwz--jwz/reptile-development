package cn.jbolt.base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.DaoTemplate;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbTemplate;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.TableMapping;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.jbolt.base.para.jbolttablemenufilter.JBoltTableMenuFilter;
import cn.jbolt.common.bean.JsTreeBean;
import cn.jbolt.common.config.MainConfig;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.DBType;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CamelCaseUtil;
import cn.jbolt.common.util.ListMap;
import cn.jbolt.common.util.TreeCheckIsParentNode;

/**
 * JBolt提供Service层的基础底层封装
  * 灵活 易用
 * @ClassName:  BaseService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年11月14日   
 */
public abstract class JBoltBaseService<M extends JBoltBaseModel<M>> extends JBoltCommonService {
	/**
	 *  获取Sql模板 model模式
	 * @param key
	 * @param data
	 * @return
	 */
	public DaoTemplate<M> daoTemplate(String key,Kv data) {
		data.setIfNotBlank("table", table());
		return dao().template(key, data);
	}
	/**
	 *  获取表名
	 * @return
	 */
	public String table(){
		if(tableName == null) {
			tableName=dao()._getTableName();
		}
		return tableName;
	}
	
	@Override
	public String dataSourceConfigName() {
		if(dataSourceConfigName==null) {
			dataSourceConfigName=dao()._getDataSourceConfigName();
		}
		return dataSourceConfigName;
	}
	@Override
	public String dbType() {
		if(dbType==null) {
			dbType=dao()._getDbType();
		}
		return dbType;
	}
	@Override
	public String primaryKey() {
		if(primaryKey==null) {
			primaryKey=dao()._getPrimaryKey();
		}
		return primaryKey;
	}
	
	@Override
	public String idGenMode() {
		if(idGenMode==null) {
			idGenMode=dao()._getIdGenMode();
		}
		return idGenMode;
	}

	/**
	 *  获取Sql模板 Db模式
	 * @param key
	 * @param data
	 * @return
	 */
	public DbTemplate dbTemplate(String key,Kv data) {
		data.setIfNotBlank("table", table());
		return Db.use(dataSourceConfigName()).template(key, data);
	}

	/**
	 * 得到下拉列表数据
	 * @param textColumn
	 * @param valueColumn
	 * @param paras
	 * @return
	 */
	public List<Record> getOptionList(String textColumn,String valueColumn,Kv paras){
		if(dao().hasColumn("sort_rank")||dao().hasColumn("SORT_RANK")) {
			return getOptionList(textColumn, valueColumn, paras, "sort_rank", null);
		}
		return getOptionList(textColumn, valueColumn, paras, null, null);
	}
	
	/**
	 * 按照指定字段和排序方式返回options
	 * @param orderColumns
	 * @param orderTypes
	 * @return
	 */
	public List<Record> getOptionListOrderBy(String orderColumns,String orderTypes){
		return getOptionList("name", "id", null, orderColumns, orderTypes);
	}
	
	/**
	 * 得到下拉列表数据
	 * @param textColumn    哪个字段作为显示的text
	 * @param valueColumn   哪个字段作为选中的值
	 * @param paras         查询条件 
	 * @param orderColumns  排序字段 可多个 
	 * @param orderTypes    排序方式 可多个
	 * @return
	 */
	public List<Record> getOptionList(String textColumn,String valueColumn,Kv paras,String orderColumns,String orderTypes){
		Kv conf=Kv.by("value",valueColumn).set("text",textColumn).set("myparas", paras).set("customCompare",false);
		if(isOk(orderColumns)){
			conf.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			conf.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return dbTemplate("common.optionlist", conf).find();
	}

	/**
	 * 得到下拉列表数据
	 * @param textColumn    哪个字段作为显示的text
	 * @param valueColumn   哪个字段作为选中的值
	 * @return
	 */
	public List<Record> getOptionList(String textColumn,String valueColumn){
		return getOptionList(textColumn, valueColumn, null);
	}
	/**
	 * 得到下拉列表数据
	 * @return
	 */
	public List<Record> getOptionList(){
		return getOptionList("name", "id");
	}
	/**
	 * 得到所有数据
	 * @return
	 */
	 public List<M> findAll(){
		 return find(selectSql().orderByIdAscIfPgSql());
	 }
	/**
	 * 抽象方法定义 dao 让调用者自己实现
	 * @return
	 */
	protected abstract M dao();
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 自定义参数compare
	 * @param columns         返回字段 多个逗号隔开
	 * @param paras           查询条件
	 * @param customCompare   是否自定义比较符
	 * @return
	 */
	public List<M> getCommonList(String columns,Kv paras,boolean customCompare){
		return daoTemplate("common.list", 
				Kv.by("customCompare",customCompare)
				.setIfNotNull("myparas", paras)
				.setIfNotBlank("columns",columns))
				.find();
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 可以自定义参数compare 默认=
	 * @param paras          查询条件
	 * @param customCompare  是否自定义比较符
	 * @return
	 */
	public List<M> getCommonList(Kv paras,boolean customCompare){
		return getCommonList("*",paras, customCompare);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 不能自定义参数compare 默认=
	 * @param paras
	 * @return
	 */
	public List<M> getCommonList(Kv paras){
		return getCommonList("*",paras, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 不能自定义参数compare 默认=
	 * @param columns 指定查询的列
	 * @return
	 */
	public List<M> getCommonList(String columns){
		return getCommonList(columns,null,false);
	}
	
	/**
	 * 得到不包含指定列的数据
	 * @param paras           查询条件
	 * @param customCompare   是否自定义比较符
	 * @param withoutColumns  不需要结果携带的列 多个 逗号隔开
	 * @return
	 */
	public List<M> getCommonListWithoutColumns(Kv paras,boolean customCompare,String... withoutColumns) {
		return daoTemplate("common.list", 
				Kv.by("customCompare",customCompare)
				.setIfNotNull("myparas", paras)
				.setIfNotBlank("columns",getTableSelectColumnsWithout(withoutColumns)))
				.find();
	}
	/**
	 * 得到表中的字段字符拼接，除了指定的withoutColumns
	 * @param withoutColumns 不需要结果携带的字段列 多个 逗号隔开
	 * @return
	 */
	public String getTableSelectColumnsWithout(String... withoutColumns) {
		if(withoutColumns==null||withoutColumns.length==0) {return Sql.KEY_STAR;}
		Set<String> tableColumns=TableMapping.me().getTable(dao().getClass()).getColumnNameSet();
		List<String> selectColumns=new ArrayList<String>(tableColumns);
		for(String col:withoutColumns) {
			selectColumns.remove(col.trim().toLowerCase());
		}
		return selectColumns.size()==0?Sql.KEY_STAR:CollectionUtil.join(selectColumns, ",");
	}
	/**
	 * 得到表中的字段字符拼接，除了指定的withoutColumns
	 * @param pre 前缀
	 * @param withoutColumns 不需要结果携带的字段列 多个 逗号隔开
	 * @return
	 */
	public String getTableSelectColumnsWithoutWithPre(String pre,String... withoutColumns) {
		if(withoutColumns==null||withoutColumns.length==0) {return Sql.KEY_STAR;}
		Set<String> tableColumns=TableMapping.me().getTable(dao().getClass()).getColumnNameSet();
		List<String> selectColumns=new ArrayList<String>(tableColumns);
		for(String col:withoutColumns) {
			selectColumns.remove(col.trim().toLowerCase());
		}
		return selectColumns.size()==0?Sql.KEY_STAR:CollectionUtil.join(selectColumns, ",", pre, null);
	}
	/**
	 * 得到不包含指定列的数据
	 * @param withoutColumns 不需要结果携带的字段列 多个 逗号隔开
	 * @return
	 */
	public List<M> getCommonListWithoutColumns(String withoutColumns) {
		return getCommonListWithoutColumns(null, false,withoutColumns);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 不能自定义参数compare 默认=
	 * @param columns 指定查询的列
	 * @param paras   查询条件
	 * @return
	 */
	public List<M> getCommonList(String columns,Kv paras){
		return getCommonList(columns,paras,false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 默认正序 
	 * 不能自定义参数compare 默认=
	 * @param paras        查询条件
	 * @param orderColums  排序列
	 * @return
	 */
	public List<M> getCommonList(Kv paras,String orderColums){
		if(notOk(orderColums)) {
			return getCommonList("*",paras);
		}
		int count=StrUtil.count(orderColums, ",");
		String orderTypes="";
		for(int i=0;i<=count;i++){
			if(i==0){
				orderTypes="asc";
			}else{
				orderTypes=orderTypes+","+"asc";
			}
		}
		return getCommonList("*",paras, orderColums, orderTypes, false);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 * @param orderColumns
	 * @param orderTypes
	 * @return
	 */
	public List<M> getCommonList(String orderColumns,String orderTypes){
		return getCommonList("*",null, orderColumns, orderTypes, false);
	}
	
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @return
	 */
	public List<M> getCommonList(Kv paras,String orderColumns,String orderTypes){
		return getCommonList("*",paras, orderColumns, orderTypes, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 * @param columns
	 * @param paras
	 * @param sort
	 * @param orderType
	 * @return
	 */
	public List<M> getCommonList(String columns,Kv paras,String orderColumns,String orderTypes){
		return getCommonList(columns,paras, orderColumns, orderTypes, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 自定义参数compare
	 * @param paras
	 * @param orderColumn
	 * @param orderType
	 * @param customCompare
	 * @return
	 */
	public List<M> getCommonList(Kv paras,String orderColumns,String orderTypes,boolean customCompare){
		return getCommonList("*", paras, orderColumns, orderTypes, customCompare);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 * @param columns
	 * @param orderColumns
	 * @param orderType
	 * @return
	 */
	public List<M> getCommonList(String columns,String orderColumns,String orderTypes){
		return getCommonList(columns,null, orderColumns, orderTypes, false);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 自定义参数compare
	 * @param columns
	 * @param paras
	 * @param orderColumn
	 * @param orderType
	 * @param customCompare
	 * @return
	 */
	public List<M> getCommonList(String columns,Kv paras,String orderColumns,String orderTypes,boolean customCompare){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		if(isOk(columns)){
			conf.set("columns",columns);
		}
		if(isOk(orderColumns)){
			conf.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			conf.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return daoTemplate("common.list",conf).find();
	}
	
	/**
	 * 分页查询
	 * @param sql
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(Sql sql){
		Sql totalCountSql=ObjectUtil.clone(sql);
		totalCountSql.count();
		return processPaginate(sql, totalCountSql);
	}
	/**
	 * 分页查询 返回Page<Record>
	 * @param sql
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginateRecord(Sql sql){
		if(!sql.hasPage()) {
			throw new RuntimeException("Please set the values of PageNumber and PageSize in the SQL");
		}
		return paginateRecord(sql, false);
	}
	
	/**
	 * 分页查询 返回Page<Record> 支持设置字段属性转为驼峰
	 * @param sql
	 * @param columnToCamelCase
	 * @return
	 */
	public Page<Record> paginateRecord(Sql sql,boolean columnToCamelCase){
		Sql totalCountSql=ObjectUtil.clone(sql);
		totalCountSql.count();
		return paginateRecord(sql, totalCountSql,columnToCamelCase);
	}
	
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 自定义参数compare
	 * 分页查询
	 * @param columns
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @param or
	 * @return
	 */
	public Page<M> paginate(String columns,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare,boolean or){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		conf.set("or",or);
		conf.setIfNotBlank("columns",columns);
		if(isOk(orderColumns)){
			conf.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			conf.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return daoTemplate("common.list",conf).paginate(pageNumber, pageSize);
	}
	
	/**
	 *根据关键词分页查询 
	 * @param orderColumn
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param matchColumns
	 * @return
	 */
	public Page<M> paginateByKeywords(String orderColumn,String orderType,int pageNumber,int pageSize,String keywords,String matchColumns){
		return paginateByKeywords(orderColumn,orderType,pageNumber,pageSize,keywords,matchColumns,null);
	}
	/**
	 *根据关键词分页查询 
	 * 默认倒序
	 * @param orderColumn
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param matchColumns
	 * @return
	 */
	public Page<M> paginateByKeywords(String orderColumn,int pageNumber,int pageSize,String keywords,String matchColumns){
		return paginateByKeywords(orderColumn,"desc",pageNumber,pageSize,keywords,matchColumns,null);
	}
	/**
	  *根据关键词分页查询 
	  * 默认倒序
	 * @param orderColumn
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param matchColumns
	 * @param otherParas
	 * @return
	 */
	public Page<M> paginateByKeywords(String orderColumn,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas){
		return paginateByKeywords(orderColumn,"desc",pageNumber,pageSize,keywords,matchColumns,otherParas);
	}
	/**
	   * 根据关键词分页查询 
	 * @param orderColumn
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param matchColumns
	 * @param otherParas
	 * @return
	 */
	public Page<M> paginateByKeywords(String orderColumn,String orderType,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas){
		return paginateByKeywords(Sql.KEY_STAR,orderColumn,orderType,pageNumber,pageSize,keywords,matchColumns,otherParas);
	}
	
	
	/**
	 * 根据关键词匹配分页查询
	 * @param returnColumns
	 * @param orderColumn
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param matchColumns
	 * @param otherParas
	 * @return
	 */
	public Page<M> paginateByKeywords(String returnColumns,String orderColumn,String orderType,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas){
		if(notOk(matchColumns)) {
			return EMPTY_PAGE;
		}
		Sql sql=selectSql().select(returnColumns).page(pageNumber, pageSize);
		Sql totalCountSql=selectSql().count();
		if(isOk(orderColumn)&&isOk(orderType)) {
			sql.orderBy(orderColumn,"desc".equals(orderType));
		}else {
			sql.orderByIdAscIfPgSql();
		}
		if(otherParas!=null&&otherParas.size()>0) {
			Set<String> keys=otherParas.keySet();
			sql.bracketLeft();
			totalCountSql.bracketLeft();
			for(String key:keys){
				sql.eq(key, otherParas.get(key));
				totalCountSql.eq(key, otherParas.get(key));
	        }
			sql.bracketRight();
			totalCountSql.bracketRight();
		}
		//如果没有给keywords字段
		if(notOk(keywords)) {
			return processPaginate(sql,totalCountSql);
		}
		//如果给了Keywords
		String[] columns=ArrayUtil.from(matchColumns, ",");
		if(columns==null||columns.length==0) {
			return EMPTY_PAGE;
		}
		int size=columns.length;
		sql.bracketLeft();
		totalCountSql.bracketLeft();
		keywords=keywords.trim();
		for(int i=0;i<size;i++) {
			sql.like(columns[i],keywords);
			totalCountSql.like(columns[i],keywords);
			if(i<size-1) {
				sql.or();
				totalCountSql.or();
			}
		}
		sql.bracketRight();
		totalCountSql.bracketRight();
		return processPaginate(sql, totalCountSql);
	}
	
	private Page<M> processPaginate(Sql sql, Sql totalCountSql) {
		if(!sql.hasPage()) {
			throw new RuntimeException("Please set the values of PageNumber and PageSize in the SQL");
		}
		totalCountSql.fromIfBlank(table());
		totalCountSql.clearOrderBy();
		totalCountSql.clearPage();
		Integer totalRow=queryInt(totalCountSql);
		if(totalRow==null||totalRow==0) {
			return EMPTY_PAGE;
		}
		sql.fromIfBlank(table());
		List<M> list=find(sql);
		int totalPage=(totalRow/sql.getPageSize())+(totalRow%sql.getPageSize()>0?1:0);
		return new Page<M>(list, sql.getPageNumber(), sql.getPageSize(), totalPage, totalRow);
	}
	/**
	 * 分页查询
	 * @param sql
	 * @param totalCountSql
	 * @param columnToCamelCase
	 * @return
	 */
	public Page<Record> paginateRecord(Sql sql, Sql totalCountSql,boolean columnToCamelCase) {
		if(!sql.hasPage()) {
			throw new RuntimeException("Please set the values of PageNumber and PageSize in the SQL");
		}
		totalCountSql.fromIfBlank(table());
		totalCountSql.clearOrderBy();
		totalCountSql.clearPage();
		Integer totalRow=queryInt(totalCountSql);
		if(totalRow==null||totalRow==0) {
			return EMPTY_PAGE;
		}
		sql.fromIfBlank(table());
		List<Record> list=findRecord(sql);
		if(columnToCamelCase) {
			CamelCaseUtil.keyToCamelCase(list);
		}
		int pageSize=sql.getPageSize();
		int pageNumber=sql.getPageNumber();
		int totalPage=(totalRow/pageSize)+(totalRow%pageSize>0?1:0);
		return new Page<Record>(list, pageNumber, pageSize, totalPage, totalRow);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param columns
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(String columns,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate(columns, paras, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @param or
	 * @return
	 */
	public Page<M> paginate(Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare,boolean or){
		return paginate(Sql.KEY_STAR, paras, orderColumns, orderTypes, pageNumber, pageSize, customCompare,or);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param paras
	 * @param orderColumns
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<M> paginate(Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare){
		return paginate("*", paras, orderColumns, orderTypes, pageNumber, pageSize, customCompare,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate("*", paras, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param orderColumns
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(String orderColumns,int pageNumber, int pageSize){
		return paginate("*", null, orderColumns, "desc", pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate("*", null, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法 返回指定列
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 * @param columns
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(String columns,String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate(columns, null, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不尅一可以排序
	 * 条件自定义 customCompare
	 * 分页查询
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<M> paginate(String columns,Kv paras,int pageNumber, int pageSize,boolean customCompare){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		if(isOk(columns)){
			conf.set("columns",columns);
		}
		return daoTemplate("common.list",conf).paginate(pageNumber, pageSize);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 条件自定义 customCompare
	 * 分页查询
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<M> paginate(Kv paras,int pageNumber, int pageSize,boolean customCompare){
		return paginate("*", paras, pageNumber, pageSize, customCompare);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<M> paginate(Kv paras,int pageNumber, int pageSize){
		return paginate("*", paras, pageNumber, pageSize, false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<M> paginate(Kv paras,int pageSize,boolean customCompare){
		return paginate("*", paras, 1, pageSize, customCompare);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(int pageNumber,int pageSize){
		return paginate("*", null, pageNumber, pageSize, false);
	}
	/**
	 * 按照sql模板分页查询
	 * @param key
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginateBySqlTemplate(String key,Kv data,int pageNumber,int pageSize){
		return paginateBySqlTemplate(key, data, pageNumber, pageSize, null, null);
	}
	/**
	 * 按照sql模板分页查询
	 * @param key
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginateBySqlTemplate(String key,Kv data,int pageNumber,int pageSize,String orderColumns,String orderTypes){
		if(isOk(orderColumns)){
			data.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			data.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return daoTemplate(key, data).paginate(pageNumber, pageSize);
	}
	/**
	 * 按照sql模板分页查询 返回Record
	 * @param key
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginateByDbSqlTemplate(String key,Kv data,int pageNumber,int pageSize){
		return paginateByDbSqlTemplate(key, data, pageNumber, pageSize, false);
	}
	/**
	 * 按照sql模板分页查询 返回Record
	 * @param key
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @param columnToCamelCase 列名是否转驼峰
	 * @return
	 */
	public Page<Record> paginateByDbSqlTemplate(String key,Kv data,int pageNumber,int pageSize,boolean columnToCamelCase){
		Page<Record> pageData=dbTemplate(key, data).paginate(pageNumber, pageSize);
		if(columnToCamelCase&&pageData!=null&&isOk(pageData.getList())) {
			CamelCaseUtil.keyToCamelCase(pageData);
		}
		return pageData;
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param paras
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(Kv paras,int pageSize){
		return paginate("*", paras, 1, pageSize, false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 * @param pageSize
	 * @return
	 */
	public Page<M> paginate(int pageSize){
		return paginate("*", null, 1, pageSize, false);
	}
	
	
	/**
	 * 通用根据ID删除数据
	 * @param id
	 * @return
	 */
	public Ret deleteById(Object id){
		return deleteById(id,null);
	}
	/**
	 * 通用根据ID删除数据
	 * @param id
	 * @param kv
	 * @return
	 */
	public Ret deleteById(Object id,Kv kv){
		return deleteById(id, false,kv);
	}
	/**
	 * 根据ID 批量删除 指定分隔符版
	 * @param ids
	 * @param split 分隔符
	 * @return
	 */
	public Ret deleteByIds(String ids,String split,boolean checkCanDelete) {
		return deleteByIds(ArrayUtil.from3(ids, split),checkCanDelete);
	}
	/**
	 * 根据ID 批量删除 指定分隔符版
	 * @param ids
	 * @param split 分隔符
	 * @return
	 */
	public Ret deleteByIds(String ids,String split) {
		return deleteByIds(ids,split,false);
	}
	
	/**
	 * 根据ID 批量删除 默认分隔符是逗号 
	 * @param ids
	 * @return
	 */
	public Ret deleteByIds(String ids) {
		return deleteByIds(ids,false);
	}
	/**
	 * 根据ID 批量删除 默认分隔符是逗号
	 * @param ids
	 * @param checkCanDelete
	 * @return
	 */
	public Ret deleteByIds(String ids,boolean checkCanDelete) {
		return deleteByIds(ids,",",checkCanDelete);
	}
	
	/**
	 * 根据ID 批量删除
	 * @param ids
	 * @return
	 */
	public Ret deleteByIds(Object[] ids) {
		return deleteByIds(ids,false);
	}
	/**
	 * 根据ID 批量删除
	 * @param ids
	 * @param checkCanDelete
	 * @return
	 */
	public Ret deleteByIds(Object[] ids,boolean checkCanDelete) {
		if(notOk(ids)) {return fail(Msg.PARAM_ERROR);}
		Ret ret;
		for(Object id:ids) {
			ret=deleteById(id,checkCanDelete);
			if(ret.isFail()) {
				return ret;
			}
		}
		return SUCCESS;
	}
	/**
	 * 通用根据ID删除数据
	 * @param id
	 * @param checkCanDelete
	 * @return
	 */
	public Ret deleteById(Object id,boolean checkCanDelete){
		return deleteById(id, checkCanDelete,null);
	}
	/**
	 * 通用根据ID删除数据
	 * @param id
	 * @param checkCanDelete
	 * @param kv 额外数据
	 * @return
	 */
	public Ret deleteById(Object id,boolean checkCanDelete,Kv kv){
		return deleteById(id, checkCanDelete,true,kv);
	}
	
	/**
	 * 通用根据ID删除数据 需要先检查是否被其他地方使用
	 * @param id
	 * @param checkCanDelete
	 * @param returnDeleteData
	 * @param kv 额外数据
	 * @return
	 */
	public Ret deleteById(Object id,boolean checkCanDelete,boolean returnDeleteData,Kv kv){
		if(MainConfig.DEMO_MODE) {return fail(Msg.DEMO_MODE_CAN_NOT_DELETE);}
		if(notOk(id)){
			return fail(Msg.PARAM_ERROR);
		}
		M m=findById(id);
		if(m==null){
			return fail(Msg.DATA_NOT_EXIST);
		}
		
		if(checkCanDelete){
			String msg=checkCanDelete(m,kv);
			if(isOk(msg)){return fail(msg);}
		}
		
		boolean success=m.deleteById(id);
		if(success) {
			String msg=afterDelete(m,kv);
			if(isOk(msg)){return fail(msg);}
		}
		//需要返回删除的数据
		if(returnDeleteData) {
			return success?success(m,Msg.SUCCESS):FAIL;
		}
		return ret(success);
	}
	/**
	 * 检测数据是否可以被其它数据引用 带额外数据
	 * @param model
	 * @param kv 额外参数
	 * @return
	 */
	public String checkInUse(M model,Kv kv){
		return null;
	}
	 
	/**
	 * 检测数据是否可以被删除 带额外数据
	 * @param model
	 * @param kv 额外参数
	 * @return
	 */
	public String checkCanDelete(M model,Kv kv){
		return null;
	}
	/**
	 * 检测数据是否字段是否可以执行切换true false
	 * @param model
	 * @param column
	 * @param kv
	 * @return
	 */
	public String checkCanToggle(M model,String column,Kv kv){
		return null;
	}

	/**
	 * 额外需要处理toggle操作
	 * @param model
	 * @param column
	 * @param kv
	 * @return
	 */
	protected String afterToggleBoolean(M model,String column,Kv kv){
		return null;
	}
	
	/**
	 * 删除成功后需要处理的事情
	 * @param model
	 * @param kv
	 * @return
	 */
	protected String afterDelete(M model,Kv kv){
		return null;
	}
	
	
	/**
	 * 判断name是否存在相同数据 排除指定ID
	 * @param name
	 * @param id
	 * @return
	 */
	public boolean existsName(String name,Object id) {
		return existsName(name, id, null);
	}
	
	/**
	 * 判断name是否存在相同数据 排除指定ID 
	 * 可以指定在哪一个PID下
	 * @param nameValue
	 * @param id
	 * @param pid
	 * @return
	 */
	public boolean existsName(String nameValue,Object idValue,Object pidValue) {
		return existsName(nameValue, idValue, "pid", pidValue);
	}
	
	/**
	 * 判断name是否存在相同数据 排除指定ID 
	 * 可以指定在哪一个PID下
	 * @param nameValue
	 * @param idValue
	 * @param pidName
	 * @param pidValue
	 * @return
	 */
	public boolean existsName(String nameValue,Object idValue,String pidName,Object pidValue) {
		return exists("name", nameValue, idValue, pidName, pidValue);
	}

	/**
	 * 判断是否存在
	 * @param columnName
	 * @param columnValue
	 * @param idValue
	 * @param pidValue
	 * @return
	 */
	public boolean exists(String columnName,Object columnValue,Object idValue,Object pidValue) {
		return exists(columnName, columnValue, idValue, "pid", pidValue);
	}
	/**
	 * 判断是否存在
	 * @param columnName
	 * @param columnValue
	 * @param idValue
	 * @param pidName
	 * @param pidValue
	 * @return
	 */
	public boolean exists(String columnName,Object columnValue,Object idValue,String pidName,Object pidValue) {
		Sql sql=selectSql().selectId().eqQM(columnName).idNoteqQM(dao()._getPrimaryKey()).first();
		Object existId=null;
		if(StrKit.notBlank(pidName)&&pidValue!=null) {
			sql.eqQM(pidName);
			existId = queryColumn(sql, columnValue,idValue,pidValue);
		}else {
			existId = queryColumn(sql, columnValue,idValue);
		}
		return isOk(existId);
	}
	/**
	 * 判断是否存在
	 * @param columnName
	 * @param columnvalue
	 * @param idValue
	 * @return
	 */
	public boolean exists(String columnName,Object columnvalue,Object idValue) {
		return exists(columnName, columnvalue, idValue, null, null);
	}
	/**
	 * 判断在子节点里是否存在指定列值数据
	 * @param columnName
	 * @param columnvalue
	 * @param pidName
	 * @param pidValue
	 * @return
	 */
	public boolean existsInSons(String columnName,Object columnvalue,String pidName,Object pidValue) {
		return exists(columnName, columnvalue, -1, pidName, pidValue);
	}
	/**
	 * 判断是否存在子节点
	 * @param pidName
	 * @param pidValue
	 * @return
	 */
	public boolean existsSon(String pidName,Object pidValue) {
		Sql sql=selectSql().selectId().eqQM(pidName).first();
		Object existId= queryColumn(sql, pidValue);
		return isOk(existId);
	}
	/**
	 * 判断是否存在子节点
	 * @param pidName
	 * @param pidValue
	 * @return
	 */
	public boolean existsSon(Object pidValue) {
		return existsSon("pid", pidValue);
	}
	/**
	 * 根据自定义sql查询数据是否存在
	 * @param sql
	 * @return
	 */
	public boolean exist(Sql sql) {
		sql.selectId().first();
		Object existId = queryColumn(sql);
		return isOk(existId);
	}
	/**
	 * 判断是否存在
	 * @param columnName
	 * @param columnValue
	 * @return
	 */
	public boolean exists(String columnName,Object columnValue) {
		return exists(columnName, columnValue, -1);
	}
	/**
	 * 判断name值为指定值的数据是否存在
	 * @param nameValue
	 * @return
	 */
	public boolean existsName(String nameValue) {
		return existsName(nameValue, -1);
	}
	/**
	 * 判断name值为指定值的数据是否存在
	 * @param nameValue
	 * @return
	 */
	public boolean existsNameWithPid(String nameValue,String pidName,Object pidValue) {
		return existsName(nameValue,-1,pidName,pidValue);
	}
	/**
	 * 判断name值为指定值的数据是否存在
	 * @param nameValue
	 * @return
	 */
	public boolean existsNameWithPid(String nameValue,Object pidValue) {
		return existsNameWithPid(nameValue, "pid", pidValue);
	}
	/**
	 * 根据ID获得一条数据 自动处理缓存
	 * @param id
	 * @return
	 */
	public M findById(Object id) {
		if(notOk(id)){return null;}
		return dao().findById(id);
	}
	/**
	 * 根据ID获得一条数据 直接从数据库查询
	 * @param id
	 * @return
	 */
	public M superFindById(Object id) {
		if(notOk(id)){return null;}
		return dao().superFindById(id);
	}
	/**
	 * 根据ID获得一条数据 去掉指定的列
	 * @param id
	 * @param withoutColumns 不需要的列
	 * @return
	 */
	public M findByIdWithoutColumns(Object id,String... withoutColumns) {
		if(notOk(id)){return null;}
		if(notOk(withoutColumns)) {return findById(id);}
		String columns = getTableSelectColumnsWithout(withoutColumns);
		return findByIdLoadColumns(id, columns);
	}
	/**
	 * 根据ID获得一条数据 指定需要的列
	 * @param id
	 * @param columns 需要的列
	 * @return
	 */
	public M findByIdLoadColumns(Object id,String... columns) {
		if(notOk(id)){return null;}
		if(notOk(columns)) {return findById(id);}
		String withColumns = ArrayUtil.join(columns, ",");
		return dao().findByIdLoadColumns(id, withColumns);
	}
	/**
	 * 根据ID获得一条数据 指定的列值
	 * @param id
	 * @param column 需要的列
	 * @return
	 */
	public <T> T getOneColumnValueById(Object id,String column) {
		if(notOk(column)) {return null;}
		M m=findByIdLoadColumns(id, column);
		return m==null?null:m.get(column);
	}
	/**
	 * 得到符合条件的第一个
	 * @param paras
	 * @return
	 */
	public M findFirst(Kv paras) {
		return findFirst(paras,false);
	}
	/**
	 * 得到符合条件的第一个
	 * @param Sql
	 * @return
	 */
	public M findFirst(Sql sql) {
		if(sql.isPrepared()) {
			return dao().findFirst(sql.toSql(), sql.getWhereValues());
		}
		return dao().findFirst(sql.toSql());
	}
	/**
	 * 得到符合条件的第一个
	 * @param Sql
	 * @return
	 */
	public Record findFirstRecord(Sql sql) {
		if(sql.isPrepared()) {
			return Db.use(dataSourceConfigName()).findFirst(sql.toSql(), sql.getWhereValues());
		}
		return Db.use(dataSourceConfigName()).findFirst(sql.toSql());
	}
	/**
	 * 得到符合条件的第一个
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public M findFirst(Kv paras,boolean customCompare) {
		if(MainConfig.isMysql()) {
			return findFirst(paras,null,null,customCompare);
		}
		return findFirst(paras,"id","asc",customCompare);
	}
	/**
	 * 得到符合条件的第一个
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @return
	 */
	public M findFirst(Kv paras,String orderColumns,String orderTypes) {
		return findFirst(paras,orderColumns,orderTypes,false);
	}
	/**
	 * 得到符合条件的第一个 返回Model
	 * @return
	 */
	public M findFirst() {
		return findFirst(KV_EMPTY);
	}
	/**
	 * 得到符合条件的第一个 返回record
	 * @return
	 */
	public Record findFirstRecord() {
		return findFirstRecord(selectSql());
	}
	/**
	 * 得到符合条件的第一个
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param customCompare
	 * @return
	 */
	public M findFirst(Kv paras,String orderColumns,String orderTypes,boolean customCompare) {
		Kv conf=Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras);
		if(isOk(orderColumns)){
			conf.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			conf.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return daoTemplate("common.first", conf).findFirst();
	}
	/**
	 * 随机得到符合条件的第一个
	 * @param paras
	 * @return
	 */
	public M getRandomOne(Kv paras) {
		return getRandomOne(paras, false);
	}
	/**
	 * 随机得到符合条件的第一个 oracle专用
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public M getOracleRandomOne(Kv paras,boolean customCompare) {
		int count=getCount(paras, customCompare);
		if(count==0) {return null;}
		if(count==1) {return findFirst(paras, customCompare);}
		Kv conf=Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras).set("randomRownum",RandomUtil.randomInt(1, count));
		return daoTemplate("common.firstrand", conf).findFirst();
	}
	/**
	 * 随机得到符合条件的第一个
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public M getRandomOne(Kv paras,boolean customCompare) {
		if(MainConfig.isOracle()) {
			return getOracleRandomOne(paras,customCompare);
		}
		Kv conf=Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras);
		switch (dbType()) {
		case DBType.MYSQL:
			conf.set("orderColumns",new String[] {"rand()"});
			break;
		case DBType.POSTGRESQL:
			conf.set("orderColumns",new String[] {"random()"});
			break;
		case DBType.SQLSERVER:
			conf.set("orderColumns",new String[] {"NEWID()"});
			break;
		case DBType.DM:
			conf.set("orderColumns",new String[] {"rand()"});
			break;
		}
		conf.set("orderTypes",new String[] {"asc"});
		return daoTemplate("common.firstrand", conf).findFirst();
	}
	/**
	 * 根据条件删除数据
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public Ret deleteBy(Kv paras,boolean customCompare) {
		if(MainConfig.DEMO_MODE) {return fail(Msg.DEMO_MODE_CAN_NOT_DELETE);}
		dbTemplate("common.delete", Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras)).delete();
		return SUCCESS;
	}
	/**
	 * 根据条件删除数据
	 * @param paras
	 * @return
	 */
	public Ret deleteBy(Kv paras) {
		return deleteBy(paras, false);
	}


	/**
	 * 切换Boolean类型字段
	 * @param id 需要切换的数据ID
	 * @param columns 需要切换的字段列表
	 * @return
	 */
	public Ret toggleBoolean(Object id,String... columns) {
		return toggleBoolean(null, id, columns);
	}
	/**
	 * 切换Boolean类型字段值
	 * @param kv 额外传入的参数 用于 toggleExtra里用
	 * @param id 需要切换的数据ID
	 * @param columns 需要切换的字段列表
	 * @return
	 */
	public Ret toggleBoolean(Kv kv,Object id,String... columns) {
		if(notOk(id)){
			return fail(Msg.PARAM_ERROR);
		}
		M model=findById(id);
		if(model==null){
			return fail(Msg.DATA_NOT_EXIST);
		}
		Boolean value;
		for(String column:columns){
			String msg = checkCanToggle(model,column,kv);
			if(StrKit.notBlank(msg)){
				return fail(msg);
			}
			value=model.getBoolean(column);
			model.set(column,(value==null?true:!value));
			//处理完指定这个字段 还需要额外处理什么？
			msg=afterToggleBoolean(model,column,kv);
			if(StrKit.notBlank(msg)){
				return fail(msg);
			}
		}
		boolean success=model.update();
		return success?success(model,Msg.SUCCESS):FAIL;
	}
	
	/**
	 * 常用的得到列表数据数量
	 * 自定义参数compare
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public int getCount(Kv paras,boolean customCompare){
		return dbTemplate("common.count",Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras)).queryInt();
	}
	/**
	 * 查询数量
	 * @param sql
	 * @return
	 */
	public int getCount(Sql sql) {
		if(!sql.isQueryCount()) {
			sql.count();
		}
		return queryInt(sql);
	}
	/**
	 * 常用的得到列表数据数量
	 * @param paras
	 * @return
	 */
	public int getCount(Kv paras){
		return getCount(paras, false);
	}
	

	/**
	 * 得到新数据的排序Rank值 默认从1开始 不带任何查询条件
	 * @return
	 */
	public int getNextSortRank(){
		return getNextSortRank(null, false);
	}
	
	/**
	 * 指定条件为pid的值 判断
	 * @param pidName
	 * @param pidValue
	 * @return
	 */
	public int getNextSortRankByPid(String pidName,Object pidValue) {
		return getNextSortRank(Kv.by(pidName, pidValue==null?0:pidValue), false);
	}
	/**
	 * 指定条件为pid的值 判断
	 * @param pidValue
	 * @return
	 */
	public int getNextSortRankByPid(Object pidValue) {
		return getNextSortRankByPid("pid",pidValue);
	}
	/**
	 * 得到新数据的排序Rank值 从0开始 不带任何查询条件
	 * @return
	 */
	public int getNextRankFromZero(){
		return getNextSortRank(null, true);
	}
	/**
	 * 得到新数据的排序Rank值 从0开始 带查询条件
	 * @param fromZero
	 * @return
	 */
	public int getNextRankFromZero(Kv paras){
		return getNextSortRank(paras, true);
	}
	/**
	 * 得到新数据的排序Rank值 自带简单条件查询默认从1开始
	 * @param paras
	 * @return
	 */
	public int getNextSortRank(Kv paras){
		return getNextSortRank(paras, false);
	}
	/**
	 * 得到新数据的排序Rank值 自带简单条件查询 可以自定义是否从零开始
	 * @param paras
	 * @param fromZero
	 * @return
	 */
	public int getNextSortRank(Kv paras,boolean  fromZero){
		int count=getCount(paras);
		if(fromZero){
			return count;
		}
		return count+1;
	}
	

	/**
	 * 得到新数据的排序Rank值 自带简单条件查询 可以自定义是否从零开始
	 * 条件可定制版
	 * @param kv
	 * @param fromZero
	 * @return
	 */
	public int getNextSortRank(Kv paras,Boolean customCompare,boolean  fromZero){
		int count=getCount(paras, customCompare);
		if(fromZero){
			return count;
		}
		return count+1;
	}
	
	/**
	 * 常用的得到列表数据数量
	 * @return
	 */
	public int getCount(){
		return getCount(null, false);
	}
	
	/**
	 * 更新同级删除数据之后数据的排序
	 * @param sortRank
	 */
	protected void updateSortRankAfterDelete(Integer sortRank) {
		updateSortRankAfterDelete(table(),sortRank);
	}
	
	/**
	 * 更新同级调整父节点数据之后数据的排序
	 * @param sortRank
	 */
	protected void updateSortRankAfterChangeParentNode(Integer sortRank) {
		updateSortRankAfterDelete(table(),sortRank);
	}
	/**
	 * 更新同级调整父节点数据之后数据的排序
	 * @param param
	 * @param sortRank
	 */
	protected void updateSortRankAfterChangeParentNode(Kv params,Integer sortRank) {
		updateSortRankAfterDelete(table(),params,sortRank);
	}
	
	/**
	 * 更新同级删除数据之后数据的排序
	 * @param params
	 * @param sortRank
	 */
	protected void updateSortRankAfterDelete(Kv params, Integer sortRank) {
		updateSortRankAfterDelete(table(), params, sortRank);
	}
	/**
	 * 删除关联子数据
	 * @param pid
	 * @return
	 */
	protected void deleteByPid(Object pid) {
			List<M> ms=getListByPid(pid);
			if(isOk(ms)) {
				//还有子数据 就继续往下找 直到叶子节点
				for(M m:ms) {
					m.delete();
					//继续根据ID去删除
					deleteByPid(m._getIdValue());
				}
			}
	}
	/**
	 * 根据PID获取子数据
	 * @param pid
	 * @return
	 */
	public List<M> getListByPid(Object pid){
		return getCommonList(Kv.by("pid", pid));
	}
	/**
	 * 检测判断表中是否存在一个指定字段是null的数据
	 * @param columnName
	 * @return
	 */
	public boolean existsColumnIsNull(String columnName) {
		columnName = columnName.toLowerCase().trim();
		Sql sql=selectSql().selectId().isNull(columnName).first();
		Object existId = queryColumn(sql);
		return isOk(existId);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * 关键词为空的时候 返回空list
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<M> getAutocompleteList(String keywords, Integer limitCount,String matchColumns) {
		return getAutocompleteList(keywords, limitCount, false,null,matchColumns);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * 关键词为空的时候 返回空list
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param returnColumns 查询字段 可以是* 也可以是 id,name这种逗号隔开
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<M> getAutocompleteList(String keywords, Integer limitCount,String returnColumns,String matchColumns) {
		return getAutocompleteList(keywords, limitCount, false, Sql.KEY_STAR,matchColumns);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * 关键词为空的时候 返回空list
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @param whereSql 额外条件sql
	 * @return
	 */
	public List<M> getAutocompleteList(String keywords, Integer limitCount,String matchColumns,Sql whereSql) {
		return getAutocompleteList(keywords, limitCount, false, Sql.KEY_STAR, matchColumns,whereSql);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<M> getAutocompleteList(String keywords, Integer limitCount,Boolean always,String matchColumns) {
		return getAutocompleteList(keywords, limitCount, always, null,matchColumns);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @param whereSql 额外条件sql
	 * @return
	 */
	public List<M> getAutocompleteList(String keywords, Integer limitCount,Boolean always,String matchColumns,Sql whereSql) {
		return getAutocompleteList(keywords, limitCount, always, Sql.KEY_STAR, matchColumns,whereSql);
	}
	
	/**
	 * 关键词查询指定返回个数的数据
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param returnColumns 查询字段 可以是* 也可以是 id,name这种逗号隔开
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<M> getAutocompleteList(String keywords, Integer limitCount,Boolean always,String returnColumns,String matchColumns) {
		return getAutocompleteList(keywords, limitCount, always, returnColumns, matchColumns,null);
	}
	/**
	 * 关键词查询指定返回个数的数据
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param returnColumns 查询字段 可以是* 也可以是 id,name这种逗号隔开
	 * @param matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @param whereSql 额外条件
	 * @return
	 */
	public List<M> getAutocompleteList(String keywords, Integer limitCount,Boolean always,String returnColumns,String matchColumns,Sql whereSql) {
		if((notOk(keywords)&&(always==null||always==false))||notOk(matchColumns)) {
			return Collections.emptyList();
		}
		if(whereSql==null) {
			whereSql=selectSql();
		}
		whereSql.select(returnColumns).firstPage(limitCount).orderByIdAscIfPgSql();
		//如果关键词为空 默认是返回空数据
			//但是如果指定了关键词是空 就按照指定个数返回数据的话
		if(notOk(keywords)&&always!=null&&always==true) {
			return find(whereSql);
		}
		
		String[] columns=ArrayUtil.from(matchColumns, ",");
		if(columns==null||columns.length==0) {
			return Collections.emptyList();
		}
		int size=columns.length;
		whereSql.bracketLeft();
		keywords=keywords.trim();
		for(int i=0;i<size;i++) {
			whereSql.like(columns[i],keywords);
			if(i<size-1) {
				whereSql.or();
			}
		}
		whereSql.bracketRight();
		return find(whereSql);
	}
	/**
	 * 快速获取sql 默认是select sql
	 * @return
	 */
	public Sql selectSql() {
		return Sql.me(dbType()).select().from(table());
	}
	/**
	 * 快速获取sql 使用jboltTableMenuFilter初始化
	 * @param jboltTableMenuFilter
	 * @param matchColumns
	 * @return
	 */
	public Sql selectSql(JBoltTableMenuFilter jboltTableMenuFilter,String... matchColumns) {
		Sql sql = selectSql();
		processJBoltTableMenuFilterSql(sql,jboltTableMenuFilter,matchColumns);
		return sql;
	}
	
	/**
	 * 快速获取sql 使用jboltTableMenuFilter初始化
	 * @param jboltTableMenuFilter
	 * @param mainTableAsName
	 * @param matchColumns
	 * @return
	 */
	public Sql selectSql(JBoltTableMenuFilter jboltTableMenuFilter,String mainTableAsName,String... matchColumns) {
		Sql sql = selectSql().from(table(), mainTableAsName);
		processJBoltTableMenuFilterSql(sql,jboltTableMenuFilter,matchColumns);
		return sql;
	}
	/**
	 * 快速获取update sql
	 * @return
	 */
	public Sql updateSql() {
		return Sql.me(dbType()).update(table());
	}
	/**
	 * 快速获取delete sql
	 * @return
	 */
	public Sql deleteSql() {
		return Sql.me(dbType()).delete().from(table());
	}
	
	
	/**
	 * 执行查询
	 * @param sql
	 * @return
	 */
	public List<M> find(Sql sql){
		if(sql.isPrepared()) {
			return find(sql.toSql(),sql.getWhereValues());
		}
		return find(sql.toSql());
	}
	/**
	 * 执行查询
	 * @param sql
	 * @return
	 */
	public List<Record> findRecord(Sql sql){
		if(sql.isPrepared()) {
			return findRecord(sql.toSql(),sql.getWhereValues());
		}
		return findRecord(sql.toSql());
	}
	/**
	 * 执行查询
	 * @param sql
	 * @param paras
	 * @return
	 */
	public List<M> find(Sql sql,Object... paras){
		return find(sql.toSql(),paras);
	}
	
	/**
	 * 执行查询
	 * @param sql
	 * @return
	 */
	public List<M> find(String sql){
		return dao().find(sql);
	}
	/**
	 * 执行查询
	 * @param sql
	 * @return
	 */
	public List<Record> findRecord(String sql){
		return Db.use(dataSourceConfigName()).find(sql);
	}
	/**
	 * 执行查询
	 * @param sql
	 * @param paras
	 * @return
	 */
	public List<M> find(String sql,Object... paras){
		return dao().find(sql, paras);
	}
	/**
	 * 执行查询
	 * @param sql
	 * @param paras
	 * @return
	 */
	public List<Record> findRecord(String sql,Object... paras){
		return Db.use(dataSourceConfigName()).find(sql, paras);
	}
	/**
	 * 通过关键词查询数据List
	 * @param keywords
	 * @param orderColumn
	 * @param orderType
	 * @param matchColumns
	 * @return
	 */
	public List<M> getCommonListByKeywords(String keywords,String orderColumn,String orderType,String matchColumns) {
		return getCommonListByKeywords(keywords, Sql.KEY_STAR, orderColumn, orderType, matchColumns, null);
	}
	/**
	 * 通过关键词查询数据LIst
	 * @param keywords
	 * @param orderColumn
	 * @param matchColumns
	 * @return
	 */
	public List<M> getCommonListByKeywords(String keywords,String orderColumn,String matchColumns) {
		return getCommonListByKeywords(keywords,orderColumn, "asc", matchColumns);
	}
	/**
	 *通过关键词查询数据List
	 * @param keywords
	 * @param orderColumn
	 * @param orderType
	 * @param matchColumns
	 * @param otherParas
	 * @return
	 */
	public List<M> getCommonListByKeywords(String keywords,String orderColumn,String orderType,String matchColumns,Kv otherParas) {
		return getCommonListByKeywords(keywords, Sql.KEY_STAR, orderColumn, orderType, matchColumns, otherParas);
	}
	/**
	 * 通过关键词查询数据List
	 * @param keywords
	 * @param returnColumns
	 * @param orderColumn
	 * @param orderType
	 * @param matchColumns
	 * @return
	 */
	public List<M> getCommonListByKeywords(String keywords,String returnColumns,String orderColumn,String orderType,String matchColumns) {
		return getCommonListByKeywords(keywords, returnColumns, orderColumn, orderType, matchColumns, null);
	}
	/**
	 *	通过关键词查询数据List底层封装 
	 * @param keywords
	 * @param returnColumns
	 * @param orderColumn
	 * @param orderType
	 * @param matchColumns
	 * @param otherParas
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<M> getCommonListByKeywords(String keywords,String returnColumns,String orderColumn,String orderType,String matchColumns,Kv otherParas) {
		if(notOk(matchColumns)) {
			return Collections.emptyList();
		}
		Sql sql=selectSql().select(returnColumns);
		if(isOk(orderColumn)&&isOk(orderType)) {
			sql.orderBy(orderColumn,"desc".equals(orderType));
		}else {
			sql.orderByIdAscIfPgSql();
		}
		if(otherParas!=null&&otherParas.size()>0) {
			sql.bracketLeft();
			otherParas.forEach((key,value)->sql.eq(key.toString(), value));
			sql.bracketRight();
		}
		//如果没有给keywords字段
		if(notOk(keywords)) {
			return find(sql);
		}
		//如果给了Keywords
		String[] columns=ArrayUtil.from(matchColumns, ",");
		if(columns==null||columns.length==0) {
			return Collections.emptyList();
		}
		int size=columns.length;
		sql.bracketLeft();
		keywords=keywords.trim();
		for(int i=0;i<size;i++) {
			sql.like(columns[i],keywords);
			if(i<size-1) {
				sql.or();
			}
		}
		sql.bracketRight();
		
		return find(sql);
		
	}
	/**
	 * 根据主键删除缓存
	 * @param id
	 */
	public void deleteCacheById(Object... ids) {
		dao().deleteCacheById(ids);
	}
	/**
	 * 根据指定列值删除缓存
	 * @param columnValue
	 */
	public void deleteCacheByKey(String columnValue) {
		dao().deleteCacheByKey(columnValue,null);
	}
	/**
	 * 根据指定列值删除缓存
	 * @param columnValue
	 * @param bindColumnValue
	 */
	public void deleteCacheByKey(String columnValue,Object bindColumnValue) {
		dao().deleteCacheByKey(columnValue,bindColumnValue);
	}
	/**
	 * 根据指定列值获得缓存
	 * @param columnValue
	 * @return
	 */
	public M getCacheByKey(String columnValue) {
		return dao().loadCacheByKey(columnValue);
	}
	/**
	 * 根据指定列值和绑定列值获得缓存
	 * @param columnValue
	 * @param bindColumnValue
	 * @return
	 */
	public M getCacheByKey(String columnValue,Object bindColumnValue) {
		return dao().loadCacheByKey(columnValue,bindColumnValue);
	}
	
	
	/**
	 * 转换为Model tree
	 * @param allList
	 * @param idColumn
	 * @param pidColumn
	 * @param checkIsParentFunc
	 * @return
	 */
	public List<M> convertToModelTree(List<M> allList,String idColumn,String pidColumn,TreeCheckIsParentNode<M> checkIsParentFunc) {
		if(notOk(allList)) {return allList;}
		List<M> parents=new ArrayList<M>();
		M m;
		for(int i=0;i<allList.size();i++) {
			m=allList.get(i);
			m.putEachLevel(1);
			if(checkIsParentFunc.isParent(m)) {
				parents.add(m);
				allList.remove(m);
				i--;
			}
		}
		if(parents.size()>0&&allList.size()>0) {
			processTreeNodes(allList,parents,idColumn,pidColumn);
		}
		return parents;
	}
	
	/**
	 * 处理tree节点
	 * @param allList
	 * @param parents
	 * @param idColumn
	 * @param pidColumn
	 */
	private void processTreeNodes(List<M> allList, List<M> parents, String idColumn, String pidColumn) {
		ListMap<String, M> map=new ListMap<String, M>();
		for(M m:allList) {
			map.addItem("p_"+m.get(pidColumn), m);
		}
		for(M p:parents) {
			processTreeSubNodes(map,p,idColumn);
		}
		
	}
	/**
	 * 递归处理tree子节点
	 * @param map
	 * @param p
	 * @param idColumn
	 */
	private void processTreeSubNodes(ListMap<String, M> map, M p,String idColumn) {
		List<M> items=map.get("p_"+p.get(idColumn));
		if(items!=null&&items.size()>0) {
			for(M item:items) {
				item.processEachLevelByParentLevel(p.getEachLevel());
				processTreeSubNodes(map, item,idColumn);
			}
		}
		p.putItems(items);
	}
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param openLevel -1全部 0 不动 >0指定层级层级
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId,int openLevel) {
		return convertJsTree(datas, selectedId, openLevel, null,false);
	}
	
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param openLevel -1全部 0 不动 >0指定层级模式
	 * @param needOriginData 是否需要保留原始Data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId,int openLevel,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, null,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级层级
	 * @param keyColumn 多级下 key字段名字
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId, int openLevel,String keyColumn) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn,null,false);
	}
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级层级
	 * @param keyColumn 多级下 key字段名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId, int openLevel,String keyColumn,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn,null,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级层级
	 * @param keyColumn 多级下 key字段名字
	 * @param rootName 根节点名字
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId, int openLevel,String keyColumn,String rootName) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn, "name",rootName,false);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级层级
	 * @param keyColumn 多级下 key字段名字
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId, int openLevel,String keyColumn,String rootName,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn, "name",rootName,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级层级
	 * @param keyColumn 多级下 key字段名字
	 * @param textColumn 显示文本字段
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId, int openLevel,String keyColumn,String textColumn,String rootName,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn,textColumn,"enable",rootName,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级层级
	 * @param keyColumn 多级下 key字段名字
	 * @param textColumn 显示文本字段
	 * @param enableColumn 启用禁用字段
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始Data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId, int openLevel,String keyColumn,String textColumn,String enableColumn,String rootName,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn, "id", "pid",textColumn,enableColumn,rootName,needOriginData,null);
	}
	
	private void processJsTreeType(List<M> jstreeMs) {
		if(isOk(jstreeMs)) {
			List<M> items;
			for(M m:jstreeMs){
				items=m.getItems();
				if(isOk(items)) {
					m.put("js_tree_type","parent");
					processJsTreeType(items);
				}else {
					m.put("js_tree_type","node");
				}
			}
		}
	}
	
	private String[] getTreeColumnKeys(M m,String idColumn,String pidColumn) {
		List<String> keys=new ArrayList<String>();
		keys.add(m.getStr(idColumn));
		Object pid=m.getStr(pidColumn);
		if(pid!=null&&pid.toString().equals("0")==false) {
			processTreeColumnKeys(keys, pid, idColumn, pidColumn);
		}
		return keys.toArray(new String[keys.size()]);
	}
	private void processTreeColumnKeys(List<String> keys,Object id,String idColumn,String pidColumn) {
		if(id==null || id.toString().equals("0")) {
			return;
		}
		M m=findById(id);
		if(m==null) {
			return;
		}
		//pid有值 需要加入keys
		keys.add(0, id.toString());
		Object pid=m.getStr(pidColumn);
		if(pid!=null&&pid.toString().equals("0")==false) {
			//pid有值 需要加入keys
			processTreeColumnKeys(keys, pid, idColumn, pidColumn);
		}
	}

	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param idColumn 节点 标识字段
	 * @param pidColumn 节点父标识字段
	 * @param textColumn 显示文本字段
	 * @param enableColumn 启用禁用字段
	 * @param rootName 跟节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<M> datas,Object selectedId,int openLevel,String keyColumn,String idColumn,String pidColumn,String textColumn,String enableColumn,String rootName,boolean needOriginData,TreeCheckIsParentNode<M> checkIsParentFunc) {
		List<JsTreeBean> treeBeans=new ArrayList<JsTreeBean>();
		boolean hasRoot=isOk(rootName);
		if(hasRoot) {
			treeBeans.add(new JsTreeBean(0, 0, rootName, openLevel!=0,openLevel==0?"root":"root_opened",true));
		}
		if(datas.size()>0){
			String[] keys=null;
			if(isOk(selectedId)){
				M m=findById(selectedId);
				if(m != null) {
					if(isOk(keyColumn)) {
						String key=m.get(keyColumn);
						if(isOk(key)) {
							keys=ArrayUtil.from(key, "_");
						}
					}else {
						keys=getTreeColumnKeys(m,idColumn,pidColumn);
					}
				}
			}
			
			List<M> jstreeMs=null;
			if(checkIsParentFunc!=null) {
				jstreeMs = convertToModelTree(datas, idColumn, pidColumn, (p)->checkIsParentFunc.isParent(p));
			}else {
				jstreeMs = convertToModelTree(datas, idColumn, pidColumn, (p)->{
					Object v=p.get(pidColumn);
					return v==null||v.toString().equals("0");
				});
			}
			
			processJsTreeType(jstreeMs);
			
			processJsTree(treeBeans,jstreeMs,openLevel,1,idColumn,pidColumn,textColumn,enableColumn,keys,selectedId,hasRoot,needOriginData);
		}
		return treeBeans;
	}
	
	private void processJsTree(List<JsTreeBean> treeBeans, List<M> jstreeMs, int openLevel,int currentLevel,String idColumn,String pidColumn, String textColumn,String enableColumn,String[] keys,Object selectedId,boolean hasRoot,boolean needOriginData) {
		if(notOk(jstreeMs)) {return;}
		boolean opened=false;
		boolean keysIsOk=isOk(keys);
		Object idValue;
		Object pidValue;
		String type;
		for(M m:jstreeMs){
			opened=openLevel==-1||false;
			boolean selected=false;
			
			pidValue=m.get(pidColumn);
			if(opened==false&&currentLevel<=openLevel) {
				opened=true;
			}
			idValue=m.get(idColumn);
			if(keysIsOk&&idValue!=null&&idValue.toString().equals("0")==false&&ArrayUtil.contains(keys,idValue.toString())){
				selected=false;
				opened=true;
			}
			if(selectedId!=null&&idValue.toString().equals(selectedId.toString())){
				selected=true;
				opened=true;
			}
			
			type=m.get("js_tree_type");
			if(notOk(type)) {
				type="parent";
			}
			if("node".equals(type)==false&&opened) {
				type="parent_opened";
			}
			treeBeans.add(convertToJsTreeBean(m, opened, selected, type, idColumn, pidColumn, textColumn, enableColumn,hasRoot,needOriginData));
			processJsTree(treeBeans, m.getItems(), openLevel,(currentLevel+1), idColumn, pidColumn, textColumn, enableColumn, keys, selectedId,hasRoot,needOriginData);
		}
		
	}

	public JsTreeBean convertToJsTreeBean(M m,boolean opened,boolean selected,String type,String idColumn,String pidColumn,String textColumn,String enableColumn,boolean hasRoot,boolean needOriginData) {
		if(needOriginData) {
			return new JsTreeBean(m.get(idColumn), m.get(pidColumn),getEnableName(m, textColumn, enableColumn),opened,selected,type,hasRoot).setData(m);
		}
		return new JsTreeBean(m.get(idColumn), m.get(pidColumn),getEnableName(m, textColumn, enableColumn),opened,selected,type,hasRoot);
	}
	
	/**
	 * 树形结构上单独使用的
	 * @return
	 */
	private String getEnableName(M m,String textColumn,String enableColumn) {
		String name=m.get(textColumn);
		Boolean enable=m.getBoolean(enableColumn);
		return (enable!=null&&enable)?name:(name+"[<span class='text-danger'>禁用</span>]");
	}
	
	/**
	 * 更新指定ID数据的指定字段的值
	 * @param id
	 * @param column
	 * @param value
	 * @return
	 */
	public Ret updateColumn(Object id,String column,Object value) {
		M m = findById(id);
		if(m==null) {
			return fail(Msg.DATA_NOT_EXIST);
		}
		//如果存在就可以操作了
		m.set(column, value);
		boolean success = m.update();
		if(success) {
			return successWithData(m);
		}
		return FAIL;
	}
	/**
	 * 更新字段值
	 * @param m
	 * @return
	 */
	public Ret updateOneColumn(M m) {
		if(m==null) {return fail(Msg.PARAM_ERROR);}
		String primaryKey=m._getPrimaryKey();
		if(m.hasColumn(primaryKey)==false) {
			return fail(Msg.PARAM_ERROR+",未设置主键参数");
		}
		Object idValue=m._getIdValue();
		if(idValue==null) {
			return fail(Msg.PARAM_ERROR+",未设置主键值");
		}
		String[] attrNames=m._getAttrNames();
		if(attrNames.length!=2) {
			return fail(Msg.PARAM_ERROR+",除了主键还需要设置更新字段与值");
		}
		String column=null;
		Object value=null;
		for(String attr:attrNames) {
			if(primaryKey.equalsIgnoreCase(attr)){
				continue;
			}
			column=attr;
			value=m.get(column);
		}
		return updateColumn(idValue, column, value);
	}
	
	/**
	 * 清空缓存
	 */
	public void clearCache() {
		dao().each(config->{
			config.clearCache();
			return true;
		}, selectSql().toSql());
	}
	
	
	/**
	 * 得到所有的一级
	 * 表结构为树结构的使用
	 * @param pidName
	 * @param parentPidValue
	 * @param sortColumnName
	 * @param desc
	 * @return
	 */
	public List<M> getAllParents(String pidName,Object parentPidValue,String sortColumnName,boolean desc) {
		return getCommonList(Kv.by(pidName, parentPidValue),sortColumnName,desc?"desc":"asc");
	}
	/**
	 * 得到所有的一级
	 * 表结构为树结构的使用
	 * @param pidName
	 * @param parentPidValue
	 * @param sortColumnName
	 * @return
	 */
	public List<M> getAllParents(String pidName,Object parentPidValue,String sortColumnName) {
		return getAllParents(pidName, parentPidValue, sortColumnName, false);
	}
	/**
	 * 得到所有的一级
	 * 表结构为树结构的使用
	 * @param parentPidValue
	 * @return
	 */
	public List<M> getAllParents(Object parentPidValue) {
		return getAllParents("pid",parentPidValue, "sort_rank");
	}
	
	/**
	 * 得到所有的一级
	 * 表结构为树结构的使用
	 * @param parentPidValue
	 * @return
	 */
	public List<M> getAllParents() {
		return getAllParents(0);
	}
	
	/**
	 * 得到指定父级的下级
	 * @return
	 */
	public List<M> getSons(String pidName,Object pidValue,String sortColumnName) {
		return getCommonList(Kv.by(pidName, pidValue),sortColumnName);
	}
	/**
	 * 得到指定父级的下级
	 * @return
	 */
	public List<M> getSons(Object pidValue) {
		return getSons("pid", pidValue, "sort_rank");
	}
	/**
	 * 得到所有的一级数据 按照ID 正序排序
	 * @return
	 */
	public List<M> getAllParentsOrderById() {
		return getAllParents("pid", 0, "id");
	}
	/**
	 * 得到指定父级的下级数据 按照ID 正序排序
	 * @param pidValue
	 * @return
	 */
	public List<M> getSonsOrderById(Object pidValue) {
		return getSons("pid", pidValue,"id");
	}
	/**
	 * 批量更新
	 * @param models
	 */
	public void batchUpdate(List<M> models) {
		int batchSize=models.size();
		batchUpdate(models, batchSize>500?500:batchSize);
	}
	/**
	 * 批量更新
	 * @param models
	 * @param batchSize
	 */
	public void batchUpdate(List<M> models, int batchSize) {
		Db.use(dataSourceConfigName()).batchUpdate(models, batchSize);
	}
	/**
	 * 批量插入
	 * @param models
	 */
	public void batchSave(List<M> models) {
		int batchSize=models.size();
		batchSave(models, batchSize>500?500:batchSize);
	}
	/**
	 * 批量插入
	 * @param models
	 * @param batchSize
	 */
	public void batchSave(List<M> models, int batchSize) {
		Db.use(dataSourceConfigName()).batchSave(models, batchSize);
	}
	
	/**
	 * 有数据
	 * @return
	 */
	public boolean notEmpty() {
		int count = getCount();
		return count>0;
	}
	/**
	 * 是否为空 没数据
	 * @return
	 */
	public boolean isEmpty() {
		int count = getCount();
		return count==0;
	}
	
	/**
	 * 查询基于menuFilter
	 * @param jboltTableMenuFilter
	 * @param matchColumns
	 * @return
	 */
	public Page<M> paginateByJboltTableMenuFilter(JBoltTableMenuFilter jboltTableMenuFilter,String[] matchColumns) {
		return paginate(selectSql(jboltTableMenuFilter,matchColumns));
	}
	
	
}
