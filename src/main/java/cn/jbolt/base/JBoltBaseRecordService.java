package cn.jbolt.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbTemplate;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.jbolt.base.para.jbolttablemenufilter.JBoltTableMenuFilter;
import cn.jbolt.common.bean.JsTreeBean;
import cn.jbolt.common.config.MainConfig;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.DBType;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.model.WechatUser;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CamelCaseUtil;
import cn.jbolt.common.util.ListMap;
import cn.jbolt.common.util.TreeCheckIsParentNode;

/**
 * 采用Db+record模式实现 单表版
 * @ClassName:  JBoltCommonRecordService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年7月20日   
 */
public abstract class JBoltBaseRecordService extends JBoltCommonService{
	/**
	 *  获取表名
	 * @return
	 */
	protected abstract String table();
	/**
	 * 获取序列名 默认就是表名
	 * @return
	 */
	public String idSequence() {
		return table();
	}
	/**
	 *  获取Sql模板 Db模式
	 * @param key
	 * @param data
	 * @return
	 */
	public DbTemplate dbTemplate(String key,Kv data) {
		data.setIfNotBlank("table", table());
		return Db.use(dataSourceConfigName()).template(key, data);
	}

	/**
	 * 得到下拉列表数据
	 * @param textColumn
	 * @param valueColumn
	 * @param paras
	 * @return
	 */
	public List<Record> getOptionList(String textColumn,String valueColumn,Kv paras){
		Kv conf=Kv.by("value",valueColumn).set("text",textColumn).set("myparas", paras).set("customCompare",false);
		return dbTemplate("common.optionlist", conf).find();
	}
	

	/**
	 * 得到下拉列表数据
	 * @param textColumn
	 * @param valueColumn
	 * @return
	 */
	public List<Record> getOptionList(String textColumn,String valueColumn){
		Kv conf=Kv.by("value",valueColumn).set("text",textColumn).set("customCompare",false);
		return dbTemplate("common.optionlist", conf).find();
	}
	/**
	 * 得到下拉列表数据
	 * @return
	 */
	public List<Record> getOptionList(){
		Kv conf=Kv.by("value",primaryKey()).set("text","name").set("customCompare",false);
		return dbTemplate("common.optionlist", conf).find();
	}
	 
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 自定义参数compare
	 * @param columns
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public List<Record> getCommonList(String columns,Kv paras,boolean customCompare){
		return dbTemplate("common.list", 
				Kv.by("customCompare",customCompare)
				.setIfNotNull("myparas", paras)
				.setIfNotBlank("columns",columns))
				.find();
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 可以自定义参数compare 默认=
	 *
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public List<Record> getCommonList(Kv paras,boolean customCompare){
		return getCommonList("*",paras, customCompare);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 不能自定义参数compare 默认=
	 *
	 * @param paras
	 * @return
	 */
	public List<Record> getCommonList(Kv paras){
		return getCommonList("*",paras, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 不能排序
	 * 不能自定义参数compare 默认=
	 *
	 * @param columns 指定查询的列
	 * @param paras
	 * @return
	 */
	public List<Record> getCommonList(String columns,Kv paras){
		return getCommonList(columns,paras,false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 默认正序 
	 * 不能自定义参数compare 默认=
	 *
	 * @param paras
	 * @param orderColums
	 * @return
	 */
	public List<Record> getCommonList(Kv paras,String orderColums){
		int count=StrUtil.count(orderColums, ",");
		String orderTypes="";
		for(int i=0;i<=count;i++){
			if(i==0){
				orderTypes="asc";
			}else{
				orderTypes=orderTypes+","+"asc";
			}
		}
		return getCommonList("*",paras, orderColums, orderTypes, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 *
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @return
	 */
	public List<Record> getCommonList(Kv paras,String orderColumns,String orderTypes){
		return getCommonList("*",paras, orderColumns, orderTypes, false);
	}
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 *
	 * @param columns
	 * @param paras
	 * @param sort
	 * @param orderType
	 * @return
	 */
	public List<Record> getCommonList(String columns,Kv paras,String orderColumns,String orderTypes){
		return getCommonList(columns,paras, orderColumns, orderTypes, false);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 不能自定义参数compare 默认=
	 * @param columns
	 * @param orderColumns
	 * @param orderType
	 * @return
	 */
	public List<Record> getCommonList(String columns,String orderColumns,String orderTypes){
		return getCommonList(columns,null, orderColumns, orderTypes, false);
	}
	
	/**
	 * 常用的得到列表数据的方法
	 * 不分页版
	 * 可以排序
	 * 自定义参数compare
	 *
	 * @param columns
	 * @param paras
	 * @param orderColumn
	 * @param orderType
	 * @param customCompare
	 * @return
	 */
	public List<Record> getCommonList(String columns,Kv paras,String orderColumns,String orderTypes,boolean customCompare){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		if(isOk(columns)){
			conf.set("columns",columns);
		}
		if(isOk(orderColumns)){
			conf.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			conf.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return dbTemplate("common.list",conf).find();
	}
	
	/**
	 * 分页查询
	 * @param sql
	 * @param columnToCamelCase 字段名转驼峰
	 * @return
	 */
	public Page<Record> paginate(Sql sql,boolean columnToCamelCase){
		Sql totalCountSql=ObjectUtil.clone(sql);
		totalCountSql.count();
		return processPaginate(sql, totalCountSql , columnToCamelCase);
	}
	
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 自定义参数compare
	 * 分页查询
	 *
	 * @param columns
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @param or
	 * @return
	 */
	public Page<Record> paginate(String columns,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare,boolean or){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		conf.set("or",or);
		conf.setIfNotBlank("columns",columns);
		if(isOk(orderColumns)){
			conf.set("orderColumns",ArrayUtil.from(orderColumns, ","));
		}
		if(isOk(orderTypes)){
			conf.set("orderTypes",ArrayUtil.from(orderTypes, ","));
		}
		return dbTemplate("common.list",conf).paginate(pageNumber, pageSize);
	}
	
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 *
	 * @param columns
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(String columns,Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate(columns, paras, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 *
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @param or
	 * @return
	 */
	public Page<Record> paginate(Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare,boolean or){
		return paginate("*", paras, orderColumns, orderTypes, pageNumber, pageSize, customCompare,or);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 *
	 * @param paras
	 * @param orderColumns
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize,boolean customCompare){
		return paginate("*", paras, orderColumns, orderTypes, pageNumber, pageSize, customCompare,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 *
	 * @param paras
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(Kv paras,String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate("*", paras, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 可以排序
	 * 条件都是等于
	 * 分页查询
	 *
	 * @param orderColumns
	 * @param orderTypes
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(String orderColumns,String orderTypes,int pageNumber, int pageSize){
		return paginate("*", null, orderColumns, orderTypes, pageNumber, pageSize, false,false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不尅一可以排序
	 * 条件自定义 customCompare
	 * 分页查询
	 *
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(String columns,Kv paras,int pageNumber, int pageSize,boolean customCompare){
		Kv conf=Kv.by("myparas", paras).set("customCompare",customCompare);
		if(isOk(columns)){
			conf.set("columns",columns);
		}
		return dbTemplate("common.list",conf).paginate(pageNumber, pageSize);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 条件自定义 customCompare
	 * 分页查询
	 *
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Kv paras,int pageNumber, int pageSize,boolean customCompare){
		return paginate("*", paras, pageNumber, pageSize, customCompare);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 *
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Kv paras,int pageNumber, int pageSize){
		return paginate("*", paras, pageNumber, pageSize, false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 *
	 * @param columns
	 * @param paras
	 * @param pageNumber
	 * @param pageSize
	 * @param customCompare
	 * @return
	 */
	public Page<Record> paginate(Kv paras,int pageSize,boolean customCompare){
		return paginate("*", paras, 1, pageSize, customCompare);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 *
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(int pageNumber,int pageSize){
		return paginate("*", null, pageNumber, pageSize, false);
	}
	/**
	 * 按照sql模板分页查询
	 *
	 * @param key
	 * @param data
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginateBySqlTemplate(String key,Kv data,int pageNumber,int pageSize){
		return dbTemplate(key, data).paginate(pageNumber, pageSize);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 *
	 * @param paras
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(Kv paras,int pageSize){
		return paginate("*", paras, 1, pageSize, false);
	}
	/**
	 * 常用的得到分页列表数据的方法
	 * 不可以排序
	 * 分页查询
	 *
	 * @param pageSize
	 * @return
	 */
	public Page<Record> paginate(int pageSize){
		return paginate("*", null, 1, pageSize, false);
	}
	/**
	 * 根据ID 批量删除 指定分隔符版
	 * @param ids
	 * @param split 分隔符
	 * @return
	 */
	public Ret deleteByIds(String ids,String split,boolean checkCanDelete) {
		return deleteByIds(ArrayUtil.from3(ids, split),checkCanDelete);
	}
	/**
	 * 根据ID 批量删除 指定分隔符版
	 * @param ids
	 * @param split 分隔符
	 * @return
	 */
	public Ret deleteByIds(String ids,String split) {
		return deleteByIds(ids,split,false);
	}
	
	/**
	 * 根据ID 批量删除 默认分隔符是逗号 
	 * @param ids
	 * @return
	 */
	public Ret deleteByIds(String ids) {
		return deleteByIds(ids,false);
	}
	/**
	 * 根据ID 批量删除 默认分隔符是逗号
	 * @param ids
	 * @param checkCanDelete
	 * @return
	 */
	public Ret deleteByIds(String ids,boolean checkCanDelete) {
		return deleteByIds(ids,",",checkCanDelete);
	}
	
	/**
	 * 根据ID 批量删除
	 * @param ids
	 * @return
	 */
	public Ret deleteByIds(Object[] ids) {
		return deleteByIds(ids,false);
	}
	/**
	 * 根据ID 批量删除
	 * @param ids
	 * @param checkCanDelete
	 * @return
	 */
	public Ret deleteByIds(Object[] ids,boolean checkCanDelete) {
		if(notOk(ids)) {return fail(Msg.PARAM_ERROR);}
		Ret ret;
		for(Object id:ids) {
			ret=deleteById(id,checkCanDelete);
			if(ret.isFail()) {
				return ret;
			}
		}
		return SUCCESS;
	}
	
	/**
	 * 通用根据ID删除数据
	 *
	 * @param id
	 * @return
	 */
	public Ret deleteById(Object id){
		return deleteById(id,null);
	}
	/**
	 * 通用根据ID删除数据
	 *
	 * @param id
	 * @param kv 额外参数
	 * @return
	 */
	public Ret deleteById(Object id,Kv kv){
		return deleteById(id, false,kv);
	}
	/**
	 * 通用根据ID删除数据
	 * @param id
	 * @param checkCanDelete
	 * @return
	 */
	public Ret deleteById(Object id,boolean checkCanDelete){
		return deleteById(id, checkCanDelete,null);
	}
	/**
	 * 通用根据ID删除数据
	 *
	 * @param id
	 * @param checkCanDelete
	 * @param kv 额外参数
	 * @return
	 */
	public Ret deleteById(Object id,boolean checkCanDelete,Kv kv){
		return deleteById(id, false,checkCanDelete,kv);
	}
	
	/**
	 * 通用根据ID删除数据 需要先检查是否被其他地方使用
	 *
	 * @param id
	 * @param checkInUse
	 * @param returnDeleteData
	 * @param kv
	 * @return
	 */
	public Ret deleteById(Object id,boolean checkInUse,boolean returnDeleteData,Kv kv){
		if(MainConfig.DEMO_MODE) {return fail(Msg.DEMO_MODE_CAN_NOT_DELETE);}
		if(notOk(id)){
			return fail(Msg.PARAM_ERROR);
		}
		String tableName=table();
		Record record=Db.use(dataSourceConfigName()).findById(tableName,id);
		if(record==null){
			return fail(Msg.DATA_NOT_EXIST);
		}
		if(checkInUse){
			String msg=checkCanDelete(record,kv);
			if(StrKit.notBlank(msg)){return fail(msg);}
		}
		
		boolean success=Db.use(dataSourceConfigName()).delete(tableName, record);
		if(success) {
			String msg=afterDelete(record,kv);
			if(isOk(msg)){return fail(msg);}
		}
		//需要返回删除的数据
		if(returnDeleteData) {
			return success?success(record,Msg.SUCCESS):FAIL;
		}
		return ret(success);
	}
	
	/**
	 * 删除成功后需要处理的事情
	 * @param record
	 * @param kv
	 * @return
	 */
	protected String afterDelete(Record record,Kv kv){
		return null;
	}
	
	/**
	 * 额外需要处理toggle操作
	 * @param record
	 * @param column
	 * @param kv
	 * @return
	 */
	protected String afterToggleBoolean(Record record,String column,Kv kv){
		return null;
	}
	
	/**
	 * 检测数据是否可以被删除 带额外数据
	 * @param record
	 * @param kv
	 * @return
	 */
	public String checkCanDelete(Record record,Kv kv){
		return null;
	}
	
	/**
	 * 检测数据是否被其它数据外键引用
	 *
	 * @param record
	 * @param kv 额外参数
	 * @return
	 */
	public String checkInUse(Record record,Kv kv){
		return null;
	}
	/**
	 * 检测数据是否字段是否可以执行切换true false
	 *
	 * @param record
	 * @param column
	 * @param kv
	 * @return
	 */
	public String checkCanToggle(Record record,String column,Kv kv){
		return null;
	}
	/**
	 * 判断字典名是否存在 排除指定ID
	 *
	 * @param name
	 * @param id
	 * @return
	 */
	public boolean existsName(String name,Object id) {
		name = name.toLowerCase().trim();
		Sql sql=selectSql().selectId().eqQM("name").idNoteqQM().first();
		Object existId = queryColumn(sql, name, id);
		return isOk(existId);
	}

	/**
	 * 判断是否存在
	 *
	 * @param columnName
	 * @param value
	 * @param id
	 * @return
	 */
	public boolean exists(String columnName,Serializable value,Object id) {
		value = value.toString().trim();
		Sql sql=selectSql().selectId().eqQM(columnName).idNoteqQM().first();
		Object existId = queryColumn(sql, value,id);
		return isOk(existId);
	}
	/**
	 * 判断是否存在
	 *
	 * @param columnName
	 * @param value
	 * @return
	 */
	public boolean exists(String columnName,Serializable value) {
		return exists(columnName, value, -1);
	}
	/**
	 * 判断字典名是否存在
	 *
	 * @param name
	 * @return
	 */
	public boolean existsName(String name) {
		return existsName(name, -1);
	}
	/**
	 * 根据ID获得一条数据
	 *
	 * @param id
	 * @return
	 */
	public Record findById(Object id) {
		if(notOk(id)){return null;}
		return Db.use(dataSourceConfigName()).findById(table(),id);
	}
	/**
	 * 根据ID获得一条数据转为WechatUser model
	 *
	 * @param id
	 * @return
	 */
	public WechatUser findByIdToWechatUser(Object id) {
		Record record=findById( id);
		if(record==null) {return null;}
		return new WechatUser()._setAttrs(record.getColumns());
	}
	/**
	 * 得到符合条件的第一个
	 *
	 * @param paras
	 * @return
	 */
	public Record findFirst(Kv paras) {
		return findFirst(paras,false);
	}
	/**
	 * 得到符合条件的第一个
	 *
	 * @return
	 */
	public Record findFirst() {
		return findFirst(null,false);
	}
	/**
	 * 得到符合条件的第一个
	 *
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public Record findFirst(Kv paras,boolean customCompare) {
		return dbTemplate("common.first", Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras)).findFirst();
	}
	/**
	 * 随机得到符合条件的第一个
	 *
	 * @param paras
	 * @return
	 */
	public Record getRandomOne(Kv paras) {
		return getRandomOne(paras, false);
	}
	/**
	 * 随机得到符合条件的第一个
	 *
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public Record getRandomOne(Kv paras,boolean customCompare) {
		Kv conf=Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras);
		conf.set("orderColumns",new String[] {"rand()"});
		conf.set("orderTypes",new String[] {"asc"});
		return dbTemplate("common.firstrand", conf).findFirst();
	}
	/**
	 * 根据条件删除数据
	 *
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	public Ret deleteBy(Kv paras,boolean customCompare) {
		if(MainConfig.DEMO_MODE) {return fail(Msg.DEMO_MODE_CAN_NOT_DELETE);}
		dbTemplate("common.delete", Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras)).delete();
		return SUCCESS;
	}
	/**
	 * 根据条件删除数据
	 *
	 * @param paras
	 * @return
	 */
	public Ret deleteBy(Kv paras) {
		return deleteBy(paras, false);
	}


	/**
	 * 切换Boolean类型字段
	 *
	 * @param id 需要切换的数据ID
	 * @param columns 需要切换的字段列表
	 * @return
	 */
	public Ret toggleBoolean(Object id,String... columns) {
		return toggleBoolean(null, id, columns);
	}
	
	/**
	 * 保存
	 * @param record
	 * @return
	 */
	public Ret save(Record record) {
		return save(record,primaryKey());
	}
	
	/**
	 * 保存
	 *
	 * @param record
	 * @param primaryKeyName
	 * @return
	 */
	public Ret save(Record record,String primaryKeyName) {
		if(record==null||notOk(primaryKeyName)) {
			return fail(Msg.PARAM_ERROR);
		}
		Object idValue = record.getObject(primaryKeyName);
		String idGenMode = idGenMode();
		//如果不需要自己赋值 但是有值了 就报错
		if(!JBoltIDGenMode.isNeedAssign(idGenMode) && isOk(idValue)) {
			return fail(Msg.PARAM_ERROR);
		}
		//如果需要赋值 但是没有赋值
		if(JBoltIDGenMode.isNeedAssign(idGenMode) && notOk(idValue)) {
			//那就自动赋值
			if(StrKit.notBlank()) {
				switch (idGenMode) {
				case JBoltIDGenMode.SNOWFLAKE:
					record.set(primaryKeyName, JBoltSnowflakeKit.me.nextId());
					break;
				case JBoltIDGenMode.UUID:
					record.set(primaryKeyName, IdUtil.fastSimpleUUID());
					break;
				case JBoltIDGenMode.SEQUENCE:
					record.set(primaryKeyName, getIdSequenceStr());
					break;
				case JBoltIDGenMode.SEQUENCE_LONG:
					record.set(primaryKeyName, getIdSequenceStr());
					break;
				}
			}
		}
		boolean success=Db.use(dataSourceConfigName()).save(table(), record);
		return ret(success);
	}
	/**
	 * 保存
	 * @param record
	 * @return
	 */
	protected Ret update(Record record) {
		return update(record,primaryKey());
	}
	
	/**
	 * 更新
	 *
	 * @param record
	 * @return
	 */
	protected Ret update(Record record,String primaryKeyName) {
		if(record==null||notOk(primaryKeyName)||notOk(record.getObject(primaryKeyName))) {
			return fail(Msg.PARAM_ERROR);
		}
		boolean success=Db.use(dataSourceConfigName()).update(table(), record);
		return ret(success);
	}
	
	/**
	 * 获得id主键 自增序列配置
	 * @param _id
	 * @param idSequence 
	 */
	private String getIdSequenceStr() {
		String dbType = dbType();
		String idSequence = idSequence();
		String idSequenceStr=null;
		//如果不是空并且不是default 说明是自己指定的 直接赋值即可
		if(StrKit.notBlank(idSequence) && JBoltConst.JBOLT_ID_SEQUENCE_DEFAULT.equals(idSequence) == false) {
			switch (dbType) {
			case DBType.POSTGRESQL:
				idSequenceStr = "nextval('"+idSequence+"')";
				break;
			case DBType.ORACLE:
				idSequenceStr = idSequence+".nextval";
				break;
			case DBType.DM:
				idSequenceStr = idSequence+".nextval";
				break;
			}
			return idSequenceStr;
		}
		//判断是不是序列类型ID 然后按照内置规则处理
		String idGenMode = idGenMode();
		if (JBoltIDGenMode.SEQUENCE.equalsIgnoreCase(idGenMode) || JBoltIDGenMode.SEQUENCE_LONG.equalsIgnoreCase(idGenMode)) {
			String tableName = table();
			switch (dbType) {
			case DBType.POSTGRESQL:
				idSequenceStr = "nextval('"+tableName+"_idsq')";
				break;
			case DBType.ORACLE:
				idSequenceStr =  tableName+"_idsq.nextval";
				break;
			case DBType.DM:
				idSequenceStr =  tableName+"_idsq.nextval";
				break;
			}
		}
		return idSequenceStr;
	}
	
	/**
	 * 批量更新
	 * @param records
	 * @param batchSize
	 */
	protected void batchUpdate(List<Record> records,int batchSize) {
		Db.use(dataSourceConfigName()).batchUpdate(table(), records, batchSize);
	}
	/**
	 * 切换Boolean类型字段值
	 *
	 * @param kv 额外传入的参数 用于 toggleExtra里用
	 * @param id 需要切换的数据ID
	 * @param columns 需要切换的字段列表
	 * @return
	 */
	protected Ret toggleBoolean(Kv kv,Object id,String... columns) {
		if(notOk(id)){
			return fail(Msg.PARAM_ERROR);
		}
		String tableName=table();
		Record record=Db.use(dataSourceConfigName()).findById(tableName,id);
		if(record==null){
			return fail(Msg.DATA_NOT_EXIST);
		}
		
			Boolean value;
			for(String column:columns){
				String msg=checkCanToggle(record,column,kv);
				if(StrKit.notBlank(msg)){
					return fail(msg);
				}
				value=record.getBoolean(column);
				record.set(column, value==null?true:!value);
				//处理完指定这个字段 还需要额外处理什么？
				msg=afterToggleBoolean(record,column,kv);
				if(StrKit.notBlank(msg)){
					return fail(msg);
				}
			}
			boolean success=Db.use(dataSourceConfigName()).update(tableName, record);
			return success?success(record,Msg.SUCCESS):FAIL;
		
	}
	
	/**
	 * 常用的得到列表数据数量
	 * 自定义参数compare
	 *
	 * @param paras
	 * @param customCompare
	 * @return
	 */
	protected int getCount(Kv paras,boolean customCompare){
		return dbTemplate("common.count",Kv.by("customCompare",customCompare).setIfNotNull("myparas", paras)).queryInt();
	}
	/**
	 * 常用的得到列表数据数量
	 *
	 * @param paras
	 * @return
	 */
	protected int getCount(Kv paras){
		return getCount(paras, false);
	}
	

	/**
	 * 得到新数据的排序Rank值 默认从1开始 不带任何查询条件
	 *
	 * @return
	 */
	protected int getNextSortRank(){
		return getNextSortRank(null, false);
	}
	/**
	 * 得到新数据的排序Rank值 从0开始 不带任何查询条件
	 *
	 * @return
	 */
	protected int getNextRankFromZero(){
		return getNextSortRank(null, true);
	}
	/**
	 * 得到新数据的排序Rank值 从0开始 带查询条件
	 *
	 * @param paras
	 * @return
	 */
	protected int getNextRankFromZero(Kv paras){
		return getNextSortRank(paras, true);
	}
	/**
	 * 得到新数据的排序Rank值 自带简单条件查询默认从1开始
	 *
	 * @param paras
	 * @return
	 */
	protected int getNextSortRank(Kv paras){
		return getNextSortRank(paras, false);
	}
	/**
	 * 得到新数据的排序Rank值 自带简单条件查询 可以自定义是否从零开始
	 *
	 * @param paras
	 * @param fromZero
	 * @return
	 */
	protected int getNextSortRank(Kv paras,boolean  fromZero){
		int count=getCount(paras);
		if(fromZero){
			return count;
		}
		return count+1;
	}
	

	/**
	 * 得到新数据的排序Rank值 自带简单条件查询 可以自定义是否从零开始
	 * 条件可定制版
	 *
	 * @param paras
	 * @param customCompare
	 * @param fromZero
	 * @return
	 */
	protected int getNextSortRank(Kv paras,Boolean customCompare,boolean  fromZero){
		int count=getCount(paras, customCompare);
		if(fromZero){
			return count;
		}
		return count+1;
	}
	
	/**
	 * 常用的得到列表数据数量
	 *
	 * @return
	 */
	protected int getCount(){
		return getCount(null, false);
	}
	
	
	public boolean tableExist() {
		String newTableName=table();
		return tableExist(newTableName);
	}
	
	/**
	 * 更新同级删除数据之后数据的排序
	 *
	 * @param sortRank
	 */
	protected void updateSortRankAfterDelete(Integer sortRank) {
		updateSortRankAfterDelete(table(),sortRank);
	}
	
	/**
	 * 更新同级删除数据之后数据的排序
	 *
	 * @param params
	 * @param sortRank
	 */
	protected void updateSortRankAfterDelete(Kv params, Integer sortRank) {
		updateSortRankAfterDelete(table(), params, sortRank);
	}
	/**
	 * 删除关联子数据
	 *
	 * @param pid
	 * @return
	 */
	protected Ret deleteByPid(Object pid) {
		return deleteBy(Kv.by("pid", pid));
	}
	
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * 关键词为空的时候 返回空list
	 *
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @paraRecord matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<Record> getAutocompleteList(String keywords, Integer limitCount,String matchColumns) {
		return getAutocompleteList(keywords, limitCount, false, Sql.KEY_STAR, matchColumns,null);
	}
	
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 * 关键词为空的时候 返回空list
	 *
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @paraRecord matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @paraRecord whereSql 额外条件sql
	 * @return
	 */
	public List<Record> getAutocompleteList(String keywords, Integer limitCount,String matchColumns,Sql whereSql) {
		return getAutocompleteList(keywords, limitCount, false, Sql.KEY_STAR, matchColumns,whereSql);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 *
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @paraRecord matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<Record> getAutocompleteList(String keywords, Integer limitCount,Boolean always,String matchColumns) {
		return getAutocompleteList(keywords, limitCount, always, Sql.KEY_STAR, matchColumns,null);
	}
	/**
	 * 关键词查询指定返回个数的数据 默认返回所有字段
	 *
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @paraRecord matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @paraRecord whereSql 额外条件sql
	 * @return
	 */
	public List<Record> getAutocompleteList(String keywords, Integer limitCount,Boolean always,String matchColumns,Sql whereSql) {
		return getAutocompleteList(keywords, limitCount, always, Sql.KEY_STAR, matchColumns,whereSql);
	}
	/**
	 * 关键词查询指定返回个数的数据
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param returnColumns 查询字段 可以是* 也可以是 id,name这种逗号隔开
	 * @paraRecord matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @return
	 */
	public List<Record> getAutocompleteList(String keywords, Integer limitCount,Boolean always,String returnColumns,String matchColumns) {
		return getAutocompleteList(keywords, limitCount, always, returnColumns, matchColumns, null);
	}
	/**
	 * 关键词查询指定返回个数的数据
	 * @param keywords 关键词
	 * @param limitCount  返回个数
	 * @param always  当关键词空的时候 是否需要查询所有数据返回指定个数
	 * @param returnColumns 查询字段 可以是* 也可以是 id,name这种逗号隔开
	 * @paraRecord matchColumns 关键词去匹配哪些字段 可以一个 可以多个 逗号隔开
	 * @paraRecord whereSql 额外条件sql
	 * @return
	 */
	public List<Record> getAutocompleteList(String keywords, Integer limitCount,Boolean always,String returnColumns,String matchColumns,Sql whereSql) {
		if((notOk(keywords)&&(always==null||always==false))||notOk(matchColumns)) {
			return Collections.emptyList();
		}
		if(whereSql == null) {
			whereSql=selectSql();
		}
		whereSql.select(returnColumns).firstPage(limitCount);
		//如果关键词为空 默认是返回空数据
			//但是如果指定了关键词是空 就按照指定个数返回数据的话
		if(notOk(keywords)&&always!=null&&always==true) {
			return find(whereSql.toSql());
		}
		
		String[] columns=ArrayUtil.from(matchColumns, ",");
		if(columns==null||columns.length==0) {
			return Collections.emptyList();
		}
		int size=columns.length;
		whereSql.bracketLeft();
		keywords=keywords.trim();
		for(int i=0;i<size;i++) {
			whereSql.like(columns[i],keywords);
			if(i<size-1) {
				whereSql.or();
			}
		}
		whereSql.bracketRight();
		return find(whereSql.toSql());
	}
	
	/**
	  *根据关键词分页查询 
	  * 默认倒序
	 *
	 * @param orderColumn
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @paraRecord matchColumns
	 * @param otherParas
	 * @param columnToCamelCase
	 * @return
	 */
	public Page<Record> paginateByKeywords(String orderColumn,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas,boolean columnToCamelCase){
		return paginateByKeywords(orderColumn,"desc",pageNumber,pageSize,keywords,matchColumns,otherParas,columnToCamelCase);
	}
	/**
	   * 根据关键词分页查询 
	 *
	 * @param orderColumn
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @paraRecord matchColumns
	 * @param otherParas
	 * @param columnToCamelCase 字段转驼峰
	 * @return
	 */
	public Page<Record> paginateByKeywords(String orderColumn,String orderType,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas,boolean columnToCamelCase){
		return paginateByKeywords(Sql.KEY_STAR,orderColumn,orderType,pageNumber,pageSize,keywords,matchColumns,otherParas,columnToCamelCase);
	}
	
	
	/**
	 * 根据关键词匹配分页查询
	 *
	 * @param returnColumns
	 * @param orderColumn
	 * @param orderType
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @paraRecord matchColumns
	 * @param otherParas
	 * @param columnToCamelCase 字段名转驼峰
	 * @return
	 */
	public Page<Record> paginateByKeywords(String returnColumns,String orderColumn,String orderType,int pageNumber,int pageSize,String keywords,String matchColumns,Kv otherParas,boolean columnToCamelCase){
		if(notOk(matchColumns)) {
			return EMPTY_PAGE;
		}
		Sql sql=selectSql().select(returnColumns).page(pageNumber, pageSize);
		Sql totalCountSql=selectSql().count();
		if(isOk(orderColumn)&&isOk(orderType)) {
			sql.orderBy(orderColumn,orderType.equals("desc"));
		}
		if(otherParas!=null&&otherParas.size()>0) {
			Set<String> keys=otherParas.keySet();
			sql.bracketLeft();
			totalCountSql.bracketLeft();
			for(String key:keys){
				sql.eq(key, otherParas.getAs(key));
				totalCountSql.eq(key, otherParas.getAs(key));
	        }
			sql.bracketRight();
			totalCountSql.bracketRight();
		}
		//如果没有给keywords字段
		if(notOk(keywords)) {
			return processPaginate(sql,totalCountSql,columnToCamelCase);
		}
		//如果给了Keywords
		String[] columns=ArrayUtil.from(matchColumns, ",");
		if(columns==null||columns.length==0) {
			return EMPTY_PAGE;
		}
		int size=columns.length;
		sql.bracketLeft();
		totalCountSql.bracketLeft();
		keywords=keywords.trim();
		for(int i=0;i<size;i++) {
			sql.like(columns[i],keywords);
			totalCountSql.like(columns[i],keywords);
			if(i<size-1) {
				sql.or();
				totalCountSql.or();
			}
		}
		sql.bracketRight();
		totalCountSql.bracketRight();
		return processPaginate(sql, totalCountSql,columnToCamelCase);
	}
	
	private Page<Record> processPaginate(Sql sql, Sql totalCountSql,boolean columnToCamelCase) {
		if(!sql.hasPage()) {
			throw new RuntimeException("Please set the values of PageNumber and PageSize in the SQL");
		}
		totalCountSql.fromIfBlank(table());
		totalCountSql.clearOrderBy();
		totalCountSql.clearPage();
		Integer totalRow=queryInt(totalCountSql);
		if(totalRow==null||totalRow==0) {
			return EMPTY_PAGE;
		}
		sql.fromIfBlank(table());
		List<Record> list=find(sql);
		if(columnToCamelCase) {
			CamelCaseUtil.keyToCamelCase(list);
		}
		int totalPage=(totalRow/sql.getPageSize())+(totalRow%sql.getPageSize()>0?1:0);
		return new Page<Record>(list, sql.getPageNumber(), sql.getPageSize(), totalPage, totalRow);
	}
	
	/**
	 * 快速获取sql 默认是select sql
	 *
	 * @return
	 */
	protected Sql selectSql() {
		return Sql.me(dbType()).select().from(table());
	}
	
	/**
	 * 快速获取sql 使用jboltTableMenuFilter初始化
	 * @param jboltTableMenuFilter
	 * @param matchColumns
	 * @return
	 */
	public Sql selectSql(JBoltTableMenuFilter jboltTableMenuFilter,String... matchColumns) {
		Sql sql = selectSql();
		processJBoltTableMenuFilterSql(sql,jboltTableMenuFilter,matchColumns);
		return sql;
	}
	/**
	 * 快速获取sql 使用jboltTableMenuFilter初始化
	 * @param jboltTableMenuFilter
	 * @param mainTableAsName
	 * @param matchColumns
	 * @return
	 */
	public Sql selectSql(JBoltTableMenuFilter jboltTableMenuFilter,String mainTableAsName,String... matchColumns) {
		Sql sql = selectSql().from(table(),mainTableAsName);
		processJBoltTableMenuFilterSql(sql,jboltTableMenuFilter,matchColumns);
		return sql;
	}
	/**
	 * 快速获取update sql
	 *
	 * @return
	 */
	protected Sql updateSql() {
		return Sql.me(dbType()).update(table());
	}
	/**
	 * 快速获取delete sql
	 * @return
	 */
	protected Sql deleteSql() {
		return Sql.me(dbType()).delete().from(table());
	}
	
	
	/**
	 * 执行查询
	 * @param sql
	 * @return
	 */
	protected List<Record> find(Sql sql){
		return find(sql.toSql());
	}
	/**
	 * 执行查询 第一个符合条件的
	 * @param sql
	 * @return
	 */
	protected Record findFirst(Sql sql){
		return Db.use(dataSourceConfigName()).findFirst(sql.toSql());
	}
	/**
	 * 执行查询
	 * @param sql
	 * @param paras
	 * @return
	 */
	protected List<Record> find(Sql sql,Object... paras){
		return find(sql.toSql(),paras);
	}
	
	/**
	 * 执行查询
	 * @param sql
	 * @return
	 */
	protected List<Record> find(String sql){
		return Db.use(dataSourceConfigName()).find(sql);
	}
	/**
	 * 执行查询
	 * @param sql
	 * @param paras
	 * @return
	 */
	protected List<Record> find(String sql,Object... paras){
		return Db.use(dataSourceConfigName()).find(sql, paras);
	}
	
	
	/**
	 * 转换为Model tree
	 * @param allList
	 * @param idColumn
	 * @param pidColumn
	 * @param checkIsParentFunc
	 * @return
	 */
	public List<Record> convertToRecordTree(List<Record> allList,String idColumn,String pidColumn,TreeCheckIsParentNode<Record> checkIsParentFunc) {
		if(notOk(allList)) {return null;}
		List<Record> parents=new ArrayList<Record>();
		Record m;
		for(int i=0;i<allList.size();i++) {
			m=allList.get(i);
			if(checkIsParentFunc.isParent(m)) {
				parents.add(m);
				allList.remove(m);
				i--;
			}
		}
		if(parents.size()>0&&allList.size()>0) {
			processTreeNodes(allList,parents,idColumn,pidColumn);
		}
		return parents;
	}
	
	/**
	 * 处理tree节点
	 * @param allList
	 * @param parents
	 * @param idColumn
	 * @param pidColumn
	 */
	private void processTreeNodes(List<Record> allList, List<Record> parents, String idColumn, String pidColumn) {
		ListMap<String, Record> map=new ListMap<String, Record>();
		for(Record m:allList) {
			map.addItem("p_"+m.get(pidColumn), m);
		}
		for(Record p:parents) {
			processTreeSubNodes(map,p,idColumn);
		}
		
	}
	/**
	 * 递归处理tree子节点
	 * @param map
	 * @param m
	 * @param idColumn
	 */
	private void processTreeSubNodes(ListMap<String, Record> map, Record m,String idColumn) {
		List<Record> items=map.get("p_"+m.get(idColumn));
		if(items!=null&&items.size()>0) {
			for(Record item:items) {
				processTreeSubNodes(map, item,idColumn);
			}
		}
		m.set("items",items);
	}
	
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId,int openLevel) {
		return convertJsTree(datas, selectedId, openLevel, null,false);
	}
	
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param needOriginData 是否需要保留原始Data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId,int openLevel,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, null,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId, int openLevel,String keyColumn) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn,null,false);
	}
	/**
	 * 转换数据成为jstreebean 无指定root
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId, int openLevel,String keyColumn,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn,null,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param rootName 根节点名字
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId, int openLevel,String keyColumn,String rootName) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn, "name",rootName,false);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId, int openLevel,String keyColumn,String rootName,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn, "name",rootName,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param textColumn 显示文本字段
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId, int openLevel,String keyColumn,String textColumn,String rootName,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn,textColumn,"enable",rootName,needOriginData);
	}
	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param textColumn 显示文本字段
	 * @param enableColumn 启用禁用字段
	 * @param rootName 根节点名字
	 * @param needOriginData 是否需要保留原始Data
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId, int openLevel,String keyColumn,String textColumn,String enableColumn,String rootName,boolean needOriginData) {
		return convertJsTree(datas, selectedId, openLevel, keyColumn, "id", "pid",textColumn,enableColumn,rootName,needOriginData,null);
	}
	
	
	
	private String[] getTreeColumnKeys(Record m,String idColumn,String pidColumn) {
		List<String> keys=new ArrayList<String>();
		keys.add(m.getStr(idColumn));
		Object pid=m.getStr(pidColumn);
		if(pid!=null&&pid.toString().equals("0")==false) {
			processTreeColumnKeys(keys, pid, idColumn, pidColumn);
		}
		return keys.toArray(new String[keys.size()]);
	}
	private void processTreeColumnKeys(List<String> keys,Object id,String idColumn,String pidColumn) {
		if(id==null || id.toString().equals("0")) {
			return;
		}
		Record m=findById(id);
		if(m==null) {
			return;
		}
		//pid有值 需要加入keys
		keys.add(0, id.toString());
		Object pid=m.getStr(pidColumn);
		if(pid!=null&&pid.toString().equals("0")==false) {
			//pid有值 需要加入keys
			processTreeColumnKeys(keys, pid, idColumn, pidColumn);
		}
	}

	/**
	 * 转换数据成为jstreebean
	 * @param datas 数据源
	 * @param selectedId 选中哪一个节点
	 * @param openLevel -1全部 0 不动 >0指定层级
	 * @param keyColumn 多级下 key字段名字
	 * @param idColumn 节点 标识字段
	 * @param pidColumn 节点父标识字段
	 * @param textColumn 显示文本字段
	 * @param enableColumn 启用禁用字段
	 * @param rootName 跟节点名字
	 * @param needOriginData 是否需要保留原始data
	 * @param checkIsParentFunc 检测是否为parent的Func
	 * @return
	 */
	public List<JsTreeBean> convertJsTree(List<Record> datas,Object selectedId, int openLevel,String keyColumn,String idColumn,String pidColumn,String textColumn,String enableColumn,String rootName,boolean needOriginData,TreeCheckIsParentNode<Record> checkIsParentFunc) {
		List<JsTreeBean> treeBeans=new ArrayList<JsTreeBean>();
		boolean hasRoot=isOk(rootName);
		if(hasRoot) {
			treeBeans.add(new JsTreeBean(0, 0, rootName, true,"root_opened",true));
		}
		if(datas.size()>0){
			String[] keys=null;
			if(isOk(selectedId)){
				Record m=findById(selectedId);
				if(m != null) {
					if(isOk(keyColumn)) {
						String key=m.get(keyColumn);
						if(isOk(key)) {
							keys=ArrayUtil.from(key, "_");
						}
					}else {
						keys=getTreeColumnKeys(m,idColumn,pidColumn);
					}
				}
			}
			List<Record> jstreeMs=null;
			if(checkIsParentFunc!=null) {
				jstreeMs = convertToRecordTree(datas, idColumn, pidColumn, (p)->checkIsParentFunc.isParent(p));
			}else {
				jstreeMs = convertToRecordTree(datas, idColumn, pidColumn, (p)->{
					Object v=p.get(pidColumn);
					return v==null||v.toString().equals("0");
				});
			}
			processRecordJsTreeType(jstreeMs);
			
			processRecordJsTree(treeBeans,jstreeMs,openLevel,1,idColumn,pidColumn,textColumn,enableColumn,keys,selectedId,hasRoot,needOriginData);
		}
		return treeBeans;
	}
	
	

	/**
	 * 查询基于menuFilter
	 * @param jboltTableMenuFilter
	 * @param columnToCamelCase
	 * @param matchColumns
	 * @return
	 */
	public Page<Record> paginateByJboltTableMenuFilter(JBoltTableMenuFilter jboltTableMenuFilter, boolean columnToCamelCase, String[] matchColumns) {
		return paginate(selectSql(jboltTableMenuFilter,matchColumns),columnToCamelCase);
	}
	/**
	 * 查询基于menuFilter
	 * @param jboltTableMenuFilter
	 * @param columnToCamelCase
	 * @param matchColumns
	 * @return
	 */
	public Page<Record> paginateByJboltTableMenuFilter2(JBoltTableMenuFilter jboltTableMenuFilter, boolean columnToCamelCase, String[] matchColumns) {
		Sql sql = selectSql(jboltTableMenuFilter,"this",matchColumns);
		sql.leftJoin("table2", "t2", "t2.id = this.t2_id");
		return paginate(sql,columnToCamelCase);
	}
	
}
