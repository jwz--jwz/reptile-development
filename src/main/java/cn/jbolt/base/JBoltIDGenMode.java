package cn.jbolt.base;

/**
 * 平台主键生成策略
 * @ClassName:  JBoltIDGenMode   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年1月23日   
 */
public interface JBoltIDGenMode {
	String AUTO="auto";
	String AUTO_LONG="auto_long";
	String UUID="uuid";
	String SEQUENCE="sequence";
	String SEQUENCE_LONG="sequence_long";
	String SERIAL="serial";
	String BIGSERIAL="bigserial";
	String SNOWFLAKE="snowflake";
	/**
	 * 判断是否需要手工赋值
	 * @param idGenMode
	 * @return
	 */
	static boolean isNeedAssign(String idGenMode) {
		return UUID.equals(idGenMode)||SEQUENCE.equals(idGenMode)||SEQUENCE_LONG.equals(idGenMode)||SNOWFLAKE.equals(idGenMode);
	}
}
