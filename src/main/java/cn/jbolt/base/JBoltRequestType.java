package cn.jbolt.base;

/**
 * JBolt平台请求类型 枚举
 * 
 * @ClassName: JBoltRequestType
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2021年2月7日
 */
public enum JBoltRequestType {
	NULL,
	AJAX,
	PJAX,
	AJAXPORTAL,
	DIALOG,
	IFRAME,
	NORMAL,
	JBOLTAPI;
}
