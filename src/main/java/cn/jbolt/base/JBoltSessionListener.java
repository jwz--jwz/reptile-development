package cn.jbolt.base;

import io.undertow.server.HttpServerExchange;
import io.undertow.server.session.Session;
import io.undertow.server.session.SessionListener;
/**
 * 如果需要session监控 可以配置使用此类
 * @ClassName:  JBoltSessionListener   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月18日   
 *    
 */
public class JBoltSessionListener implements SessionListener{

	@Override
	public void sessionCreated(Session session, HttpServerExchange exchange) {
		System.out.println("sessionCreated");
		System.out.println(session.getId());
		System.out.println(session.getAttributeNames());
		System.out.println("sessionCreated");
		SessionListener.super.sessionCreated(session, exchange);
	}

	@Override
	public void sessionDestroyed(Session session, HttpServerExchange exchange, SessionDestroyedReason reason) {
		System.out.println("sessionDestroyed");
		System.out.println(session.getId());
		System.out.println(session.getAttributeNames());
		System.out.println("sessionDestroyed");
		SessionListener.super.sessionDestroyed(session, exchange, reason);
	}

	@Override
	public void attributeAdded(Session session, String name, Object value) {
		System.out.println("attributeAdded");
		System.out.println(session.getId());
		System.out.println(name+":"+value);
		System.out.println("attributeAdded");
		SessionListener.super.attributeAdded(session, name, value);
	}

	@Override
	public void attributeUpdated(Session session, String name, Object newValue, Object oldValue) {
		System.out.println("attributeUpdated");
		System.out.println(session.getId());
		System.out.println(name+":"+oldValue+":"+newValue);
		System.out.println("attributeUpdated");
		SessionListener.super.attributeUpdated(session, name, newValue, oldValue);
	}

	@Override
	public void attributeRemoved(Session session, String name, Object oldValue) {
		System.out.println("attributeRemoved");
		System.out.println(session.getId());
		System.out.println(name);
		System.out.println("attributeRemoved");
		SessionListener.super.attributeRemoved(session, name, oldValue);
	}

	@Override
	public void sessionIdChanged(Session session, String oldSessionId) {
		System.out.println("sessionIdChanged");
		System.out.println("old:"+oldSessionId);
		System.out.println("new:"+session.getId());
		System.out.println("sessionIdChanged");
		SessionListener.super.sessionIdChanged(session, oldSessionId);
	}
	
}
