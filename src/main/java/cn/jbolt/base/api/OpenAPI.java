package cn.jbolt.base.api;

  import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
  * 声明是一个开放接口
  * 默认无需校验和设置JWT
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface OpenAPI {
	/**
	 * 如果JWT存在 就解析并设置给当前线程
	 * 用于接口既可以访客访问 又能登录用户访问
	 * 然后根据是否携带JWT后续判断执行不同处理和返回不同数据的场景
	 * @return
	 */
	boolean parseJwtIfExists() default false;
}