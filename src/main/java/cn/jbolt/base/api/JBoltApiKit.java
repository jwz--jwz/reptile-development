package cn.jbolt.base.api;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;

import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.model.Application;
import cn.jbolt.common.model.User;
import cn.jbolt.common.model.WechatMpinfo;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CACHE;
/**
 * JBolt平台API使用的工具类 只要处理用户和jwt相关threadLocal
 * @ClassName:  JBoltApiKit   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年4月1日   
 */
public class JBoltApiKit {
	/**
	  * 针对JwtParseResut的ThreadLocal
	 */
    private static final ThreadLocal<JBoltJwtParseRet> jwtTL = new ThreadLocal<JBoltJwtParseRet>();
   
    public static void setJwtParseRet(JBoltJwtParseRet jwtParseRet) {
        jwtTL.set(jwtParseRet);
        //如果绑定了系统用户jb_user 设置到当前线程中
        Object systemUserId = JBoltApiKit.getBindSystemUserId();
        if(systemUserId!=null) {
        	JBoltUserKit.setUserId(systemUserId);
        }
    }

    public static void removeJwtParseRet() {
        jwtTL.remove();
        
        //清除绑定的系统用户
        JBoltUserKit.removeUserId();
    }

    public static JBoltJwtParseRet getJwtParseRet() {
        return jwtTL.get();
    }
    
    /**
     * 针对Application的ThreadLocal
     */
    private static final ThreadLocal<Application> applicationTL = new ThreadLocal<Application>();
    
    public static void setApplication(Application application) {
    	applicationTL.set(application);
    }

    public static void removeApplication() {
    	applicationTL.remove();
    }
    /**
     * 获得用户访问来自哪个Application
     * @return
     */
    public static Application getApplication() {
    	return applicationTL.get();
    }
    /**
     * 获得用户访问来自哪个Application的appId
     * @return
     */
    public static String getAppId() {
    	Application application=getApplication(); 
    	return application==null?null:application.getAppId();
    }
    /**
     * 获得用户访问来自哪个Application的主键ID
     * @return
     */
    public static Object getApplicationId() {
    	Application application=getApplication(); 
    	return application==null?null:application.getId();
    }
    
    public static <T> T getApplicationIdAs() {
    	Application application=getApplication(); 
    	return application==null?null:((T) application.getId());
    }
    /**
	 * 获得当前访问的userId
	 * @return
	 */
	public static Object getApiUserId() {
		JBoltJwtParseRet jwtParseRet=getJwtParseRet();
		return jwtParseRet==null?null:jwtParseRet.getUserId();
	}
	/**
	 * 获得当前访问的userId
	 * @param <T>
	 * @return
	 */
	public static <T> T getApiUserIdAs() {
    	Object id = getApiUserId();
    	return id==null?null:((T) id);
	}
	/**
	 * 获得当前访问的apiUser绑定的其它用户表
	 * @return
	 */
	public static List<JBoltApiBindUserBean> getBindUsers() {
		JBoltApiUser apiUser=getApiUser();
		return apiUser==null?null:apiUser.getBindUsers();
	}
	/**
	 * 获得当前访问的ApiUser
	 * @return
	 */
	public static JBoltApiUser getApiUser() {
		JBoltJwtParseRet jwtParseRet=getJwtParseRet();
		return jwtParseRet==null?null:jwtParseRet.getApiUser();
	}
	/**
	 * 得到绑定的系统用户jb_user的ID
	 * @return
	 */
	public static Object getBindSystemUserId() {
		return getBindUserId(JBoltApiBindUserBean.TYPE_SYSTEM_USER);
	}
	/**
	 * 得到绑定的其他用户的userId
	 * @return
	 */
	public static Object getBindUserId(int bindUserType) {
		List<JBoltApiBindUserBean> bindUserBeans=getBindUsers();
		if(bindUserBeans==null||bindUserBeans.size()==0) {return null;}
		Object userId=null;
		for(JBoltApiBindUserBean bean:bindUserBeans) {
			if(bean.getType()==bindUserType) {
				userId=bean.getUserId();
				break;
			}
		}
		return userId;
	}
	/**
	 * 得到绑定的系统用户表 只有在调用接口的应用绑定的小程序等用户 绑定了系统表用户时才有效
	 * @return
	 */
	public static User getBindSystemUser() {
		Object userId=getBindSystemUserId();
		if(userId==null) {return null;}
		return CACHE.me.getUser(userId);
	}
	
	/**
	 * 清空threadLocal
	 */
	public static void clear() {
		removeJwtParseRet();
		removeApplication();
		removeApplyJwtUser();
		removeWechatAppId();
		removeWechatMpId();
	}

	  /**
	   	* 针对申请jwt的ApiUser的ThreadLocal
     */
    private static final ThreadLocal<JBoltApiUser> applyJwtUserTL = new ThreadLocal<JBoltApiUser>();
    
    public static void setApplyJwtUser(JBoltApiUser jBoltApiUser) {
    	applyJwtUserTL.set(jBoltApiUser);
    }

    public static void removeApplyJwtUser() {
    	applyJwtUserTL.remove();
    }

    public static JBoltApiUser getApplyJwtUser() {
    	return applyJwtUserTL.get();
    }
    
    
    /**
     * 检测微信公众平台（非小程序）系统配置
     * @param application
     * @return
     */
    public static JBoltApiRet processWechatConfig(Application application) {
    	//获取application关联的公众平台mpId
    	Integer mpId=application.getLinkTargetId();
    	if(mpId==null || mpId <= 0) {
    		return JBoltApiRet.APP_NOT_LINK_WECHAT_MP(application);
    	}
    	//关联的公众平台正确配置
    	WechatMpinfo mp=CACHE.me.getWechatMpInfo(mpId);
    	if(mp==null) {
    		return JBoltApiRet.APP_NOT_LINK_WECHAT_MP(application);
    	}
    	if(mp.getEnable()==null||mp.getEnable()==false) {
    		return JBoltApiRet.WECHAT_MP_DISABLED(application);
    	}
    	//获取微信公众平台配置的APPID
    	String wechatAppId=CACHE.me.getWechatConfigAppId(mpId);
    	if(StrKit.isBlank(wechatAppId)) {
    		return JBoltApiRet.WECHAT_MP_CONFIG_ERROR(application);
    	}
    	//设置到线程里
    	JBoltApiKit.setWechatAppId(wechatAppId);
    	JBoltApiKit.setWechatMpId(mpId);
    	return JBoltApiRet.success();
    }
    
    /**
	 * 检测微信小程序系统配置
	 * @param application
	 * @return
	 */
    public static JBoltApiRet processWxaConfig(Application application) {
		//获取application关联的小程序mpId
		Integer mpId=application.getLinkTargetId();
		if(mpId==null || mpId <= 0) {
			return JBoltApiRet.APP_NOT_LINK_WECHAT_XCX(application);
		}
		//关联的小程序正确配置
		WechatMpinfo mp=CACHE.me.getWechatMpInfo(mpId);
		if(mp==null) {
			return JBoltApiRet.APP_NOT_LINK_WECHAT_XCX(application);
		}
		if(mp.getEnable()==null||mp.getEnable()==false) {
			return JBoltApiRet.WECHAT_XCX_DISABLED(application);
		}
		//获取微信小程序配置的APPID
		String wechatAppId=CACHE.me.getWechatConfigAppId(mpId);
		if(StrKit.isBlank(wechatAppId)) {
			return JBoltApiRet.WECHAT_XCX_CONFIG_ERROR(application);
		}
		JBoltApiKit.setWechatAppId(wechatAppId);
		JBoltApiKit.setWechatMpId(mpId);
		return JBoltApiRet.success();
	}
    
    
    /**
     * 获取application关联绑定的targetId
     * @return
     */
    @SuppressWarnings("unchecked")
	public static <T> T getApplicationLinkTargetId() {
    	Application application=getApplication(); 
    	return application==null?null:(T)(application.getLinkTargetId());
    }
    
    private static final ThreadLocal<String> wechatAppIdTL = new ThreadLocal<String>();
    /**
     * 获取应用绑定的公众平台的APPID配置
     * @return
     */
    public static String getWechatAppId() {
    	return wechatAppIdTL.get();
    }
    public static void setWechatAppId(String wechatAppId) {
    	wechatAppIdTL.set(wechatAppId);
    }
    public static void removeWechatAppId() {
    	wechatAppIdTL.remove();
    }
   
    private static final ThreadLocal<Object> wechatMpIdTL = new ThreadLocal<Object>();
   
    /**
     * 获取应用绑定的公众平台的mpid配置
     * @return
     */
    public static Object getWechatMpId() {
    	return wechatMpIdTL.get();
    }
    
    public static <T> T getWechatMpIdAs() {
		return (T) wechatMpIdTL.get();
    }
    
    public static void setWechatMpId(Object wechatAppId) {
    	wechatMpIdTL.set(wechatAppId);
    }
    public static void removeWechatMpId() {
    	wechatMpIdTL.remove();
    }
    
    
    /**
	 * 处理绑定信息
	 * @param apiUser
	 * @param bindUser
	 * @return
	 */
	public static JBoltApiUserBean processBindUser(JBoltApiUserBean apiUser,String bindUser) {
		if(StrKit.isBlank(bindUser)) {return apiUser;}
		apiUser.setBindUsers(convertToBindUsers(bindUser));
		return apiUser;
	}
	/**
	 * 转换成Bean
	 * @param bindUser
	 * @return
	 */
	private static List<JBoltApiBindUserBean> convertToBindUsers(String bindUser) {
		String[] barray=ArrayUtil.from(bindUser, ",");
		if(barray==null||barray.length==0) {return null;}
		List<JBoltApiBindUserBean> beans=new ArrayList<JBoltApiBindUserBean>();
		JBoltApiBindUserBean bean;
		for(String bu:barray) {
			bean=covertToBindUser(bu);
			if(bean!=null) {
				beans.add(bean);
			}
		}
		return beans;
	}
	private static JBoltApiBindUserBean covertToBindUser(String bindUser) {
		if(StrKit.isBlank(bindUser)||bindUser.indexOf("_")==-1) {return null;}
		String[] arr=bindUser.split("_");
		if(arr==null||arr.length==0||arr.length!=2||StrKit.isBlank(arr[0])||StrKit.isBlank(arr[1])) {return null;}
		
		return new JBoltApiBindUserBean(Integer.parseInt(arr[0].trim()), arr[1].trim());
	}

}
