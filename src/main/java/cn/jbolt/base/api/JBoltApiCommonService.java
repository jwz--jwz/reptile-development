package cn.jbolt.base.api;

import java.util.Date;

import com.jfinal.upload.UploadFile;

import cn.jbolt.base.para.IJBoltParaValidator;
import cn.jbolt.base.para.JBoltParaValidator;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.util.DateUtil;

/**
 * Api Service底层通用封装
 * 
 * @ClassName: CommonService
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2019年12月10日
 */
public abstract class JBoltApiCommonService implements IJBoltParaValidator{
	protected static final String TRUE = Sql.TRUE;
	protected static final String FALSE = Sql.FALSE;

	/**
	 * 判断Object参数有效性
	 * 
	 * @param param
	 */
	public boolean isOk(Object param) {
		return JBoltParaValidator.isOk(param);
	}

	/**
	 * 判断Object参数是否无效
	 */
	public boolean notOk(Object param) {
		return JBoltParaValidator.notOk(param);
	}

	/**
	 * 判断上传文件是图片
	 * 
	 * @param isImage
	 */
	public boolean isImage(String contentType) {
		return JBoltParaValidator.isImage(contentType);
	}

	/**
	 * 判断上传文件不是图片
	 * 
	 * @param notImage
	 */
	public boolean notImage(String contentType) {
		return JBoltParaValidator.notImage(contentType);
	}

	/**
	 * 判断上传文件类型不是图片
	 * 
	 * @param file
	 */
	public boolean notImage(UploadFile file) {
		return JBoltParaValidator.notImage(file);
	}

	/**
	 * 判断上传文件类型是否为图片
	 * 
	 * @param file
	 */
	public boolean isImage(UploadFile file) {
		return JBoltParaValidator.isImage(file);
	}

	/**
	 * 判断Object[]数组类型数据是否正确
	 * 
	 * @param param
	 * @return
	 */
	public boolean isOk(Object[] param) {
		return JBoltParaValidator.isOk(param);
	}

	/**
	 * 判断Object[]数组类型数据不正确
	 * 
	 * @param param
	 * @return
	 */
	public boolean notOk(Object[] param) {
		return JBoltParaValidator.notOk(param);
	}

	/**
	 * 转为startTime
	 * 
	 * @param dateTime
	 * @param isOracle
	 * @return
	 */
	public String toStartTime(Date startTime,boolean isOracle) {
		return toDateTime(DateUtil.HHmmssTo000000Str(startTime),isOracle);
	}

	/**
	 * 转为endTime
	 * 
	 * @param endTime
	 * @param isOracle
	 * @return
	 */
	public String toEndTime(Date endTime,boolean isOracle) {
		return toDateTime(DateUtil.HHmmssTo235959Str(endTime),isOracle);
	}

	/**
	 * 转为表达式处理date
	 * 
	 * @param dateTime
	 * @return
	 */
	public String toDateTime(String dateTime,boolean isOracle) {
		if (isOracle) {
			return String.format("TO_DATE('%s','yyyy-mm-dd hh24:mi:ss')", dateTime);
		}
		return dateTime;
	}
	@Override
	public boolean notExcel(UploadFile file) {
		return JBoltParaValidator.notExcel(file);
	}
	@Override
	public boolean isExcel(UploadFile file) {
		return JBoltParaValidator.isExcel(file);
	}

	
}
