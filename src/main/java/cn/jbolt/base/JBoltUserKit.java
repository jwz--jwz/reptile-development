package cn.jbolt.base;

import cn.jbolt.common.model.OnlineUser;
import cn.jbolt.common.model.User;
import cn.jbolt.common.util.CACHE;
/**
 * JBolt平台后端用户登录ThreadLocal相关
 * @ClassName:  JBoltUserKit   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月21日   
 */
public class JBoltUserKit {
	
	/**
	 * 针对JBolt平台登录用户SessionId的ThreadLocal
	 */
	private static final ThreadLocal<String> jboltSessionIdTL = new ThreadLocal<String>();
	/**
	  * 针对JBolt平台登录用户UserId的ThreadLocal
	 */
	private static final ThreadLocal<Object> jboltUserTL = new ThreadLocal<Object>();
    /**
     * 存是否锁屏了
     */
	private static final ThreadLocal<Boolean> jboltUserScreenLockedTL = new ThreadLocal<Boolean>();
   
   /**
    * 删除用户ID
    */
   public static void removeUserId() {
	   jboltUserTL.remove();
   }
   /**
    * 获取当前用户
    * @return
    */
   public static User getUser() {
       return CACHE.me.getUser(getUserId());
   }
   
   /**
    * 获得当前访问的userId
    * @return
    */
   public static Object getUserId() {
	   return jboltUserTL.get();
   }
   /**
    * 获得当前访问的userId
    * @return
    */
   public static <T> T getUserIdAs() {
	   return (T) jboltUserTL.get();
   }
   
   /**
    * 设置当前用户ID
    * @param userId
    */
   public static void setUserId(Object userId) {
	   jboltUserTL.set(userId);
   }
   
   /**
    * 获得当前访问的用户name
    * @return
    */
   public static String getUserName() {
	   User user=getUser();
	   return user==null?null:user.getName();
   }
   /**
    * 获得当前访问的用户头像
    * @return
    */
   public static String getUserAvatar() {
	   User user=getUser();
	   return user==null?null:user.getAvatar();
   }
   /**
    * 获得当前访问的用户登录账号用户名username
    * @return
    */
   public static String getUserUserName() {
	   User user=getUser();
	   return user==null?null:user.getUsername();
   }
   
   /**
	 * 获得当前访问的用户roles字段值
	 * @return
	 */
	public static String getUserRoleIds() {
		User user=getUser();
		return user==null?null:user.getRoles();
	}
	 
	/**
	 * 判断是否为超管
	 * @return
	 */
	public static boolean isSystemAdmin() {
		User user=getUser();
		if(user==null) {return false;}
		return user.getIsSystemAdmin();
	}
	/**
	 * 判断是否为有效可用用户
	 * @return
	 */
	public static boolean isEnable() {
		User user=getUser();
		if(user==null) {return false;}
		return user.getEnable();
	}
	
	
	 /**
     * 设置当前用户sessionId
    * @param userId
    */
   public static void setUserSessionId(String sessionId) {
	   jboltSessionIdTL.set(sessionId);
	}
   /**
    * 获取当前用户sessionId
    * @return
    */
   public static String getUserSessionId() {
	  return jboltSessionIdTL.get();
   }
   /**
    * 删除当前用户的sessionId
    */
   public static void removeUserSessionId() {
	   jboltSessionIdTL.remove();
   }
   /**
    * 获取当前onlineuser 信息
    */
   public static OnlineUser getOnlineUser() {
	   return CACHE.me.getOnlineUserBySessionId(getUserSessionId());
   }
	
	/**
	 * 清空threadLocal
	 */
	public static void clear() {
		removeUserId();
		removeUserSessionId();
		removeScreenLocked();
	}
	/**
	 * 设置用户锁屏
	 */
	public static void setScreenIsLocked() {
		jboltUserScreenLockedTL.set(true);
	}
	/**
	 * 设置用户锁屏
	 */
	public static boolean userScreenIsLocked() {
		Boolean result=jboltUserScreenLockedTL.get();
		return result!=null && result.booleanValue();
	}
	/**
	 * 清除用户锁屏
	 */
	public static void removeScreenLocked() {
		jboltUserScreenLockedTL.remove();
	}
	
}
