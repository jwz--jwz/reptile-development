package cn.jbolt.base;

import com.jfinal.core.NotAction;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
/**
 * Controller层基础封装 基于CommonController
 * @ClassName:  BaseController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年8月8日   
 */
public class JBoltBaseController extends JBoltCommonController {
	
	/**
	 * 返回错误信息 自动判断返回类型
	 * @param msg
	 */
	@NotAction
	public void renderFail(String msg) {
		renderFail(msg, null);
	}
	/**
	 * 返回错误信息 携带数据data
	 * 自动判断返回类型
	 * @param msg
	 */
	@NotAction
	public void renderFail(String msg,Object data) {
		JBoltControllerKit.renderFail(this,msg,data);
	}
	/**
	 * dialog里请求页面 返回错误信息
	 * @param msg
	 */
	@NotAction
	public void renderDialogFail(String msg) {
		JBoltControllerKit.renderDialogFail(this,msg);
	}
	/**
	 * dialog里请求页面返回错误信息
	 * @param ret
	 */
	@NotAction
	public void renderDialogFailRet(Ret ret) {
		renderDialogFail(ret.getStr("msg"));
	}
	/**
	 * 加载或者提交form表单 返回错误信息 与Dialog错误信息目前一致
	 * @param msg
	 */
	@NotAction
	public void renderFormFail(String msg) {
		JBoltControllerKit.renderFormFail(this,msg);
	}
	@NotAction
	public void renderFormFailRet(Ret ret) {
		renderFormFail(ret.getStr("msg"));
	}
	/**
	 * 跳转页面 返回错误信息 与Dialog错误信息目前一致
	 * @param msg
	 */
	@NotAction
	public void renderPageFail(String msg) {
		JBoltControllerKit.renderPageFail(this,msg);
	}
	@NotAction
	public void renderPageFailRet(Ret ret) {
		renderPageFail(ret.getStr("msg"));
	}
	@NotAction
	public void renderPjaxFail(String msg) {
		JBoltControllerKit.renderPjaxFail(this, msg);
	}
	@NotAction
	public void renderPjaxFail(String msg,String backUrl) {
		set("backUrl", backUrl);
		JBoltControllerKit.renderPjaxFail(this, msg);
	}
	protected void renderAjaxPortalFail(String msg) {
		JBoltControllerKit.renderAjaxPortalFail(this,msg);
	}
	protected void renderPjaxSuccess(String msg) {
		JBoltControllerKit.renderPjaxSuccess(this, msg);
	}
	protected void renderPjaxRet(Ret ret) {
		if(ret.isOk()){
			renderPjaxSuccess(ret.getStr("msg"));
		}else{
			renderPjaxFail(ret.getStr("msg"));
		}
	}
	/**
	 *返回上传文件的成功消息
	 * 使用Bootstrap-fileinput组件
	 * @param msg
	 */
	protected void renderBootFileUploadSuccess(String msg) {
		renderJson(Kv.by("success", msg));
	}
	/**
	 *返回上传文件的失败消息
	 * 使用Bootstrap-fileinput组件
	 * @param msg
	 */
	protected void renderBootFileUploadFail(String msg) {
		 renderJson(Kv.by("error", msg));
	}
  
	/**
	 * 得到后台管理用户是否登录
	 * @return
	 */
	@NotAction
	public boolean isAdminLogin(){
		return isOk(JBoltUserKit.getUserId());
	}
}
