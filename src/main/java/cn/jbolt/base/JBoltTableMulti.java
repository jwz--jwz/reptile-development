package cn.jbolt.base;

import java.util.LinkedHashMap;
/**
 * 多表格批量提交 数据接收
 * @ClassName:  JBoltTableMulti   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年4月13日   
 *    
 * 注意：本内容仅限于JFinal学院 JBolt平台VIP成员内部传阅，请尊重开发者劳动成果，不要外泄出去用于其它商业目的
 */
public class JBoltTableMulti extends LinkedHashMap<String, JBoltTable> {
	private static final long serialVersionUID = 7121746923071858703L;
	public static final String PARAM_KEY="jboltTables";
	public JBoltTable getJBoltTable(String name) {
		return get(name);
	}
}
