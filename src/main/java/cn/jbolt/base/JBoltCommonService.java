package cn.jbolt.base;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.upload.UploadFile;

import cn.hutool.core.io.FileUtil;
import cn.jbolt._admin.systemlog.SystemLogService;
import cn.jbolt.base.para.IJBoltParaValidator;
import cn.jbolt.base.para.JBoltParaValidator;
import cn.jbolt.base.para.jbolttablemenufilter.JBoltTableMenuFilter;
import cn.jbolt.common.bean.JsTreeBean;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.config.PageSize;
import cn.jbolt.common.db.sql.DBType;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.db.sql.SqlExpress;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CACHE;
import cn.jbolt.common.util.DateUtil;
import cn.jbolt.common.util.ListMap;
import cn.jbolt.common.util.TreeCheckIsParentNode;

/**
 * Service底层通用封装
 * 
 * @ClassName: CommonService
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2019年12月10日
 */
public abstract class JBoltCommonService implements IJBoltParaValidator{
	protected static final Log LOG = Log.getLog(JBoltCommonService.class);
	protected static final BigDecimal ZERO_BIGDECIMAL=new BigDecimal("0");
	protected static final String TRUE = Sql.TRUE;
	protected static final String FALSE = Sql.FALSE;
	protected String dataSourceConfigName;//数据源配置名
	protected String tableName;//主表名
	protected String dbType;//数据库类型
	protected String primaryKey;//主键
	protected String idGenMode;//主键策略
	
	/**
	 * 获取数据源配置名称
	 * @return
	 */
	public abstract String dataSourceConfigName();
	/**
	 *  获取数据库类型
	 * @return
	 */
	public abstract String dbType();
	/**
	 *  获取主键名称
	 * @return
	 */
	public abstract String primaryKey();
	/**
	 *  获取主键策略
	 * @return
	 */
	public abstract String idGenMode();
	@Inject
	protected SystemLogService systeLogService;
	/**
	 * 成功消息默认返回值
	 */
	protected static final Ret SUCCESS = Ret.ok("msg", Msg.SUCCESS);
	/**
	 * 失败消息默认返回值
	 */
	protected static final Ret FAIL = Ret.fail("msg", Msg.FAIL);
	/**
	 * 默认空KV
	 */
	protected static final Kv KV_EMPTY = Kv.create();

	/**
	 * 设置失败返回消息
	 * 
	 * @param msg
	 * @return
	 */
	protected Ret fail(String msg) {
		return Ret.fail("msg", msg);
	}

	/**
	 * 自动判断返回值
	 * 
	 * @param success
	 * @return
	 */
	protected Ret ret(boolean success) {
		return success ? SUCCESS : FAIL;
	}
	/**
	 * 过来返回值 model专用
	 * @param ret
	 * @param columns
	 * @return
	 */
	protected Ret filterModelRet(Ret ret,String... columns) {
		if(columns==null) {
			return ret;
		}
		JBoltBaseModel<? extends JBoltBaseModel<?>> myModel=ret.getAs("data");
		if(columns.length==1) {
			ret.set("data",myModel.get(columns[0]));
		}else {
			Kv kv=Kv.create();
			for(String column:columns) {
				kv.set("column",myModel.get(column));
			}
			ret.set("data",kv);
		}
		return ret;
	}

	/**
	 * 设置成功返回消息
	 * 
	 * @param msg
	 * @return
	 */
	protected Ret success(String msg) {
		return Ret.ok("msg", msg);
	}

	/**
	 * 设置成功返回消息 设置返回data
	 * 
	 * @param data
	 * @return
	 */
	protected Ret successWithData(Object data) {
		return Ret.ok("data", data);
	}

	/**
	 * 设置成功返回值带着数据和消息
	 * 
	 * @param data
	 * @param msg
	 * @return
	 */
	protected Ret success(Object data, String msg) {
		return Ret.ok("msg", msg).set("data", data);
	}

	/**
	 * 调用日志服务 添加日志信息 操作类型是Save
	 * 
	 * @param targetId   关联操作目标数据的ID
	 * @param userId     操作人
	 * @param targetType 操作目标的ID
	 * @param modelName  操作目标的具体数据的名字
	 */
	protected void addSaveSystemLog(Object targetId, Object userId, int targetType, String modelName) {
		addSystemLog(targetId, userId, SystemLog.TYPE_SAVE, targetType, modelName, null);
	}

	/**
	 * 调用日志服务 添加日志信息 操作类型是Update
	 * 
	 * @param targetId   关联操作目标数据的ID
	 * @param userId     操作人
	 * @param targetType 操作目标的ID
	 * @param modelName  操作目标的具体数据的名字
	 */
	protected void addUpdateSystemLog(Object targetId, Object userId, int targetType, String modelName) {
		addSystemLog(targetId, userId, SystemLog.TYPE_UPDATE, targetType, modelName, null);
	}

	/**
	 * 调用日志服务 添加日志信息 操作类型是Update
	 * 
	 * @param targetId   关联操作目标数据的ID
	 * @param userId     操作人
	 * @param targetType 操作目标的ID
	 * @param modelName  操作目标的具体数据的名字
	 * @param append     额外信息
	 */
	protected void addUpdateSystemLog(Object targetId, Object userId, int targetType, String modelName, String append) {
		addSystemLog(targetId, userId, SystemLog.TYPE_UPDATE, targetType, modelName, append);
	}

	/**
	 * 调用日志服务 添加日志信息 操作类型是Delete
	 * 
	 * @param targetId   关联操作目标数据的ID
	 * @param userId     操作人
	 * @param targetType 操作目标的ID
	 * @param modelName  操作目标的具体数据的名字
	 */
	protected void addDeleteSystemLog(Object targetId, Object userId, int targetType, String modelName) {
		addSystemLog(targetId, userId, SystemLog.TYPE_DELETE, targetType, modelName, null);
	}

	/**
	 * 调用日志服务 添加日志信息
	 * 
	 * @param targetId   关联操作目标数据的ID
	 * @param userId     操作人
	 * @param type       操作类型
	 * @param targetType 操作目标的ID
	 * @param modelName  操作目标的具体数据的名字
	 */
	protected void addSystemLog(Object targetId, Object userId, int type, int targetType, String modelName) {
		addSystemLog(targetId, userId, type, targetType, modelName, null);
	}

	/**
	 * 调用日志服务 添加日志信息
	 * 
	 * @param targetId   关联操作目标数据的ID
	 * @param userId     操作人
	 * @param type       操作类型
	 * @param targetType 操作目标的ID
	 * @param modelName  操作目标的具体数据的名字
	 * @param append     额外信息
	 */
	protected void addSystemLog(Object targetId, Object userId, int type, int targetType, String modelName,
			String append) {
		String userName = CACHE.me.getUserName(userId);
		String userUsername = CACHE.me.getUserUsername(userId);
		StringBuilder title = new StringBuilder();
		title.append("<span class='text-danger'>[").append(userName).append("(").append(userUsername).append(")").append("]</span>")
				.append(SystemLogService.typeName(type)).append(SystemLogService.targetTypeName(targetType))
				.append("<span class='text-danger'>[").append(modelName).append("]</span>");
		if (StrKit.notBlank(append)) {
			title.append(append);
		}
		// 调用底层方法
		addSystemLogWithTitle(targetId, userId, type, targetType, title.toString());
	}

	/**
	 * 调用日志服务 添加日志信息
	 * 
	 * @param targetId
	 * @param userId
	 * @param type
	 * @param targetType
	 * @param title
	 */
	protected void addSystemLogWithTitle(Object targetId, Object userId, int type, int targetType, String title) {
		String userName = CACHE.me.getUserName(userId);
		systeLogService.saveLog(type, targetType, targetId, title, 0, userId, userName);
	}
	 
	/**
	 * 创建id自增序列
	 * 
	 * @param tableName
	 * @return
	 */
	protected boolean createIdSequence(String tableName) {
		String sql = null;
		switch (dbType()) {
		case DBType.ORACLE:
			sql = "create sequence %s_idsq"+ " minvalue 10000" + 
					" maxvalue 9999999999999999999999999999" + 
					" start with 10001" + 
					" increment by 1" + 
					" cache 20";
			break;
		case DBType.POSTGRESQL:
			sql = "create sequence %s_idsq"+ " minvalue 10000" + 
					" maxvalue 9999999999999999999999999999" + 
					" start with 10001" + 
					" increment by 1" + 
					" cache 20";
			break;
		case DBType.DM:
			sql = "create sequence %s_idsq"+ " minvalue 10000" + 
					" maxvalue 9999999999999999999999999999" + 
					" start with 10001" + 
					" increment by 1" + 
					" cache 20";
			break;
		}
		return jdbcExeSql(String.format(sql, tableName));
	}

	/**
	 * 创建分表
	 * 
	 * @param newTableName
	 * @param mainTableName
	 * @return
	 */
	protected boolean createTheTable(String newTableName, String mainTableName) {
		String sql = null;
		switch (dbType()) {
		case DBType.MYSQL:
			sql = "create table " + newTableName + " LIKE " + mainTableName;
			break;
		case DBType.DM:
			sql = "create table " + newTableName + " LIKE " + mainTableName;
			break;
		case DBType.POSTGRESQL:
			sql = "create table " + newTableName + " (LIKE " + mainTableName + ")";
			break;
		case DBType.SQLSERVER:
			sql = "select  * into " + newTableName + " from " + mainTableName;
			break;
		case DBType.ORACLE:
			sql = "create table " + newTableName + " as select * from " + mainTableName + " where 1=2";
			break;
		}
		boolean success = jdbcExeSql(sql);
		if (success == false) {
			return false;
		}
		return tableExist(newTableName);
	}

	private boolean jdbcExeSql(String sql) {
		Connection connection = null;
		Statement statement = null;
		try {
			connection = DbKit.getConfig(dataSourceConfigName()).getConnection();
			if (connection == null) {
				return false;
			}
			statement = connection.createStatement();
			statement.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (statement != null) {
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	/**
	 * 检测表是否存在
	 * 
	 * @param tableName
	 * @return
	 */
	protected boolean tableExist(String tableName) {
		if (dbType().equals(DBType.ORACLE)) {
			tableName = tableName.toUpperCase();
		}
		boolean flag = false;
		Connection connection=null;
		try {
			connection = DbKit.getConfig(dataSourceConfigName()).getConnection();
			DatabaseMetaData meta = connection.getMetaData();
			String type[] = { "TABLE" };
			ResultSet rs = meta.getTables(null, null, tableName, type);
			flag = rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}finally {
			if(connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return flag;
	}
	
	
	/**
	 * 转换为Record tree
	 * @param allList
	 * @param idColumn
	 * @param pidColumn
	 * @param checkIsParentFunc
	 * @return
	 */
	public List<Record> convertToRecordTree(List<Record> allList,String idColumn,String pidColumn,TreeCheckIsParentNode<Record> checkIsParentFunc) {
		if(notOk(allList)) {return Collections.emptyList();}
		List<Record> parents=new ArrayList<Record>();
		Record m;
		for(int i=0;i<allList.size();i++) {
			m=allList.get(i);
			if(checkIsParentFunc.isParent(m)) {
				parents.add(m);
				allList.remove(m);
				i--;
			}
		}
		if(parents.size()>0&&allList.size()>0) {
			processTreeNodes(allList,parents,idColumn,pidColumn);
		}
		return parents;
	}
	/**
	 * 处理tree节点
	 * @param allList
	 * @param parents
	 * @param idColumn
	 * @param pidColumn
	 */
	private void processTreeNodes(List<Record> allList, List<Record> parents, String idColumn, String pidColumn) {
		ListMap<String, Record> map=new ListMap<String, Record>();
		for(Record m:allList) {
			map.addItem("p_"+m.get(pidColumn), m);
		}
		for(Record p:parents) {
			processTreeSubNodes(map,p,idColumn);
		}
		
	}
	/**
	 * 递归处理tree子节点
	 * @param map
	 * @param m
	 * @param idColumn
	 */
	private void processTreeSubNodes(ListMap<String, Record> map, Record m,String idColumn) {
		List<Record> items=map.get("p_"+m.get(idColumn));
		if(items!=null&&items.size()>0) {
			for(Record item:items) {
				processTreeSubNodes(map, item,idColumn);
			}
		}
		m.set("items",items);
	}
	
	
	
	/**
	 * 空分页数据返回值
	 */
	protected static final Page EMPTY_PAGE = new Page(Collections.EMPTY_LIST, 1, PageSize.PAGESIZE_ADMIN_LIST, 0, 0);
	public Page getEmptyPage(){
		return EMPTY_PAGE;
	}
	/**
	 * 添加拼接like
	 * 
	 * @param column
	 */
	protected String columnLike(String column) {
		if (notOk(column)) {
			return " like ''";
		}
		if (column.indexOf("'") != -1) {
			column = column.replace("'", "''");
		}
		return " like '%" + column + "%'";
	}

	 
	/**
	 * 更新同级删除数据之后数据的排序
	 * 
	 * @param sortRank
	 */
	protected void updateSortRankAfterDelete(String table, Integer sortRank) {
		updateSortRankAfterDelete(table, null, sortRank);
	}

	/**
	 * 更新同级删除数据之后数据的排序
	 * 
	 * @param params
	 * @param sortRank
	 */
	protected void updateSortRankAfterDelete(String table, Kv params, Integer sortRank) {
		if (params == null || params.isEmpty()) {
			Db.use(dataSourceConfigName()).update("update " + table + " set sort_rank=sort_rank-1 where sort_rank>?", sortRank);
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append(" 1=1 ");
			int size = params.size();
			Object[] paramArray = new Object[size + 1];
			int i = 0;
			for (Object key : params.keySet()) {
				sb.append(" and ").append(key.toString()).append("=? ");
				paramArray[i] = params.get(key.toString());
				i++;
			}
			paramArray[size] = sortRank;
			Db.use(dataSourceConfigName()).update("update " + table + " set sort_rank=sort_rank-1 where " + sb.toString() + " and sort_rank>?",
					paramArray);
		}
	}

	/**
	 * 查询一个字段
	 * 
	 * @param sql
	 * @return
	 */
	protected <T> T queryColumn(Sql sql) {
		if (sql.isPrepared()) {
			return queryColumn(sql, sql.getWhereValues());
		}
		return queryColumn(sql.toSql());
	}

	/**
	 * 查询一个字段
	 * 
	 * @param <T>
	 * @param sql
	 * @param paras
	 * @return
	 */
	protected <T> T queryColumn(Sql sql, Object... paras) {
		return queryColumn(sql.toSql(), paras);
	}

	/**
	 * 查询一个字段
	 * 
	 * @param <T>
	 * @param sql
	 * @return
	 */
	protected <T> T queryColumn(String sql) {
		return Db.use(dataSourceConfigName()).queryColumn(sql);
	}

	/**
	 * 查询一个字段
	 * 
	 * @param <T>
	 * @param sql
	 * @param paras
	 * @return
	 */
	protected <T> T queryColumn(String sql, Object... paras) {
		return Db.use(dataSourceConfigName()).queryColumn(sql, paras);
	}

	protected Integer queryInt(Sql sql) {
		if (sql.isPrepared()) {
			return queryInt(sql, sql.getWhereValues());
		}
		return queryInt(sql.toSql());
	}

	protected Integer queryInt(Sql sql, Object... paras) {
		return queryInt(sql.toSql(), paras);
	}

	protected Integer queryInt(String sql) {
		return Db.use(dataSourceConfigName()).queryInt(sql);
	}

	protected Integer queryInt(String sql, Object... paras) {
		return Db.use(dataSourceConfigName()).queryInt(sql, paras);
	}
	protected Long queryLong(Sql sql, Object... paras) {
		return queryLong(sql.toSql(), paras);
	}

	protected Long queryLong(String sql) {
		return Db.use(dataSourceConfigName()).queryLong(sql);
	}
	protected Long queryLong(String sql, Object... paras) {
		return Db.use(dataSourceConfigName()).queryLong(sql, paras);
	}
	protected int update(Sql sql) {
		if (sql.isPrepared()) {
			return update(sql, sql.getWhereValues());
		}
		return update(sql.toSql());
	}

	protected int update(Sql sql, Object... paras) {
		return update(sql.toSql(), paras);
	}

	protected int update(String sql) {
		return Db.use(dataSourceConfigName()).update(sql);
	}

	protected int update(String sql, Object... paras) {
		return Db.use(dataSourceConfigName()).update(sql, paras);
	}
	
	protected int delete(Sql sql) {
		if (sql.isPrepared()) {
			return delete(sql, sql.getWhereValues());
		}
		return delete(sql.toSql());
	}
	
	protected int delete(Sql sql, Object... paras) {
		return delete(sql.toSql(), paras);
	}
	
	protected int delete(String sql, Object... paras) {
		return Db.use(dataSourceConfigName()).delete(sql, paras);
	}

	protected <T> List<T> query(String sql, Object... paras) {
		return Db.use(dataSourceConfigName()).query(sql, paras);
	}

	protected <T> List<T> query(String sql) {
		return Db.use(dataSourceConfigName()).query(sql);
	}

	protected <T> List<T> query(Sql sql) {
		if (sql.isPrepared()) {
			return query(sql.toSql(), sql.getWhereValues());
		}
		return query(sql.toSql());
	}

	protected <T> List<T> query(Sql sql, Object... paras) {
		return query(sql.toSql(), paras);
	}
	/**
	 * 判断Object参数有效性
	 * 
	 * @param param
	 */
	public boolean isOk(Object param) {
		return JBoltParaValidator.isOk(param);
	}

	/**
	 * 判断Object参数是否无效
	 */
	public boolean notOk(Object param) {
		return JBoltParaValidator.notOk(param);
	}

	/**
	 * 判断上传文件是图片
	 * 
	 * @param isImage
	 */
	public boolean isImage(String contentType) {
		return JBoltParaValidator.isImage(contentType);
	}

	/**
	 * 判断上传文件不是图片
	 * 
	 * @param notImage
	 */
	public boolean notImage(String contentType) {
		return JBoltParaValidator.notImage(contentType);
	}

	/**
	 * 判断上传文件类型不是图片
	 * 
	 * @param file
	 */
	public boolean notImage(UploadFile file) {
		return JBoltParaValidator.notImage(file);
	}

	/**
	 * 判断上传文件类型是否为图片
	 * 
	 * @param file
	 */
	public boolean isImage(UploadFile file) {
		return JBoltParaValidator.isImage(file);
	}

	/**
	 * 判断Object[]数组类型数据是否正确
	 * 
	 * @param param
	 * @return
	 */
	public boolean isOk(Object[] param) {
		return JBoltParaValidator.isOk(param);
	}

	/**
	 * 判断Object[]数组类型数据不正确
	 * 
	 * @param param
	 * @return
	 */
	public boolean notOk(Object[] param) {
		return JBoltParaValidator.notOk(param);
	}
	/**
	 * 判断Serializable[]数组类型数据不正确
	 * 
	 * @param param
	 * @return
	 */
	public boolean notOk(Serializable[] param) {
		return JBoltParaValidator.notOk(param);
	}
	/**
	 * 判断Integer[]数组类型数据不正确
	 * 
	 * @param param
	 * @return
	 */
	public boolean notOk(Integer[] param) {
		return JBoltParaValidator.notOk(param);
	}

	/**
	 * 转为startTime
	 * 
	 * @param dateTime
	 * @return
	 */
	public Object toStartTime(Date startTime) {
		return toDateTime(DateUtil.HHmmssTo000000Str(startTime));
	}

	/**
	 * 转为endTime
	 * 
	 * @param endTime
	 * @return
	 */
	public Object toEndTime(Date endTime) {
		return toDateTime(DateUtil.HHmmssTo235959Str(endTime));
	}
	/**
	 * 转为表达式处理date
	 * 
	 * @param dateTime
	 * @return
	 */
	public Object toDateTime(String dateTime) {
		if(notOk(dateTime)) {return null;}
		if (dbType().equals(DBType.ORACLE)) {
			return new SqlExpress(String.format("TO_DATE('%s','yyyy-mm-dd hh24:mi:ss')", dateTime));
		}
		return dateTime;
	}
	
	@Override
	public boolean notExcel(UploadFile file) {
		return JBoltParaValidator.notExcel(file);
	}
	@Override
	public boolean isExcel(UploadFile file) {
		return JBoltParaValidator.isExcel(file);
	}
	/**
	 * 删除webapp下的文件
	 * @param filePath
	 * @return
	 */
	public Ret deleteWebappFile(String filePath) {
		if(notOk(filePath)) {
			return fail("文件地址有误");
		}
		if(!FileUtil.isAbsolutePath(filePath)) {
			filePath = FileUtil.normalize(PathKit.getWebRootPath()+File.separator+filePath);
		}
		if(!FileUtil.exist(filePath)) {
			return fail("文件不存在");
		}
		boolean success = FileUtil.del(new File(filePath));
		return ret(success);
	}
	protected void processRecordJsTree(List<JsTreeBean> treeBeans, List<Record> jstreeMs, int openLevel,int currentLevel,String idColumn,String pidColumn, String textColumn,String enableColumn,String[] keys,Object selectedId,boolean hasRoot,boolean needOriginData) {
		if(notOk(jstreeMs)) {return;}
		boolean opened=false;
		boolean keysIsOk=isOk(keys);
		Object idValue;
		Object pidValue;
		String type;
		for(Record m:jstreeMs){
			opened=openLevel==-1||false;
			boolean selected=false;
			
			pidValue=m.get(pidColumn);
			if(opened==false&&currentLevel<=openLevel) {
				opened=true;
			}
			idValue=m.get(idColumn);
			if(keysIsOk&&idValue!=null&&idValue.toString().equals("0")==false&&ArrayUtil.contains(keys,idValue.toString())){
				selected=false;
				opened=true;
			}
			if(selectedId!=null&&idValue.toString().equals(selectedId.toString())){
				selected=true;
				opened=true;
			}
			
			type=m.get("js_tree_type");
			if(notOk(type)) {
				type="parent";
			}
			if(type.equals("node")==false&&opened) {
				type="parent_opened";
			}
			treeBeans.add(convertToJsTreeBean(m, opened, selected, type, idColumn, pidColumn, textColumn, enableColumn,hasRoot,needOriginData));
			processRecordJsTree(treeBeans, m.get("items"), openLevel,(currentLevel+1), idColumn, pidColumn, textColumn, enableColumn, keys, selectedId,hasRoot,needOriginData);
		}
		
	}

	public JsTreeBean convertToJsTreeBean(Record m,boolean opened,boolean selected,String type,String idColumn,String pidColumn,String textColumn,String enableColumn,boolean hasRoot,boolean needOriginData) {
		if(needOriginData) {
			return new JsTreeBean(m.get(idColumn), m.get(pidColumn),getEnableName(m, textColumn, enableColumn),opened,selected,type,hasRoot).setData(m);
		}
		return new JsTreeBean(m.get(idColumn), m.get(pidColumn),getEnableName(m, textColumn, enableColumn),opened,selected,type,hasRoot);
	}
	
	/**
	 * 树形结构上单独使用的
	 * @return
	 */
	private String getEnableName(Record m,String textColumn,String enableColumn) {
		String name=m.get(textColumn);
		String enableValue=m.get(enableColumn);
		Boolean enable=isOk(enableValue)&&(Boolean.parseBoolean(enableValue)||enableValue.equals(TRUE));
		return (enable!=null&&enable)?name:(name+"[<span class='text-danger'>禁用</span>]");
	}
	
	protected void processRecordJsTreeType(List<Record> jstreeMs) {
		if(isOk(jstreeMs)) {
			List<Record> items;
			for(Record m:jstreeMs){
				items=m.get("items");
				if(isOk(items)) {
					m.set("js_tree_type","parent");
					processRecordJsTreeType(items);
				}else {
					m.set("js_tree_type","node");
				}
			}
		}
	}
	
	/**
	 * 处理事务
	 * @param atom
	 * @return
	 */
	protected boolean tx(IAtom atom) {
		return Db.use(dataSourceConfigName()).tx(atom);
	}
	
	/**
	 * 处理事务
	 * @param transactionLevel
	 * @param atom
	 * @return
	 */
	protected boolean tx(int transactionLevel, IAtom atom) {
		return Db.use(dataSourceConfigName()).tx(transactionLevel,atom);
	}
	
	/**
	 * 处理sql 使用jboltTableMenuFilter构造
	 * @param sql
	 * @param jboltTableMenuFilter
	 * @param matchColumns
	 */
	protected void processJBoltTableMenuFilterSql(Sql sql,JBoltTableMenuFilter jboltTableMenuFilter,String... matchColumns) {
		// TODO Auto-generated method stub
		if(jboltTableMenuFilter.getPaging()) {
			sql.page(jboltTableMenuFilter.getPageNumber(), jboltTableMenuFilter.getPageSize());
		}
		if(isOk(jboltTableMenuFilter.getSortColumn())) {
			sql.orderBy(jboltTableMenuFilter.getSortColumn(), jboltTableMenuFilter.getSortType());
		}
		if(isOk(jboltTableMenuFilter.getKeywords())) {
			if(jboltTableMenuFilter.getInclude()) {
				sql.likeMulti(jboltTableMenuFilter.getKeywords(), matchColumns);
			}else {
				sql.notLikeMulti(jboltTableMenuFilter.getKeywords(), matchColumns);
			}
		}
		if(isOk(jboltTableMenuFilter.getItems())) {
			jboltTableMenuFilter.getItems().forEach(item->{
				if(item.getValue()!=null && item.getValue().toString().length()>0) {
					switch (item.getComparison()) {
						case "like":
							sql.like(item.getColumn(), item.getValue().toString());
							break;
						case "notLike":
							sql.notLike(item.getColumn(), item.getValue().toString());
							break;
						case "startWith":
							sql.startWith(item.getColumn(), item.getValue().toString());
							break;
						case "endWith":
							sql.endWith(item.getColumn(), item.getValue().toString());
							break;
						case "notStartWith":
							sql.notStartWith(item.getColumn(), item.getValue().toString());
							break;
						case "notEndWith":
							sql.notEndWith(item.getColumn(), item.getValue().toString());
							break;
						case "eq":
							sql.eq(item.getColumn(), item.getValue());
							break;
						case "noteq":
							sql.noteq(item.getColumn(), item.getValue());
							break;
						case "ge":
							sql.ge(item.getColumn(), item.getValue());
							break;
						case "gt":
							sql.gt(item.getColumn(), item.getValue());
							break;
						case "le":
							sql.le(item.getColumn(), item.getValue());
							break;
						case "lt":
							sql.lt(item.getColumn(), item.getValue());
							break;
					}
				}else {
					switch (item.getComparison()) {
						case "empty":
							sql.bracketLeft();
							sql.isNull(item.getColumn());
							sql.or();
							sql.eq(item.getColumn(), "");
							sql.bracketRight();
							break;
						case "notEmpty":
							sql.bracketLeft();
							sql.isNotNull(item.getColumn());
							sql.or();
							sql.noteq(item.getColumn(), "");
							sql.bracketRight();
							break;
				}
				}
			});
		}
	}
}
