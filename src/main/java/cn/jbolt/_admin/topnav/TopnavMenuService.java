package cn.jbolt._admin.topnav;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.model.Topnav;
import cn.jbolt.common.model.TopnavMenu;
import cn.jbolt.common.util.ArrayUtil;

public class TopnavMenuService extends JBoltBaseService<TopnavMenu> {
	private TopnavMenu dao=new TopnavMenu().dao();
	@Inject
	private TopnavService topnavService;
	@Override
	protected TopnavMenu dao() {
		return dao;
	}
	/**
	 * 查询顶部导航下的所有菜单列表 一级
	 * @param topnavId
	 * @return
	 */
	public List<TopnavMenu> getCheckedLeftMenus(Integer topnavId) {
		if(notOk(topnavId)) {
			return Collections.emptyList();
		}
		return getCommonList(Kv.by("topnav_id", topnavId));
	}
	/**
	 * 清空一个顶部导航下的所有菜单列表  一级
	 * @param topnavId
	 * @return
	 */
	public Ret clearTopnavMenus(Integer topnavId) {
		return deleteBy(Kv.by("topnav_id", topnavId));
	}
	/**
	 * 提交
	 * @param topnavId
	 * @param menusStr
	 * @return
	 */
	public Ret submit(Integer topnavId, String menusStr) {
		if (notOk(topnavId) || notOk(menusStr)) {
			return fail(Msg.PARAM_ERROR);
		}
		Topnav topnav = topnavService.findById(topnavId);
		if (topnav == null) {
			return fail("顶部导航信息不存在");
		}
		String[] ids = ArrayUtil.from(menusStr, ",");
		if (ids == null || ids.length == 0) {
			return fail("请选择分配的菜单");
		}
		// 先删除以前的
		clearTopnavMenus(topnavId);
		// 添加现在的
		saveTopnavMenus(topnavId, ids);
		// 添加日志
		addUpdateSystemLog(topnavId, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_TOPNAV, topnav.getName(), "下的菜单");
		return SUCCESS;
	}
	/**
	 * 保存一个顶部导航下的菜单
	 * @param topnavId
	 * @param ids
	 */
	private void saveTopnavMenus(Integer topnavId, String[] ids) {
		List<TopnavMenu> topnavMenus = new ArrayList<TopnavMenu>();
		TopnavMenu menu;
		for (String id : ids) {
			menu = new TopnavMenu();
			menu.setTopnavId(topnavId);
			menu.setPermissionId(Integer.parseInt(id));
			topnavMenus.add(menu);
		}
		batchSave(topnavMenus);
		
	}
	
	/**
	 * 清空所有配置
	 * @return
	 */
	public Ret clearAllTopnavMenus() {
		delete(deleteSql().toSql());
		return SUCCESS;
	}
	
	/**
	 * 删除一个顶部导航下的菜单配置
	 * @param topnavId
	 */
	public void deleteByTopnavId(Integer topnavId) {
		if(isOk(topnavId)) {
			deleteBy(Kv.by("topnav_id", topnavId));
		}
	}
	/**
	 * 检测topnav是否分配菜单
	 * @param topnavId
	 * @return
	 */
	public boolean checkTopnavUsed(Integer topnavId) {
		if(notOk(topnavId)) {return false;}
		return exists("topnav_id", topnavId);
	}

}
