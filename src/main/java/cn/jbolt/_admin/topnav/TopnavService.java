package cn.jbolt._admin.topnav;

import java.util.Collections;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.model.Topnav;
import cn.jbolt.common.util.CACHE;
/**
 * 
 * @ClassName: TopnavService   
 * @author: JFinal学院-小木
 * @date: 2020-08-28 00:48  
 */
public class TopnavService extends JBoltBaseService<Topnav> {
	private Topnav dao=new Topnav().dao();
	@Inject
	private TopnavMenuService topnavMenuService;
	@Override
	protected Topnav dao() {
		return dao;
	}
		
  /**
	 * 后台管理list查询
	 * @param keywords
	 * @return
	 */
	public List<Topnav> getAdminList(String keywords) {
		return  getCommonListByKeywords(keywords, "sort_rank","asc", "name");
	}
	
	/**
	 * 保存
	 * @param topnav
	 * @return
	 */
	public Ret save(Topnav topnav) {
		return submit(topnav, false);
	}
	
	/**
	 * 保存
	 * @param topnav
	 * @return
	 */
	public Ret update(Topnav topnav) {
		return submit(topnav, true);
	}
	
   /**
	 * 提交
	 * @param topnav
	 * @param update
	 */
	public Ret submit(Topnav topnav, boolean update) {
		if(topnav==null
				||(!update&&isOk(topnav.getId()))
				||(update&&notOk(topnav.getId()))) {
			return fail(Msg.PARAM_ERROR);
		}
		if(update) {
			//更新时需要判断数据存在
			Topnav dbTopnav=findById(topnav.getId());
			if(dbTopnav==null) {return fail(Msg.DATA_NOT_EXIST);}
		}
		boolean success=false;
		//save和update分别处理
		if(update) {
			if(existsName(topnav.getName(), topnav.getId())) {return fail(Msg.DATA_SAME_NAME_EXIST);}
			success=topnav.update();
		}else {
			if(existsName(topnav.getName(), -1)) {return fail(Msg.DATA_SAME_NAME_EXIST);}
			topnav.setSortRank(getNextSortRank());
			success=topnav.save();
		}
		if(success) {
			//添加日志
			addSystemLog(topnav.getId(), JBoltUserKit.getUserId(), update?SystemLog.TYPE_UPDATE:SystemLog.TYPE_SAVE, SystemLog.TARGETTYPE_TOPNAV, topnav.getName());
		}
		return ret(success);
	}
	
   /**
	  * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		Ret ret=deleteById(id,false);
		if(ret.isOk()){
			Topnav topnav=ret.getAs("data");
			//重新设置其他剩余顺序
			updateSortRankAfterDelete(topnav.getSortRank());
			//删除menus配置
			topnavMenuService.deleteByTopnavId(id);
			//添加日志
			addDeleteSystemLog(id, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_TOPNAV, topnav.getName());
		}
		return ret;
	}
	
	
   /**
	  * 上移
	 * @param id
	 * @return
	 */
	public Ret up(Integer id) {
		Topnav topnav=findById(id);
		if(topnav==null){
			return fail("数据不存在或已被删除");
		}
		Integer rank=topnav.getSortRank();
		if(rank==null||rank<=0){
			return fail("顺序需要初始化");
		}
		if(rank==1){
			return fail("已经是第一个");
		}
		Topnav upTopnav=findFirst(Kv.by("sort_rank", rank-1));
		if(upTopnav==null){
			return fail("顺序需要初始化");
		}
		upTopnav.setSortRank(rank);
		topnav.setSortRank(rank-1);
		
		upTopnav.update();
		topnav.update();
		return SUCCESS;
	}
	
/**
	 * 下移
	 * @param id
	 * @return
	 */
	public Ret down(Integer id) {
		Topnav topnav=findById(id);
		if(topnav==null){
			return fail("数据不存在或已被删除");
		}
		Integer rank=topnav.getSortRank();
		if(rank==null||rank<=0){
			return fail("顺序需要初始化");
		}
		int max=getCount();
		if(rank==max){
			return fail("已经是最后已一个");
		}
		Topnav upTopnav=findFirst(Kv.by("sort_rank", rank+1));
		if(upTopnav==null){
			return fail("顺序需要初始化");
		}
		upTopnav.setSortRank(rank);
		topnav.setSortRank(rank+1);
		
		upTopnav.update();
		topnav.update();
		return SUCCESS;
	}
	
   /**
	  * 初始化排序
	 */
	public Ret initRank(){
		List<Topnav> allList=findAll();
		if(allList.size()>0){
			for(int i=0;i<allList.size();i++){
				allList.get(i).setSortRank(i+1);
			}
			batchUpdate(allList);
			CACHE.me.removeTopNavs();
			//添加日志
			addUpdateSystemLog(null, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_TOPNAV, "所有数据", "的顺序:初始化所有");
		}
		return SUCCESS;
	}
	
   /**
	 * 切换禁用启用状态
	 * @param id
	 * @return
	 */
	public Ret toggleEnable(Integer id) {
		//说明:如果需要日志处理 就解开下面部分内容 如果不需要直接删掉即可
		Ret ret=toggleBoolean(id, "enable");
		if(ret.isOk()){
			//添加日志
			Topnav topnav=ret.getAs("data");
			addUpdateSystemLog(id, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_TOPNAV, topnav.getName(),"的启用状态:"+topnav.getEnable());
		} 
		return ret;
	}
	
	@Override
	public String checkCanToggle(Topnav topnav, String column, Kv kv) {
		if("enable".equals(column) && (topnav.getEnable()==null || topnav.getEnable() == false)) {
			boolean inUse = topnavMenuService.checkTopnavUsed(topnav.getId());
			if(!inUse) {
				return "此顶部导航尚未分配任何菜单";
			}
		}
		
		return null;
	}
	/**
	 * 获取启用的数据
	 * @return
	 */
	public List<Topnav> getEnableList() {
		return getCommonList(Kv.by("enable", TRUE),"sort_rank","asc");
	}
	/**
	 * 检测是否存在启用的topnav
	 * @return
	 */
	public boolean checkHasEnableTopnav() {
		return exists("enable", TRUE);
	}
	
	/**
	 * 获取指定ids里enable=true的数据
	 * @param ids
	 * @return
	 */
	public List<Topnav> getEnableListByIds(List<Integer> ids) {
		if(!isOk(ids)) {Collections.emptyList();}
		return find(selectSql().inList("id", ids).eqQM("enable").orderBy("sort_rank"),TRUE);
	}
	
}