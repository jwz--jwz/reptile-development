package cn.jbolt._admin.topnav;

import java.util.List;

import com.jfinal.aop.Inject;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.PermissionService;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Permission;
import cn.jbolt.common.model.Topnav;

@CheckPermission(PermissionKey.TOPNAV)
public class TopnavMenuAdminController extends JBoltBaseController {
	@Inject
	private PermissionService permissionService;
	@Inject
	private TopnavMenuService service;
	@Inject
	private TopnavService topnavService;
	/**
	 * 设置导航菜单
	 */
	public void setting() {
		Integer topnavId = getInt(0);
		if (notOk(topnavId)) {
			renderDialogFail(Msg.PARAM_ERROR);
			return;
		}
		Topnav topnav = topnavService.findById(topnavId);
		if (topnav == null) {
			renderDialogFail(Msg.PARAM_ERROR);
			return;
		}
		List<Permission> permissions = permissionService.getLevel1MenusJoinTopnavMenu();
		set("dataList", permissions);
		set("topnavId", topnavId);
		render("editleftmenus.html");
	}

	/**
	 * 获取左侧导航选中数据
	 */
	public void getChecked() {
		renderJsonData(service.getCheckedLeftMenus(getInt(0)));
	}
	/**
	 * 清空一个顶部导航下的菜单配置
	 */
	public void clear() {
		renderJson(service.clearTopnavMenus(getInt(0)));
	}
	/**
	 * 清空所有顶部导航下的菜单配置
	 */
	public void clearAll() {
		renderJson(service.clearAllTopnavMenus());
	}
	/**
	 * 提交一个顶部导航下的菜单配置
	 */
	public void submit() {
		Integer topnavId=getInt("topnavId");
		String permissionStr=get("permissions");
		renderJson(service.submit(topnavId,permissionStr));
	}
}
