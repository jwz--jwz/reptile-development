package cn.jbolt._admin.updatemgr;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.JboltVersion;
import cn.jbolt.common.model.JboltVersionFile;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.poi.excel.JBoltExcel;
import cn.jbolt.common.poi.excel.JBoltExcelHeader;
import cn.jbolt.common.poi.excel.JBoltExcelSheet;

/**
 * JBolt版本更新文件管理Service
 * @ClassName:  JboltVersionFileService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年11月9日   
 */
public class JboltVersionFileService extends JBoltBaseService<JboltVersionFile> {
	private JboltVersionFile dao = new JboltVersionFile().dao();
	@Inject
	private JBoltVersionService jBoltVersionService;
	@Override
	protected JboltVersionFile dao() {
		return dao;
	}
	public Page<JboltVersionFile> pageFilesByJboltVersionId(Integer jboltVersionId,Integer pageNumber,int pageSize) {
		return paginate(Kv.by("jbolt_version_id", jboltVersionId), pageNumber, pageSize);
	}
	/**
	 * 得到一个新版本的更新文件列表
	 * @param jboltVersionId
	 * @return
	 */
	public List<JboltVersionFile> getFilesByJboltVersionId(Integer jboltVersionId) {
		return getCommonList(Kv.by("jbolt_version_id", jboltVersionId));
	}
	/**
	 * 保存
	 * @param jboltVersionFile
	 * @return
	 */
	public Ret save(JboltVersionFile jboltVersionFile) {
		if(jboltVersionFile==null||isOk(jboltVersionFile.getId())||notOk(jboltVersionFile.getUrl())||notOk(jboltVersionFile.getJboltVersionId())){return fail(Msg.PARAM_ERROR);}
		JboltVersion jboltVersion=jBoltVersionService.findById(jboltVersionFile.getJboltVersionId());
		if(jboltVersion==null){return fail("JBolt的版本信息不存在");}
		boolean success=jboltVersionFile.save();
		if(success){
			addSaveSystemLog(jboltVersionFile.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_JBOLT_VERSION_FILE,jboltVersion.getVersion()+":"+jboltVersionFile.getUrl());
		}
		return ret(success);
	}
	/**
	 * 更新
	 * @param jboltVersionFile
	 * @return
	 */
	public Ret update(JboltVersionFile jboltVersionFile) {
		if(jboltVersionFile==null||notOk(jboltVersionFile.getId())||notOk(jboltVersionFile.getUrl())||notOk(jboltVersionFile.getJboltVersionId())){return fail(Msg.PARAM_ERROR);}
		JboltVersion jboltVersion=jBoltVersionService.findById(jboltVersionFile.getJboltVersionId());
		if(jboltVersion==null){return fail("JBolt的版本信息不存在");}
		boolean success=jboltVersionFile.update();
		if(success){
			addUpdateSystemLog(jboltVersionFile.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_JBOLT_VERSION_FILE,jboltVersion.getVersion()+":"+jboltVersionFile.getUrl());
		}
		return ret(success);
	}
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id){
		Ret ret=deleteById(id);
		if(ret.isOk()){
			JboltVersionFile file=ret.getAs("data");
			JboltVersion jboltVersion=jBoltVersionService.findById(file.getJboltVersionId());
			addDeleteSystemLog(file.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_JBOLT_VERSION_FILE,jboltVersion.getVersion()+":"+file.getUrl());
		}
		return ret;
	}
	
	 
	/**
	 * 删除一个版本下的更新文件
	 * @param jboltVersionId
	 * @return
	 */
	public Ret deleteByVersion(Object jboltVersionId) {
		return deleteBy(Kv.by("jbolt_version_id", jboltVersionId));
	}
		/**
		 * 生成Excel数据 byte[]
		 * @return
		 */
		public JBoltExcel getFilesExcel(Integer jboltVersionId) {
		return JBoltExcel
				//创建
				.create()
	    		//设置工作表
	    		.setSheets(
	    				//设置工作表 列映射 顺序 标题名称
	    				JBoltExcelSheet
	    				.create("files")
	    				//表头映射关系
	    				.setHeaders(0,JBoltExcelHeader.create("url", "下载地址",100))
	    		    	//设置导出的数据源 来自于数据库查询出来的Model List
	    		    	.setModelDatas(1,getFilesByJboltVersionId(jboltVersionId))
	    				);
		}

}
