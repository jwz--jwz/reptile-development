package cn.jbolt._admin.updatemgr;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.model.JboltVersion;
import cn.jbolt.common.model.JboltVersionFile;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.poi.excel.JBoltExcel;
import cn.jbolt.common.poi.excel.JBoltExcelHeader;
import cn.jbolt.common.poi.excel.JBoltExcelMerge;
import cn.jbolt.common.poi.excel.JBoltExcelPositionData;
import cn.jbolt.common.poi.excel.JBoltExcelSheet;
import cn.jbolt.common.poi.excel.JBoltExcelUtil;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CACHE;

/**
 * JBolt版本管理Service
 * @ClassName:  JBoltVersionService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年11月9日   
 */
public class JBoltVersionService extends JBoltBaseService<JboltVersion> {
	private JboltVersion dao = new JboltVersion().dao();
	@Inject
	private JboltVersionFileService jboltVersionFileService;
	@Override
	protected JboltVersion dao() {
		return dao;
	}
	/**
	 * 保存
	 * @param jboltVersion
	 * @return
	 */
	public Ret save(JboltVersion jboltVersion) {
		if(isOk(jboltVersion.getId())||notOk(jboltVersion.getVersion())||notOk(jboltVersion.getPublishTime())){return fail(Msg.PARAM_ERROR);}
		jboltVersion.setCreateTime(new Date());
		jboltVersion.setUserId(JBoltUserKit.getUserIdAs());
		jboltVersion.setIsNew(false);
		boolean success=jboltVersion.save();
		if(success){
			//添加日志
			addSaveSystemLog(jboltVersion.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_JBOLT_VERSION, jboltVersion.getVersion());
		}
		return ret(success);
	}
	/**
	 * 更新
	 * @param jboltVersion
	 * @return
	 */
	public Ret update(JboltVersion jboltVersion) {
		if(notOk(jboltVersion.getId())||notOk(jboltVersion.getVersion())||notOk(jboltVersion.getPublishTime())){return fail(Msg.PARAM_ERROR);}
		boolean success=jboltVersion.update();
		if(success){
			//添加日志
			addUpdateSystemLog(jboltVersion.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_JBOLT_VERSION, jboltVersion.getVersion());
		}
		return ret(success);
	}
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Object id){
		Ret ret=deleteById(id);
		if(ret.isOk()){
			Ret delRet=jboltVersionFileService.deleteByVersion(id);
			if(delRet.isFail()) {return delRet;}
			JboltVersion jboltVersion=ret.getAs("data");
			//日志
			addDeleteSystemLog(jboltVersion.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_JBOLT_VERSION, jboltVersion.getVersion());
		}
		return ret;
	}
	
	
 
	/**
	 * 切换是否是最新版
	 * @param id
	 * @return
	 */
	public Ret doToggleIsNew( Integer id) {
		Ret ret=toggleBoolean(id, "is_new");
		if(ret.isOk()){
			JboltVersion jboltVersion=ret.getAs("data");
			//日志
			addUpdateSystemLog(jboltVersion.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_JBOLT_VERSION, jboltVersion.getVersion(),"的是否为最新版状态:"+jboltVersion.getIsNew());
			return successWithData(jboltVersion.getIsNew());
		}
		return ret;
	}
	
	@Override
	public String afterToggleBoolean(JboltVersion jboltVersion, String column,Kv kv) {
		if("is_new".equals(column)){
			if(jboltVersion.getIsNew()){
				 changeAllIsNewFlase();
			}
		}
		return null;
	}
	/**
	 * 切换所有isNew=true的变为false
	 */
	private void changeAllIsNewFlase() {
		update(updateSql().set("is_new", false).eq("is_new",true));
	}
 
	/**
	 * 获取主程序更新数据
	 * @return
	 */
	public String getMainUpdateDatas() {
		JboltVersion jboltVersion=getNewJboltVersion();
		if(jboltVersion==null){return null;}
		List<JboltVersionFile> files=jboltVersionFileService.getFilesByJboltVersionId(jboltVersion.getId());
		if(files==null||files.size()==0){return null;}
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("version", jboltVersion.getVersion());
		jsonObject.put("datas", ArrayUtil.getStringArray(files,"url"));
		return jsonObject.toJSONString();
	}
	/**
	 * 得到最新版
	 * @return
	 */
	private JboltVersion getNewJboltVersion() {
		return findFirst(Kv.by("is_new", TRUE));
	}
	
	/**
	  * 分页条件查询
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Page<JboltVersion> paginateAdminList(Integer pageNumber, Integer pageSize,String sortColumn,String sortType, String keywords, Date startTime, Date endTime) {
		Kv paras=Kv.create();
		if(StrKit.notBlank(keywords)){
			paras.set("version",columnLike(keywords.trim()));
		}
		if(isOk(startTime)){
			paras.set("create_time >=",toStartTime(startTime));
		}
		if(isOk(endTime)){
			paras.set("create_time <=",toEndTime(endTime));
		}
		
		return paginate(paras, sortColumn, sortType, pageNumber, pageSize, true);
	}
	/**
	 * 按照条件查询数据 不进行分页
	 * @param keywords
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<JboltVersion> getAdminList(String orderColumn,String orderType,String keywords, Date startTime, Date endTime) {
		Sql sql=selectSql();
		if(isOk(keywords)) {
			sql.like("version", keywords);
		}
		if(isOk(startTime)){
			sql.ge("create_time",toStartTime(startTime));
		}
		if(isOk(endTime)){
			sql.le("create_time",toEndTime(endTime));
		}
		sql.orderBy(orderColumn, orderType);
		return find(sql);
	}
	/**
	 * 生成Excel 导入模板的数据 byte[]
	 * @return
	 */
	public JBoltExcel getExcelImportTpl() {
		return JBoltExcel
				//创建
				.create()
				.setSheets(
						JBoltExcelSheet.create("versions")
						//设置列映射 顺序 标题名称 不处理别名
						.setHeaders(2,false,
								JBoltExcelHeader.create("版本号",16),
								JBoltExcelHeader.create("发布日期",15),
								JBoltExcelHeader.create("记录时间",15),
								JBoltExcelHeader.create("是否最新版",12),
								JBoltExcelHeader.create("用户名",10)
								)
						.setMerges(JBoltExcelMerge.create("A","E",1, 1, "JBolt版本信息"))
					);
	}
	/**
	 * 生成Excel数据 byte[]
	 * @return
	 */
	public JBoltExcel getExcelReport() {
	return JBoltExcel
			//创建
			.create()
    		//设置工作表
    		.setSheets(
    				//设置工作表 列映射 顺序 标题名称
    				JBoltExcelSheet
    				.create("versions")
    				//表头映射关系
    				.setHeaders(2,
    						JBoltExcelHeader.create("version", "版本号",16),
    						JBoltExcelHeader.create("publish_time", "发布日期",15),
    						JBoltExcelHeader.create("create_time", "记录时间",15),
    						JBoltExcelHeader.create("is_new", "是否最新版",12),
    						JBoltExcelHeader.create("user_username", "用户名",10)
    						)
    				//设置单元格合并区域
    		    	.setMerges(JBoltExcelMerge.create("A","E",1, 1, "JBolt版本信息"))
    		    	//特殊数据转换器
    		    	.setDataChangeHandler((data,index) ->{
    		    			//将user_id转为user_name
    		    			data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
    		    			data.changeBooleanToStr("is_new","是","否");
    		    	})
    		    	//设置导出的数据源 来自于数据库查询出来的Model List
    		    	.setModelDatas(3,findAll())
    				);
	}
	/**
	  * 删除多个数据
	 * @param ids
	 * @return
	 */
	@Before(Tx.class)
	public Ret deleteByIds(Object[] ids) {
		if(notOk(ids)) {return fail(Msg.PARAM_ERROR);}
		Ret ret;
		for(Object id:ids) {
			ret=delete(id);
			if(ret.isFail()) {
				return ret;
			}
		}
		return SUCCESS;
	}
	/**
	 * 读取excel文件
	 * @param file
	 * @return
	 */
	public Ret importExcelDatas(File file) {
		StringBuilder errorMsg=new StringBuilder();
		JBoltExcel jBoltExcel=JBoltExcel
		//从excel文件创建JBoltExcel实例
		.from(file)
		//设置工作表信息
		.setSheets(
				JBoltExcelSheet.create("versions")
				//设置列映射 顺序 标题名称
				.setHeaders(2,
					JBoltExcelHeader.create("version", "版本号"),
					JBoltExcelHeader.create("publish_time", "发布日期"),
					JBoltExcelHeader.create("create_time", "记录时间"),
					JBoltExcelHeader.create("is_new", "是否最新版"),
					JBoltExcelHeader.create("user_id", "用户名")
					)
				//特殊数据转换器
				.setDataChangeHandler((data,index) ->{
					//将user_id转为user_name
					data.changeStrToBoolean("is_new","是");
					data.change("user_id",CACHE.me.getUserIdByUsername(data.getStr("user_id")));
				})
				//从第三行开始读取
				.setDataStartRow(3)
				);		
		//从指定的sheet工作表里读取数据
		List<JboltVersion> jboltVersions=JBoltExcelUtil.readModels(jBoltExcel,"versions", JboltVersion.class,errorMsg);
		if(notOk(jboltVersions)) {
			if(errorMsg.length()>0) {
				return fail(errorMsg.toString());
			}else {
				return fail(Msg.DATA_IMPORT_FAIL_EMPTY);
			}
		}
		//执行批量操作
		boolean success=tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				batchSave(jboltVersions);
				return true;
			}
		});
		
		if(!success) {
			return fail(Msg.DATA_IMPORT_FAIL);
		}
		return SUCCESS;
	}
	/**
	 * 导出复杂表头
	 * @return
	 */
	public JBoltExcel getExcelReport2() {
		return JBoltExcel
				//创建
				.create()
				//设置工作表
				.setSheets(
						//设置工作表 列映射 顺序 标题名称
						JBoltExcelSheet
						.create("versions")
						//表头映射关系
						.setHeaders(
								JBoltExcelHeader.create("version", "版本号",16),
								JBoltExcelHeader.create("publish_time", "发布日期",15),
								JBoltExcelHeader.create("create_time", "记录时间",15),
								JBoltExcelHeader.create("is_new", "是否最新版",12),
								JBoltExcelHeader.create("user_username", "用户名",10)
								)
						//设置单元格合并区域
						.setMergeAsHeader(true)
						.setMerges(
								JBoltExcelMerge.create(1, 1, 1,5, "JBolt版本信息-1",true),
								JBoltExcelMerge.create(2, 3, 1,1, "版本号",true),
								JBoltExcelMerge.create(2, 2, 2,3, "时间",true),
								JBoltExcelMerge.create(2, 3, 4,4, "是否最新版",true),
								JBoltExcelMerge.create(2, 3, 5,5, "用户名",true),
								JBoltExcelMerge.create(3, 3, 2,2, "发布时间",true),
								JBoltExcelMerge.create(3, 3, 3,3, "记录时间",true)
								)
						//特殊数据转换器
						.setDataChangeHandler((data,index) ->{
							//将user_id转为user_name
							data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
							data.changeBooleanToStr("is_new","是","否");
						})
						//设置导出的数据源 来自于数据库查询出来的Model List
						.setModelDatas(4,findAll())
						);
	}
	/**
	 * 导出复杂表头 加载导出模板
	 * @return
	 */
	public JBoltExcel getExcelReport3() {
		return JBoltExcel
				.createByTpl("jboltversiontpl.xls")
				.setSheets(
						JBoltExcelSheet.create("versions")
						.setHeaders(
								JBoltExcelHeader.create("version", "版本号",16),
								JBoltExcelHeader.create("publish_time", "发布日期",15),
								JBoltExcelHeader.create("create_time", "记录时间",15),
								JBoltExcelHeader.create("is_new", "是否最新版",12),
								JBoltExcelHeader.create("user_username", "用户名",10)
								)
				    	//特殊数据转换器
				    	.setDataChangeHandler((data,index) ->{
				    			//将user_id转为user_name
				    			data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
				    			data.changeBooleanToStr("is_new","是","否");
				    	})
				    	//设置导出的数据源 来自于数据库查询出来的Model List
				    	.setModelDatas(4,findAll())
						);
				
		}
	
	
	/**
	 * 导出复杂表头 加载导出模板
	 * @return
	 */
	public JBoltExcel getExcelReport4() {
		List<JBoltExcelPositionData> excelPositionDatas=new ArrayList<JBoltExcelPositionData>();
		for(int i=0;i<31;i++) {
			excelPositionDatas.add(JBoltExcelPositionData.create(2+i,5,1+i));
			excelPositionDatas.add(JBoltExcelPositionData.create(2+i,6,2+i));
			excelPositionDatas.add(JBoltExcelPositionData.create(2+i,7,1+i));
		}
		
		excelPositionDatas.add(JBoltExcelPositionData.create("L",2,6));
		excelPositionDatas.add(JBoltExcelPositionData.create("M:2",7));
		excelPositionDatas.add(JBoltExcelPositionData.create("N2",8));
		for(int i=0;i<24;i++) {
			excelPositionDatas.add(JBoltExcelPositionData.create(2+i,"L",6+i));
			excelPositionDatas.add(JBoltExcelPositionData.create("M:"+(2+i),7+i));
			excelPositionDatas.add(JBoltExcelPositionData.create("N"+(2+i),8+i));
		}
		return JBoltExcel
				.createByTpl("testtpl.xls")
				.setSheets(
						JBoltExcelSheet.create("datas")
						.setPositionDatas(excelPositionDatas)
						);
				
		}
	
	
	/**
	 * 生成Excel数据 byte[] 带公式
	 * @return
	 */
	public JBoltExcel getExcelReport5() {
	//数据开始行（真实的从1开始）
	int dataStartRow=3;
	//准备数据
	List<JboltVersion> datas=findAll();
	int size=datas.size();
	
	//定义需要坐标定位公式的具体坐标点(真实的从1开始)
	int formluaCol=6;//第6列
	int formluaRow=dataStartRow+size;//计算出第几行
	//转为字母列标识
	String colStr=JBoltExcelUtil.colNumToStr(formluaCol);
	//处理好开头和结尾需要公式包含的数据行（真实的从1开始）
	String start=colStr+dataStartRow;
	String end=colStr+(formluaRow-1);
	//创建一个坐标定位数据作为公式（真实的从1开始）
	JBoltExcelPositionData formlua=JBoltExcelPositionData.create(formluaRow, formluaCol, "SUM("+start+":"+end+")",true);
	return JBoltExcel
			//创建
			.create()
    		//设置工作表
    		.setSheets(
    				//设置工作表 列映射 顺序 标题名称
    				JBoltExcelSheet
    				.create("versions")
    				.setForceFormulaRecalculation(true)
    				//表头映射关系
    				.setHeaders(2,
    						JBoltExcelHeader.create("version", "版本号",16),
    						JBoltExcelHeader.create("publish_time", "发布日期",15),
    						JBoltExcelHeader.create("create_time", "记录时间",15),
    						JBoltExcelHeader.create("is_new", "是否最新版",12),
    						JBoltExcelHeader.create("user_username", "用户名",10),
    						JBoltExcelHeader.create("test_data", "测试公式计算",15)
    						)
    				//设置单元格合并区域
    		    	.setMerges(JBoltExcelMerge.create("A","F",1, 1, "JBolt版本信息"))
    		    	//特殊数据转换器
    		    	.setDataChangeHandler((data,index) ->{
    		    			data.put("test_data", index);
    		    			//将user_id转为user_name
    		    			data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
    		    			data.changeBooleanToStr("is_new","是","否");
    		    	})
    		    	//设置导出的数据源 来自于数据库查询出来的Model List
    		    	.setModelDatas(dataStartRow,datas)
    		    	//增加定位公式数据
    		    	.addCellFormluas(formlua)
    				);
	}
}
