package cn.jbolt._admin.post;

import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Post;
import cn.jbolt.common.model.SystemLog;
/**
 * 
 * @ClassName: PostService   
 * @author: JFinal学院-小木
 * @date: 2021-02-11 12:12  
 */
public class PostService extends JBoltBaseService<Post> {
	private Post dao=new Post().dao();
	@Override
	protected Post dao() {
		return dao;
	}
		
  /**
	 * 分页获取数据
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @return
	 */
	public Page<Post> paginateAdminDatas(int pageNumber, int pageSize, String keywords) {
		return paginateByKeywords("sort_rank","asc",pageNumber, pageSize, keywords, "name,sn,remark");
	}
	
	/**
	 * 获取可用list
	 * @return
	 */
	public List<Post> getEnableList() {
		return getCommonList(Kv.by("enable", TRUE),"sort_rank","asc");
	}
	
  /**
	 * 保存
	 * @param post
	 * @return
	 */
	public Ret save(Post post) {
		if(post==null||isOk(post.getId())||notOk(post.getName())||notOk(post.getSn())||notOk(post.getType())) {
			return fail(Msg.PARAM_ERROR);
		}
		if(existsName(post.getName())) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		if(exists("sn",post.getSn())) {return fail(Msg.DATA_SAME_SN_EXIST);}
		post.setObjectUserId(JBoltUserKit.getUserId());
		post.setSortRank(getNextSortRank());
		post.setEnable(true);
		boolean success=post.save();
		if(success) {
			//添加日志
			addSaveSystemLog(post.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_POST, post.getName());
		}
		return ret(success);
	}
	
  /**
	 * 保存
	 * @param post
	 * @return
	 */
	public Ret update(Post post) {
		if(post==null||notOk(post.getId())||notOk(post.getName())||notOk(post.getSn())||notOk(post.getType())) {
			return fail(Msg.PARAM_ERROR);
		}
		//更新时需要判断数据存在
		Post dbPost=findById(post.getId());
		if(dbPost==null) {return fail(Msg.DATA_NOT_EXIST);}
		if(existsName(post.getName(), post.getId())) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		if(exists("sn",post.getSn(), post.getId())) {return fail(Msg.DATA_SAME_SN_EXIST);}
		boolean success=post.update();
		if(success) {
			//添加日志
			addUpdateSystemLog(post.getId(), JBoltUserKit.getUserId(),SystemLog.TARGETTYPE_POST, post.getName());
		}
		return ret(success);
	}
	
   /**
	 * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		Ret ret=deleteById(id,false);
		if(ret.isOk()){
			//添加日志
			Post post=ret.getAs("data");
			addDeleteSystemLog(id, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_POST, post.getName());
		}
		return ret;
	}

	/**@Override
	public String checkInUse(Post post) {
		//这里用来覆盖 检测Post是否被其它表引用
		return null;
	}*/
	
   /**
	 * 切换禁用启用状态
	 * @param id
	 * @return
	 */
	public Ret toggleEnable(Integer id) {
		Ret ret=toggleBoolean(id, "enable");
		if(ret.isOk()){
			//添加日志
			Post post=ret.getAs("data");
			addUpdateSystemLog(id, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_POST, post.getName(),"的启用状态:"+post.getEnable());
		} 
		return ret;
	}
	
   /**
	 * 上移
	 * @param id
	 * @return
	 */
	public Ret up(Integer id) {
		Post post=findById(id);
		if(post==null){
			return fail("数据不存在或已被删除");
		}
		Integer rank=post.getSortRank();
		if(rank==null||rank<=0){
			return fail("顺序需要初始化");
		}
		if(rank==1){
			return fail("已经是第一个");
		}
		Post upPost=findFirst(Kv.by("sort_rank", rank-1));
		if(upPost==null){
			return fail("顺序需要初始化");
		}
		upPost.setSortRank(rank);
		post.setSortRank(rank-1);
		
		upPost.update();
		post.update();
		return SUCCESS;
	}
	
	/**
	 * 下移
	 * @param id
	 * @return
	 */
	public Ret down(Integer id) {
		Post post=findById(id);
		if(post==null){
			return fail("数据不存在或已被删除");
		}
		Integer rank=post.getSortRank();
		if(rank==null||rank<=0){
			return fail("顺序需要初始化");
		}
		int max=getCount();
		if(rank==max){
			return fail("已经是最后已一个");
		}
		Post upPost=findFirst(Kv.by("sort_rank", rank+1));
		if(upPost==null){
			return fail("顺序需要初始化");
		}
		upPost.setSortRank(rank);
		post.setSortRank(rank+1);
		
		upPost.update();
		post.update();
		return SUCCESS;
	}
	
   /**
	  * 初始化排序
	 */
	public Ret initRank(){
		List<Post> allList=findAll();
		if(allList.size()>0){
			for(int i=0;i<allList.size();i++){
				allList.get(i).setSortRank(i+1);
			}
			batchUpdate(allList);
		}
		//添加日志
		addUpdateSystemLog(null, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_POST, "所有数据", "的顺序:初始化所有");
		return SUCCESS;
	}
	
	
	public List<Record> getEnableOptionsList() {
		return getOptionList("name", "id", Kv.by("enable", TRUE), "sort_rank", "asc");
	}
		
}