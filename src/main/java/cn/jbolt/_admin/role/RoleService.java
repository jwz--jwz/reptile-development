package cn.jbolt._admin.role;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;

import cn.jbolt._admin.rolepermission.RolePermissionService;
import cn.jbolt._admin.systemlog.SystemLogService;
import cn.jbolt._admin.user.UserService;
import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Role;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.util.ArrayUtil;
/**
 * 角色管理Service
 * @ClassName:  RoleService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年3月27日 上午11:54:25   
 */
public class RoleService extends JBoltBaseService<Role> {
	private Role dao = new Role().dao();
	@Inject
	private UserService userService;
	@Inject
	private SystemLogService systeLogService;
	@Inject
	private RolePermissionService rolePermissionService;
	@Override
	protected Role dao() {
		return dao;
	}
	
	/**
	 * 保存role数据
	 * @param user
	 * @return
	 */
	public Ret save(Role role) {
		if(role==null||isOk(role.getId())||notOk(role.getName())){
			return fail(Msg.PARAM_ERROR);
		}
		String name=role.getName().trim();
		String sn=role.getSn();
		if(name.indexOf(" ")!=-1){
			return saveAll(role.getPid(),ArrayUtil.from3(name, " "),ArrayUtil.from3(sn, " "));
		}
		if (existsName(name)) {
			return fail(Msg.DATA_SAME_NAME_EXIST+":["+name+"]");
		}
		if(isOk(sn)) {
			if (exists("sn", sn)) {
				return fail(Msg.DATA_SAME_SN_EXIST+":["+sn+"]");
			}
			role.setSn(sn);
		}
		role.setName(name);
		if(role.getPid()==null) {
			role.setPid(0);
		}
		boolean success=role.save();
		if(success){
			addSaveSystemLog(role.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_ROLE,role.getName()+"["+role.getSn()+"]");
		}
		return ret(success);
	}
	/**
	 * 添加多个
	 * @param pid
	 * @param names
	 * @param sns
	 * @return
	 */
	private Ret saveAll(Integer pid, String[] names, String[] sns) {
		if(names==null) {
			return fail(Msg.PARAM_ERROR+"[名称]");
		}
		if(sns!=null && names.length != sns.length) {
			return fail(Msg.DATA_NAME_SN_COUNT_NOT_EQUALS);
		}
		Ret ret=null;
		Role role;
		int index = 0;
		for(String name:names){
			role = new Role().setName(name).setPid(isOk(pid)?pid:0);
			if(sns!=null) {
				role.setSn(sns[index].trim());
			}
			ret=save(role);
			if(ret.isFail()){
				return ret;
			}
			index++;
		 }
		return SUCCESS;
	}

	
	
	/**
	 * 修改role数据
	 * @param user
	 * @return
	 */
	public Ret update(Role role) {
		if(role==null||notOk(role.getId())||notOk(role.getName())){
			return fail(Msg.PARAM_ERROR);
		}
		String name=role.getName().trim();
		if (existsName(name,role.getId())) {
			return fail(Msg.DATA_SAME_NAME_EXIST+":["+name+"]");
		}
		String sn=role.getSn();
		if(isOk(sn)) {
			if (exists("sn",sn,role.getId())) {
				return fail(Msg.DATA_SAME_SN_EXIST+":["+sn+"]");
			}
			role.setSn(sn);
		}
		role.setName(name);
		boolean success=role.update();
		if(success){
			addUpdateSystemLog(role.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_ROLE,role.getName());
		}
		return ret(success);
	}
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		//调用底层删除
		Ret ret=deleteById(id, true);
		if(ret.isOk()){
			// 删除rolePermissions绑定
			Ret delRet=rolePermissionService.deleteRolePermission(id);
			if(delRet.isFail()) {return delRet;}
			//添加日志
			Role role=ret.getAs("data");
			addDeleteSystemLog(id, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_ROLE,role.getName());
		}
		return ret;
	}
	@Override
	public String checkCanDelete(Role role,Kv kv) {
		return checkInUse(role,kv);
	}
	@Override
	public String checkInUse(Role role,Kv kv) {
		boolean hasChild =checkHasChild(role.getId());
		if(hasChild) {
			return "此角色存在下级角色，不能直接删除";
		}
		boolean inUse=userService.checkRoleInUse(role.getId());
		return inUse?"此角色已被用户分配使用，不能直接删除":null;
	}
	/**
	 * 判断存在下级节点
	 * @param pid
	 * @return
	 */
	private boolean checkHasChild(Integer pid) {
		return exists("pid", pid);
	}

	/**
	 * 得到所有角色是树形数据结构
	 * @return
	 */
	public List<Role> getAllRoleTreeDatas() {
		return convertToModelTree(findAll(), "id", "pid", (m)->notOk(m.getPid()));
	}

}
