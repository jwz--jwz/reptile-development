package cn.jbolt._admin.role;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheck;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt._admin.user.UserService;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Role;
@CheckPermission(PermissionKey.ROLE)
@UnCheckIfSystemAdmin
public class RoleAdminCotroller extends JBoltBaseController {
	@Inject
	private RoleService service;
	@Inject
	private UserService userService;
	/**
	 * 管理首页
	 */
	public void index(){
		render("index.html");
	}
	
	public void datas() {
		renderJsonData(service.getAllRoleTreeDatas());
	}
	
	@UnCheck
	public void options(){
		renderJsonData(service.getAllRoleTreeDatas());
	}
	
	/**
	 * 查询role上所有用户列表进入页面
	 */
	public void users() {
		set("roleId", getInt(0));
		set("isSystemAdmin",JBoltUserKit.isSystemAdmin());
		render("users.html");
	}
	/**
	 * 角色下用户数据查询
	 */
	public void userDatas() {
		renderJsonData(userService.paginateUsersByRoleId(getPageNumber(),getPageSize(),getInt("roleId")));
	}
	
	/**
	 * 新增
	 */
	public void add(){
		render("add.html");
	}
	/**
	 * 新增Item
	 */
	public void addItem(){
		set("pid", getInt(0,0));
		render("add.html");
	}
	/**
	 * 编辑
	 */
	public void edit(){
		Role role=service.findById(getInt(0));
		if(role==null) {
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		set("role", role);
		set("pid", role.getPid());
		render("edit.html");
	}
	/**
	 * 保存
	 */
	public void save(){
		renderJson(service.save(getModel(Role.class, "role")));
	}
	/**
	 * 更新
	 */
	public void update(){
		renderJson(service.update(getModel(Role.class, "role")));
	}
	/**
	 * 删除
	 */
	@Before(Tx.class)
	public void delete(){
		renderJson(service.delete(getInt()));
	}
	
	/**
	 * 清空角色上的用户列表
	 */
	@Before(Tx.class)
	public void clearUsers() {
		renderJson(userService.clearUsersByRole(getInt()));
	}
}
