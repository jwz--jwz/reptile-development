package cn.jbolt._admin.permission;
/**
 * 由PermissionKeyGen生成的 权限定义KEY 
 * 用于在注解里使用
 */
public class PermissionKey {
	/**
	 * 操作台
	 */
	public static final String DASHBOARD = "dashboard";
	/**
	 * 系统管理
	 */
	public static final String SYSTEMMGR = "systemmgr";
	/**
	 * 用户管理
	 */
	public static final String USER = "user";
	/**
	 * 权限配置
	 */
	public static final String ROLE_PERMISSION_MENU = "role_permission_menu";
	/**
	 * 角色管理
	 */
	public static final String ROLE = "role";
	/**
	 * 顶部导航
	 */
	public static final String TOPNAV = "topnav";
	/**
	 * 权限资源
	 */
	public static final String PERMISSION = "permission";
	/**
	 * 字典参数
	 */
	public static final String DICTIONARY_CONFIG = "dictionary_config";
	/**
	 * 数据字典
	 */
	public static final String DICTIONARY = "dictionary";
	/**
	 * 全局参数
	 */
	public static final String GLOBALCONFIG = "globalconfig";
	/**
	 * 系统监控
	 */
	public static final String JBOLT_MONITOR = "jbolt_monitor";
	/**
	 * Druid数据库监控
	 */
	public static final String DRUID_MONITOR = "druid_monitor";
	/**
	 * 服务器监控
	 */
	public static final String JBOLT_SERVER_MONITOR = "jbolt_server_monitor";
	/**
	 * 日志监控
	 */
	public static final String JBOLT_LOG_MONITOR = "jbolt_log_monitor";
	/**
	 * 登录日志
	 */
	public static final String JBOLT_LOGIN_LOG = "jbolt_login_log";
	/**
	 * 关键操作日志
	 */
	public static final String SYSTEMLOG = "systemlog";
	/**
	 * 开发平台
	 */
	public static final String DEV_PLATEFORM = "dev_plateform";
	/**
	 * 应用中心
	 */
	public static final String APPLICATION = "application";
	/**
	 * 微信公众平台
	 */
	public static final String WECHAT_MPINFO = "wechat_mpinfo";
	/**
	 * 基础配置
	 */
	public static final String WECHAT_CONFIG_BASEMGR = "wechat_config_basemgr";
	/**
	 * 菜单配置
	 */
	public static final String WECHAT_MENU = "wechat_menu";
	/**
	 * 支付配置
	 */
	public static final String WECHAT_CONFIG_PAYMGR = "wechat_config_paymgr";
	/**
	 * 关注回复
	 */
	public static final String WECHAT_AUTOREPLY_SUBSCRIBE = "wechat_autoreply_subscribe";
	/**
	 * 关键词回复
	 */
	public static final String WECHAT_AUTOREPLY_KEYWORDS = "wechat_autoreply_keywords";
	/**
	 * 默认回复
	 */
	public static final String WECHAT_AUTOREPLY_DEFAULT = "wechat_autoreply_default";
	/**
	 * 素材库
	 */
	public static final String WECHAT_MEDIA = "wechat_media";
	/**
	 * 用户管理
	 */
	public static final String WECHAT_USER = "wechat_user";
	/**
	 * 其它配置
	 */
	public static final String WECHAT_CONFIG_EXTRAMGR = "wechat_config_extramgr";
	/**
	 * Demo与教程
	 */
	public static final String DEMO = "demo";
	/**
	 * 电商平台
	 */
	public static final String MALL = "mall";
	/**
	 * 商品管理
	 */
	public static final String MALL_GOODS = "mall_goods";
	/**
	 * 商品类目_后端
	 */
	public static final String MALL_GOODS_BACK_CATEGORY = "mall_goods_back_category";
	/**
	 * 商品类目_前端
	 */
	public static final String MALL_GOODS_FRONT_CATEGORY = "mall_goods_front_category";
	/**
	 * 物流配送
	 */
	public static final String MALL_SHIPPING = "mall_shipping";
	/**
	 * 系统规格管理
	 */
	public static final String MALL_SPEC = "mall_spec";
	/**
	 * 商品类型
	 */
	public static final String MALL_GOODSTYPE = "mall_goodstype";
	/**
	 * 品牌库
	 */
	public static final String MALL_BRAND = "mall_brand";
	/**
	 * 部门管理
	 */
	public static final String DEPT = "dept";
	/**
	 * 岗位管理
	 */
	public static final String POST = "post";
	/**
	 * 在线用户
	 */
	public static final String ONLINE_USER = "online_user";
	/**
	 * 登录
	 */
	public static final String ADMIN_LOGIN = "admin_login";
	/**
	 * 登录页1
	 */
	public static final String DEMO_LOGIN1 = "demo_login1";
	/**
	 * 系统通知
	 */
	public static final String SYS_NOTICE = "sys_notice";
	/**
	 * ureport 设计器
	 */
	public static final String UREPORT_DESIGNER = "ureport_designer";
	/**
	 * 独立逻辑权限
	 */
	public static final String LOGIC_PERMISSION = "logic_permission";
	/**
	 * ureport 报表详情
	 */
	public static final String UREPORT_DETAIL = "ureport_detail";
	/**
	 * species
	 */
	public  static final  String PLANT_SPECIES = "PLANT_SPECIES";
	public static  final  String  PLANT_GENUS=  "plant_genus";
	public static  final  String  PLANT_FAMiLIA=  "plant_familia";
}