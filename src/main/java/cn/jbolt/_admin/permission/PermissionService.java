package cn.jbolt._admin.permission;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.kit.StrKit;

import cn.hutool.core.map.MapUtil;
import cn.jbolt._admin.role.RoleService;
import cn.jbolt._admin.rolepermission.RolePermissionService;
import cn.jbolt._admin.systemlog.SystemLogService;
import cn.jbolt._admin.topnav.TopnavMenuService;
import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.model.Permission;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.model.TopnavMenu;
import cn.jbolt.common.util.CACHE;
import cn.jbolt.common.util.ListMap;
/**
 * 系统权限资源Service
 * @author 小木
 *
 */
public class PermissionService extends JBoltBaseService<Permission> {
	private Permission dao = new Permission().dao();
	@Inject
	private RoleService roleService;
	@Inject
	private SystemLogService systeLogService;
	@Inject
	private RolePermissionService rolePermissionService;
	@Inject
	private TopnavMenuService topnavMenuService;
	@Override
	protected Permission dao() {
		return dao;
	}
	
	/**
	 * 得到所有permission 通过级别处理
	 * @return
	 */
	public List<Permission> getAllPermissionsWithLevel() {
		List<Permission> permissions=find(selectSql().orderBy("sort_rank"));
		return convertToModelTree(permissions, "id", "pid", (p)->notOk(p.getPid()));
	}
	/**
	 * 得到 符合条件的数据 通过级别处理
	 * @return
	 */
	public List<Permission> getAdminPermissionsWithLevel(Integer topnavId) {
		Sql sql=selectSql();
		List<Permission> permissions;
		if(isOk(topnavId)) {
			sql.from(table(), "this");
			sql.leftJoin(topnavMenuService.table(), "tm", "tm.permission_id = this.id");
			sql.select("this.*").orderBy("this.sort_rank");
			sql.eq("tm.topnav_id", topnavId);
			List<Permission> temps=find(sql);
			permissions=new ArrayList<Permission>();
			if(isOk(temps)) {
				List<Permission> all=getAllPermissionsWithLevel();
				//这是第一级拿到了
				for(Permission p:temps) {
					all.forEach(ap->{
						if(ap.getPermissionKey().equals(p.getPermissionKey())) {
							permissions.add(ap);
						}
					});
				}
			}
		}else {
			sql.orderBy("sort_rank");
			permissions = find(sql);
		}
		
		return convertToModelTree(permissions, "id", "pid", (p)->notOk(p.getPid()));
	}
	/**
	 * 得到所有permission 通过级别处理 options下拉数据专用
	 * @return
	 */
	public List<Permission> getAllPermissionsOptionsWithLevel() {
		List<Permission> permissions=find(selectSql().select("id,title,pid,permission_level").orderBySortRank());
		//处理分级
		List<Permission> parents=new ArrayList<Permission>();
		processParentPermission(permissions,parents);
		if(parents.size()>0&&permissions.size()>0) {
			processPermissionItems(permissions,parents);
		}
		return parents;
	}
	 
	/**
	 * 保存数据
	 * @param permission
	 * @return
	 */
	public Ret save(Permission permission) {
		if(permission==null||isOk(permission.getId())){
			return fail(Msg.PARAM_ERROR);
		}
		if(notOk(permission.getPermissionKey())){
			return fail("请设置权限KEY");
		}
		
		if (existsTitleWithSameParent(-1,permission.getTitle(),permission.getPid())) {
			return fail("同一父节点下资源【"+permission.getTitle()+"】已经存在，请更换");
		}
		if (exists("permission_key",permission.getPermissionKey())) {
			return fail("资源权限KEY【"+permission.getPermissionKey()+"】已经存在，请更换");
		}
		if(notOk(permission.getPid())){
			permission.setPid(0);
		}
		
		permission.setTitle(permission.getTitle().trim());
		permission.setSortRank(getNextSortRankByPid(permission.getPid()));
		processPermissionNewLevel(permission);
		boolean success=permission.save();
		if(success){
			CACHE.me.removeMenusAndPermissionsByRoleGroups();
			//添加日志
			addSaveSystemLog(permission.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PERMISSION, permission.getTitle());
		}
		return ret(success);
	}
	/**
	 * 检测同一个pid下的title是否存在重复数据
	 * @param title
	 * @param pid
	 * @return
	 */
	private boolean existsTitleWithSameParent(Integer id,String title,Integer pid) {
		Sql sql=selectSql().selectId().eqQM("title","pid").idNoteqQM().first();
		Integer existId = queryInt(sql, title.trim(),pid,id);
		return isOk(existId);
	}
	
	/**
	 * 修改数据
	 * @param permission
	 * @return
	 */
	public Ret update(Permission permission) {
		if(permission==null||notOk(permission.getId()) || permission.getPid()==null ||notOk(permission.getTitle())||notOk(permission.getPermissionLevel())){
			return fail(Msg.PARAM_ERROR);
		}
		if(isOk(permission.getPid())&&permission.getPid().intValue()==permission.getId().intValue()) {
			return fail("上级权限不能是自身，请换一个");
		}
		Permission dbPermission=findById(permission.getId());
		//如果数据库不存在
		if(dbPermission==null) {
			return fail(Msg.DATA_NOT_EXIST);
		}
		if (existsTitleWithSameParent(permission.getId(),permission.getTitle(),permission.getPid())) {
			return fail("同一父节点下资源【"+permission.getTitle()+"】已经存在，请输入其它名称");
		}
		if (exists("permission_key",permission.getPermissionKey(),permission.getId())) {
			return fail("资源权限KEY【"+permission.getPermissionKey()+"】已经存在，请更换");
		}
		permission.setTitle(permission.getTitle().trim());
		//其他地方调用传过来的一个sort请求 指明是排序后调用update
		Boolean sort=permission.getBoolean("sort");
		if(sort==null){
			processPermissionNewLevel(permission);
		}
		if(dbPermission.getPid().intValue()!=permission.getPid().intValue()) {
			permission.setSortRank(getNextSortRankByPid(permission.getPid()));
		}
		boolean success=permission.update();
		if(success){
			if(dbPermission.getPid().intValue()!=permission.getPid().intValue()) {
				updateSortRankAfterChangeParentNode(Kv.by("pid", dbPermission.getPid()),dbPermission.getSortRank());
				//说明数据的父节点变更了 level相应的跟着变化了
				processSonItemLevel(permission.getId(),permission.getPermissionLevel());
			}
			//需要处理菜单缓存
			CACHE.me.removeMenusAndPermissionsByRoleGroups();
			//如果是排序后update 日志会多加一部分说明
			if(sort!=null){
				addUpdateSystemLog(permission.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PERMISSION, permission.getTitle(),"的顺序");
			}else{
				addUpdateSystemLog(permission.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PERMISSION, permission.getTitle());
			}
			
		}
		return ret(success);
	}
	
	 
	private void processSonItemLevel(Integer id, int level) {
		List<Permission> permissions=getSons(id);
		if(isOk(permissions)) {
			permissions.forEach(p->{
				p.setPermissionLevel(level+1);
				processSonItemLevel(p.getId(), p.getPermissionLevel());
			});
			batchUpdate(permissions);
		}
		
	}
	private void processPermissionNewLevel(Permission permission) {
		if(notOk(permission.getPid())){
			permission.setPermissionLevel(Permission.LEVEL_1);
		}else{
			int level=getParentLevel(permission.getPid());
			permission.setPermissionLevel(level+1);
		}
	}
	private int getParentLevel(Integer pid) {
		Permission permission=findById(pid);
		return permission.getPermissionLevel();
	}
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public Ret delPermissionById(Integer id,boolean processRoleCache) {
		Ret ret=deleteById(id);
		if(ret.isOk()) {
			Permission permission=ret.getAs("data");
			//删除后需要把此数据之后的数据更新顺序
			updateSortRankAfterDelete(Kv.by("pid",permission.getPid()),permission.getSortRank());
			//删除子节点
			deleteByPid(id);
			//根据被删除的permission去删掉给role上的数据
			Ret delret=rolePermissionService.deleteByPermission(permission.getId());
			if(delret.isFail()) {return delret;}
		
			if(processRoleCache){
				CACHE.me.removeMenusAndPermissionsByRoleGroups();
			}
			//添加日志
			addDeleteSystemLog(permission.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PERMISSION, permission.getTitle());
		}	
		return ret;
	}
	 /**
	  * 删除子节点
	  * @param pid
	  */
	private void deleteByPid(Integer pid) {
		List<Permission> permissions=getSons(pid);
		for(Permission permission:permissions){
			delPermissionById(permission.getId(),false);
		}
	}
	 
	/**
	 * 上移
	 * @param id
	 * @return
	 */
	public Ret up(Integer id) {
		Permission permission=findById(id);
		if(permission==null){
			return fail("数据不存在或已被删除");
		}
		Integer rank=permission.getSortRank();
		if(rank==null||rank<=0){
			return fail("顺序需要初始化");
		}
		if(rank==1){
			return fail("已经是第一个");
		}
		Permission upPermission=findFirst(Kv.by("sort_rank", rank-1).set("pid", permission.getPid()));
		if(upPermission==null){
			return fail("顺序需要初始化");
		}
		upPermission.setSortRank(rank);
		permission.setSortRank(rank-1);
		upPermission.put("sort", true);
		permission.put("sort", true);
		update(upPermission);
		update(permission);
		return SUCCESS;
	}
 
	
	
	/**
	 * 下移
	 * @param id
	 * @return
	 */
	public Ret down(Integer id) {
		Permission permission=findById(id);
		if(permission==null){
			return fail("数据不存在或已被删除");
		}
		Integer rank=permission.getSortRank();
		if(rank==null||rank<=0){
			return fail("顺序需要初始化");
		}
		int max=getCount(Kv.by("pid",permission.getPid()));
		if(rank==max){
			return fail("已经是最后已一个");
		}
		Permission upPermissions=findFirst(Kv.by("sort_rank", rank+1).set("pid", permission.getPid()));
		if(upPermissions==null){
			return fail("顺序需要初始化");
		}
		upPermissions.setSortRank(rank);
		permission.setSortRank(rank+1);
		upPermissions.put("sort", true);
		permission.put("sort", true);
		update(upPermissions);
		update(permission);
		return SUCCESS;
	}
	
	private void doInitOneRank(List<Permission> permissions) {
		if(notOk(permissions)){
			return;
		}
		for(int i=0;i<permissions.size();i++){
			permissions.get(i).setSortRank(i+1);
		}
		batchUpdate(permissions);
		for(Permission f:permissions){
//			deleteCacheById(f.getId());
//			deleteCacheByKey(f.getPermissionKey());
			//继续向下递归
			List<Permission> items=getSonsOrderById(f.getId());
			doInitOneRank(items);
			
		}
	
	}
	
	/**
	 * 初始化排序
	 */
	public Ret initRank(){
		List<Permission> parents=getAllParentsOrderById();
		if(parents.size()>0){
			doInitOneRank(parents);
		}
		CACHE.me.removeMenusAndPermissionsByRoleGroups();
		//添加日志
		addUpdateSystemLog(null, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PERMISSION, "全部","初始化顺序");
		return SUCCESS;
		
	}
	/**
	 * 通过权限资源的KEY获取数据
	 * @param permissionKey
	 * @return
	 */
	public Permission getByPermissionkey(String permissionKey) {
		if(notOk(permissionKey)) {return null;}
		return findFirst(Kv.by("permission_key", permissionKey));
	}
	
	public List<Permission> getParentPermissionsWithLevel(Integer roleId) {
		List<Permission> permissions=getPermissionsByRole(roleId);
		//处理分级
		List<Permission> parents=new ArrayList<Permission>();
		processParentPermission(permissions,parents);
		if(parents.size()>0&&permissions.size()>0) {
			processPermissionItems(permissions,parents);
		}
		return parents;
	}
	private void processPermissionItems(List<Permission> permissions, List<Permission> parents) {
		ListMap<String, Permission> map=new ListMap<String, Permission>();
		for(Permission p:permissions) {
			map.addItem("p_"+p.getPid(), p);
		}
		for(Permission p:parents) {
			processSubItems(map,p);
		}
		
	}
	private void processSubItems(ListMap<String, Permission> map, Permission permission) {
		List<Permission> items=map.get("p_"+permission.getId());
		if(items!=null&&items.size()>0) {
			for(Permission item:items) {
				processSubItems(map, item);
			}
		}
		permission.putItems(items);
	}
	private void processParentPermission(List<Permission> permissions, List<Permission> parents) {
		Permission permission;
		for(int i=0;i<permissions.size();i++) {
			permission=permissions.get(i);
			if(permission.getPermissionLevel()==Permission.LEVEL_1&&notOk(permission.getPid())) {
				parents.add(permission);
				permissions.remove(i);
				i--;
			}
		}
	}
	/**
	 * 根据角色ID获取到绑定的permission
	 * @param roleId
	 * @return
	 */
	public List<Permission> getPermissionsByRole(Integer roleId) {
		return find(selectSql().select("p.*").from(rolePermissionService.table(),"rp").leftJoin(table(),"p","p.id=rp.permission_id").eqQM("rp.role_id").orderBy("p.sort_rank"),roleId);
	}
	
	/**
	 * 根据角色IDs获取到绑定的permission isMenu
	 * @param roleId
	 * @return
	 */
	public List<Permission> getIsMenuPermissionsByRoles(String roleIds) {
		return find(selectSql().distinct("p.*").from(rolePermissionService.table(),"rp").leftJoin(table(),"p","p.id=rp.permission_id").eq("p.is_menu",TRUE).in("rp.role_id",roleIds).orderBy("p.sort_rank"));
	}
	/**
	 * 切换是否为超管默认权限
	 * @param self
	 * @return
	 */
	public Ret toggleSystemAdminDefault(Integer id) {
		Ret ret=toggleBoolean(id, "is_system_admin_default");
		if(ret.isOk()) {
			Permission selfPermission=ret.getAs("data");
			if(selfPermission.getIsSystemAdminDefault()) {
				processParentPermissionTrue(selfPermission.getPid());
				processSonPermissionTrue(selfPermission);
			}else {
				processSonPermissionFalse(selfPermission);
			}
			CACHE.me.removeMenusAndPermissionsByRoleGroups();
			return successWithData(selfPermission.getIsSystemAdminDefault());
		}
		return ret;
	}
	/**
	 * 处理下级全部true
	 * @param permission
	 */
	private void processSonPermissionTrue(Permission permission) {
		List<Permission> sons=getSons(permission.getId());
		if(sons==null||sons.size()==0) {return;}
		boolean success=false;
		for(Permission son:sons) {
			son.setIsSystemAdminDefault(true);
			success=son.update();
			if(success) {
				processSonPermissionTrue(son);
			}
		}
	}
	/**
	 * 处理下级全部false
	 * @param permission
	 */
	private void processSonPermissionFalse(Permission permission) {
		List<Permission> sons=getSons(permission.getId());
		if(sons==null||sons.size()==0) {return;}
		boolean success=false;
		for(Permission son:sons) {
			son.setIsSystemAdminDefault(false);
			success=son.update();
			if(success) {
				processSonPermissionFalse(son);
			}
		}
	}
	/**
	 * 处理多级上级全部true
	 * @param pid
	 */
	private void processParentPermissionTrue(Integer pid) {
		Permission permission=findById(pid);
		if(permission==null||permission.getIsSystemAdminDefault()) {return;}
		permission.setIsSystemAdminDefault(true);
		boolean success=permission.update();
		if(success) {
			if(permission.getPermissionLevel()==Permission.LEVEL_1) {return;}
			//继续找关联
			processParentPermissionTrue(permission.getPid());
		}
			
		
	}
	public List<Permission> getIsMenuSystemAdminDefaultPermissions() {
		return getCommonList(Kv.by("is_system_admin_default", TRUE).set("is_menu", TRUE),"sort_rank");
	}
	
	public List<Permission> getIsMenuSystemAdminDefaultListWithLevel() {
			List<Permission> permissions=getIsMenuSystemAdminDefaultPermissions();
			//处理分级
			List<Permission> parents=new ArrayList<Permission>();
			processParentPermission(permissions,parents);
			if(parents.size()>0&&permissions.size()>0) {
				processPermissionItems(permissions,parents);
			}
			return parents;
	}
	/**
	 * 得到后台登录后显示的菜单
	 * @param roleIds
	 * @return
	 */
	public List<Permission> getMenusByRoles(String roleIds) {
		List<Permission> permissions=getIsMenuPermissionsByRoles(roleIds);
		//处理分级
		List<Permission> parents=new ArrayList<Permission>();
		processParentPermission(permissions,parents);
		if(parents.size()>0&&permissions.size()>0) {
			processPermissionItems(permissions,parents);
		}
		return parents;
	}
	/**
	 * 得到后台登录后显示的菜单
	 * @param roleIds
	 * @return
	 */
	public List<Permission> getMenusByRolesWithSystemAdminDefault(String roleIds) {
		if(StrKit.isBlank(roleIds)) {
			return getIsMenuSystemAdminDefaultListWithLevel();
		}
		List<Permission> permissions=getIsMenuPermissionsByRoles(roleIds);
		if(notOk(permissions)) {
			return getIsMenuSystemAdminDefaultListWithLevel();
		}
		List<Permission> adminDefaultPermissions=getIsMenuSystemAdminDefaultPermissions();
		if(isOk(adminDefaultPermissions)) {
			processMergePermissions(permissions,adminDefaultPermissions);
		}
		//处理分级
		List<Permission> parents=new ArrayList<Permission>();
		processParentPermission(permissions,parents);
		if(parents.size()>0&&permissions.size()>0) {
			processPermissionItems(permissions,parents);
		}
		return parents;
	}
	/**
	 * 合并去重
	 * @param permissions
	 * @param adminDefaultPermissions
	 */
	private void processMergePermissions(List<Permission> permissions, List<Permission> adminDefaultPermissions) {
		Set<String> set=new HashSet<String>();
		for(Permission permission:permissions) {
			set.add("p_"+permission.getId());
		}
		for(Permission permission:adminDefaultPermissions) {
			if(set.add("p_"+permission.getId())) {
				permissions.add(permission);
			}
		}
		
		permissions.sort(new Comparator<Permission>() {
			@Override
			public int compare(Permission o1, Permission o2) {
				return o1.getSortRank()-o2.getSortRank();
			}
		});
		
	}
	/**
	 * 获取一级菜单 带着关联topnavmenu
	 * @return
	 */
	public List<Permission> getLevel1MenusJoinTopnavMenu() {
		List<Permission> permissions=getAllParents();
		List<TopnavMenu> menus=topnavMenuService.findAll();
		Map<String, Object> map=MapUtil.newHashMap(menus.size()*2);
		for(TopnavMenu menu:menus) {
			map.put("mid_"+menu.getPermissionId(),menu.getTopnavId());
			map.put("mname_"+menu.getPermissionId(),CACHE.me.getTopMenuName(menu.getTopnavId()));
		}
		for(Permission p:permissions) {
			p.put("topnavId",map.get("mid_"+p.getId()));
			p.put("topnavName",map.get("mname_"+p.getId()));
		}
		return permissions;
	}
}