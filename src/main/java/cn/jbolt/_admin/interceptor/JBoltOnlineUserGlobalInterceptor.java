package cn.jbolt._admin.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.base.JBoltConst;
import cn.jbolt.base.JBoltControllerKit;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.base.JBoltUserOnlineState;
import cn.jbolt.base.enumutil.JBoltEnum;
import cn.jbolt.common.model.OnlineUser;

public class JBoltOnlineUserGlobalInterceptor implements Interceptor {

	@Override
	public void intercept(Invocation inv) {
		//不是JBoltBaseController 不管他
		Controller controller = inv.getController();
		if (!(controller instanceof JBoltBaseController)) {
			inv.invoke();
			return;
		}
		//如果拿不到OnlineUser  直接过
		OnlineUser onlineUser = JBoltUserKit.getOnlineUser();
		if(onlineUser == null) {
			inv.invoke();
			return;
		}
		//拿到了就开始判断用户当前状态  正常就判断锁屏
		if(onlineUser.isOnlineOk()) {

			//判断没有锁屏 直接过
			if(!JBoltUserKit.userScreenIsLocked()) {
				inv.invoke();
				return;
			}
			//判断访问解锁直接过
			if(JBoltConst.UNLOCKSYSTEM_ACTION_URL.equals(inv.getActionKey())) {
				inv.invoke();
				return;
			}
			
			//直接访问action
			if(JBoltControllerKit.isNormal(controller) || JBoltControllerKit.isIframe(controller) || JBoltControllerKit.isDialog(controller)) {
				JBoltControllerKit.renderSystemLockedPage(controller);
			}else {
				//ajax pjax ajaxPortal等访问
				JBoltControllerKit.renderAdminSystemLockedInfo(controller);
			}
			return;
		}
		
		int state = onlineUser.getOnlineState();
		JBoltUserOnlineState stateEnum = JBoltEnum.getEnumObjectByValue(JBoltUserOnlineState.class, state);
		switch (stateEnum) {
		case OFFLINE://正常离线
			JBoltControllerKit.renderAdminInterceptorNotLoginInfo(controller);
			break;
		case TERMINAL_OFFLINE://被顶
			JBoltControllerKit.renderAdminInterceptorTerminalOfflineInfo(controller);
			break;
		case FORCED_OFFLINE://强退
			JBoltControllerKit.renderAdminInterceptorForcedOfflineInfo(controller);
			break;
		default:
			JBoltControllerKit.renderAdminInterceptorNotLoginInfo(controller);
			break;
		}
		
	}
}
