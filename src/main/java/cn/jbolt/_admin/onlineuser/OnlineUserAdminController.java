package cn.jbolt._admin.onlineuser;

import com.jfinal.aop.Inject;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.base.JBoltUserOnlineState;
import cn.jbolt.base.enumutil.JBoltEnum;

/**
 * 在线用户管理 Controller
 * @ClassName: OnlineUserAdminController   
 * @author: JBolt-Generator
 * @date: 2021-03-18 23:00  
 */
@CheckPermission(PermissionKey.ONLINE_USER)
@UnCheckIfSystemAdmin
public class OnlineUserAdminController extends JBoltBaseController {

	@Inject
	private OnlineUserService service;
	
   /**
	* 首页
	*/
	public void index() {
		render("index.html");
	}
  	
  	/**
	* 数据源
	*/
	public void datas() {
		renderJsonData(service.paginateAdminDatas(getPageNumber(),getPageSize(),getKeywords(),getState()));
	}
	/**
	 * 状态选项
	 */
	public void stateOptions() {
		renderJsonData(JBoltEnum.getEnumOptionList(JBoltUserOnlineState.class));
	}
	
	/**
	 * 强退用户
	 */
	public void forcedOffline() {
		renderJson(service.forcedOffline(getInt(0)));
	}
	
	
}