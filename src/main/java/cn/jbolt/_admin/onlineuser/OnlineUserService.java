package cn.jbolt._admin.onlineuser;

import java.util.Date;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

import cn.hutool.core.util.IdUtil;
import cn.jbolt._admin.user.UserService;
import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltSnowflakeKit;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.base.JBoltUserOnlineState;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.model.LoginLog;
import cn.jbolt.common.model.OnlineUser;
import cn.jbolt.common.model.User;
import cn.jbolt.common.util.CACHE;
import cn.jbolt.common.util.DateUtil;
/**
 * 在线用户
 * @ClassName:  OnlineUserService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月23日   
 *    
 * 注意：本内容仅限于JFinal学院 JBolt平台VIP成员内部传阅，请尊重开发者劳动成果，不要外泄出去用于其它商业目的
 */
public class OnlineUserService extends JBoltBaseService<OnlineUser> {
	private OnlineUser dao = new OnlineUser().dao();
	@Inject
	private UserService userService;
	@Override
	protected OnlineUser dao() {
		return dao;
	}
	/**
	 * 获取后台管理数据
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param state 
	 * @return
	 */
	public Page<OnlineUser> paginateAdminDatas(int pageNumber,int pageSize,String keywords, Integer state){
		Sql sql =selectSql().page(pageNumber, pageSize);
		if(isOk(keywords)) {
			sql.from(table(),"ou");
			sql.leftJoin(userService.table(), "jbu", "jbu.id=ou.user_id");
			sql.likeMulti(keywords, "jbu.name","jbu.username","jbu.phone","jbu.pinyin","jbu.login_ip");
			sql.eqIfOk("ou.online_state", state);
			sql.orderBy("ou.update_time",true);
		}else {
			sql.eqIfOk("online_state", state);
			sql.orderBy("update_time",true);
		}
		return paginate(sql);
	}
	
	/**
	 * 处理登录用户的onlineUser
	 * @param keepLogin
	 * @param user
	 * @param log
	 * @return
	 */
	public Kv processUserLogin(boolean keepLogin,User user,LoginLog log) {
		//获取配置 保持登录的时长
		int configKeepLoginSeconds = CACHE.me.getGlobalConfigKeepLoginSeconds();
		//获取配置 不保持登录的默认时长
		int configNotKeepLoginSeconds = CACHE.me.getGlobalConfigNotKeepLoginSeconds();
		int keepLoginSeconds = keepLogin?configKeepLoginSeconds:configNotKeepLoginSeconds;
		//获取系统配置是否允许多端同时登录一个账号
		boolean isLoginTerminalOnlyOne = CACHE.me.isLoginTerminalOnlyOne();
		OnlineUser onlineUser = null;
		if(isLoginTerminalOnlyOne) {
			//如果设置了多端不能同时使用一个账号登录的话 就需要把现在登录的下线掉
			processAllSameUserOffline(user.getId());
		}
		//创建一个新会话用户
		onlineUser=saveOnlineUser(user,log,keepLoginSeconds);
		if(onlineUser==null) {
			return null;
		}
		return Kv.by("sessionId", onlineUser.getSessionId()).set("keepLoginSeconds",keepLoginSeconds);
	}
	/**
	 * 异端顶替下线处理
	 * @param userId
	 */
	private void processAllSameUserOffline(Integer userId) {
		Date now = new Date();
		Sql sql= selectSql().eqQM("user_id", "online_state");
		dao.each(ou->{
			ou.setUpdateTime(now);
			ou.setOnlineState(JBoltUserOnlineState.TERMINAL_OFFLINE.getValue());
			ou.setOfflineTime(now);
			ou.update();
			return true;
		}, sql.toSql(), userId,JBoltUserOnlineState.ONLINE.getValue());
	}

	/**
	 * 当前用户自行下线
	 */
	public void currentUserLogout() {
		OnlineUser onlineUser=JBoltUserKit.getOnlineUser();
		if(onlineUser!=null) {
			onlineUser.setOnlineState(JBoltUserOnlineState.OFFLINE.getValue());
			Date nowDate = new Date();
			onlineUser.setOfflineTime(nowDate);
			onlineUser.setExpirationTime(nowDate);
			onlineUser.update();
		}
	}
	
	/**
	 * 生成一个sessionId
	 * @return
	 */
	private String genNewSessionId() {
		return IdUtil.fastSimpleUUID()+JBoltSnowflakeKit.me.nextIdStr();
	}
	
	/**
	 * 保存onlineUser信息
	 * @param user
	 * @param log
	 * @param keepLoginSeconds
	 * @return
	 */
	private OnlineUser saveOnlineUser(User user, LoginLog log,int keepLoginSeconds) {
		OnlineUser onlineUser=new OnlineUser();
		onlineUser.setSessionId(genNewSessionId());
		onlineUser.setUserId(user.getId());
		onlineUser.setLoginLogId(log.getId());
		onlineUser.setLoginTime(user.getLoginTime());
		onlineUser.setScreenLocked(false);
		onlineUser.setOnlineState(JBoltUserOnlineState.ONLINE.getValue());
		onlineUser.setExpirationTime(DateUtil.getSecondsAfter(log.getLoginTime(), keepLoginSeconds));
		boolean success=onlineUser.save();
		return success?onlineUser:null;
	}
	/**
	 * 通过UserId获取 保证一个user 只有一个onlineUser
	 * @param userId
	 * @return
	 */
	public OnlineUser getByUserId(Object userId) {
		if(notOk(userId)) {return null;}
		return findFirst(Kv.by("user_id", userId));
	}
	
	/**
	 * 操作当前用户锁屏
	 * @return
	 */
	public Ret lockCurrentUserScreen() {
		OnlineUser onlineUser = JBoltUserKit.getOnlineUser();
		onlineUser.setScreenLocked(true);
		boolean success=onlineUser.update();
		return ret(success);
	}
	
	/**
	 * 操作当前用户解锁
	 * @return
	 */
	public Ret unlockCurrentUserScreen() {
		OnlineUser onlineUser = JBoltUserKit.getOnlineUser();
		onlineUser.setScreenLocked(false);
		boolean success=onlineUser.update();
		return ret(success);
	}
	/**
	 * 强退用户
	 * @param onlineUserId
	 * @return
	 */
	public Ret forcedOffline(Integer onlineUserId) {
		if(!JBoltUserKit.isSystemAdmin()) {
			return fail("只有超管才有强退权限");
		}
 		if(notOk(onlineUserId)) {return fail(Msg.PARAM_ERROR);}
 		Integer sessionUserId = JBoltUserKit.getUserIdAs();
 		if(notOk(sessionUserId)) {
 			return fail("当前操作用户信息异常");
 		}
 		String currentUserSessionId = JBoltUserKit.getUserSessionId();
 		if(notOk(currentUserSessionId)) {
 			return fail("当前操作用户信息异常");
 		}
		OnlineUser onlineUser = findById(onlineUserId);
		if(onlineUser==null) {
			return fail(Msg.DATA_NOT_EXIST);
		}
		User user = userService.findById(onlineUser.getUserId());
		if(user == null) {
			return fail("异常，被强退用户信息未找到");
		}
		if(currentUserSessionId.equals(onlineUser.getSessionId())) {
			//如果当前用户和被操作强退是一个人 也就是自己强退自己 是不可以的
			return fail("自己不能强退自己");
		}
	
		if(user.getIsSystemAdmin()) {
			return fail("无法强退超管");
		}
		
		onlineUser.setOnlineState(JBoltUserOnlineState.FORCED_OFFLINE.getValue());
		Date nowDate = new Date();
		onlineUser.setOfflineTime(nowDate);
		boolean success=onlineUser.update();
		return ret(success);
	}
	/**
	 * 清理离线用户信息
	 */
	public void deleteOfflineAndExpirationUser() {
		dao.each(ou->{
			ou.delete();
			return true;
		}, selectSql().noteqQM("online_state").toSql(),JBoltUserOnlineState.ONLINE.getValue());
		
		dao.each(ou->{
			ou.delete();
			return true;
		}, selectSql().eqQM("online_state").le("expiration_time", toDateTime(DateUtil.format(new Date(), DateUtil.YMDHMS))).toSql(),JBoltUserOnlineState.ONLINE.getValue());
	}

}
