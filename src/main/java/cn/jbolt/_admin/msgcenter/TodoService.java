package cn.jbolt._admin.msgcenter;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.bean.JBoltDateRange;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.model.Todo;
import cn.jbolt.common.util.ArrayUtil;

/**
 * 待办事项管理 Service
 * 
 * @ClassName: TodoService
 * @author: JBolt-Generator
 * @date: 2021-04-17 17:43
 */
public class TodoService extends JBoltBaseService<Todo> {
	private Todo dao = new Todo().dao();

	@Override
	protected Todo dao() {
		return dao;
	}

	/**
	 * 保存
	 * 
	 * @param todo
	 * @return
	 */
	public Ret save(Todo todo) {
		if (todo == null || isOk(todo.getId()) || notOk(todo.getState()) || notOk(todo.getType())) {
			return fail(Msg.PARAM_ERROR);
		}
		int type=todo.getType().intValue();
		switch (type) {
		case 1://无url 无content 只有title
			todo.remove("url","content");
			break;
		case 2://无url 有content 有title
			if(notOk(todo.getContent())) {
				return fail(Msg.PARAM_ERROR+" 如选择【无链接有内容】类型，需要填充内容");
			}
			todo.remove("url");
			break;
		case 3://有url 无content 有title
			if(notOk(todo.getUrl())) {
				return fail(Msg.PARAM_ERROR+" 如选择【有链接无内容】类型，需要设置链接");
			}
			todo.remove("content");
			break;
		case 4:
			if(notOk(todo.getUrl())) {
				return fail(Msg.PARAM_ERROR+" 如选择【有链接有内容】类型，需要设置链接");
			}
			if(notOk(todo.getContent())) {
				return fail(Msg.PARAM_ERROR+" 如选择【有链接有内容】类型，需要填充内容");
			}
			break;
		}
		Integer userId = JBoltUserKit.getUserIdAs();
		todo.setCreateUserId(userId);
		todo.setUpdateUserId(userId);
		todo.setUserId(userId);
		boolean success = todo.save();
		return ret(success);
	}

	/**
	 * 更新
	 * 
	 * @param todo
	 * @return
	 */
	public Ret update(Todo todo) {
		if (todo == null || notOk(todo.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		// 更新时需要判断数据存在
		Todo dbTodo = findById(todo.getId());
		if (dbTodo == null) {
			return fail(Msg.DATA_NOT_EXIST);
		}
		boolean success = todo.update();
		return ret(success);
	}

	/**
	 * 更新状态
	 * @param todo
	 * @return
	 */
	public Ret updateState(Integer todoId, Integer state) {
		if (notOk(todoId) || notOk(state)) {
			return fail(Msg.PARAM_ERROR);
		}
		// 更新时需要判断数据存在
		Todo todo = findById(todoId);
		if (todo == null) {
			return fail(Msg.DATA_NOT_EXIST);
		}
		Integer userId = JBoltUserKit.getUserIdAs();
		if(userId.intValue() != todo.getUserId()) {
			return fail(Msg.NOPERMISSION+",只能修改属于自己的待办事项");
		}
		todo.setState(state);
		todo.setUpdateUserId(userId);
		boolean success = todo.update();
		return ret(success);
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		return deleteById(id, false);
	}

	/**
	 * 读取判断是否存在需要用户处理的数据
	 * @param userId
	 * @return
	 */
	public boolean existNeedProcess(Integer userId) {
		Sql sql = selectSql().eq("user_id", userId).bracketLeft().eq("state", 1).or().eq("state", 2).bracketRight();
		return exist(sql);
	}
	
	/**
	 * 获取待办 在 右上角消息中心layer中的未完成待办项
	 * @return
	 */
	public List<Todo> getMsgCenterPortalDatas() {
		Page<Todo> pageData = paginateUserTodos(1, 10, JBoltUserKit.getUserIdAs(), "id", "desc",true,new Integer[] {1,2},true,"id","title","priority_level","state","specified_finish_time","create_time","update_time");
		return pageData.getList();
	}
	/**
	 * 查询用户待办数据
	 * @param pageNumber
	 * @param pageSize
	 * @param userId
	 * @param sortColumn
	 * @param sortType
	 * @param inState
	 * @param states
	 * @param columnWithOrWithout
	 * @param columns
	 * @return
	 */
	public Page<Todo> paginateUserTodos(int pageNumber, int pageSize, Integer userId, String sortColumn, String sortType,Boolean inState,Integer[] states,Boolean columnWithOrWithout,String... columns) {
		Sql sql = selectSql().page(pageNumber, pageSize);
		sql.eq("user_id", userId);
		sql.orderBy(sortColumn, sortType);
		if(inState!=null && isOk(states)) {
			if(inState) {
				sql.in("state", states);
			}else {
				sql.notIn("state", states);
			}
		}
		if(columnWithOrWithout!=null && isOk(columns)) {
			if(columnWithOrWithout) {
				sql.select(columns);
			}else {
				sql.select(getTableSelectColumnsWithout(columns));
			}
		}
		return paginate(sql);
	}
	/**
	 *  底层查询方法 分页查询
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param userId
	 * @param sortColumn
	 * @param sortType
	 * @param type
	 * @param state
	 * @param dateTimeColumn
	 * @param dateRange
	 * @param columnWithOrWithout
	 * @param columns
	 * @return
	 */
	public Page<Todo> paginateUserTodos(Integer pageNumber, Integer pageSize, String keywords, Integer userId,
			String sortColumn, String sortType, Integer type, Integer state, String dateTimeColumn,JBoltDateRange dateRange,Boolean columnWithOrWithout, String... columns) {
		if(isOk(dateTimeColumn) && !dao().hasColumn(dateTimeColumn)) {
			throw new RuntimeException("数据中没有这一列:"+dateTimeColumn);
		}
		Sql sql = selectSql().page(pageNumber, pageSize);
		if(isOk(keywords)) {
			sql.likeMulti(keywords, "title","content");
		}
		if(isOk(type)) {
			sql.eq("type", type);
		}
		if(isOk(state)) {
			sql.eq("state", state);
		}
		if(isOk(sortColumn)) {
			if(isOk(sortType)){
				sql.orderBy(sortColumn, sortType);
			}else {
				sql.orderBy(sortColumn);
			}
		}
		if(columnWithOrWithout != null) {
			if(columnWithOrWithout) {
				sql.select(columns);
			}else {
				sql.select(getTableSelectColumnsWithout(columns));
			}
		}
		
		if(isOk(dateTimeColumn) && dateRange!=null) {
			Date startDate = dateRange.getStartDate();
			if(isOk(startDate)) {
				sql.ge(dateTimeColumn, toStartTime(startDate));
			}
			Date endDate = dateRange.getEndDate();
			if(isOk(endDate)) {
				sql.le(dateTimeColumn, toEndTime(endDate));
			}
		}
		return paginate(sql);
	}
	
	/**
	 * 批量删除 指定用户的todos
	 * @param ids
	 * @return
	 */
	public Ret deleteMyTodoByBatchIds(String ids) {
		if(notOk(ids)) {return fail(Msg.PARAM_ERROR);}
		Integer userId = JBoltUserKit.getUserIdAs();
		if(notOk(userId)) {
			return fail(Msg.NOPERMISSION);
		}
		delete(deleteSql().eq("user_id", userId).in("id", ids));
		return SUCCESS;
	}
	
	/**
	 * 批量修改状态
	 * @param state
	 * @param ids
	 * @return
	 */
	public Ret batchUpdateState(Integer state,String ids) {
		if(notOk(state) || notOk(ids)) {return fail(Msg.PARAM_ERROR);}
		Integer[] idArray = ArrayUtil.toDisInt(ids, ",");
		if(notOk(idArray)) {return fail(Msg.PARAM_ERROR);}
		boolean success = tx(new IAtom() {
			@Override
			public boolean run() throws SQLException {
				Integer userId = JBoltUserKit.getUserIdAs();
				List<Todo> todos = find(selectSql().eq("user_id", userId).in("id", idArray));
				if(todos.size()==0) {
					return true;
				}
				for(Todo todo:todos) {
					todo.setState(state);
					todo.setUpdateUserId(userId);
				}
				try {
					batchUpdate(todos);
					return true;
				} catch (Exception e) {
					LOG.error("批量更新todo状态失败:"+e.getMessage());
					throw new RuntimeException("批量更新todo状态失败");
				}
			}
		});
		return ret(success);
	}

}