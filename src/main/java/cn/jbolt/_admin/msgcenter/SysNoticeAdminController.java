package cn.jbolt._admin.msgcenter;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.upload.UploadFile;

import cn.jbolt._admin.jboltfile.JBoltFileService;
import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.config.JBoltUploadFolder;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.JboltFile;
import cn.jbolt.common.model.SysNotice;
import cn.jbolt.common.util.CACHE;

/**
 * 系统通知管理 Controller
 * @ClassName: SysNoticeAdminController   
 * @author: JBolt-Generator
 * @date: 2021-04-03 18:56  
 */
@CheckPermission(PermissionKey.SYS_NOTICE)
public class SysNoticeAdminController extends JBoltBaseController {

	@Inject
	private SysNoticeService service;
	@Inject
	private JBoltFileService jBoltFileService;
	
   /**
	* 首页
	*/
	public void index() {
		setDefaultSortInfo("create_time","desc");
		render("index.html");
	}
  	
  	/**
	* 数据源
	*/
	public void datas() {
		renderJsonData(service.paginateAdminDatas(getPageNumber(),getPageSize(),getKeywords(),getBoolean("delFlag",false),getSortColumn("create_time"),getSortType("desc")));
	}
	
   /**
	* 新增
	*/
	public void add() {
		render("add.html");
	}
	
	/**
	 * 编辑
	 */
	public void edit() {
		Integer noticeId = getInt(0);
		if(notOk(noticeId)) {
			renderDialogFail(Msg.PARAM_ERROR);
			return;
		}
		SysNotice sysNotice=service.findById(noticeId); 
		if(sysNotice == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		service.processReceiverValues(sysNotice);
		set("sysNotice",sysNotice);
		set("files",jBoltFileService.getListByIds(sysNotice.getFiles()));
		render("edit.html");
	}
	/**
	 * 查看附件列表
	 */
	public void files() {
		Integer noticeId = getInt(0);
		if(notOk(noticeId)) {
			renderDialogFail(Msg.PARAM_ERROR);
			return;
		}
		SysNotice sysNotice=service.findById(noticeId); 
		if(sysNotice == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		set("noticeId", noticeId);
		set("filesValue",sysNotice.getFiles());
		set("files",jBoltFileService.getListByIds(sysNotice.getFiles()));
		render("files.html");
	}
	
	
	/**
	 *  更新附件列表
	 */
	public void updateFiles() {
		renderJson(service.updateColumn(getInt("id"), "files", get("files")));
	}
	
	/**
	 * 上传多文件
	 */
	public void uploadFiles(){
		//上传到今天的文件夹下
		String uploadPath=JBoltUploadFolder.todayFolder(JBoltUploadFolder.SYSNOTICE_FILES);
		List<UploadFile> files=getFiles(uploadPath);
		if(files==null || files.size()==0) {
			renderJsonFail("请选择文件后上传");
			return;
		}
		
		List<JboltFile> retFiles=new ArrayList<JboltFile>();
		JboltFile jboltFile;
		StringBuilder errormsg = new StringBuilder();
		for(UploadFile uploadFile:files) {
			jboltFile=jBoltFileService.saveJBoltFile(uploadFile,uploadPath,JboltFile.FILE_TYPE_ATTACHMENT);
			if(jboltFile!=null){
				retFiles.add(jboltFile);
			}else {
				errormsg.append(uploadFile.getOriginalFileName()+"上传失败;");
			}
		}
		if(retFiles.size()==0) {
			renderJsonFail(errormsg.toString());
			return;
		}
		renderJsonData(retFiles,errormsg.toString());
	}
	
   /**
	* 详情
	*/
	public void detail() {
		SysNotice sysNotice=service.findById(getInt(0)); 
		if(sysNotice == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		service.processReceiverValues(sysNotice);
		set("sysNotice",sysNotice);
		set("files",jBoltFileService.getListByIds(sysNotice.getFiles()));
		render("detail.html");
	}
	
  /**
	* 保存
	*/
	public void save() {
		renderJson(service.save(getModel(SysNotice.class, "sysNotice")));
	}
	
   /**
	* 更新
	*/
	public void update() {
		renderJson(service.update(getModel(SysNotice.class, "sysNotice")));
	}
	
	/**
	 * 批量删除 
	 */
	public void batchDelete() {
		renderJson(service.deleteNotices(get("ids"),false));
	}
   /**
	* 批量还原恢复回收站数据
	*/
	public void batchRestore() {
		renderJson(service.restoreNotices(get("ids")));
	}
	
	/**
	 * 根据类型进入不同选择界面
	 */
	public void chooseReceiver() {
		Integer receiverType = getInt("receiverType");
		if(notOk(receiverType)) {
			renderPageFail(Msg.PARAM_ERROR);
			return;
		}
		//5是在字典表里定义的 接收者类型里的编码sn 定死
		if(receiverType.intValue() == 5) {
			set("hasDept", CACHE.me.checkHasDept());
			set("hasPost", CACHE.me.checkHasPost());
		}
		render("choose_"+receiverType.intValue()+".html");
	}
	
}