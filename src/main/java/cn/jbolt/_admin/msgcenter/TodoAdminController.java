package cn.jbolt._admin.msgcenter;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.jbolt._admin.permission.UnCheck;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Todo;

/**
 * 待办事项管理 Controller
 * @ClassName: TodoAdminController   
 * @author: JBolt-Generator
 * @date: 2021-04-17 17:43  
 */
@UnCheck
public class TodoAdminController extends JBoltBaseController {

	@Inject
	private TodoService service;

	/**
	* 新增
	*/
	public void add() {
		render("add.html");
	}
	
   /**
	* 编辑
	*/
	public void edit() {
		Todo todo=service.findById(getInt(0)); 
		if(todo == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		set("todo",todo);
		render("edit.html");
	}
	
  /**
	* 保存
	*/
	@Before(Tx.class)
	public void save() {
		renderJson(service.save(getModel(Todo.class, "todo")));
	}
	
   /**
	* 更新
	*/
	@Before(Tx.class)
	public void update() {
		renderJson(service.update(getModel(Todo.class, "todo")));
	}
	
	/**
	 * 删除
	 */
	@Before(Tx.class)
	public void delete() {
		renderJson(service.delete(getInt(0)));
	}
	/**
	 * 批量删除
	 */
	@Before(Tx.class)
	public void deleteByIds() {
		renderJson(service.deleteMyTodoByBatchIds(get("ids")));
	}
	/**
	 * 改状态
	 */
	@Before(Tx.class)
	public void updateState() {
		renderJson(service.updateState(getInt(0),getInt(1)));
	}
   /**
	* 批量改状态
	*/
	@Before(Tx.class)
	public void batchUpdateState() {
		renderJson(service.batchUpdateState(getState(),get("ids")));
	}
	
	/**
	 * 详情
	 */
	public void detail() {
		Integer todoId = getInt(0);
		if(notOk(todoId)) {
			renderDialogFail(Msg.PARAM_ERROR);
			return;
		}
		Todo todo=service.findById(todoId); 
		if(todo == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		Integer userId = JBoltUserKit.getUserIdAs();
		if(todo.getUserId().intValue() != userId.intValue()) {
			renderDialogFail(Msg.NOPERMISSION);
		}
		set("todo",todo);
		render("detail.html");
	}
	
}