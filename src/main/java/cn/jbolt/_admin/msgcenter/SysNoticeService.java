package cn.jbolt._admin.msgcenter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.db.sql.SqlExpress;
import cn.jbolt.common.model.SysNotice;
import cn.jbolt.common.model.User;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CACHE;
/**
 * 系统通知管理 Service
 * @ClassName: SysNoticeService   
 * @author: JBolt-Generator
 * @date: 2021-04-03 18:56  
 */
public class SysNoticeService extends JBoltBaseService<SysNotice> {
	private SysNotice dao=new SysNotice().dao();
	@Inject
	private SysNoticeReaderService sysNoticeReaderService;
	@Override
	protected SysNotice dao() {
		return dao;
	}
		
    /**
	 * 后台管理分页查询true 
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param delFlag 
	 * @param sortType 
	 * @param sortColumn 
	 * @return
	 */
	public Page<SysNotice> paginateAdminDatas(int pageNumber, int pageSize, String keywords, Boolean delFlag, String sortColumn, String sortType) {
		Page<SysNotice> page = paginateByKeywords(getTableSelectColumnsWithout("content"),sortColumn,sortType, pageNumber, pageSize, keywords, "title,content",Kv.by("del_flag", delFlag==null?false:delFlag));
		if(page!=null&&page.getTotalRow()>0) {
			page.getList().forEach(notice->processReceiverValues(notice));
		}
		return page;
	}
	public void processReceiverValues(SysNotice notice) {
		notice.put("receiverValues",processReceiverValues(notice.getReceiverType(),notice.getReceiverValue()));
	}
    private String processReceiverValues(Integer receiverType, String receiverValue) {
	  if(notOk(receiverType)) {return "全部";}
	  if(receiverType.intValue()==1) {return "全部";}
	  if(notOk(receiverValue)) {return "未设置";}
	  Integer[] values = ArrayUtil.toDisInt(receiverValue, ",");
	  if(values==null||values.length==0) {return "未设置";}
	  List<String> names = new ArrayList<String>();
	  switch (receiverType.intValue()) {
		  case 2://角色
			  for(Integer id:values) {
				  names.add(CACHE.me.getRoleName(id));
			  }
			  break;
		  case 3://部门
			  for(Integer id:values) {
				  names.add(CACHE.me.getDeptSnAndName(id));
			  }
			  break;
		  case 4://岗位
			  for(Integer id:values) {
				  names.add(CACHE.me.getPostName(id));
			  }
			  break;
	  	  case 5://用户
	  		 for(Integer id:values) {
				  names.add(CACHE.me.getUserName(id));
			  }
	  	      break;
	   }
	  if(names.size()==0) {
		  return "未设置";
	  }
	return ArrayUtil.join(names, ",");
}

    /**
	 * 保存
	 * @param sysNotice
	 * @return
	 */
	public Ret save(SysNotice sysNotice) {
		if(sysNotice==null || isOk(sysNotice.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		sysNotice.setCreateUserId(JBoltUserKit.getUserIdAs());
		sysNotice.setUpdateUserId(JBoltUserKit.getUserIdAs());
		sysNotice.setDelFlag(false);
		boolean success=sysNotice.save();
		return ret(success);
	}
	
   /**
	 * 保存
	 * @param sysNotice
	 * @return
	 */
	public Ret update(SysNotice sysNotice) {
		if(sysNotice==null || notOk(sysNotice.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		//更新时需要判断数据存在
		SysNotice dbSysNotice=findById(sysNotice.getId());
		if(dbSysNotice==null) {return fail(Msg.DATA_NOT_EXIST);}
		sysNotice.setUpdateUserId(JBoltUserKit.getUserIdAs());
		boolean success=sysNotice.update();
		return ret(success);
	}
	
   /**
	  * 更新 指定多个ID 假删除
	 * @param ids
	 * @param delFlag
	 * @return
	 */
	public Ret updateDelFlagByBatchIds(String ids,boolean delFlag) {
		if(notOk(ids)) {return fail(Msg.PARAM_ERROR);}
		update(updateSql().set("del_flag", delFlag).in("id", ids));
		return SUCCESS;
	}
	
	/**
	 * 
	 * @param user
	 * @param sysNotice
	 * @return
	 */
	public boolean checkUserHasAuth(User user, SysNotice sysNotice) {
		return false;
	}
	
	/**
	 * 批量删除
	 * @param ids
	 * @param realDelete
	 * @return
	 */
	public Ret deleteNotices(String ids,Boolean realDelete) {
		if(notOk(ids)||realDelete==null) {
			return fail(Msg.PARAM_ERROR);
		}
		if(realDelete) {
			return deleteByIds(ids);
		}
		return updateDelFlagByBatchIds(ids, true);
	}
	
	/**
	 * 还原数据
	 * @param ids
	 * @return
	 */
	public Ret restoreNotices(String ids) {
		return updateDelFlagByBatchIds(ids, false);
	}
	/**
	 * 获取消息中心前十条数据
	 * @return
	 */
	public List<SysNotice> getMsgCenterPortalDatas() {
		Page<SysNotice> pageData = paginateUserSysNotices(1, 10, null, JBoltUserKit.getUserIdAs(), "id", "desc", false,false,false,true,"id","title","create_time","update_time","type","priority_level");
		return pageData.getList();
	}
	/**
	 * 标记已读
	 * @param id
	 * @return
	 */
	public Ret markAsRead(Integer id) {
		Ret ret = sysNoticeReaderService.addReader(id);
		if(ret.isFail()) {
			return ret;
		}
		//更新阅读数量
		updateSysNoticeReadCount(id);
		return SUCCESS;
	}
	/**
	 * 更新通知的阅读数
	 * @param id
	 */
	public void updateSysNoticeReadCount(Integer id) {
		update(updateSql().set("read_count", new SqlExpress("read_count+1")).eq("id", id));
	}
	
	/**
	 * 标记全部已读
	 * @return
	 */
	public Ret markAllAsRead() {
		List<Integer> ids = getAllUnReadIds(JBoltUserKit.getUserIdAs());
		if(notOk(ids)) {return SUCCESS;}
		return markMultiAsRead(ids);
	}
	/**
	 * 获取指定用户未读IDS
	 * @param userId
	 * @return
	 */
	public List<Integer> getAllUnReadIds(Integer userId) {
		Sql sql = selectSql().selectId();
		Sql notInSql = sysNoticeReaderService.selectSql().select("sys_notice_id").eq("user_id", userId);
		sql.notInSql("id", notInSql);
		return query(sql);
	}
	/**
	 * 获取指定用户未读IDS
	 * @param userId
	 * @return
	 */
	public boolean existUnread(Integer userId) {
		Sql sql = selectSql().selectId().first();
		Sql notInSql = sysNoticeReaderService.selectSql().select("sys_notice_id").eq("user_id", userId);
		sql.notInSql("id", notInSql);
		return exist(sql);
	}
	/**
	 * 分页获取指定用户可读数据
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords
	 * @param userId
	 * @param sortColumn
	 * @param sortType
	 * @param readed
	 * @param delFlag
	 * @param needProcessReadedState
	 * @param columnWithOrWithout
	 * @param columns
	 * @return
	 */
	public Page<SysNotice> paginateUserSysNotices(int pageNumber,int pageSize,String keywords,Integer userId,String sortColumn, String sortType,Boolean readed,boolean delFlag,boolean needProcessReadedState,Boolean columnWithOrWithout,String... columns) {
		Sql sql = selectSql().page(pageNumber, pageSize);
		String mainPre="";
		String joinPre="";
		if(needProcessReadedState) {
			mainPre="sn.";
			joinPre="sr.";
		}
		if(isOk(keywords)) {
			sql.likeMulti(keywords, mainPre+"title",mainPre+"content");
		}
		if(isOk(sortColumn)) {
			if(isOk(sortType)){
				sql.orderBy(mainPre+sortColumn, mainPre+sortType);
			}else {
				sql.orderBy(mainPre+sortColumn);
			}
		}
		Sql subSql = sysNoticeReaderService.selectSql().select("sys_notice_id").eq("user_id", userId);
		if(readed!=null) {
			if(readed) {
				sql.inSql(mainPre+"id", subSql);
			}else {
				sql.notInSql(mainPre+"id", subSql);
			}
		}
		sql.eq(mainPre+"del_flag", delFlag);
		if(needProcessReadedState) {
			sql.leftJoin(sysNoticeReaderService.table(), "sr", "sr.sys_notice_id = sn.id");
			sql.from(table(), "sn");
			
			if(columnWithOrWithout!=null && isOk(columns)) {
				if(columnWithOrWithout) {
					List<String> newCols=new ArrayList<String>();
					for(String s:columns) {
						newCols.add(mainPre="."+s);
					}
					newCols.add("sr.sys_notice_id as readed");
					columns = newCols.toArray(new String[columns.length]);
					sql.select(columns);
				}else {
					sql.select(getTableSelectColumnsWithoutWithPre(mainPre,columns)+" ,sr.sys_notice_id as readed ");
				}
			}
		}else {
			if(columnWithOrWithout!=null) {
				if(columnWithOrWithout) {
					sql.select(columns);
				}else {
					sql.select(getTableSelectColumnsWithout(columns));
				}
			}
		}
		
		return paginate(sql);
	}
	/**
	 * 批量处理已读
	 * @param idsStr
	 * @return
	 */
	public Ret markMultiAsRead(String idsStr) {
		if(notOk(idsStr)) {return fail(Msg.PARAM_ERROR);}
		Integer[] ids = ArrayUtil.toDisInt(idsStr, ",");
		if(notOk(ids)) {return fail(Msg.PARAM_ERROR);}
		return markMultiAsRead(ids);
	}
	/**
	 * 批量处理已读
	 * @param ids
	 * @return
	 */
	public Ret markMultiAsRead(Integer[] ids) {
		return markMultiAsRead(Arrays.stream(ids).collect(Collectors.toList()));
	}

	/**
	 * 批量处理已读
	 * @param ids
	 * @return
	 */
	public Ret markMultiAsRead(List<Integer> ids) {
		if(!isOk(ids)) {return fail(Msg.PARAM_ERROR);}
		ids.forEach(id->{
			sysNoticeReaderService.addReader(id);
			updateSysNoticeReadCount(id);
		});
		return SUCCESS;
	}
}