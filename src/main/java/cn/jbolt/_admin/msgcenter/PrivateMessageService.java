package cn.jbolt._admin.msgcenter;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.common.model.PrivateMessage;

public class PrivateMessageService extends JBoltBaseService<PrivateMessage> {
	private PrivateMessage dao = new PrivateMessage();
	@Override
	protected PrivateMessage dao() {
		return dao;
	}
	
	public boolean existUnread(Integer userId) {
		return false;
	}

}
