package cn.jbolt._admin.msgcenter;

import com.jfinal.kit.Ret;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.model.SysNoticeReader;

public class SysNoticeReaderService extends JBoltBaseService<SysNoticeReader> {
	private SysNoticeReader dao =new SysNoticeReader();
	@Override
	protected SysNoticeReader dao() {
		return dao;
	}
	/**
	 * 添加一个通知读者
	 * @param sysNoticeId
	 * @return
	 */
	public Ret addReader(Integer sysNoticeId) {
		boolean exist = existsReader(sysNoticeId, JBoltUserKit.getUserIdAs());
		if(exist) {
			return SUCCESS;
		}
		SysNoticeReader noticeReader=new SysNoticeReader();
		noticeReader.setSysNoticeId(sysNoticeId);
		noticeReader.setUserId(JBoltUserKit.getUserIdAs());
		boolean success=noticeReader.save();
		return ret(success);
	}
	/**
	 * 是否存在指定通知读者
	 * @param sysNoticeId
	 * @param userId
	 * @return
	 */
	public boolean existsReader(Integer sysNoticeId, Integer userId) {
		return exist(selectSql().eq("sys_notice_id", sysNoticeId).eq("user_id", userId));
	}

}
