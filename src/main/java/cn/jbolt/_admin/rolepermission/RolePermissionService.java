package cn.jbolt._admin.rolepermission;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;

import cn.jbolt._admin.interceptor.JBoltSecurityCheck;
import cn.jbolt._admin.permission.PermissionService;
import cn.jbolt._admin.role.RoleService;
import cn.jbolt._admin.systemlog.SystemLogService;
import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.MainConfig;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Permission;
import cn.jbolt.common.model.Role;
import cn.jbolt.common.model.RolePermission;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.model.User;
import cn.jbolt.common.util.ArrayUtil;
import cn.jbolt.common.util.CACHE;
/**
 * 角色权限中间表Service
 * @ClassName:  RolePermissionService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年11月9日   
 */
public class RolePermissionService extends JBoltBaseService<RolePermission> {
	private RolePermission dao = new RolePermission().dao();
	@Inject
	private PermissionService permissionService;
	@Inject
	private RoleService roleService;
	@Inject
	private SystemLogService systeLogService;

	@Override
	protected RolePermission dao() {
		return dao;
	}

	/**
	 * 删除一个角色下的权限资源
	 * 
	 * @param roleId
	 * @return
	 */
	public Ret deleteRolePermission(Integer roleId) {
		if (notOk(roleId)) {
			return fail(Msg.PARAM_ERROR);
		}
		Ret ret = deleteBy(Kv.by("role_id", roleId));
		if (ret.isOk()) {
			// 删除缓存
			CACHE.me.removeMenusAndPermissionsByRoleGroups();
		}
		return ret;
	}

	/**
	 * 处理角色变动资源
	 * @param roleId
	 * @param permissionStr
	 * @return
	 */
	public Ret doSubmit(Integer roleId, String permissionStr) {
		if(MainConfig.DEMO_MODE) {return fail(Msg.DEMO_MODE_LIMIT_MSG);}
		if (notOk(roleId) || notOk(permissionStr)) {
			return fail("参数有误");
		}
		Role role = roleService.findById(roleId);
		if (role == null) {
			return fail("角色信息不存在");
		}
		String[] ids = ArrayUtil.from(permissionStr, ",");
		if (ids == null || ids.length == 0) {
			return fail("请选择分配的权限");
		}
		// 先删除以前的
		deleteRolePermission(roleId);
		// 添加现在的
		saveRolePermissions(roleId, ids);
		// 添加日志
		addUpdateSystemLog(roleId, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_ROLE, role.getName(), "的可用权限设置");
		return SUCCESS;
	}

	/**
	 * 保存一个角色分配的所有资源信息
	 * 
	 * @param roleId
	 * @param ids
	 */
	private void saveRolePermissions(Integer roleId, String[] ids) {
		List<RolePermission> permissions = new ArrayList<RolePermission>();
		RolePermission rolePermission = null;
		for (String id : ids) {
			rolePermission = new RolePermission();
			rolePermission.autoProcessIdValue();
			rolePermission.setRoleId(roleId);
			rolePermission.setPermissionId(Integer.parseInt(id));
			permissions.add(rolePermission);
		}
		batchSave(permissions);
	}

	/**
	 * 获取指定多个角色的所有权限资源
	 * 
	 * @param roleIds
	 * @return
	 */
	public Set<String> getPermissionsKeySetByRoles(String roleIds) {
		List<RolePermission> rolePermissions = getListByRoles(roleIds);
		if (rolePermissions == null || rolePermissions.size() == 0) {
			return null;
		}
		Set<String> set=new HashSet<String>();
		for (RolePermission rf : rolePermissions) {
			Permission f = CACHE.me.getPermission(rf.getPermissionId());
			if (f != null) {
				set.add(f.getPermissionKey());
			}
		}
		return set;
	}
	
	/**
	 * 获取指定多个角色的所有权限资源
	 * 
	 * @param roleIds
	 * @return
	 */
	public List<Permission> getPermissionsByRoles(String roleIds) {
		List<RolePermission> rolePermissions = getListByRoles(roleIds);
		if (rolePermissions == null || rolePermissions.size() == 0) {
			return null;
		}
		List<Permission> permissions = new ArrayList<Permission>();
		for (RolePermission rf : rolePermissions) {
			Permission f = CACHE.me.getPermission(rf.getPermissionId());
			if (f != null) {
				permissions.add(f);
			}
		}
		return permissions;
	}

	/**
	 * 获取一个角色下的分配的权限资源数据
	 * 
	 * @param roleId
	 * @return
	 */
	public List<RolePermission> getListByRole(Integer roleId) {
		return getCommonList(Kv.by("role_id", roleId));
	}

	/**
	 * 获取多个角色下分配的权限资源数据
	 * 
	 * @param roleIds
	 * @return
	 */
	public List<RolePermission> getListByRoles(String roleIds) {
		return find(selectSql().distinct("permission_id").in("role_id", roleIds).orderBy("permission_id"));
	}
	

	/**
	 * 检测一个角色是否包含指定的权限
	 * 
	 * @param roleId
	 * @param permissionKey
	 * @return
	 */
	public boolean checkRoleHasPermission(Integer roleId, String permissionKey) {
		Permission permission = permissionService.getByPermissionkey(permissionKey);
		if (permission == null) {
			return false;
		}
		RolePermission rolePermission = findFirst(Kv.by("role_id", roleId).set("permission_id", permission.getId()));
		return (rolePermission != null);
	}

	/**
	 * 检测一个用户是否包含指定的权限
	 * @param userId
	 * @param checkAll
	 * @param permissionKeys
	 * @return
	 */
	public boolean checkUserHasPermission(Object userId,boolean checkAll, String... permissionKeys) {
		if (permissionKeys == null || permissionKeys.length == 0) {
			return false;
		}
		User user = CACHE.me.getUser(userId);
		if (user == null) {
			return false;
		}
		//是超管并且包含超管权限
		if(user.getIsSystemAdmin()) {
			boolean isSystemAdminPermission=JBoltSecurityCheck.checkIsSystemAdminDefaultPermission(checkAll,permissionKeys);
			if(isSystemAdminPermission) {
				return true;
			}
			//如果是超管员 但是这个权限不是超管员专属的就再去普通校验把 如果是超管的就直接返回true了
		}
		//超管权限校验后就开始校验角色分配权限了
		String roles = user.getRoles();
		if (notOk(roles)) {
			return false;
		}
		//不是超管就直接check
		return JBoltSecurityCheck.checkHasPermission(checkAll,roles,permissionKeys);
	}



	/**
	 * 删除关联一个资源的所有role和资源的绑定数据
	 * 
	 * @param permissionId
	 */
	public Ret deleteByPermission(Integer permissionId) {
		return deleteBy(Kv.by("permission_id", permissionId));
	}
	/**
	 * 根据角色清空权限绑定
	 * @param roleId
	 * @return
	 */
	public Ret deleteByRole(Integer roleId) {
		Ret ret=deleteRolePermission(roleId);
		if(ret.isOk()) {
			addUpdateSystemLog(roleId, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_ROLE, CACHE.me.getRoleName(roleId), "清空了角色绑定设置的所有权限资源");
		}
		return ret;
	}

}
