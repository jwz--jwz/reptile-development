package cn.jbolt._admin.dept;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheck;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Dept;
/**
 * 
 * @ClassName: DeptAdminController   
 * @author: JFinal学院-小木
 * @date: 2021-02-07 20:34  
 */
@CheckPermission(PermissionKey.DEPT)
@UnCheck
public class DeptAdminController extends JBoltBaseController {

	@Inject
	private DeptService service;
	
  /**
	* 首页
	*/
	public void index() {
		render("index.html");
	}
	/**
	 * 数据源
	 */
	public void datas() {
		renderJsonData(service.getTreeDatas(false,false));
	}
	/**
	 * select数据源
	 */
	@UnCheck
	public void options() {
		renderJsonData(service.getTreeDatas(false,true));
	}
	/**
	 * select数据源 只需要enable=true的
	 */
	@UnCheck
	public void enableOptions() {
		renderJsonData(service.getTreeDatas(true,true));
	}
  /**
	* JSTree树形数据源
	*/
	@UnCheck
	public void enableJsTreeDatas() {
		renderJsonData(service.getEnableJsTree(getInt("selectId"),getInt("openLevel",0)));
	}
	
  /**
	* 新增
	*/
	public void add() {
		set("pid", getInt(0,0));
		render("add.html");
	}
	
  /**
	* 编辑
	*/
	public void edit() {
		Dept dept=service.findById(getInt(0)); 
		if(dept == null){
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		set("dept",dept);
		render("edit.html");
	}
	
  /**
	* 保存
	*/
	@Before(Tx.class)
	public void save() {
		renderJson(service.save(getModel(Dept.class, "dept")));
	}
	
  /**
	* 更新
	*/
	@Before(Tx.class)
	public void update() {
		renderJson(service.update(getModel(Dept.class, "dept")));
	}
	
  /**
	* 删除
	*/
	@Before(Tx.class)
	public void delete() {
		renderJson(service.delete(getInt(0)));
	}
	
  /**
	* 切换启用状态
	*/
	@Before(Tx.class)
	public void toggleEnable() {
		renderJson(service.toggleEnable(getInt(0)));
	}
	
  /**
	* 排序 上移
	*/
	@Before(Tx.class)
	public void up() {
		renderJson(service.up(getInt(0)));
	}
	
  /**
	* 排序 下移
	*/
	@Before(Tx.class)
	public void down() {
		renderJson(service.down(getInt(0)));
	}
	
  /**
	* 排序 初始化
	*/
	@Before(Tx.class)
	public void initRank() {
		renderJson(service.initRank());
	}
	
	
}