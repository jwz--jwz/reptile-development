package cn.jbolt._admin.dept;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;

import cn.jbolt._admin.user.UserService;
import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.bean.JsTreeBean;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.model.Dept;
import cn.jbolt.common.model.SystemLog;
/**
 * 
 * @ClassName: DeptService   
 * @author: JFinal学院-小木
 * @date: 2021-02-07 20:34  
 */
public class DeptService extends JBoltBaseService<Dept> {
	private Dept dao=new Dept().dao();
	@Inject
	private UserService userService;
	@Override
	protected Dept dao() {
		return dao;
	}
	
  /**
	 * 保存
	 * @param dept
	 * @return
	 */
	public Ret save(Dept dept) {
		if(dept==null ||dept.getPid()==null ||notOk(dept.getName()) || notOk(dept.getSn()) ||notOk(dept.getFullName()) || isOk(dept.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		if(existsNameWithPid(dept.getName(),dept.getPid())) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		if(exists("full_name",dept.getSn(), -1 ,dept.getPid())) {return fail(Msg.DEPT_SAME_FULLNAME_EXIST);}
		if(exists("sn",dept.getSn())) {return fail(Msg.DEPT_SAME_SN_EXIST);}
		dept.setSortRank(getNextSortRankByPid(dept.getPid()));
		dept.setEnable(true);
		boolean success=dept.save();
		if(success) {
			//添加日志
			addSaveSystemLog(dept.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_DEPT, dept.getName());
		}
		return ret(success);
	}
	
  /**
	 * 保存
	 * @param userId
	 * @param dept
	 * @return
	 */
	public Ret update( Dept dept) {
		if(dept==null ||dept.getPid()==null ||notOk(dept.getName()) || notOk(dept.getSn()) ||notOk(dept.getFullName())
				|| notOk(dept.getId())) {
			return fail(Msg.PARAM_ERROR);
		}
		if(isOk(dept.getPid())&&dept.getPid().intValue()==dept.getId().intValue()) {
			return fail(Msg.PARENT_CAN_NOT_SAME_SELF);
		}
		//更新时需要判断数据存在
		Dept dbDept=findById(dept.getId());
		if(dbDept==null) {return fail(Msg.DATA_NOT_EXIST);}
		if(existsName(dept.getName(), dept.getId(),dept.getPid())) {return fail(Msg.DATA_SAME_NAME_EXIST);}
		if(exists("full_name",dept.getFullName(), dept.getId(),dept.getPid())) {return fail(Msg.DEPT_SAME_FULLNAME_EXIST);}
		if(exists("sn",dept.getSn(), dept.getId())) {return fail(Msg.DEPT_SAME_SN_EXIST);}
		boolean changeParent=dbDept.getPid().intValue()!=dept.getPid().intValue();
		if(changeParent) {
			dept.setSortRank(getNextSortRankByPid(dept.getPid()));
		}
		
		boolean success=dept.update();
		if(success) {
			if(changeParent) {
				updateSortRankAfterChangeParentNode(Kv.by("pid", dbDept.getPid()),dbDept.getSortRank());
			}
			//添加日志
			addUpdateSystemLog(dept.getId(), JBoltUserKit.getUserId(),  SystemLog.TARGETTYPE_DEPT, dept.getName());
		}
		return ret(success);
	}
	/**
	 * 得到JSTree数据源
	 * @param checkedId 默认选中节点
	 * @param openLevel -1全部 0 不动 >0指定层级层级
	 * @return
	 */
	public List<JsTreeBean> getEnableJsTree(Integer checkedId,int openLevel) {
		List<Dept> depts=getEnableList();
		return convertJsTree(depts,checkedId,openLevel);
	}
	/**
	 * 
	 * @return
	 */
	public List<Dept> getEnableList() {
		return getCommonList(Kv.by("enable", TRUE),"sort_rank","asc");
	}

	/**
	  * 删除
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		Ret ret=deleteById(id,true);
		if(ret.isOk()){
			//添加日志
			Dept dept=ret.getAs("data");
			addDeleteSystemLog(id, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_DEPT, dept.getName());
		}
		return ret;
	}
	@Override
	public String checkCanDelete(Dept model, Kv kv) {
		return checkInUse(model, kv);
	}
	
	@Override
	public String checkInUse(Dept dept, Kv kv) {
		boolean existSons=existsSon(dept.getId());
		if(existSons) {
			return Msg.EXIST_SON;
		}
		
		boolean userUse=userService.checkDeptInUse(dept.getId());
		if(userUse) {
			return Msg.DEPT_USER_USE;
		}
		
		return null;
	}
	
	/**
	 * 获取树形结构数据
	 * @return
	 */
	public List<Dept> getTreeDatas(boolean onlyEnable,boolean asOptions){
		Sql sql=selectSql().orderBySortRank();
		if(asOptions) {
			sql.select("id","name","pid","sn");
		}
		if(onlyEnable) {
			sql.eq("enable", TRUE);
		}
		List<Dept> datas=find(sql);
		return convertToModelTree(datas, "id", "pid", (p)->notOk(p.getPid()));	
	}
	
   /**
	 * 切换禁用启用状态
	 * @param id
	 * @return
	 */
	public Ret toggleEnable(Integer id) {
		//说明:如果需要日志处理 就解开下面部分内容 如果不需要直接删掉即可
		Ret ret=toggleBoolean(id, "enable");
		if(ret.isOk()){
			//添加日志
			Dept dept=ret.getAs("data");
			if(dept.getEnable()) {
				processParentEnableTrue(dept.getPid());
			}else {
				processSonEnableFalse(dept.getId());
			}
			addUpdateSystemLog(id, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_DEPT, dept.getName(),"的启用状态:"+dept.getEnable());
		} 
		return ret;
	}
	/**
	 * 处理所有父节点enable=true
	 * @param pid
	 */
	private void processParentEnableTrue(Integer pid) {
		if(isOk(pid)) {
			Dept dept=findById(pid);
			if(dept!=null) {
				if(!dept.getEnable()) {
					dept.setEnable(true);
					dept.update();
					processParentEnableTrue(dept.getPid());
				}
			}
		}
	}
	
	/**
	 * 处理所有子节点enable=false
	 * @param pid
	 */
	private void processSonEnableFalse(Integer id) {
		dao().each(son->{
			son.setEnable(false);
			son.update();
			processSonEnableFalse(son.getId());
			return true;
		}, selectSql().pidEqQM().eq("enable",TRUE).toSql(), id);
	}

	/**
	  * 上移
	 * @param id
	 * @return
	 */
	public Ret up(Integer id) {
		Dept dept=findById(id);
		if(dept==null){
			return fail("数据不存在或已被删除");
		}
		Integer rank=dept.getSortRank();
		if(rank==null||rank<=0){
			return fail("顺序需要初始化");
		}
		if(rank==1){
			return fail("已经是第一个");
		}
		Dept upDept=findFirst(Kv.by("sort_rank", rank-1).set("pid",dept.getPid()));
		if(upDept==null){
			return fail("顺序需要初始化");
		}
		upDept.setSortRank(rank);
		dept.setSortRank(rank-1);
		
		upDept.update();
		dept.update();
		return SUCCESS;
	}
	
/**
	 * 下移
	 * @param id
	 * @return
	 */
	public Ret down(Integer id) {
		Dept dept=findById(id);
		if(dept==null){
			return fail("数据不存在或已被删除");
		}
		Integer rank=dept.getSortRank();
		if(rank==null||rank<=0){
			return fail("顺序需要初始化");
		}
		int max=getCount();
		if(rank==max){
			return fail("已经是最后已一个");
		}
		Dept upDept=findFirst(Kv.by("sort_rank", rank+1).set("pid",dept.getPid()));
		if(upDept==null){
			return fail("顺序需要初始化");
		}
		upDept.setSortRank(rank);
		dept.setSortRank(rank+1);
		
		upDept.update();
		dept.update();
		return SUCCESS;
	}
	
 
	
	private void initOneRank(List<Dept> depts) {
		if(notOk(depts)){
			return;
		}
		for(int i=0;i<depts.size();i++){
			depts.get(i).setSortRank(i+1);
		}
		batchUpdate(depts);
		for(Dept dept:depts){
			//批量更新里已经处理了cache相关删除工作
//			deleteCacheById(dept.getId());
//			deleteCacheByKey(dept.getSn());
			//继续向下递归
			List<Dept> items=getSonsOrderById(dept.getId());
			initOneRank(items);
		}
	
	}

	/**
	 * 初始化排序
	 */
	public Ret initRank(){
		List<Dept> parents=getAllParentsOrderById();
		if(parents.size()>0){
			initOneRank(parents);
		}
		//添加日志
		addUpdateSystemLog(null, JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_PERMISSION, "全部","初始化顺序");
		return SUCCESS;
		
	}
	
	public List<Integer> processSonDeptIds(Integer deptId) {
		List<Integer> ids=new ArrayList<Integer>();
		if(isOk(deptId)) {
			processSonDeptIds(ids, deptId);
		}
		return ids;
	}
	private void processSonDeptIds(List<Integer> ids, Integer deptId) {
		if(isOk(deptId)) {
			dao().each(son->{
				ids.add(son.getId());
				processSonDeptIds(ids, son.getId());
				return true;
			}, selectSql().pidEqQM().toSql(), deptId);
		}
	}
	
}