package cn.jbolt._admin.jboltfile;

import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.jfinal.core.JFinal;
import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;

import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltConst;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.model.JboltFile;

/**  
 * 系统文件库管理 
 * @ClassName:  JBoltFileService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年3月24日 上午12:40:27   
 */
public class JBoltFileService extends JBoltBaseService<JboltFile> {
	private JboltFile dao = new JboltFile().dao();
	@Override
	protected JboltFile dao() {
		return dao;
	}
	

	public Page<JboltFile> paginateAdminData(int pageNumber, int pageSize) {
		return paginate("id", "desc", pageNumber, pageSize);
	}

	/**
	 * 保存图片
	 * @param file
	 * @param uploadPath
	 * @return
	 */
	public Ret saveImageFile(UploadFile file, String uploadPath) {
		return saveFile(file,uploadPath,JboltFile.FILE_TYPE_IMAGE);
	}
	
	/**
	 * 保存Excel
	 * @param file
	 * @param uploadPath
	 * @return
	 */
	public Ret saveExcelFile(UploadFile file, String uploadPath) {
		return saveFile(file,uploadPath,JboltFile.FILE_TYPE_OFFICE);
	}
	/**
	 * 保存音频
	 * @param file
	 * @param uploadPath
	 * @return
	 */
	public Ret saveAudioFile(UploadFile file, String uploadPath) {
		return saveFile(file,uploadPath,JboltFile.FILE_TYPE_AUDIO);
	}
	/**
	 * 保存视频
	 * @param file
	 * @param uploadPath
	 * @return
	 */
	public Ret saveVideoFile(UploadFile file, String uploadPath) {
		return saveFile(file,uploadPath,JboltFile.FILE_TYPE_VEDIO);
	}
	/**
	 * 保存其它附件
	 * @param file
	 * @param uploadPath
	 * @return
	 */
	public Ret saveAttachmentFile(UploadFile file, String uploadPath) {
		return saveFile(file,uploadPath,JboltFile.FILE_TYPE_ATTACHMENT);
	}
	/**
	 * 保存文件底层方法
	 * @param file
	 * @param uploadPath
	 * @param fileType
	 * @return
	 */
	public Ret saveFile(UploadFile file, String uploadPath,int fileType) {
		JboltFile jboltFile = saveJBoltFile(file, uploadPath, fileType);
		return jboltFile!=null?success(jboltFile.getLocalUrl(),"上传成功"):fail("上传失败");
	}
	/**
	 * 保存文件底层方法
	 * @param file
	 * @param uploadPath
	 * @param fileType
	 * @return
	 */
	public JboltFile saveJBoltFile(UploadFile file, String uploadPath,int fileType) {
		String localPath=file.getUploadPath()+File.separator+file.getFileName();
		String localUrl=JFinal.me().getConstants().getBaseUploadPath()+JBoltConst.SLASH+uploadPath+JBoltConst.SLASH+file.getFileName();
		localPath=FileUtil.normalize(localPath);
		JboltFile jboltFile=new JboltFile();
		jboltFile.setObjectUserId(JBoltUserKit.getUserId());
		jboltFile.setCreateTime(new Date());
		jboltFile.setFileName(file.getOriginalFileName());
		jboltFile.setFileType(fileType);
		jboltFile.setLocalPath(localPath);
		jboltFile.setLocalUrl(localUrl);
		File realFile=file.getFile();
		long fileSize=FileUtil.size(realFile);
		jboltFile.setFileSize((int)fileSize);
		String fileSuffix=FileTypeUtil.getType(realFile);
		jboltFile.setFileSuffix(fileSuffix);
		boolean success=jboltFile.save();
		return success?jboltFile:null;
	}

	/**
	 * 根据IDs获取数据
	 * @param ids
	 * @return
	 */
	public List<JboltFile> getListByIds(String ids) {
		if(notOk(ids)) {return Collections.emptyList();}
		return find(selectSql().in("id", ids));
	}




}
