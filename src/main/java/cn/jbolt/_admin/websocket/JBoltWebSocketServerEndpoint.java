package cn.jbolt._admin.websocket;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import com.jfinal.kit.LogKit;

import cn.jbolt.common.util.CACHE;

/**
 * JBolt内置websocket服务端处理
 * @ClassName:  JBoltWebSocketServerEndpoint   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年5月31日   
 *    
 * 注意：本内容仅限于JFinal学院 JBolt平台VIP成员内部传阅，请尊重开发者劳动成果，不要外泄出去用于其它商业目的
 */
@ServerEndpoint("/websocket.ws/{token}")
public class JBoltWebSocketServerEndpoint {

    //客户端
    private static final Map<String, JBoltWebSocketServerEndpoint> CLIENTS = new ConcurrentHashMap<String, JBoltWebSocketServerEndpoint>();
    //会话
    private Session session;
    //登录用户授权的TOKEN
    private String token;
    //用户UserId
    private Object userId;
    

    /**
     * 连接建立成功调用的方法
     * @param session  可选的参数。session为与某个客户端的连接会话，需要通过它来给客户端发送数据
     */
    @OnOpen
    public void onOpen(@PathParam("token")String token,Session session){
    	this.token = token;
        this.session = session;
        CLIENTS.put(token, this);
        this.userId = CACHE.me.getUserIdBySessionId(token);
        
        LogKit.info("有新连接加入！当前在线人数为" + CLIENTS.size());
        LogKit.info("session=" + session.getId());
        LogKit.info("token=" + this.token);
        LogKit.info("userId=" + this.userId);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(){
    	CLIENTS.remove(token);
        LogKit.info("有一连接关闭！当前在线人数为" + CLIENTS.size());
        LogKit.info("session=" + session.getId());
        LogKit.info("userId=" + this.userId);
    }

    /**
     * 收到客户端消息后调用的方法
     * @param message 客户端发送过来的消息
     * @param session 可选的参数
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        LogKit.info("来自客户端的消息:" + message);
        LogKit.info("session=" + session.getId());
        LogKit.info("userId=" + this.userId);
    }

    /**
     * 发生错误时调用
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error){
        LogKit.error("发生错误", error);
    }
    
    /**
     * 发送群消息
     */
    protected void sendAllMessage(String message){
    	sendAllMessage(message, true);
    }
    
    /**
     * 发送群消息
     * @param message 消息
     * @param isThis 是否给自己 也发
     */
    protected void sendAllMessage(String message, boolean isThis){
        for(Entry<String, JBoltWebSocketServerEndpoint> entry: CLIENTS.entrySet()){
        	//群发消息
        	JBoltWebSocketServerEndpoint me = entry.getValue();
			if (isThis || me != this) {
				me.sendMessage(message);
			}
        }
    }
    
    /**
     * 指定 发送消息
     */
    protected void sendMessage(String token, String message){
    	JBoltWebSocketServerEndpoint me = CLIENTS.get(token);
    	if (me != null) {
    		me.sendMessage(message);
		}else{
			this.sendMessage("【系统消息】 不在线");
		}
    }
    
    /**
     * 向客户端 发送消息
     */
    protected void sendMessage(String message){
        try {
			this.session.getBasicRemote().sendText(message);
		} catch (IOException e) {
			LogKit.error("客户端异常", e);
		}
    }
    
    
}
