package cn.jbolt._admin.dictionary;


import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheck;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Dictionary;
import cn.jbolt.common.model.DictionaryType;
/**
 * 字典管理Controller
 * @ClassName:  DictionaryAdminController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年11月9日   
 */
@CheckPermission({PermissionKey.DICTIONARY})
@UnCheckIfSystemAdmin
public class DictionaryAdminController extends JBoltBaseController {
	@Inject
	private DictionaryService service;
	@Inject
	private DictionaryTypeService dictionaryTypeService;
	public void datas(){
		Integer typeId = getInt("typeId");
		DictionaryType type = dictionaryTypeService.findById(typeId);
		if(type == null) {
			renderJsonData("");
			return;
		}
		List<Dictionary> dics = service.getListByType(typeId,getKeywords());
		Kv extraData=Kv.by("typeLevel", type.getModeLevel());
		//使用这个专门的方法 render出去
		renderJBoltTableJsonData(dics,extraData);
	}
	/**
	 * options
	 */
	@UnCheck
	public void options(){
		renderJsonData(service.getOptionListByType(get("key")));
	}
	/**
	 * 根据获取一级options
	 */
	@UnCheck
	public void poptions(){
		renderJsonData(service.getRootOptionListByType(get("key")));
	}
	/**
	 * 子类级别数据 根据父类ID获取数据
	 */
	@UnCheck
	public void soptions(){
		renderJsonData(service.getSonOptionListByType(get("key"),getInt("pid")));
	}
	/**
	 * 子类级别数据 根据父类SN获取数据
	 */
	@UnCheck
	public void soptionsByPsn(){
		renderJsonData(service.getSonOptionListByTypeAndPsn(get("key"),get("psn")));
	}
	
	public void checkandinit() {
		renderJson(service.checkAndInit());
	}
	/**
	 * 加载管理portal
	 */
	public void mgr(){
		Integer typeId=getInt(0);
		if(notOk(typeId)){
			renderAjaxPortalFail("选择左侧分类查询数据");
			return;
		}else{
			DictionaryType type=dictionaryTypeService.findById(typeId);
			if(type==null){
				renderAjaxPortalFail("参数异常，加载失败");
				return;
			}
			initMgr(type);
		}
		
		render("mgrportal.html");
	}
	
	private void initMgr(DictionaryType type){
		set("dictionaryType", type);
		List<Dictionary> dictionaries=service.getListByType(type.getId(),null);
		set("dictionaries",dictionaries);
		if(type.getModeLevel()==DictionaryType.MODE_LEVEL_MUTIL){
			set("dataTotalCount", service.getCountByType(type.getId()));
		}else{
			set("dataTotalCount", dictionaries.size());
		}
	}
	/**
	 * 管理首页
	 */
	public void index(){
		render("index_ajax.html");
	}
	/**
	 * 从日志过来的显示一个
	 */
	public void show(){
		Integer dictionaryId=getInt(0);
		if(notOk(dictionaryId)){
			renderPjaxFail("数据不存在或已被删除");
		}else{
			Dictionary dictionary=service.findById(dictionaryId);
			if(dictionary==null){
				renderPjaxFail("数据不存在或已被删除");
			}else{
				Integer typeId=dictionary.getTypeId();
				set("typeId", typeId);
				set("dataList",service.getListByType(typeId,null));
				set("dataTotalCount", service.getCountByType(typeId));
				set("showId", dictionaryId);
				//TODO #mmm 前端页面实现show效果
				render("index.html");
			}
			
		}
	}
	/**
	 * 除了自己以外的其它所有数据
	 */
	public void select(){
		renderJsonData(service.getListByType(getInt(0),null));
	}
	
	/**
	 * 新增
	 */
	public void add(){
		Integer typeId=getInt("typeId");
		if(notOk(typeId)) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		set("typeId", typeId);
		DictionaryType dictionaryType=dictionaryTypeService.findById(typeId);
		if(dictionaryType.getModeLevel()==DictionaryType.MODE_LEVEL_MUTIL) {
			set("needPidSelect",true);
		}
		render("add.html");
	}
	/**
	 * 新增
	 */
	public void addItem(){
		Integer typeId=getInt(0);
		Integer pid=getInt(1);
		if(notOk(typeId)||notOk(pid)) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		DictionaryType dictionaryType=dictionaryTypeService.findById(typeId);
		if(dictionaryType==null) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		if(dictionaryType.getModeLevel()==DictionaryType.MODE_LEVEL_MUTIL) {
			set("needPidSelect",true);
		}
		set("typeId",typeId);
		set("pid", pid);
		render("add.html");
	}
	/**
	 * 编辑
	 */
	public void edit(){
		Integer typeId=getInt(0);
		Integer id=getInt(1);
		if(notOk(typeId)||notOk(id)) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		Dictionary dictionary=service.findById(id);
		if(dictionary==null) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		DictionaryType dictionaryType=dictionaryTypeService.findById(typeId);
		if(dictionaryType==null) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		if(dictionaryType.getModeLevel()==DictionaryType.MODE_LEVEL_MUTIL) {
			set("needPidSelect",true);
		}
		set("dictionary",dictionary );
		set("typeId", typeId);
		set("pid", dictionary.getPid());
		render("edit.html");
	}
	/**
	 * 编辑
	 */
	public void editItem(){
		Integer typeId=getInt(0);
		Integer id=getInt(1);
		if(notOk(typeId)||notOk(id)) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		Dictionary dictionary=service.findById(id);
		if(dictionary==null) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		DictionaryType dictionaryType=dictionaryTypeService.findById(typeId);
		if(dictionaryType==null) {
			renderFormFail(Msg.PARAM_ERROR);
			return;
		}
		if(dictionaryType.getModeLevel()==DictionaryType.MODE_LEVEL_MUTIL) {
			set("needPidSelect",true);
		}
		set("typeId", typeId);
		set("dictionary",dictionary );
		set("pid", dictionary.getPid());
		render("edit.html");
	}
	/**
	 * 保存
	 */
	public void save(){
		Dictionary dictionary=getModel(Dictionary.class, "dictionary");
		renderJson(service.save(dictionary));
	}
	/**
	 * 更新
	 */
	public void update(){
		Dictionary dictionary=getModel(Dictionary.class, "dictionary");
		renderJson(service.update(dictionary));
	}
	/**
	 * 删除
	 */
	@Before(Tx.class)
	public void delete(){
		renderJson(service.deleteDictionaryById(getInt(0)));
	}
	/**
	 * 上移
	 */
	public void up(){
		renderJson(service.doUp(getInt(0)));
	}
	/**
	 * 下移
	 */
	public void down(){
		renderJson(service.down(getInt(0)));
	}
	/**
	 * 初始化顺序
	 */
	public void initRank(){
		renderJson(service.initRank(getInt("typeId")));
	}
	/**
	 * 清空分类下的字典数据
	 */
	public void clearByType(){
		renderJson(service.clearByType(getInt("typeId")));
	}
}
