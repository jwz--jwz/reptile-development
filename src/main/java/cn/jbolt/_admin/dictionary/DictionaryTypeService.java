package cn.jbolt._admin.dictionary;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.DictionaryType;
import cn.jbolt.common.model.SystemLog;

/**
 * 字典类型Service
 * @ClassName:  DictionaryTypeService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年11月9日   
 */
public class DictionaryTypeService extends JBoltBaseService<DictionaryType> {
	private DictionaryType dao = new DictionaryType().dao();
	@Inject
	private DictionaryService dictionaryService;
	@Override
	protected DictionaryType dao() {
		return dao;
	}
	/**
	 * 新增 保存
	 * @param type
	 * @return
	 */
	public Ret save(DictionaryType type) {
		if(type==null||isOk(type.getId())||notOk(type.getName())||notOk(type.getModeLevel())||notOk(type.getTypeKey())){
			return fail(Msg.PARAM_ERROR);
		}
		if(existsName(type.getName())){
			return fail("数据分类["+type.getName()+"]已经存在");
		}
		if(exists("type_key",type.getTypeKey())){
			return fail("数据分类标识Key["+type.getTypeKey()+"]已经存在");
		}
		boolean success=type.save();
		if(success){
			//添加日志
			addSaveSystemLog(type.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_DICTIONARY_TYPE,type.getName());
		}
		return ret(success);
	}
	/**
	 * 更新
	 * @param type
	 * @return
	 */
	public Ret update(DictionaryType type) {
		if(type==null||notOk(type.getId())||notOk(type.getName())||notOk(type.getModeLevel())||notOk(type.getTypeKey())){
			return fail(Msg.PARAM_ERROR);
		}
		DictionaryType db=findById(type.getId());
		if(db==null) {
			return fail(Msg.DATA_NOT_EXIST);
		}
		if(existsName(type.getName(), type.getId())){
			return fail("数据分类["+type.getName()+"]已经存在");
		}
		if(exists("type_key",type.getTypeKey(), type.getId())){
			return fail("数据分类标识Key["+type.getTypeKey()+"]已经存在");
		}
		boolean success=type.update();
		if(success){
			if(db.getTypeKey().equals(type.getTypeKey())==false) {
				//如果修改了typeKey就要去更新一下字典数据表的绑定了
				dictionaryService.updateTypeKey(type.getId(),type.getTypeKey());
			}
			addUpdateSystemLog(type.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_DICTIONARY_TYPE,type.getName());
		}
		return ret(success);
	}
	
	/**
	 * 删除字典分类
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		return deleteById(id, true);
	}
	/**
	 *  删除一个分类后额外处理
	 */
	@Override
	protected String afterDelete(DictionaryType type, Kv kv) {
		addDeleteSystemLog(type.getId(), JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_DICTIONARY_TYPE,type.getName());
		return null;
	}
	/**
	 * 检测是否可以删除
	 */
	@Override
	public String checkCanDelete(DictionaryType dictionaryType,Kv kv) {
		return checkInUse(dictionaryType,kv);
	}
	/**
	 * 检测是否被使用
	 */
	@Override
	public String checkInUse(DictionaryType dictionaryType,Kv kv) {
		boolean inUse=dictionaryService.checkTypeInUse(dictionaryType.getId());
		return inUse?"此类型下已经存在数据项!":null;
	}
	
	/**
	 * 通过typeKey获取字典分类
	 * @param typeKey
	 * @return
	 */
	public DictionaryType findByTypeKey(String typeKey) {
		return findFirst(Kv.by("type_key", typeKey));
	}

}
