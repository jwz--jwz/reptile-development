
package cn.jbolt._admin.globalconfig;

import java.util.Date;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;

import cn.jbolt._admin.user.UserService;
import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltConst;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.GlobalConfigKey;
import cn.jbolt.common.config.MainConfig;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.model.GlobalConfig;
import cn.jbolt.common.model.GlobalConfigType;
import cn.jbolt.common.model.SystemLog;
import cn.jbolt.common.model.User;
import cn.jbolt.common.util.CACHE;
import cn.jbolt.common.util.DateUtil;

/**
 * 全局配置 service
 * 
 * @author 小木 qq:909854136
 * @version 创建时间：2018年12月25日 下午11:18:26
 */
public class GlobalConfigService extends JBoltBaseService<GlobalConfig> {
	private GlobalConfig dao = new GlobalConfig().dao();
	@Inject
	private UserService userService;
	@Inject
	private GlobalConfigTypeService globalConfigTypeService;
	@Override
	protected GlobalConfig dao() {
		return dao;
	}
	
	/**
	 * 更新
	 * @param globalConfig
	 * @return
	 */
	public Ret update(GlobalConfig globalConfig) {
		if (globalConfig == null || notOk(globalConfig.getId()) || notOk(globalConfig.getConfigKey())|| notOk(globalConfig.getTypeId())
				||
				(globalConfig.getConfigKey().equals(GlobalConfigKey.SYSTEM_ADMIN_NAV_MENU_DEFAULT_ICON)==false&&notOk(globalConfig.getConfigValue()))
				) {
			return fail(Msg.PARAM_ERROR);
		}
		GlobalConfig db=findById(globalConfig.getId());
		if(db==null){return fail(Msg.DATA_NOT_EXIST);}
		GlobalConfigType type=CACHE.me.getGlobalConfigType(globalConfig.getTypeId());
		if(type==null) {return fail("关联类型数据不存在");}
		if(notOk(db.getTypeId())||db.getTypeId().intValue()!=globalConfig.getTypeId().intValue()) {
			globalConfig.setTypeKey(type.getTypeKey());
		}
		if(db.getBuiltIn()==null) {
			globalConfig.setBuiltIn(false);
		}else {
			globalConfig.setBuiltIn(db.getBuiltIn());
		}
		boolean success = globalConfig.update();
		if (success) {
			refreshMainConfig(globalConfig.getConfigKey());
			//增加日志
			addUpdateSystemLog(globalConfig.getId(), JBoltUserKit.getUserId(),SystemLog.TARGETTYPE_GLOBAL_CONFIG, globalConfig.getName());
		}
		return ret(success);
	}

	/**
	 * 判断与MainConfig有关的配置 更新调用
	 * @param configKey
	 */
	private void refreshMainConfig(String configKey) {
		switch (configKey) {
			case GlobalConfigKey.JBOLT_ACTION_REPORT_WRITER:
				MainConfig.configActionReportWriter();
				break;
			case GlobalConfigKey.JBOLT_AUTO_CACHE_LOG:
				MainConfig.configJBoltAutoCacheLog();
				break;
			case GlobalConfigKey.ASSETS_VERSION:
				MainConfig.configAssetsVersion();
				break;
			default:
				break;
		}
	}

	/**
	 * 处理valueType=null的组件
	 */
	private void processDatasIfValueTypeIsNull() {
		Sql sql=selectSql().isNull("value_type");
		List<GlobalConfig> configs=find(sql.toSql());
		if(isOk(configs)) {
			for(GlobalConfig config:configs) {
				switch (config.getConfigKey()) {
				case GlobalConfigKey.WECHAT_MP_SERVER_DOMAIN:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.WECHAT_WXA_SERVER_DOMAIN:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.WECHAT_ASSETS_SERVER_DOMAIN:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.SYSTEM_NAME:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_LOGO:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.SYSTEM_COPYRIGHT_COMPANY:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.SYSTEM_COPYRIGHT_LINK:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.JBOLT_ADMIN_STYLE:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.JBOLT_ADMIN_WITH_TABS:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.JBOLT_LOGIN_FORM_STYLE_GLASS:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.JBOLT_LOGIN_USE_CAPTURE:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.JBOLT_LOGIN_BGIMG_BLUR:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.JBOLT_LOGIN_CAPTURE_TYPE:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.JBOLT_LOGIN_BGIMG:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.JBOLT_LOGIN_NEST:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.JBOLT_ACTION_REPORT_WRITER:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.JBOLT_AUTO_CACHE_LOG:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.ASSETS_VERSION:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_H50:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_NAV_MENU_DEFAULT_ICON:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_GLOBAL_SEARCH_SHOW:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_LEFT_NAV_WIDTH:
					config.setValueType(GlobalConfig.TYPE_INT);
					break;
				case GlobalConfigKey.JBOLT_ADMIN_USER_KEEPLOGIN_SECONDS:
					config.setValueType(GlobalConfig.TYPE_INT);
					break;
				case GlobalConfigKey.JBOLT_ADMIN_USER_NOT_KEEPLOGIN_SECONDS:
					config.setValueType(GlobalConfig.TYPE_INT);
					break;
				case GlobalConfigKey.JBOLT_AUTO_LOCKSCREEN_SECONDS:
					config.setValueType(GlobalConfig.TYPE_INT);
					break;
				case GlobalConfigKey.JBOLT_LOGIN_TERMINAL_ONLYONE:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.JBOLT_ADMIN_LOGIN_HTML_FILE:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				case GlobalConfigKey.SYSTEM_DEPT_ENABLE:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				case GlobalConfigKey.SYSTEM_POST_ENABLE:
					config.setValueType(GlobalConfig.TYPE_BOOLEAN);
					break;
				default:
					config.setValueType(GlobalConfig.TYPE_STRING);
					break;
				}
			}
			batchUpdate(configs);
		}
		
	}
	/**
	 * 检测和初始化配置
	 */
	public void checkAndInit() {
		processDatasIfValueTypeIsNull();
		
		globalConfigTypeService.checkAndInitTypes();
		
		checkAndInitConfig(GlobalConfigKey.WECHAT_MP_SERVER_DOMAIN);
		checkAndInitConfig(GlobalConfigKey.WECHAT_WXA_SERVER_DOMAIN);
		checkAndInitConfig(GlobalConfigKey.WECHAT_ASSETS_SERVER_DOMAIN);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_NAME);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_ADMIN_LOGO);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_COPYRIGHT_COMPANY);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_COPYRIGHT_LINK);
		checkAndInitConfig(GlobalConfigKey.JBOLT_ADMIN_STYLE);
		checkAndInitConfig(GlobalConfigKey.JBOLT_ADMIN_WITH_TABS);
		checkAndInitConfig(GlobalConfigKey.JBOLT_LOGIN_FORM_STYLE_GLASS);
		checkAndInitConfig(GlobalConfigKey.JBOLT_LOGIN_USE_CAPTURE);
		checkAndInitConfig(GlobalConfigKey.JBOLT_LOGIN_BGIMG_BLUR);
		checkAndInitConfig(GlobalConfigKey.JBOLT_LOGIN_CAPTURE_TYPE);
		checkAndInitConfig(GlobalConfigKey.JBOLT_LOGIN_BGIMG);
		checkAndInitConfig(GlobalConfigKey.JBOLT_LOGIN_NEST);
		checkAndInitConfig(GlobalConfigKey.JBOLT_ACTION_REPORT_WRITER);
		checkAndInitConfig(GlobalConfigKey.JBOLT_AUTO_CACHE_LOG);
		checkAndInitConfig(GlobalConfigKey.ASSETS_VERSION);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_ADMIN_H50);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_ADMIN_NAV_MENU_DEFAULT_ICON);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_ADMIN_GLOBAL_SEARCH_SHOW);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_ADMIN_LEFT_NAV_WIDTH);
		checkAndInitConfig(GlobalConfigKey.JBOLT_ADMIN_USER_KEEPLOGIN_SECONDS);
		checkAndInitConfig(GlobalConfigKey.JBOLT_ADMIN_USER_NOT_KEEPLOGIN_SECONDS);
		checkAndInitConfig(GlobalConfigKey.JBOLT_AUTO_LOCKSCREEN_SECONDS);
		checkAndInitConfig(GlobalConfigKey.JBOLT_LOGIN_TERMINAL_ONLYONE);
		checkAndInitConfig(GlobalConfigKey.JBOLT_ADMIN_LOGIN_HTML_FILE);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_DEPT_ENABLE);
		checkAndInitConfig(GlobalConfigKey.SYSTEM_POST_ENABLE);
	}
	
	private GlobalConfigType getConfigType(String configKey) {
		GlobalConfigType type=null;
		switch (configKey) {
		case GlobalConfigKey.WECHAT_MP_SERVER_DOMAIN:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.WECHAT_DEV.getValue());
			break;
		case GlobalConfigKey.WECHAT_WXA_SERVER_DOMAIN:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.WECHAT_DEV.getValue());
			break;
		case GlobalConfigKey.WECHAT_ASSETS_SERVER_DOMAIN:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.WECHAT_DEV.getValue());
			break;
		case GlobalConfigKey.SYSTEM_NAME:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.SYSTEM_ADMIN_LOGO:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_UI.getValue());
			break;
		case GlobalConfigKey.SYSTEM_COPYRIGHT_COMPANY:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.SYSTEM_COPYRIGHT_LINK:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_ADMIN_STYLE:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_UI.getValue());
			break;
		case GlobalConfigKey.JBOLT_ADMIN_WITH_TABS:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_UI.getValue());
			break;
		case GlobalConfigKey.JBOLT_LOGIN_FORM_STYLE_GLASS:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_LOGIN_USE_CAPTURE:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_LOGIN_BGIMG_BLUR:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_LOGIN_CAPTURE_TYPE:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_LOGIN_BGIMG:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_LOGIN_NEST:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_ACTION_REPORT_WRITER:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.SYS_CONFIG.getValue());
			break;
		case GlobalConfigKey.JBOLT_AUTO_CACHE_LOG:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.SYS_CONFIG.getValue());
			break;
		case GlobalConfigKey.ASSETS_VERSION:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.SYS_CONFIG.getValue());
			break;
		case GlobalConfigKey.SYSTEM_ADMIN_H50:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_UI.getValue());
			break;
		case GlobalConfigKey.SYSTEM_ADMIN_NAV_MENU_DEFAULT_ICON:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_UI.getValue());
			break;
		case GlobalConfigKey.SYSTEM_ADMIN_GLOBAL_SEARCH_SHOW:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_UI.getValue());
			break;
		case GlobalConfigKey.SYSTEM_ADMIN_LEFT_NAV_WIDTH:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_UI.getValue());
			break;
		case GlobalConfigKey.JBOLT_ADMIN_USER_KEEPLOGIN_SECONDS:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_ADMIN_USER_NOT_KEEPLOGIN_SECONDS:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_AUTO_LOCKSCREEN_SECONDS:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_LOGIN_TERMINAL_ONLYONE:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.JBOLT_ADMIN_LOGIN_HTML_FILE:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.ADMIN_LOGIN.getValue());
			break;
		case GlobalConfigKey.SYSTEM_DEPT_ENABLE:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.SYS_CONFIG.getValue());
			break;
		case GlobalConfigKey.SYSTEM_POST_ENABLE:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.SYS_CONFIG.getValue());
			break;
		default:
			type=CACHE.me.getGlobalConfigTypeByKey(JBoltGlobalConfigTypeBuiltInEnum.SYS_CONFIG.getValue());
			break;
		}
		
		return type;
	}
	/**
	 * 检查并初始化全局配置表数据
	 * @param configKey
	 */
	public void checkAndInitConfig(String configKey) {
		GlobalConfig config = findFirst(Kv.by("config_key", configKey));
		GlobalConfigType type=getConfigType(configKey);
		if (config==null) {
			config= new GlobalConfig();
			config.setConfigKey(configKey);
			config.setBuiltIn(true);
			switch (configKey) {
			case GlobalConfigKey.WECHAT_MP_SERVER_DOMAIN:
				config.setName("微信公众号_服务器配置_根URL");
				config.setConfigValue(MainConfig.DOMAIN+"/wx/msg");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.WECHAT_WXA_SERVER_DOMAIN:
				config.setName("微信小程序_客服消息推送配置_根URL");
				config.setConfigValue(MainConfig.DOMAIN+"/wxa/msg");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.WECHAT_ASSETS_SERVER_DOMAIN:
				config.setName("微信_静态资源_根URL");
				config.setConfigValue(MainConfig.DOMAIN);
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.SYSTEM_NAME:
				config.setName("系统名称");
				config.setConfigValue("JBolt极速开发平台");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.SYSTEM_ADMIN_LOGO:
				config.setName("系统后台主页LOGO");
				config.setConfigValue("assets/img/logo.png");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.SYSTEM_COPYRIGHT_COMPANY:
				config.setName("系统版权所有人");
				config.setConfigValue("©JBolt(JBOLT.CN)");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.SYSTEM_COPYRIGHT_LINK:
				config.setName("系统版权所有人的网址链接");
				config.setConfigValue("http://jbolt.cn");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.JBOLT_ADMIN_STYLE:
				config.setName("系统Admin后台样式");
				config.setConfigValue("default");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.JBOLT_ADMIN_WITH_TABS:
				config.setName("系统Admin后台是否启用多选项卡");
				config.setConfigValue("false");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.JBOLT_LOGIN_FORM_STYLE_GLASS:
				config.setName("系统登录页面是否启用透明玻璃风格");
				config.setConfigValue("false");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.JBOLT_LOGIN_USE_CAPTURE:
				config.setName("系统登录页面是否启用验证码");
				config.setConfigValue("true");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.JBOLT_LOGIN_BGIMG_BLUR:
				config.setName("系统登录页面背景图是否启用模糊风格");
				config.setConfigValue("false");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.JBOLT_LOGIN_CAPTURE_TYPE:
				config.setName("系统登录页验证码类型");
				config.setConfigValue(GlobalConfig.CAPTCHA_TYPE_DEFAULT);
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.JBOLT_LOGIN_BGIMG:
				config.setName("系统登录页背景图");
				config.setConfigValue("assets/css/img/login_bg.jpg");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.JBOLT_LOGIN_NEST:
				config.setName("系统登录页是否开启线条特效");
				config.setConfigValue("true");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.JBOLT_ACTION_REPORT_WRITER:
				config.setName("系统 Action Report输出方式");
				config.setConfigValue("sysout");
				config.setValueType(GlobalConfig.TYPE_STRING);
				MainConfig.ACTION_REPORT_WRITER="sysout";
				break;
			case GlobalConfigKey.JBOLT_AUTO_CACHE_LOG:
				config.setName("系统自动缓存Debug日志");
				config.setConfigValue("false");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				MainConfig.JBOLT_AUTO_CACHE_LOG=false;
				break;
			case GlobalConfigKey.ASSETS_VERSION:
				config.setName("系统静态资源版本号");
				config.setConfigValue(DateUtil.format(new Date(), DateUtil.YMDHMSS2));
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.SYSTEM_ADMIN_H50:
				config.setName("系统后台整体样式高度使用H50");
				config.setConfigValue("false");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.SYSTEM_ADMIN_NAV_MENU_DEFAULT_ICON:
				config.setName("系统后台导航菜单默认图标");
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.SYSTEM_ADMIN_GLOBAL_SEARCH_SHOW:
				config.setName("系统后台全局搜索输入框是否启用");
				config.setConfigValue("true");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.SYSTEM_ADMIN_LEFT_NAV_WIDTH:
				config.setName("系统后台左侧导航宽度");
				config.setConfigValue("220");
				config.setValueType(GlobalConfig.TYPE_INT);
				break;
			case GlobalConfigKey.JBOLT_ADMIN_USER_KEEPLOGIN_SECONDS:
				config.setName("后台用户保持登录时Cookie时长(秒)");
				config.setConfigValue(JBoltConst.JBOLT_KEEPLOGIN_DEFAULT_SECONDS+"");
				config.setValueType(GlobalConfig.TYPE_INT);
				break;
			case GlobalConfigKey.JBOLT_ADMIN_USER_NOT_KEEPLOGIN_SECONDS:
				config.setName("后台用户不保持登录时Cookie时长(秒)");
				config.setConfigValue(JBoltConst.JBOLT_NOT_KEEPLOGIN_DEFAULT_SECONDS+"");
				config.setValueType(GlobalConfig.TYPE_INT);
				break;
			case GlobalConfigKey.JBOLT_AUTO_LOCKSCREEN_SECONDS:
				config.setName("用户多长时间(秒)无操作自动锁屏");
				config.setConfigValue(JBoltConst.JBOLT_AUTO_LOCKSCREEN_DEFAULT_SECONDS+"");
				config.setValueType(GlobalConfig.TYPE_INT);
				break;
			case GlobalConfigKey.JBOLT_LOGIN_TERMINAL_ONLYONE:
				config.setName("同一账号不能多端登录");
				config.setConfigValue("false");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.JBOLT_ADMIN_LOGIN_HTML_FILE:
				config.setName("登录页文件配置");
				config.setConfigValue(JBoltConst.JBOLT_ADMIN_LOGIN_DEFAULT_FILE);
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			case GlobalConfigKey.SYSTEM_DEPT_ENABLE:
				config.setName("启用部门");
				config.setConfigValue("true");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			case GlobalConfigKey.SYSTEM_POST_ENABLE:
				config.setName("启用岗位");
				config.setConfigValue("true");
				config.setValueType(GlobalConfig.TYPE_BOOLEAN);
				break;
			default:
				config.setValueType(GlobalConfig.TYPE_STRING);
				break;
			}
			if(type!=null) {
				config.setTypeKey(type.getTypeKey());
				config.setTypeId(type.getId());
			}
			Integer userId = JBoltUserKit.getUserIdAs();
			if(notOk(userId)) {
				User user=userService.getOneSystemAdmin();
				if(user!=null) {
					userId=user.getId();
				}
			}
			config.setObjectUserId(userId);
			config.setObjectUpdateUserId(userId);
			config.save();
		}else {
			boolean needUpdateValue=false;
			if(notOk(config.getConfigValue())) {
				switch (configKey) {
				case GlobalConfigKey.WECHAT_MP_SERVER_DOMAIN:
					config.setConfigValue(MainConfig.DOMAIN+"/wx/msg");
					break;
				case GlobalConfigKey.WECHAT_WXA_SERVER_DOMAIN:
					config.setConfigValue(MainConfig.DOMAIN+"/wxa/msg");
					break;
				case GlobalConfigKey.WECHAT_ASSETS_SERVER_DOMAIN:
					config.setConfigValue(MainConfig.DOMAIN);
					break;
				case GlobalConfigKey.SYSTEM_NAME:
					config.setConfigValue("JBolt极速开发平台");
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_LOGO:
					config.setConfigValue("assets/img/logo.png");
					break;
				case GlobalConfigKey.SYSTEM_COPYRIGHT_COMPANY:
					config.setConfigValue("©JBolt(JBOLT.CN)");
					break;
				case GlobalConfigKey.SYSTEM_COPYRIGHT_LINK:
					config.setConfigValue("http://jbolt.cn");
					break;
				case GlobalConfigKey.JBOLT_ADMIN_STYLE:
					config.setConfigValue("default");
					break;
				case GlobalConfigKey.JBOLT_ADMIN_WITH_TABS:
					config.setConfigValue("false");
					break;
				case GlobalConfigKey.JBOLT_LOGIN_FORM_STYLE_GLASS:
					config.setConfigValue("false");
					break;
				case GlobalConfigKey.JBOLT_LOGIN_USE_CAPTURE:
					config.setConfigValue("true");
					break;
				case GlobalConfigKey.JBOLT_LOGIN_BGIMG_BLUR:
					config.setConfigValue("false");
					break;
				case GlobalConfigKey.JBOLT_LOGIN_CAPTURE_TYPE:
					config.setConfigValue(GlobalConfig.CAPTCHA_TYPE_DEFAULT);
					break;
				case GlobalConfigKey.JBOLT_LOGIN_BGIMG:
					config.setConfigValue("assets/css/img/login_bg.jpg");
					break;
				case GlobalConfigKey.JBOLT_LOGIN_NEST:
					config.setConfigValue("true");
					break;
				case GlobalConfigKey.JBOLT_ACTION_REPORT_WRITER:
					config.setConfigValue("sysout");
					MainConfig.ACTION_REPORT_WRITER="sysout";
					break;
				case GlobalConfigKey.JBOLT_AUTO_CACHE_LOG:
					config.setConfigValue("false");
					MainConfig.JBOLT_AUTO_CACHE_LOG=false;
					break;
				case GlobalConfigKey.ASSETS_VERSION:
					config.setConfigValue(DateUtil.format(new Date(), DateUtil.YMDHMSS2));
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_H50:
					config.setConfigValue("false");
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_NAV_MENU_DEFAULT_ICON:
					needUpdateValue=false;
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_GLOBAL_SEARCH_SHOW:
					config.setConfigValue("true");
					break;
				case GlobalConfigKey.SYSTEM_ADMIN_LEFT_NAV_WIDTH:
					config.setConfigValue("220");
					break;
				case GlobalConfigKey.JBOLT_ADMIN_USER_KEEPLOGIN_SECONDS:
					config.setConfigValue(JBoltConst.JBOLT_KEEPLOGIN_DEFAULT_SECONDS+"");
					break;
				case GlobalConfigKey.JBOLT_ADMIN_USER_NOT_KEEPLOGIN_SECONDS:
					config.setConfigValue(JBoltConst.JBOLT_NOT_KEEPLOGIN_DEFAULT_SECONDS+"");
					break;
				case GlobalConfigKey.JBOLT_AUTO_LOCKSCREEN_SECONDS:
					config.setConfigValue(JBoltConst.JBOLT_AUTO_LOCKSCREEN_DEFAULT_SECONDS+"");
					break;
				case GlobalConfigKey.JBOLT_LOGIN_TERMINAL_ONLYONE:
					config.setConfigValue("false");
					break;
				case GlobalConfigKey.JBOLT_ADMIN_LOGIN_HTML_FILE:
					config.setConfigValue("login.html");
					break;
				case GlobalConfigKey.SYSTEM_DEPT_ENABLE:
					config.setConfigValue("true");
					break;
				case GlobalConfigKey.SYSTEM_POST_ENABLE:
					config.setConfigValue("true");
					break;
				default:
					break;
				}
			}
			boolean needTypeInfo=false;
			if((config.getBuiltIn()==null||notOk(config.getTypeId())||notOk(config.getTypeKey()))&&type!=null) {
				config.setTypeKey(type.getTypeKey());
				config.setTypeId(type.getId());
				config.setBuiltIn(true);
				needTypeInfo=true;
			}
			
			if(needTypeInfo||needUpdateValue) {
				config.update();
			}
		}

	}
	/**
	 * 根据configKey获取全局配置
	 * @param configKey
	 * @return
	 */
	public GlobalConfig getByConfigKey(String configKey) {
		return findFirst(Kv.by("config_key", configKey));
	}
	/**
	 * 更新jbolt Style
	 * @param style
	 * @return
	 */
	public Ret updateJboltStyle(String style) {
		GlobalConfig globalConfig=getByConfigKey(GlobalConfigKey.JBOLT_ADMIN_STYLE);
		if(globalConfig==null) {return fail(Msg.DATA_NOT_EXIST);}
		globalConfig.setObjectUpdateUserId(JBoltUserKit.getUserId());
		globalConfig.setConfigValue(style);
		boolean success = globalConfig.update();
		if (success) {
			//增加日志
			addUpdateSystemLog(globalConfig.getId(),JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_GLOBAL_CONFIG, globalConfig.getName());
		}
		return ret(success);
	}
	/**
	 * 更换 assets version
	 */
	public Ret changeAssetsVersion() {
		
		checkAndInitConfig(GlobalConfigKey.ASSETS_VERSION);
		GlobalConfig globalConfig=getByConfigKey(GlobalConfigKey.ASSETS_VERSION);
		String configValue=DateUtil.format(new Date(), DateUtil.YMDHMSS2);
		globalConfig.setConfigValue(configValue);
		boolean success = globalConfig.update();
		if (success) {
			refreshMainConfig(globalConfig.getConfigKey());
			//增加日志
			addUpdateSystemLog(globalConfig.getId(),JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_GLOBAL_CONFIG, globalConfig.getName());
		}
		return ret(success);
	}
	/**
	 * 后台查询管理使用
	 * 获取对应条件数据List
	 * @param typeId
	 * @param builtIn
	 * @param keywords
	 * @return
	 */
	public List<GlobalConfig> getAdminList(Integer typeId,Boolean builtIn, String keywords) {
		Kv parasKv=Kv.create();
		if(isOk(typeId)) {
			parasKv.set("type_id", typeId);
		}
		if(builtIn!=null) {
			parasKv.set("built_in", builtIn?TRUE:FALSE);
		}
		return getCommonListByKeywords(keywords, "id", "asc", "name,config_key,config_value,type_key", parasKv);
	}
	
	/**
	 * 切换Boolean类型数据值
	 * @param id
	 * @return
	 */
	public Ret toggleBooleanValue(Integer id) {
		if(notOk(id)) {return fail(Msg.PARAM_ERROR);}
		GlobalConfig config=findById(id);
		if(config==null) {
			return fail(Msg.DATA_NOT_EXIST);
		}
		if(config.getValueType()==null||!config.getValueType().equalsIgnoreCase(GlobalConfig.TYPE_BOOLEAN)) {return fail("数据类型非Boolean不能执行此操作");}
		boolean isTrue=Boolean.parseBoolean(config.getConfigValue());
		config.setConfigValue(isTrue?"false":"true");
		boolean success=config.update();
		return success?success(config,Msg.SUCCESS):FAIL;
	}
	/**
	 * 检测类型存在性
	 * @param typeId
	 * @param typeKey 
	 * @return
	 */
	public boolean checkTypeInUse(Integer typeId, String typeKey) {
		if(notOk(typeId)||notOk(typeKey)) {return false;}
		return exist(selectSql().eq("type_id", typeId).eq("type_key", typeKey));
	}
	/**
	 * 更新TypeKey
	 * @param typeId
	 * @param typekey
	 */
	public void updateTypeKey(Integer typeId,String typekey) {
		update(updateSql().set("type_key", typekey).eq("type_id", typeId));
	}
	/**
	 * 根据TypeKey更新typeId
	 * @param typeKey
	 */
	public void updateTypeId(String typeKey,Integer typeId) {
		update(updateSql().set("type_id", typeId).eq("type_key", typeKey));
	}
	
	/**
	 * 清空重来
	 * @return
	 */
	public Ret clearReset() {
		clearCache();
		globalConfigTypeService.clearCache();
		update(updateSql().set("type_id", null).set("config_value",null));
		checkAndInit();
		return SUCCESS;
	}
	

	/**
	 * 更新
	 * @param globalConfig
	 * @return
	 */
	public Ret saveCustomConfig(GlobalConfig globalConfig) {
		if (globalConfig == null || isOk(globalConfig.getId()) || notOk(globalConfig.getConfigKey())|| notOk(globalConfig.getTypeId())) {
			return fail(Msg.PARAM_ERROR);
		}
		GlobalConfigType type=CACHE.me.getGlobalConfigType(globalConfig.getTypeId());
		if(type==null) {return fail("关联类型数据不存在");}
		globalConfig.setTypeKey(type.getTypeKey());
		globalConfig.setBuiltIn(false);
		boolean success = globalConfig.save();
		if (success) {
			//增加日志
			addSaveSystemLog(globalConfig.getId(),JBoltUserKit.getUserId(), SystemLog.TARGETTYPE_GLOBAL_CONFIG, globalConfig.getName());
		}
		return ret(success);
	}

	/**
	 * 更新自定义参数
	 * @param globalConfig
	 * @return
	 */
	public Ret updateCustomConfig(GlobalConfig globalConfig) {
		if (globalConfig == null || notOk(globalConfig.getId()) || notOk(globalConfig.getConfigKey())|| notOk(globalConfig.getTypeId())) {
			return fail(Msg.PARAM_ERROR);
		}
		GlobalConfig db=findById(globalConfig.getId());
		if(db==null){return fail(Msg.DATA_NOT_EXIST);}
		GlobalConfigType type=CACHE.me.getGlobalConfigType(globalConfig.getTypeId());
		if(type==null) {return fail("关联类型数据不存在");}
		if(notOk(db.getTypeId())||db.getTypeId().intValue()!=globalConfig.getTypeId().intValue()) {
			globalConfig.setTypeKey(type.getTypeKey());
		}
		globalConfig.setBuiltIn(false);
		boolean success = globalConfig.update();
		if (success) {
			//增加日志
			addUpdateSystemLog(globalConfig.getId(), JBoltUserKit.getUserId(),SystemLog.TARGETTYPE_GLOBAL_CONFIG, globalConfig.getName());
		}
		return ret(success);
	}
	/**
	 * 删除自定义参数配置
	 * @param id
	 * @return
	 */
	public Ret deleteCustomConfig(Integer id) {
		return deleteById(id,true);
	}
	@Override
	protected String afterDelete(GlobalConfig config,Kv kv) {
		addDeleteSystemLog(config.getId(), JBoltUserKit.getUserId(),SystemLog.TARGETTYPE_GLOBAL_CONFIG, config.getName());
		return null;
	}
	@Override
	public String checkCanDelete(GlobalConfig config,Kv kv) {
		if(config.getBuiltIn()!=null&&config.getBuiltIn()==true) {
			return "此参数为系统内置参数不可删除";
		}
		return null;
	}
	
}
