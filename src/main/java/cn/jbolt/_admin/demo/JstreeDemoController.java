package cn.jbolt._admin.demo;

import com.jfinal.aop.Inject;
import com.jfinal.core.Path;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.admin.mall.goodscategory.back.GoodsBackCategoryService;
import cn.jbolt.base.JBoltBaseController;
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
@Path(value = "/demo/jstree",viewPath = "/jstree")
public class JstreeDemoController extends JBoltBaseController {
	@Inject
	private GoodsBackCategoryService service;
	public void index() {
		render("index.html");
	}
	public void datas() {
		renderJsonData(service.getEnableTree(getInt("selectId",getInt(0,0)),getInt("openLevel",0)));
	}
	public void asyncDatas() {
		renderJsonData(service.getEnableTreeByPid(getInt("selectId",getInt(0,0)),getInt("pid",0)));
	}
}
