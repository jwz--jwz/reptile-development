package cn.jbolt._admin.demo;

import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
@UnCheckIfSystemAdmin
public class FullcalendarDemoController extends JBoltBaseController {
	
	public void index() {
		render("index.html");
	}
	
}
