package cn.jbolt._admin.demo;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
/**
 * Form表单和控件
 * @ClassName:  FormDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月11日   
 *    
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class FormDemoController extends JBoltBaseController {
	public void index() {
		render("index.html");
	}
}
