package cn.jbolt._admin.demo.export;

import java.awt.Font;
import java.io.ByteArrayOutputStream;
import java.util.List;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.TableRowAlign;
import org.apache.poi.xwpf.usermodel.TableWidthType;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Kv;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.poi.word.TableUtil;
import cn.hutool.poi.word.Word07Writer;
import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt._admin.updatemgr.JBoltVersionService;
import cn.jbolt.admin.mall.brand.BrandService;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.model.Brand;
import cn.jbolt.common.model.JboltVersion;
import cn.jbolt.common.model.User;
import cn.jbolt.common.poi.word.JBoltWord;
import cn.jbolt.common.util.DateUtil;
/**
 * word导出
 * @ClassName:  WordDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月15日   
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class WordDemoController extends JBoltBaseController {
	@Inject
	private JBoltVersionService jBoltVersionService;
	@Inject
	private BrandService brandService;
	
	public void index() {
		render("word.html");
	}
	
	/**
	 * 通过模板 导出列表
	 */
	public void exportWord1() {
		//1、准备原数据
		List<JboltVersion> versions = jBoltVersionService.findAll();
		
		//2、准备JBoltWord
		JBoltWord jBoltWord=JBoltWord.useTpl("versions.xml")//加载模板
				.setDatas(Kv.by("datas", versions))//传进去数据
				.setFileName("版本报告");//设置导出文件名
		
		//3、执行数据流导出到客户端
		renderBytesToWordDocFile(jBoltWord);
	}
	
	/**
	 * 通过模板 导出档案
	 */
	public void exportWord2() {
		//1、准备原数据
		User user= JBoltUserKit.getUser();
		
		//2、准备JBoltWord
		JBoltWord jBoltWord=JBoltWord.useTpl("user.xml")//加载模板
				.setDatas(Kv.by("user", user))//传进去数据
				.setFileName("用户档案");//设置导出文件名
		
		//3、执行数据流导出到客户端
		renderBytesToWordDocFile(jBoltWord);
	}
	
	/**
	 * 通过模板 导出列表 带图版
	 */
	public void exportWord3() {
		//1、准备原数据
		List<Brand> brands = brandService.findAll();
		
		//2、准备JBoltWord
		JBoltWord jBoltWord=JBoltWord.useTpl("brands.xml")//加载模板
				.setDatas(Kv.by("datas", brands))//传进去数据
				.setFileName("品牌列表");//设置导出文件名
		
		//3、执行数据流导出到客户端
		renderBytesToWordDocFile(jBoltWord);
	}
	
	/**
	 * 通过模板 导出列表
	 */
	public void exportWord4() {
		//1、准备原数据
		List<JboltVersion> versions = jBoltVersionService.getAdminList("id", "asc", getKeywords(), getStartTime(), getEndTime());
		
		//2、准备JBoltWord
		JBoltWord jBoltWord=JBoltWord.useTpl("versions.xml")//加载模板
				.setDatas(Kv.by("datas", versions))//传进去数据
				.setFileName("版本报告带Form参数");//设置导出文件名
		
		//3、执行数据流导出到客户端
		renderBytesToWordDocFile(jBoltWord);
	}
	
	
	/**
	 * 通过POI API 导出列表 带图版
	 */
	public void exportWord5() {
		//1、准备原数据
		List<JboltVersion> versions = jBoltVersionService.findAll();
		//2、创建word doc文档
		XWPFDocument doc=new XWPFDocument();
		Word07Writer wordWriter=new Word07Writer(doc);
		//3、写标题
		wordWriter.addText(ParagraphAlignment.CENTER, new Font("黑体", Font.PLAIN, 20), "JBolt版本报告");
		//4、创建表格
		XWPFTable table=TableUtil.createTable(doc);
		//5、创建表格标题行并写出标题
		XWPFTableRow titleRow=table.createRow();
		String[] title=new String[] {"序号","版本号","发布时间","是否最新"};
		TableUtil.writeRow(titleRow, CollUtil.toList(title));
		titleRow.getCell(0).setWidthType(TableWidthType.PCT);
		titleRow.getCell(1).setWidthType(TableWidthType.PCT);
		titleRow.getCell(2).setWidthType(TableWidthType.PCT);
		titleRow.getCell(3).setWidthType(TableWidthType.PCT);
		titleRow.getCell(0).setWidth("10%");
		titleRow.getCell(1).setWidth("30%");
		titleRow.getCell(2).setWidth("30%");
		titleRow.getCell(3).setWidth("30%");

		//6、循环写表格数据
		int index=0;
		XWPFTableRow row;
		for(JboltVersion version:versions) {
			index++;
			row=table.createRow();
			TableUtil.writeRow(row, CollUtil.toList(new String[] {index+"",version.getVersion(),DateUtil.format(version.getPublishTime(), DateUtil.YMDHMS),version.getIsNew()?"是":"否"}));
			row.getCell(0).setWidthType(TableWidthType.PCT);
			row.getCell(1).setWidthType(TableWidthType.PCT);
			row.getCell(2).setWidthType(TableWidthType.PCT);
			row.getCell(3).setWidthType(TableWidthType.PCT);
			row.getCell(0).setWidth("10%");
			row.getCell(1).setWidth("30%");
			row.getCell(2).setWidth("30%");
			row.getCell(3).setWidth("30%");
		}
		table.setWidthType(TableWidthType.PCT);
		table.setWidth("100%");
		table.setTableAlignment(TableRowAlign.RIGHT);
		
		//7、转为byte[]
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		wordWriter.flush(os, true);
		wordWriter.close();
		//8、执行数据流导出到客户端
		renderBytesToWordDocxFile(os.toByteArray(),"JBolt 使用 API生成 Word");
	}
	
	
}
