package cn.jbolt._admin.demo.export;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.HorizontalAlignment;

import com.jfinal.aop.Inject;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt._admin.updatemgr.JBoltVersionService;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.model.JboltVersion;
import cn.jbolt.common.poi.excel.JBoltExcel;
import cn.jbolt.common.poi.excel.JBoltExcelHeader;
import cn.jbolt.common.poi.excel.JBoltExcelMerge;
import cn.jbolt.common.poi.excel.JBoltExcelPositionChange;
import cn.jbolt.common.poi.excel.JBoltExcelPositionData;
import cn.jbolt.common.poi.excel.JBoltExcelSheet;
import cn.jbolt.common.poi.excel.JBoltExcelUtil;
import cn.jbolt.common.util.CACHE;
/**
 * Excel导出
 * @ClassName:  ExcelDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月15日   
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class ExcelDemoController extends JBoltBaseController {
	@Inject
	private JBoltVersionService jBoltVersionService;
	public void index() {
		render("excel.html");
	}
	
	/**
	 * 普通导出列表
	 */
	public void exportExcel1() {
		//1、准备数据
		List<JboltVersion> jboltVersions=jBoltVersionService.findAll();
		//2、创建JBoltExcel
		JBoltExcel jBoltExcel=JBoltExcel
				.create()//创建JBoltExcel
				.addSheet(//设置sheet
						JBoltExcelSheet.create("version数据")//创建sheet
						.setMerges(JBoltExcelMerge.create(5, "Jbolt Version 数据导出"))
						.setHeaders(2,//sheet里添加表头
								JBoltExcelHeader.create("version", "版本号",20),
								JBoltExcelHeader.create("publish_time", "发布时间",30),
								JBoltExcelHeader.create("create_time", "创建时间",30),
								JBoltExcelHeader.create("is_new", "是否最新",20),
								JBoltExcelHeader.create("user_username", "操作用户",20)
								)
						.setDataChangeHandler((data,index)->{//设置数据转换处理器
							//将user_id转为user_name
			    			data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
							data.changeBooleanToStr("is_new", "是", "否");
						})
//						.setStyleHandler((excelWriter)->{
//							//设置样式处理器
//							excelWriter.getStyleSet().getHeadCellStyle().setAlignment(HorizontalAlignment.RIGHT);
//						})
						.setModelDatas(3, jboltVersions)//设置数据
						)
				.setFileName("普通列表导出");
		//3、导出
		renderBytesToExcelXlsFile(jBoltExcel);
		
	}
	/**
	 * 导出列表 设置合并复杂表头
	 */
	public void exportExcel2() {
		//1、准备数据
		List<JboltVersion> jboltVersions=jBoltVersionService.findAll();
		//2、创建JBoltExcel
		JBoltExcel jBoltExcel=JBoltExcel
				.create()//创建JBoltExcel
				.addSheet(//设置sheet
						JBoltExcelSheet.create("version数据")//创建sheet
						.setMerges(JBoltExcelMerge.create(5, "Jbolt Version 数据导出"))
						.setHeaders(//设置表头映射
								JBoltExcelHeader.create("version", "版本号",20),
								JBoltExcelHeader.create("publish_time", "发布时间",30),
								JBoltExcelHeader.create("create_time", "创建时间",30),
								JBoltExcelHeader.create("is_new", "是否最新",20),
								JBoltExcelHeader.create("user_username", "操作用户",20)
						 )
						//设置单元格合并区域
						.setMergeAsHeader(true)
						.setMerges(
								JBoltExcelMerge.create(1, 1, 1,5, "JBolt版本信息",true),
								JBoltExcelMerge.create(2, 3, 1,1, "版本号",true),
								JBoltExcelMerge.create(2, 2, 2,3, "时间",true),
								JBoltExcelMerge.create(2, 3, 4,4, "是否最新版",true),
								JBoltExcelMerge.create(2, 3, 5,5, "用户名",true),
								JBoltExcelMerge.create(3, 3, 2,2, "发布时间",true),
								JBoltExcelMerge.create(3, 3, 3,3, "记录时间",true)
								)
						.setDataChangeHandler((data,index)->{//设置数据转换处理器
							//将user_id转为user_name
			    			data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
							data.changeBooleanToStr("is_new", "是", "否");
						})
						.setModelDatas(4, jboltVersions)//设置数据
					)
				.setFileName("JBolt Version 合并表头导出");
		//3、导出
		renderBytesToExcelXlsFile(jBoltExcel);
		
	}
	
	

	/**
	 * 导出列表 通过加载excel模板后导出数据
	 */
	public void exportExcel3() {
		//1、准备数据
		List<JboltVersion> jboltVersions=jBoltVersionService.findAll();
		//2、创建JBoltExcel
		JBoltExcel jBoltExcel=JBoltExcel
				.createByTpl("jboltversiontpl.xls")//创建JBoltExcel 从模板加载创建
				.addSheet(//设置sheet
						JBoltExcelSheet.create("versions")//创建sheet name保持与模板中的sheet一致
						.setHeaders(//sheet里添加表头
								JBoltExcelHeader.create("version", "版本号",20),
								JBoltExcelHeader.create("publish_time", "发布时间",30),
								JBoltExcelHeader.create("create_time", "创建时间",30),
								JBoltExcelHeader.create("is_new", "是否最新",20),
								JBoltExcelHeader.create("user_username", "操作用户",20)
								)
						.setDataChangeHandler((data,index)->{//设置数据转换处理器
							//将user_id转为user_name
							data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
							data.changeBooleanToStr("is_new", "是", "否");
						})
						.setModelDatas(4, jboltVersions)//设置数据
						)
				.setFileName("从模板加载并导出");
		//3、导出
		renderBytesToExcelXlsFile(jBoltExcel);
	}
	
	/**
	 * 通过加载excel模板后 定位输出指定单元格数据
	 */
	public void exportExcel4() {
		//1、准备数据
		List<JBoltExcelPositionData> excelPositionDatas=new ArrayList<JBoltExcelPositionData>();
		for(int i=0;i<31;i++) {
			excelPositionDatas.add(JBoltExcelPositionData.create(2+i,5,1+i));
			excelPositionDatas.add(JBoltExcelPositionData.create(2+i,6,2+i));
			excelPositionDatas.add(JBoltExcelPositionData.create(2+i,7,1+i));
		}
		
		excelPositionDatas.add(JBoltExcelPositionData.create("L",2,6));
		excelPositionDatas.add(JBoltExcelPositionData.create("M:2",7));
		excelPositionDatas.add(JBoltExcelPositionData.create("N2",8));
		for(int i=0;i<24;i++) {
			excelPositionDatas.add(JBoltExcelPositionData.create(2+i,"L",6+i));
			excelPositionDatas.add(JBoltExcelPositionData.create("M:"+(2+i),7+i));
			excelPositionDatas.add(JBoltExcelPositionData.create("N"+(2+i),8+i));
		}
		
		//2、创建JBoltExcel
		JBoltExcel jBoltExcel=JBoltExcel
				.createByTpl("testtpl.xls")//创建JBoltExcel 从模板加载创建
				.addSheet(//设置sheet
						JBoltExcelSheet.create("datas")//创建sheet name保持与模板中的sheet一致
						.setPositionDatas(excelPositionDatas)//设置定位数据
						)
				.setFileName("从模板加载并导出定位数据");
		//3、导出
		renderBytesToExcelXlsFile(jBoltExcel);
	}
	
	/**
	 * 导出列表 定位输出指定单元格公式
	 */
	public void exportExcel5() {
		//1、准备数据
		List<JboltVersion> jboltVersions=jBoltVersionService.findAll();
		//2、准备公式
		//数据开始行（真实的从1开始）
		int dataStartRow=3;
		int size=jboltVersions.size();
		
		//定义需要坐标定位公式的具体坐标点(真实的从1开始)
		int formluaCol=6;//第6列
		int formluaRow=dataStartRow+size;//计算出第几行
		//转为字母列标识
		String colStr=JBoltExcelUtil.colNumToStr(formluaCol);
		//处理好开头和结尾需要公式包含的数据行（真实的从1开始）
		String start=colStr+dataStartRow;
		String end=colStr+(formluaRow-1);
		//创建一个坐标定位数据作为公式（真实的从1开始）
		JBoltExcelPositionData formlua=JBoltExcelPositionData.create(formluaRow, formluaCol, "SUM("+start+":"+end+")",true);
		
		//3、创建JBoltExcel
		JBoltExcel jBoltExcel=JBoltExcel
				.create()
				.addSheet(//设置sheet
						JBoltExcelSheet.create("带计算公式")
						.setHeaders(2,//sheet里添加表头 放在第二行 第一行是合并大标题
								JBoltExcelHeader.create("version", "版本号",20),
								JBoltExcelHeader.create("publish_time", "发布时间",30),
								JBoltExcelHeader.create("create_time", "创建时间",30),
								JBoltExcelHeader.create("is_new", "是否最新",20),
								JBoltExcelHeader.create("user_username", "操作用户",20),
								JBoltExcelHeader.create("test_data", "测试公式计算",15)//这是一列公式计算测试数据
								)
						//设置单元格合并区域 标题 第一行
	    		    	.setMerges(JBoltExcelMerge.create("A","F",1, 1, "测试-带公式"))
	    		    	//特殊数据转换器
	    		    	.setDataChangeHandler((data,index) ->{
	    		    		data.put("test_data", index);
							//将user_id转为user_name
							data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
							data.changeBooleanToStr("is_new", "是", "否");
						})
						.setModelDatas(dataStartRow, jboltVersions)//设置数据
						.addCellFormluas(formlua)//添加公式 一个用add 多个用set list;
						)
				.setFileName("带公式导出");
		
		//4、导出
		renderBytesToExcelXlsFile(jBoltExcel);
	}
	
	
	/**
	 * 导出列表 通过加载excel模板后导出数据 模板下方部分区域做移动位置处理
	 */
	public void exportExcel6() {
		//1、准备数据
		List<JboltVersion> jboltVersions=jBoltVersionService.findAll();
		int dataSize=jboltVersions.size();
		//2、创建JBoltExcel
		JBoltExcel jBoltExcel=JBoltExcel
				.createByTpl("jboltversiontpl_2.xls")//创建JBoltExcel 从模板加载创建
				.addSheet(//设置sheet
						JBoltExcelSheet.create("versions")//创建sheet name保持与模板中的sheet一致
						.setHeaders(//sheet里添加表头
								JBoltExcelHeader.create("version", "版本号",20),
								JBoltExcelHeader.create("publish_time", "发布时间",30),
								JBoltExcelHeader.create("create_time", "创建时间",30),
								JBoltExcelHeader.create("is_new", "是否最新",20),
								JBoltExcelHeader.create("user_username", "操作用户",20)
								)
						.setDataChangeHandler((data,index)->{//设置数据转换处理器
							//将user_id转为user_name
							data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
							data.changeBooleanToStr("is_new", "是", "否");
						})
						.setModelDatas(4, jboltVersions)//设置数据
						.addPositionChanges(JBoltExcelPositionChange.create(4, 4, 1, 5, dataSize))
						.addPositionDatas(
								JBoltExcelPositionData.create(4+dataSize, 2,JBoltUserKit.getUserName())
								)
						)
				.setFileName("从模板加载并转移下方模板位置");
		//3、导出
		renderBytesToExcelXlsFile(jBoltExcel);
	}
	
	/**
	 * 导出数据 带form查询
	 */
	public void exportExcel7() {
		//1、准备原数据
		List<JboltVersion> jboltVersions = jBoltVersionService.getAdminList("id", "asc", getKeywords(), getStartTime(), getEndTime());
		
		//2、创建JBoltExcel
		JBoltExcel jBoltExcel=JBoltExcel
				.create()//创建JBoltExcel
				.addSheet(//设置sheet
						JBoltExcelSheet.create("version数据")//创建sheet
						.setMerges(JBoltExcelMerge.create(5, "Jbolt Version 数据导出"))
						.setHeaders(2,//sheet里添加表头
								JBoltExcelHeader.create("version", "版本号",20),
								JBoltExcelHeader.create("publish_time", "发布时间",30),
								JBoltExcelHeader.create("create_time", "创建时间",30),
								JBoltExcelHeader.create("is_new", "是否最新",20),
								JBoltExcelHeader.create("user_username", "操作用户",20)
								)
						.setDataChangeHandler((data,index)->{//设置数据转换处理器
							//将user_id转为user_name
			    			data.changeWithKey("user_id","user_username", CACHE.me.getUserUsername(data.get("user_id")));
							data.changeBooleanToStr("is_new", "是", "否");
						})
						.setModelDatas(3, jboltVersions)//设置数据
						)
				.setFileName("普通列表导出");
		//3、导出
		renderBytesToExcelXlsFile(jBoltExcel);
	}
}
