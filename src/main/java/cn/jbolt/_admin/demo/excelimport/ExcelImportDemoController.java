package cn.jbolt._admin.demo.excelimport;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.jfinal.upload.UploadFile;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt._admin.updatemgr.JBoltVersionService;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.config.JBoltUploadFolder;
/**
 * Excel导入
 * @ClassName:  ExcelDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月15日   
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class ExcelImportDemoController extends JBoltBaseController {
	@Inject
	private JBoltVersionService jBoltVersionService;
	public void index() {
		render("excel.html");
	}
	/**
	 *  数据源
	 */
	public void datas() {
		renderJsonData(jBoltVersionService.paginate("id", getPageNumber(), getPageSize()));
	}
	/**
	 * 下载导入模板
	 */
	public void downloadTpl() {
		renderBytesToExcelXlsFile(jBoltVersionService.getExcelImportTpl().setFileName("版本信息导入模板"));
	}
	
	public void initImport() {
		render("importExcel.html");
	}
	/**
	 * 执行导入
	 */
	@Before(Tx.class)
	public void importExcel() {
		String uploadPath=JBoltUploadFolder.todayFolder(JBoltUploadFolder.DEMO_JBOLTTABLE_EXCEL);
		UploadFile file=getFile("file",uploadPath);
		if(notExcel(file)){
			renderJsonFail("请上传excel文件");
			return;
		}
		renderJson(jBoltVersionService.importExcelDatas(file.getFile()));
	}
	
}
