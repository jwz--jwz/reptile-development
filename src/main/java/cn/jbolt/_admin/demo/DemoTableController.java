package cn.jbolt._admin.demo;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.paragetter.Para;
import com.jfinal.plugin.activerecord.tx.Tx;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.model.Demotable;
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class DemoTableController extends JBoltBaseController {
	@Inject
	private DemoTableService service;
	public void filterbox() {
		render("filterbox.html");
	}
	public void jsonoption() {
		render("jsonoption.html");
	}
	public void jsonoption2() {
		render("jsonoption2.html");
	}
	public void jsonoption3() {
		render("jsonoption3.html");
	}
	public void jsonoption4() {
		render("jsonoption4.html");
	}
	public void jsonoption5() {
		render("jsonoption5.html");
	}
	public void editableSingle() {
		render("editable_single.html");
	}
	public void editableSingleWithMenu() {
		render("editable_single_withmenu.html");
	}
	public void editableSingleWithMenu2() {
		render("editable_single_withmenu2.html");
	}
	public void normalSingleWithMenu() {
		render("normal_withmenu.html");
	}
	public void normalSingleWithMenu2() {
		render("normal_withmenu2.html");
	}
	public void editableMulti() {
		render("editable_multi.html");
	}
	public void editableMultiAll() {
		render("editable_multi_submitall.html");
	}
	
	/**
	 * 可编辑表格案例 单实例 with rightbox
	 */
	public void editableSingleWithRightBox() {
		render("editable_single_rightbox.html");
	}
	/**
	 * 可编辑表格案例 单实例 with rightbox
	 */
	public void editableSingleWithRightInputBox() {
		render("editable_single_rightbox_input.html");
	}
	/**
	 * 可编辑表格案例 单实例 with headbox
	 */
	public void editableSingleWithHeadBox() {
		render("editable_single_headbox.html");
	}
	/**
	 * 可编辑表格案例 单实例 with triggerSummaryColumn
	 */
	public void editableSingleWithTriggerSummary() {
		render("editable_triggersummary.html");
	}
	
	public void chooseData() {
		render("choosedata.html");
	}
	public void chooseData2() {
		set("linkId", get("linkId"));
		render("choosedata2.html");
	}
	
	public void datas() {
		renderJsonData(service.getAdminList(getKeywords(),getStartTime(),getEndTime()));
	}
	
	public void updateColumn(@Para("") Demotable demotable) {
		renderJson(service.updateOneColumn(demotable));
	}
	/**
	 * JBoltTable 可编辑表格整体提交
	 */
	@Before(Tx.class)
	public void submit() {
		renderJson(service.submitByJBoltTable(getJBoltTable()));
	}
	/**
	 * JBoltTable 可编辑表格整体提交 多表格
	 */
	@Before(Tx.class)
	public void submitMulti() {
		renderJson(service.submitByJBoltTables(getJBoltTables()));
	}
}
