package cn.jbolt._admin.demo.pdfviewer;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
/**
 * PDF js viewer demo
 * @ClassName:  PdfViewerDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月21日   
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class PdfViewerDemoController extends JBoltBaseController {
	public void index() {
		render("index.html");
	}
	
	public void pdf1() {
		render("pdf1.html");
	}
}
