package cn.jbolt._admin.demo;

import com.jfinal.core.JFinal;
import com.jfinal.upload.UploadFile;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
/**
 * webcamjs组件 demo
 * @ClassName:  WebcamDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年10月29日   
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class WebcamjsDemoController extends JBoltBaseController {
	public void index() {
		render("webcamjs.html");
	}
	public void demo2() {
		render("webcamjs2.html");
	}
	
	public void upload() {
		UploadFile file=getFile("file","demo/webcamjs");
		if(notImage(file)){
			renderJsonFail("请上传图片类型文件");
			return;
		}
		renderJsonData(JFinal.me().getConstants().getBaseUploadPath()+"/demo/webcamjs/"+file.getFileName());
	}
}
