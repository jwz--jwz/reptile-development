package cn.jbolt._admin.demo;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
/**
 * Video.js demo
 * @ClassName:  VideojsDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月26日   
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class VideojsDemoController extends JBoltBaseController {
	public void index() {
		render("index.html");
	}
	public void video() {
		render("video.html");
	}
}
