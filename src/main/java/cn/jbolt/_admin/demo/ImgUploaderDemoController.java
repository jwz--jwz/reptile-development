package cn.jbolt._admin.demo;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import com.jfinal.upload.UploadFile;

import cn.jbolt._admin.jboltfile.JBoltFileService;
import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.config.JBoltUploadFolder;
import cn.jbolt.common.model.JboltFile;
import cn.jbolt.common.util.ArrayUtil;
/**
 * Demo演示-图片上传
 * @ClassName:  ImgUploaderDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年10月1日   
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class ImgUploaderDemoController extends JBoltBaseController {
	@Inject
	private JBoltFileService jboltFileService;
	public void index() {
		render("imguploader.html");
	}
	public void multiUpload() {
		String[] urls = {
				"assets/img/defaultposter.jpg",
				"assets/css/img/login_bg2.jpg",
				"assets/img/avatar.jpg",
				"assets/img/uploadimg.png"
		};
		set("files", ArrayUtil.join(urls, ","));
		render("imguploader_multi.html");
	}
	
	public void submit() {
		keepPara();
		setMsg("这是提交表单跳转页面，返回到本页面后的效果，输入框里带着值，图片上传组件data-value='图片地址' 就能显示！");
		render("imguploader.html");
	}
	
	/**
	 * 上传图片
	 */
	public void uploadImage(){
		//上传到今天的文件夹下
		String uploadPath=JBoltUploadFolder.todayFolder(JBoltUploadFolder.DEMO_IMAGE_UPLOADER);
		UploadFile file=getFile("file",uploadPath);
		if(notImage(file)){
			renderJsonFail("请上传图片类型文件");
			return;
		}
		renderJson(jboltFileService.saveImageFile(file, uploadPath));
	}
	/**
	 * 上传图片 多图
	 */
	public void uploadImages(){
		//上传到今天的文件夹下
		String uploadPath=JBoltUploadFolder.todayFolder(JBoltUploadFolder.DEMO_IMAGE_UPLOADER);
		List<UploadFile> files=getFiles(uploadPath);
		if(files==null || files.size()==0) {
			renderJsonFail("请选择图片后上传");
			return;
		}
		StringBuilder msg = new StringBuilder();
		files.forEach(file->{
			if(notImage(file)){
				msg.append(file.getFileName()+"不是图片类型文件;");
			}
		});
		if(msg.length()>0) {
			renderJsonFail(msg.toString());
			return;
		}
		
		List<String> retFiles=new ArrayList<String>();
		Ret ret;
		StringBuilder errormsg = new StringBuilder();
		for(UploadFile uploadFile:files) {
			ret=jboltFileService.saveImageFile(uploadFile,uploadPath);
			if(ret.isOk()){
				retFiles.add(ret.getStr("data"));
			}else {
				errormsg.append(uploadFile.getOriginalFileName()+"上传失败;");
			}
		}
		if(retFiles.size()==0) {
			renderJsonFail(errormsg.toString());
			return;
		}
		renderJsonData(retFiles,errormsg.toString());
	}
	/**
	 * 上传图片 多图
	 */
	public void uploadImages2(){
		//上传到今天的文件夹下
		String uploadPath=JBoltUploadFolder.todayFolder(JBoltUploadFolder.DEMO_IMAGE_UPLOADER);
		List<UploadFile> files=getFiles(uploadPath);
		if(files==null || files.size()==0) {
			renderJsonFail("请选择图片后上传");
			return;
		}
		StringBuilder msg = new StringBuilder();
		files.forEach(file->{
			if(notImage(file)){
				msg.append(file.getFileName()+"不是图片类型文件;");
			}
		});
		if(msg.length()>0) {
			renderJsonFail(msg.toString());
			return;
		}
		
		List<JboltFile> retFiles=new ArrayList<JboltFile>();
		JboltFile jboltFile;
		StringBuilder errormsg = new StringBuilder();
		for(UploadFile uploadFile:files) {
			jboltFile=jboltFileService.saveJBoltFile(uploadFile,uploadPath,JboltFile.FILE_TYPE_IMAGE);
			if(jboltFile!=null){
				retFiles.add(jboltFile);
			}else {
				errormsg.append(uploadFile.getOriginalFileName()+"上传失败;");
			}
		}
		if(retFiles.size()==0) {
			renderJsonFail(errormsg.toString());
			return;
		}
		renderJsonData(retFiles,errormsg.toString());
	}
	
	
}
