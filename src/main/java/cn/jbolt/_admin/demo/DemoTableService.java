package cn.jbolt._admin.demo;

import java.util.Date;
import java.util.List;

import com.jfinal.kit.Ret;
import com.jfinal.plugin.activerecord.Page;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.base.JBoltTable;
import cn.jbolt.base.JBoltTableMulti;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.db.sql.Sql;
import cn.jbolt.common.model.Demotable;

public class DemoTableService extends JBoltBaseService<Demotable> {
	private Demotable dao = new Demotable().dao();

	@Override
	protected Demotable dao() {
		return dao;
	}
	/**
	 * 管理页面按照关键词和时间段查询数据
	 * @param keywords
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<Demotable> getAdminList(String keywords, Date startTime, Date endTime) {
		Sql sql = selectSql();
		if (isOk(keywords)) {
			sql.likeMulti(keywords, "name","brief_info");
		}
		if (isOk(startTime)) {
			sql.ge("create_time", toStartTime(startTime));
		}
		if (isOk(endTime)) {
			sql.le("create_time", toEndTime(endTime));
		}
		return find(sql);
	}

	/**
	 * 保存
	 * 
	 * @param demotable
	 * @return
	 */
	public Ret save(Demotable demotable) {
		return submit(demotable, false);
	}

	/**
	 * 保存
	 * 
	 * @param demotable
	 * @return
	 */
	public Ret update(Demotable demotable) {
		return submit(demotable, true);
	}

	/**
	 * 提交
	 * 
	 * @param demotable
	 * @param update
	 */
	public Ret submit(Demotable demotable, boolean update) {
		if (demotable == null || (!update && isOk(demotable.getId())) || (update && notOk(demotable.getId()))) {
			return fail(Msg.PARAM_ERROR);
		}
		if (update) {
			// 更新时需要判断数据存在
			Demotable dbDemotable = findById(demotable.getId());
			if (dbDemotable == null) {
				return fail(Msg.DATA_NOT_EXIST);
			}
		}
		boolean success = false;
		// save和update分别处理
		if (update) {
			success = demotable.update();
		} else {
			demotable.setSortRank(getNextSortRank());
			success = demotable.save();
		}
		if (success) {
			// 添加日志
		}
		return ret(success);
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	public Ret delete(Integer id) {
		Ret ret = deleteById(id, false);
		if (ret.isOk()) {
			// 添加日志
			// Demotable demotable=ret.getAs("data");
			// addDeleteSystemLog(id, userId, SystemLog.TARGETTYPE_xxx,
			// demotable.getName());
		}
		return ret;
	}
	/**
	 * 后台管理 demo 分页查询
	 * @param pageNumber
	 * @param pageSize
	 * @param keywords 关键词
	 * @param startTime 开始日期
	 * @param endTime 结束日期
	 * @param sortColumn 排序字段
	 * @param sortType  排序方式
	 * @return
	 */
	public Page<Demotable> paginateAdminDatas(int pageNumber, int pageSize, String keywords,Date startTime,Date endTime, String sortColumn, String sortType) {
		//1、自己利用参数条件 拼接完整的sql语句 进行分页查询 调用的是jfinal底层分页方法
		//2、写一个完整的sql模板 在这里调用 传入参数即可
		//3、使用JBolt平台提供的Sql.java工具类 完成复杂sql
		
		Sql sql=selectSql().page(pageNumber, pageSize);
		//关键词模糊条件查询 like
		sql.like("name", keywords);
		
		//时间段
		sql.ge("create_time", toStartTime(startTime));
		sql.le("create_time", toEndTime(endTime));
		
		//排序 id desc
//		sql.orderById(true);
		
//		使用sortColumn和SortType去查询并排序
		sql.orderBy(sortColumn, sortType);
		
		//执行调用
		return paginate(sql);
	}
	/**
	 * 执行JBoltTable表格整体提交
	 * @param jBoltTable
	 * @return
	 */
	public Ret submitByJBoltTable(JBoltTable jBoltTable) {
		if(jBoltTable==null||jBoltTable.isBlank()) {
			return fail(Msg.JBOLTTABLE_IS_BLANK);
		}
		//获取额外传递参数 比如订单ID等信息 在下面数据里可能用到
		if(jBoltTable.paramsIsNotBlank()) {
			System.out.println(jBoltTable.getParams().toJSONString());
		}
		

		//获取Form对应的数据
		if(jBoltTable.formIsNotBlank()) {
		//这里需要根据自己的需要 从Form这个JSON里去获取自己需要的数据
			//1、如果前端Form没有使用模型驱动方式例如user.xxx这样的name属性的话 直接使用class就能获取到model和javaBean
			//User user=jBoltTable.getFormModel(User.class);
			//Record record=jBoltTable.getFormRecord();
			//xxxBean bean=jBoltTable.getFormBean(xxxBean.class);
			
			//2、如果前端Form使用了模型驱动 就需要使用下面方式 指定modelName
			//User user=jBoltTable.getFormModel(User.class,"user");
			//xxxBean bean=jBoltTable.getFormBean(xxxBean.class,"xxx");
			
		}
		
		//获取待保存数据 执行保存
		if(jBoltTable.saveIsNotBlank()) {
			batchSave(jBoltTable.getSaveModelList(Demotable.class));
		}
		//获取待更新数据 执行更新
		if(jBoltTable.updateIsNotBlank()) {
			batchUpdate(jBoltTable.getUpdateModelList(Demotable.class));
		}
		//获取待删除数据 执行删除
		if(jBoltTable.deleteIsNotBlank()) {
			deleteByIds(jBoltTable.getDelete());
		}
		 
		return SUCCESS;
	}
	
	/**
	 * 多个可编辑表格同时提交
	 * @param jboltTables
	 * @return
	 */
	public Ret submitByJBoltTables(JBoltTableMulti jboltTableMulti) {
		if(jboltTableMulti==null||jboltTableMulti.isEmpty()) {
			return fail(Msg.JBOLTTABLE_IS_BLANK);
		}
		//这里可以循环遍历 保存处理每个表格 也可以 按照name自己get出来单独处理
		jboltTableMulti.entrySet().forEach(en->{
			JBoltTable jBoltTable = en.getValue();
			//获取额外传递参数 比如订单ID等信息 在下面数据里可能用到
			if(jBoltTable.paramsIsNotBlank()) {
				System.out.println(jBoltTable.getParams().toJSONString());
			}
			
	
			//获取Form对应的数据
			if(jBoltTable.formIsNotBlank()) {
			//这里需要根据自己的需要 从Form这个JSON里去获取自己需要的数据
				//1、如果前端Form没有使用模型驱动方式例如user.xxx这样的name属性的话 直接使用class就能获取到model和javaBean
				//User user=jBoltTable.getFormModel(User.class);
				//Record record=jBoltTable.getFormRecord();
				//xxxBean bean=jBoltTable.getFormBean(xxxBean.class);
				
				//2、如果前端Form使用了模型驱动 就需要使用下面方式 指定modelName
				//User user=jBoltTable.getFormModel(User.class,"user");
				//xxxBean bean=jBoltTable.getFormBean(xxxBean.class,"xxx");
				
			}
			
			//获取待保存数据 执行保存
			if(jBoltTable.saveIsNotBlank()) {
				batchSave(jBoltTable.getSaveModelList(Demotable.class));
			}
			//获取待更新数据 执行更新
			if(jBoltTable.updateIsNotBlank()) {
				batchUpdate(jBoltTable.getUpdateModelList(Demotable.class));
			}
			//获取待删除数据 执行删除
			if(jBoltTable.deleteIsNotBlank()) {
				deleteByIds(jBoltTable.getDelete());
			}
		});
		return SUCCESS;
	}
}
