package cn.jbolt._admin.demo;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Inject;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt._admin.updatemgr.ChangeLogService;
import cn.jbolt._admin.updatemgr.JBoltVersionService;
import cn.jbolt._admin.updatemgr.JboltVersionFileService;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.bean.Option;
import cn.jbolt.common.bean.OptionBean;
import cn.jbolt.common.config.PageSize;
import cn.jbolt.common.poi.excel.JBoltExcel;
/**
 * 主从表管理案例
 * @ClassName:  MasterSlaveDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年9月10日   
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class MasterSlaveDemoController extends JBoltBaseController {
	@Inject
	private JBoltVersionService service;
	@Inject
	private ChangeLogService changeLogService;
	@Inject
	private JboltVersionFileService jboltVersionFileService;
	/**
	 * demo主页
	 */
	public void index() {
		set("pageData", service.paginateAdminList(getPageNumber(), getPageSize(PageSize.PAGESIZE_API_LIST_10), "id", "desc",getKeywords(), getDate("startTime"), getDate("endTime")));
		render("index.html");
	}
	public void isNewOptions() {
		List<Option> options=new ArrayList<Option>();
		options.add(new OptionBean("是", true));
		options.add(new OptionBean("否", false));
		renderJsonData(options);
	}
	/**
	 * JBolttable版主页
	 */
	public void jbolttable() {
		render("jbolttable.html");
	}
	/**
	 * JBolttable版主页 带tree
	 */
	public void jbolttable3() {
		render("jbolttable3.html");
	}
	/**
	 * JBolttable版主页 复杂头部
	 */
	public void jbolttable4() {
		render("jbolttable4.html");
	}
	/**
	 * 主表数据源 分页
	 */
	public void masterDatas() {
		renderJsonData(service.paginate(getPageNumber(), getPageSize(PageSize.PAGESIZE_API_LIST_10)));
	}
	/**
	 * 主更新文件列表
	 */
	public void mainFilesDatas() {
		Integer jboltVersionId=getInt(0);
		if(notOk(jboltVersionId)) {
			renderJsonData(jboltVersionFileService.getEmptyPage());
			return;
		}
		renderJsonData(jboltVersionFileService.pageFilesByJboltVersionId(jboltVersionId,getPageNumber(),getPageSize()));
	}
	/**
	 * 主更新文件列表
	 */
	public void mainFiles() {
		Integer jboltVersionId=getInt(0);
		if(notOk(jboltVersionId)) {
			renderAjaxPortalFail("请选择主表记录后，再来点击下载excel");
			return;
		}
		set("jboltVersionId", jboltVersionId);
		set("files", jboltVersionFileService.getFilesByJboltVersionId(jboltVersionId));
		render("files.html");
	}
	/**
	 * 主更新文件列表
	 */
	public void downloadMainFilesDatas() {
		Integer jboltVersionId=getInt(0);
		if(notOk(jboltVersionId)) {
			renderJsonFail("暂无数据");
//			renderBytesToExcelXlsFile(JBoltExcel.create().setFileName("暂无数据"));
			return;
		}
		renderBytesToExcelXlsFile(jboltVersionFileService.getFilesExcel(jboltVersionId).setFileName("文件列表"));
	}
	/**
	 * 读取changelog
	 */
	public void changelogPortal() {
		Integer jboltVersionId=getInt(0);
		if(notOk(jboltVersionId)) {
			renderAjaxPortalFail("暂无数据");
			return;
		}
		set("jboltVersionId", jboltVersionId);
		set("changelog", changeLogService.findByJboltVersionId(jboltVersionId));
		render("changelog.html");
	}
	/**
	 * 读取changelog
	 */
	public void changelog() {
		Integer jboltVersionId=getInt(0);
		if(notOk(jboltVersionId)) {
			renderAjaxPortalFail("暂无数据");
			return;
		}
		set("jboltVersionId", jboltVersionId);
		set("changelog", changeLogService.findByJboltVersionId(jboltVersionId));
		render("changelog.html");
	}
}
