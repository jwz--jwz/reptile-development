package cn.jbolt._admin.demo;

import com.jfinal.aop.Inject;
import com.jfinal.core.paragetter.Para;

import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Demotable;

/**
 * JBoltTable教程录制使用
 * 
 * @ClassName: JBoltTableCourseDemoController
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2020年12月28日
 * 
 *        注意：本内容仅限于JFinal学院 JBolt平台VIP成员内部传阅，请尊重开发者劳动成果，不要外泄出去用于其它商业目的
 */
@UnCheckIfSystemAdmin
public class JBoltTableCourseDemoController extends JBoltBaseController {
	@Inject
	private DemoTableService service;

	/**
	 * 首页
	 */
	public void index() {
		String sortColumn = getSortColumn("create_time");
		String sortType   = getSortType("desc");
		set("pageData", service.paginateAdminDatas(getPageNumber(), getPageSize(),getKeywords(),getStartTime(),getEndTime(),sortColumn,sortType));
//		set("pageData", service.paginateByKeywords("id", "desc", getPageNumber(), getPageSize(), getKeywords(), "name"));
		keepPara();
		setDefaultSortInfo(sortColumn, sortType);
		render("index.html");
	}
	
	/**
	 * 异步读取数据表格演示Demo
	 */
	public void ajax() {
		setDefaultSortInfo("create_time", "desc");
		render("ajax.html");
	}
	
	/**
	 * ajax数据接口
	 */
	public void ajaxDatas() {
		renderJsonData(service.findAll());
	}
	
	/**
	 * 可编辑表格
	 */
	public void editable() {
		setDefaultSortInfo("create_time", "desc");
		render("editable.html");
	}
	
	/**
	 * 单个单元格提交更新
	 */
	public void updateOneColumn(@Para("") Demotable demotable) {
		renderJson(service.updateOneColumn(demotable));
	}
	
	
	
	
	/**
	 * 批量删除
	 */
	public void batchDelete() {
		renderJson(service.deleteByIds(get("ids")));
	}
	
	/**
	 * ajax数据接口 分页版
	 */
	public void ajaxDatas2() {
		String sortColumn = getSortColumn("create_time");
		String sortType   = getSortType("asc");
		renderJsonData(service.paginateAdminDatas(getPageNumber(), getPageSize(),getKeywords(),getStartTime(),getEndTime(),sortColumn,sortType));
	}
	

	/**
	 * 新增
	 */
	public void add() {
		render("add.html");
	}

	/**
	 * 新增
	 */
	public void edit() {
		Demotable demotable = service.findById(getInt(0));
		if (demotable == null) {
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		set("demotable", demotable);
		render("edit.html");
	}

	/**
	 * 保存
	 */
	public void save() {
		renderJson(service.save(getModel(Demotable.class, "demotable")));
	}

	/**
	 * 更新
	 */
	public void update() {
		renderJson(service.update(getModel(Demotable.class, "demotable")));
	}

	/**
	 * 删除
	 */
	public void delete() {
		renderJson(service.delete(getInt(0)));
	}
	
	/**
	 * toggleEnable
	 */
	public void toggleEnable() {
		renderJson(service.toggleBoolean(getInt(0),"enable"));
	}
	
	
	

}
