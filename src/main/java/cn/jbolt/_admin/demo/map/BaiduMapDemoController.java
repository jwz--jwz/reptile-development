package cn.jbolt._admin.demo.map;

import cn.jbolt._admin.permission.CheckPermission;
import cn.jbolt._admin.permission.PermissionKey;
import cn.jbolt._admin.permission.UnCheckIfSystemAdmin;
import cn.jbolt.base.JBoltBaseController;
/**
 * 百度地图 demo
 * @ClassName:  BaiduMapDemoController   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月24日   
 *    
 */
@CheckPermission(PermissionKey.DEMO)
@UnCheckIfSystemAdmin
public class BaiduMapDemoController extends JBoltBaseController{
	
	public void index() {
		render("index.html");
	}
	public void map() {
		render("map.html");
	}
	
}
