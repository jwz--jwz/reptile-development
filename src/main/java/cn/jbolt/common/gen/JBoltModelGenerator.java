package cn.jbolt.common.gen;

import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.generator.ModelGenerator;
import com.jfinal.plugin.activerecord.generator.TableMeta;

import cn.jbolt.common.util.JBoltConsoleUtil;


/**
 * Model 生成器
 */
public class JBoltModelGenerator extends ModelGenerator {
	private String dataSourceConfigName;
	private String dbType;
	private String idGenMode;
	public JBoltModelGenerator(String dataSourceConfigName,String dbType,String idGenMode,String modelPackageName, String baseModelPackageName, String modelOutputDir) {
		super(modelPackageName, baseModelPackageName, modelOutputDir);
		this.dataSourceConfigName=dataSourceConfigName;
		this.dbType=dbType;
		this.idGenMode=idGenMode;
	}
	@Override
	protected void initEngine() {
		super.initEngine();
		engine.setDevMode(true);
		setTemplate("/gentpl/model_template.jf");
	}

	@Override
	public void generate(List<TableMeta> tableMetas) {
		JBoltConsoleUtil.printMessage("-------------------------Model-------------------------");
		JBoltConsoleUtil.printMessageWithDate(" Generate Model :Starting ...");
		JBoltConsoleUtil.printMessageWithDate(" Model Output Dir: " + modelOutputDir);
		JBoltConsoleUtil.printMessageWithDate(" JBolt Model Generator is Working...");
		
		for (TableMeta tableMeta : tableMetas) {
			genModelContent(tableMeta);
		}
		writeToFile(tableMetas);
		
		JBoltConsoleUtil.printMessageWithDate(" Generate Model :Done ...");
	}
	@Override
	protected void genModelContent(TableMeta tableMeta) {
		JBoltConsoleUtil.printMessageWithDate(" Generate Model:"+modelPackageName+"."+tableMeta.modelName);
		Kv data = Kv.by("modelPackageName", modelPackageName);
		data.set("baseModelPackageName", baseModelPackageName);
		data.set("generateDaoInModel", generateDaoInModel);
		data.set("tableMeta", tableMeta);
		data.set("dataSource",dataSourceConfigName);
		data.set("dbType",dbType);
		data.set("idGenMode","JBoltIDGenMode."+idGenMode.toUpperCase());
		
		String ret = engine.getTemplate(template).renderToString(data);
		tableMeta.modelContent = ret;
	}
}


