package cn.jbolt.common.gen;
/**
 * index.html页面布局类型枚举
 */
public enum IndexHtmlLayoutType{
	/**
	 * 普通crud
	 */
	NORMAL_CRUD,
	/**
	 * 普通只做查询表
	 */
	NORMAL_TABLE,
	/**
	 * 主从表结构 crud
	 */
	MASTER_SLAVE_CRUD,
	/**
	 * 主从表结构 只做查询
	 */
	MASTER_SLAVE_TABLE; 
}