package cn.jbolt.common.gen;

import java.io.File;
import java.util.function.Predicate;

import com.jfinal.kit.StrKit;

import cn.hutool.core.util.ArrayUtil;
import cn.jbolt.base.JBoltIDGenMode;

/**
 * JBolt项目生成器配置
 * @ClassName:  JBoltProjectGenConfig   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年12月10日   
 */
public class JBoltProjectGenConfig {
	public static final String SEPARATOR=File.separator;
	public static String projectRootPath=System.getProperty("user.dir");
	public static final String projectType="maven";//项目类型 normalormaven
	public static String dbType=null;//数据库类型 需要读取文件
	public static String idGenMode=JBoltIDGenMode.AUTO;//默认主键生成模式
	public static String jdbcUrl=null;//jdbcURl 需要读取文件
	public static String user=null;//数据库用户名
	public static String password=null;//数据库密码
	public static boolean genModel=true;//是否生成model和baseModel
	public static final boolean generateDaoInModel=false;//是否dao生成在Model中
	public static final boolean generateChainSetterInBaseModel=true;
	public static boolean generateHtmlDataDictionary=true;//是否生成JFinal默认的html格式数据字典
	public static String removedTableNamePrefixes="jb_";//生成的Model 去掉前缀
	public static final boolean charToBoolean=true;//生成时 char(1)转Boolean
	public static final boolean baseModelExtendsJBoltBaseModel=true;//继承JBoltBaseModel
	public static String modelPackageName="cn.jbolt.common.model";//model生成的包
	public static String baseModelPackageName=null;//basemodel生成的包
	public static String modelOutputDir=null;//model输出路径
	public static String baseModelOutputDir=null;//base model输出路径
	public static boolean genJBoltCoreModel=false;//是否生成JBolt核心表
	public static JBoltColumnToBuildAttrNameFunction columnTobuildAttrNameFun;
	public static String[] tableNames=null;//单次生成直接指定需要生成的表名
	/**
	 * 初始化项目model生成配置
	 * @param projectRootPath
	 * @param modelPackageName
	 * @param genModel
	 * @param idGenMode
	 * @param genHtmlDataDictionary
	 * @param genJBoltCoreModel
	 * @param removedTableNamePrefixes 
	 * @param columnTobuildAttrNameFun 
	 */
	public static void init(String projectRootPath,String modelPackageName,boolean genModel,String idGenMode,boolean genHtmlDataDictionary,boolean genJBoltCoreModel, String removedTableNamePrefixes, JBoltColumnToBuildAttrNameFunction columnTobuildAttrNameFun,String[] tableNames) {
		JBoltProjectGenConfig.projectRootPath=projectRootPath;
		JBoltProjectGenConfig.modelPackageName=modelPackageName;
		modelOutputDir=projectRootPath+SEPARATOR+"src"+SEPARATOR+"main"+SEPARATOR+"java"+SEPARATOR+modelPackageName.replace(".", SEPARATOR);
		baseModelPackageName=modelPackageName+".base";
		baseModelOutputDir=modelOutputDir+SEPARATOR+"base";
		if(StrKit.isBlank(idGenMode)) {
			idGenMode=JBoltIDGenMode.AUTO;
		}
		JBoltProjectGenConfig.idGenMode=idGenMode;
		JBoltProjectGenConfig.genModel=genModel;
		JBoltProjectGenConfig.generateHtmlDataDictionary=genHtmlDataDictionary;
		JBoltProjectGenConfig.genJBoltCoreModel=genJBoltCoreModel;
		if(StrKit.notBlank(removedTableNamePrefixes)) {
			JBoltProjectGenConfig.removedTableNamePrefixes=JBoltProjectGenConfig.removedTableNamePrefixes+","+removedTableNamePrefixes;
		}
		if(columnTobuildAttrNameFun!=null) {
			JBoltProjectGenConfig.columnTobuildAttrNameFun = columnTobuildAttrNameFun;
		}
		if(tableNames!=null && tableNames.length>0) {
			JBoltProjectGenConfig.tableNames = tableNames;
		}
		
	}
	/**
	 * 过滤规则
	 */
	public static Predicate<String> tableSkipPredicate=new Predicate<String>() {
		@Override
		public boolean test(String tableName) {
			tableName=tableName.toLowerCase();
			if(tableNames!=null && tableNames.length>0) {
				return ArrayUtil.containsIgnoreCase(tableNames, tableName) == false;
			}
			if(!genJBoltCoreModel && tableName.startsWith("jb_")) {
				return true;
			}
			return tableNamesInFiterList(tableName)||tableNamesIndexOf(tableName)||tableNamesMatch(tableName);
		}
	};
	/**
	 * 初始化数据库配置
	 * @param dbType
	 * @param jdbcUrl
	 * @param user
	 * @param password
	 */
	public static void setDbConfig(String dbType,String jdbcUrl,String user,String password) {
		JBoltProjectGenConfig.dbType=dbType;
		JBoltProjectGenConfig.jdbcUrl=jdbcUrl;
		JBoltProjectGenConfig.user=user;
		JBoltProjectGenConfig.password=password;
	}
	/**
	 * 判断表名是否包含指定自定义字符串
	 * @param tableName
	 * @return
	 */
	public static boolean tableNamesIndexOf(String tableName) {
		boolean in=false;
		for(String str:tableNamesIndexOfStr) {
			if(tableName.indexOf(str)!=-1) {
				in=true;
				break;
			}
		}
		return in;
	}
	/**
	 * 判断表名符合正则
	 * @param tableName
	 * @return
	 */
	public static boolean tableNamesMatch(String tableName) {
		boolean in=false;
		for(String p:tableNamesPatterns) {
			if(tableName.matches(p)) {
				in=true;
				break;
			}
		}
		return in;
	}
	/**
	 * 判断是否在完整过滤表名中存在
	 * @param tableName
	 * @return
	 */
	public static boolean tableNamesInFiterList(String tableName) {
		return ArrayUtil.containsIgnoreCase(filterTableNames, tableName);
	}
	/**
	 * 不需要生成的表名（全部小写），直接完整的表名，一般都是一些数据库内置表什么的
	 * 这里默认给了几个，需要的自己加
	 */
	public static final String[] filterTableNames=new String[] {
			"dept","emp","salgrade","bonus","dtproperties"
	};
	/**
	 * 这里是判断表名中有包含以下字符串的需要过滤掉
	 * 一些分表不需要生成 例如jb_wechat_user_
	 * 一些sqlite中的临时表和内置不需要
	 */
	public static final String[] tableNamesIndexOfStr=new String[] {
			"sqlite_","_old_"	
	};
	/**
	 * 这里是判断表名符合的正则表达式
	 */
	public static final String[] tableNamesPatterns=new String[] {
			"jb_wechat_user_-?[1-9]\\d*"
	};
}
