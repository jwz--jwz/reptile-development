package cn.jbolt.common.gen;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.plugin.activerecord.generator.TableMeta;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import cn.jbolt.common.config.MainConfig;
import cn.smallbun.screw.core.Configuration;
import cn.smallbun.screw.core.engine.EngineConfig;
import cn.smallbun.screw.core.engine.EngineFileType;
import cn.smallbun.screw.core.engine.EngineTemplateType;
import cn.smallbun.screw.core.execute.DocumentationExecute;
import cn.smallbun.screw.core.process.ProcessConfig;
/**
 * JBolt平台生成html版数据库字典
 * @ClassName:  JBoltScrewDataDictionaryGenerator   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年8月16日   
 */
public class JBoltHtmlDataDictionaryGenerator {
	public static final String SEPARATOR = File.separator;
	private String fileOutputDir;
	private String fileName;
	private String version;
	private String description;
	private HikariConfig hikariConfig;

	public JBoltHtmlDataDictionaryGenerator(String configName, String fileOutputDir, String version,String description) {
		this.fileOutputDir = fileOutputDir;
		this.description = description;
		this.fileName = "_data_dictionary_" + configName.toLowerCase();
		this.version = version;

		// 数据源配置
		this.hikariConfig = new HikariConfig();
		hikariConfig.setDriverClassName(MainConfig.getDriverClass(JBoltProjectGenConfig.dbType));
		hikariConfig.setJdbcUrl(JBoltProjectGenConfig.jdbcUrl);
		hikariConfig.setUsername(JBoltProjectGenConfig.user);
		hikariConfig.setPassword(JBoltProjectGenConfig.password);
		// 设置可以获取tables remarks信息
		hikariConfig.addDataSourceProperty("useInformationSchema", "true");
		hikariConfig.setMinimumIdle(2);
		hikariConfig.setMaximumPoolSize(5);
	}
	/**
	 * 获取表名配置
	 * @param allMatas
	 * @return
	 */
	private ProcessConfig getProduceConfig(List<TableMeta> allMatas) {
		List<String> tableNames = new ArrayList<String>();
		allMatas.forEach(table -> {
			tableNames.add(table.name);
		});
		return ProcessConfig.builder()
				// 根据名称指定表生成
				.designatedTableName(tableNames).build();
	}
	
	/**
	 * 执行生成
	 * @param allMatas
	 */
	public void generate(List<TableMeta> allMatas) {
		EngineConfig engineConfig = EngineConfig.builder()
				.fileOutputDir(fileOutputDir)
				.openOutputDir(false)
				.fileType(EngineFileType.HTML)
				.produceType(EngineTemplateType.freemarker)
				.fileName(fileName)
				.build();
		Configuration config = Configuration.builder()
				.version(version)
				.description(description)
				.dataSource(new HikariDataSource(hikariConfig))
				.engineConfig(engineConfig)
				.produceConfig(getProduceConfig(allMatas))
				.build();
		// 执行生成
		new DocumentationExecute(config).execute();
	}
}
