package cn.jbolt.common.gen;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface JBoltField {
	/**
	 * java里的属性名称 驼峰
	 * @return
	 */
	String name();
	/**
	 * Java类型
	 * @return
	 */
	String type();
	/**
	 * 数据库里的列名
	 * @return
	 */
	String columnName();
	/**
	 * 备注
	 * @return
	 */
	String remark();
	/**
	 * 是否必填 默认true
	 * @return
	 */
	boolean required() default true;
	/**
	 * 最大长度
	 * @return
	 */
	int maxLength() default 20;
	/**
	 * 保留小数位
	 * @return
	 */
	int fixed() default 0;
	/**
	 * 顺序
	 * @return
	 */
	int order() default 1;
}
