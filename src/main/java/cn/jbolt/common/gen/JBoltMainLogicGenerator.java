package cn.jbolt.common.gen;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.template.Engine;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Filter;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import cn.jbolt.base.JBoltConst;
import cn.jbolt.base.JBoltIDGenMode;
import cn.jbolt.common.config.TableBind;
import cn.jbolt.common.model.Demotable;
import cn.jbolt.common.util.JBoltConsoleUtil;
import duorou.model.PlantDoFamilia;
import duorou.model.PlantDoGenus;
import duorou.model.PlantDoInfo;

/**
 * Controller Service html生成器
 */
public class JBoltMainLogicGenerator{
	/**
	 * 执行入口
	 * @param args
	 */
	public static void main(String[] args) {
		new JBoltMainLogicGenerator().generate();
	}
	
	/**
	 * 执行生成任务
	 */
	public void generate() {
		JBoltConsoleUtil.printMessageWithDate("开始执行生成 ...");
		genLogicBeans.forEach(genLogicBean->generateOne(genLogicBean));
 		JBoltConsoleUtil.printMessageWithDate("全部生成 结束 ...");
 		JBoltConsoleUtil.printMessageWithDate("==请刷新项目目录，检查生成结果==");
	}
	
	/**
	 * 初始化需要 生成的逻辑的model 配置
	 */
	protected void initGenConfig() {
		JBoltConsoleUtil.printMessageWithDate("初始化需要生成的Model-JavaPackage-viewFolder对应关系 ...");
		/*
		 * index.html使用的UI布局 
		 *  默认普通表格CRUD布局 normal_crud
		 *  可选normal_crud、normal_table、master_slave 
		 *  对应普通crud类型、普通表格查询类型、主从表
		 */
		IndexHtmlLayoutType indexHtmlLayoutType = IndexHtmlLayoutType.NORMAL_CRUD;
		//生成java代码里的作者信息 默认 JBolt-Generator
		String author                    ="wz";
		//controller service等java代码生成的报名 路径
		String packageName               = "duorou.controller.plant";
		//在路由配置里的controllerKey参数 也用在生成其它URL的前缀
		String controllerKey             = "/plant/familia";
		//生成html存放位置 从src/main/webapp根目录下开始 /作为前缀
		String viewFolder                = "/duorou/plant/familia";
		//生成Index.html左上角页面标题
		String pageTitle                 = "多肉基本科类管理";
		//在页面里使用增加 修改 删除 提示信息等用到的针对此模块操作的数据名称 例如 商品管理中是【商品】 品牌管理中是【品牌】
		String dataName                  = "基本信息";
		//是否需要分页查询
		boolean needPaginate             = true;
		//index.html 是否需要启用表格的工具条 toolbar
		boolean needToolbar              = true;
		//执行删除时是否做检测校验
		boolean checkDelete              = true;
		//关键词查询匹配字段 多个用逗号隔开
		String matchColumns              = "familia_name,familia_describe";
		//查询用默认排序字段
		String orderColumn               = "id";
		//查询用默认排序方式 desc asc
		String orderType                 = "desc";
		/*
		 * 需要在Controller上方声明的@CheckPermission(PermissionKey.USER) 
		 * 可以这样写 	String checkPermissionKeys = PermissionKey.XXX;  多个用逗号隔开
		 * 这个XXX需要自己后台权限资源管理处定义出来 然后生成到PermissionKey.java中
		 */
		String checkPermissionKeys       = "PermissionKey.PLANT_FAMiLIA";
		//是否使用@path注解 就不用去配置路由了 默认false
		boolean usePathAnnotation        = false;
		//访问Controller权限是是否支持超管员不校验直接放行 默认false
		boolean unCheckIfSystemAdmin     = true;
		
		//创建主逻辑生成配置Bean
		JBoltMainLogicBean mainLogicBean = new JBoltMainLogicBean(PlantDoFamilia.class,projectPath, packageName,controllerKey, viewFolder ,pageTitle,dataName,needPaginate,needToolbar,checkDelete,matchColumns,orderColumn,orderType,checkPermissionKeys,usePathAnnotation,unCheckIfSystemAdmin,indexHtmlLayoutType,author);
		//index.html页面表格是否需要行末尾的编辑按钮
		mainLogicBean.setNeedTrEditBtn(true);
		//index.html页面表格是否需要行末尾的删除按钮
		mainLogicBean.setNeedTrDeleteBtn(true);
		//index.html页面主表格区域 是否启用headBox
		mainLogicBean.setNeedHeadBox(false);
		//index.html页面主表格区域 是否启用footBox
		mainLogicBean.setNeedFootBox(false);
		//index.html页面主表格区域 是否启用leftBox
		mainLogicBean.setNeedLeftBox(false);
		//index.html页面主表格区域 是否启用rightBox
		mainLogicBean.setNeedRightBox(false);
		if(mainLogicBean.isCrudType()) {
			//设置crud 操作里 dialog的宽高尺寸
			mainLogicBean.setDialogArea("700,800");
			//设置表单几列呈现
			mainLogicBean.setFormColCount(1);
			//设置一列最少几个控件摆放 只有表单列设置为大于1列的时候才有效
			mainLogicBean.setFormColControlMinCount(8);
		}
		//加入到生成队列中
		genLogicBeans.add(mainLogicBean);
	}
	
	
	/**
	 * 构造函数
	 */
	public JBoltMainLogicGenerator() {
		JBoltConsoleUtil.printMessage("-------------------------JFinal Main Logic Generator-------------------------");
		initGenConfig();
		initEngine();
	}
	
	/**
	 * 初始化生成模板引擎
	 */
	private void initEngine() {
		JBoltConsoleUtil.printMessageWithDate("初始化模板引擎 ...");
		engine = new Engine();
		engine.setToClassPathSourceFactory();	// 从 class path 内读模板文件
		engine.addSharedMethod(new StrKit());
		engine.addSharedFunction(controllerCommonTemplate);
		engine.addSharedFunction(serviceCommonTemplate);
		engine.addEnum(IndexHtmlLayoutType.class);
	}
	
	/**
	 * 针对一个Modelclass进行生成
	 * @param clazz
	 * @param mainLogicBean
	 */
	public void generateOne(JBoltMainLogicBean mainLogicBean) {
		JBoltConsoleUtil.printMessageWithDate(String.format("正在生成:%s...",mainLogicBean.getMainModelClass().getName()));
		//生成Controller
		genController(mainLogicBean);
		//生成Service
		genService(mainLogicBean);
		//生成index.html
		genIndexHtml(mainLogicBean);
		//也有crud的话 才生成form add edit的html
		if(mainLogicBean.isCrudType()) {
			//生成add.html
			genAddHtml(mainLogicBean);
			//生成edit.html
			genEditHtml(mainLogicBean);
			//生成_form.html
			genFormHtml(mainLogicBean);
		}
	}
	
	/**
	 * 生成Index.htlm
	 * @param viewFolder
	 */
	private void genIndexHtml(JBoltMainLogicBean mainLogicBean) {
		//model名
		String modelName=mainLogicBean.getMainModelClass().getSimpleName();
		JBoltConsoleUtil.printMessageWithDate("正在生成index.html...");
		//view相对路径
		String viewFolder=mainLogicBean.getViewFolder();
		//view物理路径
		String targetOutputDir=getDirFromViewForlder(viewFolder);
		JBoltConsoleUtil.printMessageWithDate(String.format("index.html生成路径:%s...",targetOutputDir));
		//如果存在 忽略
		if(FileUtil.exist(targetOutputDir+ File.separator+"index.html")) {
			JBoltConsoleUtil.printMessageWithDate("index.html文件[%s]已存在,忽略生成！");
			return;
		}
		//模板数据
		Kv datas=Kv.by("modelName",modelName).set("mainLogicBean",mainLogicBean);
		//处理针对一个ModelClass在Controller层要生成的方法
		processIndexHtmlJboltField(mainLogicBean.getMainModelClass(),mainLogicBean.getPrimaryKeyAttr(),datas);
		//执行渲染模板得到代码文本
		String content=engine.getTemplate(indexHtmlTemplate).renderToString(datas);
		//执行文件生成
		writeToFile(targetOutputDir, "index.html", content);
	
	}
	
	/**
	 * 生成_form.html
	 * @param clazz
	 * @param viewFolder
	 */
	private void genFormHtml(JBoltMainLogicBean mainLogicBean) {
		//model名
		String modelName=mainLogicBean.getMainModelClass().getSimpleName();
		JBoltConsoleUtil.printMessageWithDate("正在生成_form.html...");
		String viewFolder=mainLogicBean.getViewFolder();
		String targetOutputDir=getDirFromViewForlder(viewFolder);
		JBoltConsoleUtil.printMessageWithDate(String.format("_form.html生成路径:%s...",targetOutputDir));
		if(FileUtil.exist(targetOutputDir+ File.separator+"_form.html")) {
			JBoltConsoleUtil.printMessageWithDate("_form.html文件[%s]已存在,忽略生成！");
			return;
		}
		//模板数据
		  Kv datas=Kv.by("modelName",modelName).set("mainLogicBean",mainLogicBean);
		  //处理针对一个ModelClass在Form里的字段元素
		  processFormHtmlJboltField(mainLogicBean.getMainModelClass(),mainLogicBean.getPrimaryKeyAttr(),datas);
		 
		//执行渲染模板得到代码文本
		String content=engine.getTemplate(formHtmlTemplate).renderToString(datas);
		//执行文件生成
		writeToFile(targetOutputDir, "_form.html", content);
		
	}

	/**
	 * 生成Edit.html
	 * @param clazz
	 * @param viewFolder
	 */
	private void genEditHtml(JBoltMainLogicBean mainLogicBean) {
		JBoltConsoleUtil.printMessageWithDate("正在生成edit.html...");
		String viewFolder=mainLogicBean.getViewFolder();
		String targetOutputDir=getDirFromViewForlder(viewFolder);
		JBoltConsoleUtil.printMessageWithDate(String.format("edit.html生成路径:%s...",targetOutputDir));
		if(FileUtil.exist(targetOutputDir+ File.separator+"edit.html")) {
			JBoltConsoleUtil.printMessageWithDate("edit.html文件[%s]已存在,忽略生成！");
			return;
		}
		//执行渲染模板得到代码文本
		String content=engine.getTemplate(editHtmlTemplate).renderToString(Kv.by("action", mainLogicBean.getRealControllerKey()+"/update"));
		//执行文件生成
		writeToFile(targetOutputDir, "edit.html", content);
		
	}

	/**
	 * 生成add.html
	 * @param clazz
	 * @param viewFolder
	 */
	private void genAddHtml(JBoltMainLogicBean mainLogicBean) {
		JBoltConsoleUtil.printMessageWithDate("正在生成add.html...");
		String viewFolder=mainLogicBean.getViewFolder();
		String targetOutputDir=getDirFromViewForlder(viewFolder);
		JBoltConsoleUtil.printMessageWithDate(String.format("add.html生成路径:%s...",targetOutputDir));
		if(FileUtil.exist(targetOutputDir+ File.separator+"add.html")) {
			JBoltConsoleUtil.printMessageWithDate("add.html文件[%s]已存在,忽略生成！");
			return;
		}
		//执行渲染模板得到代码文本
		String content=engine.getTemplate(addHtmlTemplate).renderToString(Kv.by("action", mainLogicBean.getRealControllerKey()+"/save"));
		//执行文件生成
		writeToFile(targetOutputDir, "add.html", content);
	}
	
	/**
	 * 通用controller模板
	 * @param mainLogicBean 
	 * @return
	 */
	private List<String> getCommonControllerMethods(JBoltMainLogicBean mainLogicBean){
		List<String> genMehtods=new ArrayList<String>();
		genMehtods.add("index");
		if(mainLogicBean.isCrudType()) {
			genMehtods.add("add");
			genMehtods.add("edit");
			genMehtods.add("save");
			genMehtods.add("update");
			if(mainLogicBean.isNeedToolbar() && mainLogicBean.isCrudType()) {
				genMehtods.add("deleteByIds");
			}
			if(mainLogicBean.isNeedTrDeleteBtn()) {
				genMehtods.add("delete");
			}
		}
		return genMehtods;
	}
	private List<JBoltGenMethod> getCommonServiceMethods(JBoltMainLogicBean mainLogicBean){
		List<JBoltGenMethod> genMehtods=new ArrayList<JBoltGenMethod>();
		if(mainLogicBean.isNeedPaginate()) {
			genMehtods.add(new JBoltGenMethod("paginateAdminDatas"));
		}else {
			genMehtods.add(new JBoltGenMethod("getAdminDatas"));
		}
		if(mainLogicBean.isCrudType()) {
			genMehtods.add(new JBoltGenMethod("save"));
			genMehtods.add(new JBoltGenMethod("update"));
			if(mainLogicBean.isNeedToolbar() && mainLogicBean.isCrudType()) {
				genMehtods.add(new JBoltGenMethod("deleteByIds"));
			}
			if(mainLogicBean.isNeedTrDeleteBtn()) {
				genMehtods.add(new JBoltGenMethod("delete"));
			}
		}
		return genMehtods;
	}
	
	/**
	 * 生成Service
	 * @param clazz
	 * @param targetPackageName
	 */
	private void genService(JBoltMainLogicBean mainLogicBean) {
		Class<? extends Model> modelClass = mainLogicBean.getMainModelClass();
		//model名
		String modelName=modelClass.getSimpleName();
		//service名
		String serviceName=modelName+"Service";
		//service全名
		String serviceFullName=serviceName+".java";
		//目标package
		String targetPackageName=mainLogicBean.getJavaPackage();
		JBoltConsoleUtil.printMessageWithDate(String.format("正在生成Service:%s...",serviceFullName));
		//生成到指定目录
		String targetOutputDir=getDirFromPackage(targetPackageName);
		JBoltConsoleUtil.printMessageWithDate(String.format("Service生成路径:%s...",targetOutputDir));
		//如果已经存在就忽略
		if(FileUtil.exist(targetOutputDir+ File.separator+serviceFullName)) {
			JBoltConsoleUtil.printMessageWithDate(String.format("Service文件[%s]已存在,忽略生成！",serviceFullName));
			return;
		}
		//模板数据
		Kv data=Kv.by("author", mainLogicBean.getAuthor()).set("targetPackageName", targetPackageName).set("modelName", modelName).set("serviceName",serviceName).set("modelImport",modelClass.getPackage().getName()+"."+modelName);
		//所需生成的方法名
		List<JBoltGenMethod> genMehtods=processServiceGenMethods(mainLogicBean);
		data.set("methods",genMehtods);
		data.set("mainLogicBean",mainLogicBean);
		data.set("paramIdType",getServiceParamIdTypeByGenMode(modelClass));
		String primaryKey = mainLogicBean.getPrimaryKey();
		if(primaryKey.indexOf(",")!=-1) {
			data.set("getIdMethodName","getId");
		}else {
			data.set("getIdMethodName","get"+StrUtil.upperFirst(StrUtil.toCamelCase(primaryKey)));
		}
		//执行渲染模板得到代码文本
		String content=engine.getTemplate(serviceTemplate).renderToString(data);
		//执行文件生成
		writeToFile(targetOutputDir, serviceFullName, content);
	}
	
	/**
	 * 生成Controller
	 * @param clazz
	 * @param targetPackageName
	 * @param needPaginate 
	 */
	private void genController(JBoltMainLogicBean mainLogicBean) {
		//model名字
		String modelName=mainLogicBean.getMainModelClass().getSimpleName();
		//Controller名字
		String controllerName=modelName+"AdminController";
		//Controller全名
		String controllerFullName=controllerName+".java";
		//目标package
		String targetPackageName=mainLogicBean.getJavaPackage();
		JBoltConsoleUtil.printMessageWithDate(String.format("正在生成Controller:%s...",controllerFullName));
		//输出目录
		String targetOutputDir=getDirFromPackage(targetPackageName);
		JBoltConsoleUtil.printMessageWithDate(String.format("Controller生成路径:%s...",targetOutputDir));
		//如果文件已经存在 忽略生成
		if(FileUtil.exist(targetOutputDir+ File.separator+controllerFullName)) {
			JBoltConsoleUtil.printMessageWithDate(String.format("Controller文件[%s]已存在,忽略生成！",controllerFullName));
			return;
		}
		//service名字
		String serviceName=modelName+"Service";
		//准备模板数据
		Kv data=Kv.by("author", mainLogicBean.getAuthor()).set("targetPackageName", targetPackageName).set("modelName", modelName).set("controllerName",controllerName).set("serviceName",serviceName).set("modelImport",mainLogicBean.getMainModelClass().getPackage().getName()+"."+modelName);
		//处理所需生成的方法名
		List<String> genMehtods=processContrllerGenMethods(mainLogicBean);
		data.set("methods",genMehtods);
		data.set("needPaginate",mainLogicBean.getNeedPaginate());
		data.set("unCheckIfSystemAdmin",mainLogicBean.isUnCheckIfSystemAdmin());
		data.set("getIdType",getGetIdTypeByGenMode(mainLogicBean.getMainModelClass()));
		data.set("controllerRemark",mainLogicBean.getPageTitle());
		data.set("pathValue",mainLogicBean.getControllerKey());
		data.set("pathViewPath",mainLogicBean.getViewFolder());
		data.set("usePathAnnotation",mainLogicBean.isUsePathAnnotation());
		data.set("checkPermissionKeys",mainLogicBean.getCheckPermissionKeys());
		data.set("isCRUD",mainLogicBean.getIsCRUD());
		//执行渲染模板得到代码文本
		String content=engine.getTemplate(controllerTemplate).renderToString(data);
		//执行文件生成 写到文件中
		writeToFile(targetOutputDir, controllerFullName, content);
	}
	
	/**
	 * 根据ID策略获取IDType
	 * @param idGenMode
	 * @return
	 */
	private String getGetIdTypeByGenMode(Class<?> clazz) {
		TableBind tableBind=clazz.getAnnotation(TableBind.class);
		String idGenMode = tableBind.idGenMode();
		String idType="Int";
		switch (idGenMode) {
		case JBoltIDGenMode.AUTO:
			idType="Int";
			break;
		case JBoltIDGenMode.AUTO_LONG:
			idType="Long";
			break;
		case JBoltIDGenMode.BIGSERIAL:
			idType="Int";
			break;
		case JBoltIDGenMode.SERIAL:
			idType="Int";
			break;
		case JBoltIDGenMode.UUID:
			idType="Str";
			break;
		case JBoltIDGenMode.SEQUENCE:
			idType="Int";
			break;
		case JBoltIDGenMode.SEQUENCE_LONG:
			idType="Long";
			break;
		case JBoltIDGenMode.SNOWFLAKE:
			idType="Long";
			break;
		default:
			idType="Int";
			break;
		}
		return idType;
	}
	
	/**
	 * 根据ID策略获取serivce里方法参数中的IDType
	 * @param idGenMode
	 * @return
	 */
	private String getServiceParamIdTypeByGenMode(Class<?> modelClass) {
		TableBind tableBind=modelClass.getAnnotation(TableBind.class);
		String idGenMode = tableBind.idGenMode();
		String idType="Integer";
		switch (idGenMode) {
		case JBoltIDGenMode.AUTO:
			idType="Integer";
			break;
		case JBoltIDGenMode.AUTO_LONG:
			idType="Long";
			break;
		case JBoltIDGenMode.BIGSERIAL:
			idType="Integer";
			break;
		case JBoltIDGenMode.SERIAL:
			idType="Integer";
			break;
		case JBoltIDGenMode.UUID:
			idType="String";
			break;
		case JBoltIDGenMode.SEQUENCE:
			idType="Integer";
			break;
		case JBoltIDGenMode.SEQUENCE_LONG:
			idType="Long";
			break;
		case JBoltIDGenMode.SNOWFLAKE:
			idType="Long";
			break;
		default:
			idType="Integer";
			break;
		}
		return idType;
	}
	
	
	/**
	 * 得到package物理路径
	 * @param targetPackageName
	 * @return
	 */
	private String getDirFromPackage(String targetPackageName) {
		return projectPath+"/src/main/java/"+targetPackageName.replace(".", "/");
	}
	
	/**
	 * 得到view物理路径
	 * @param viewFolder
	 * @return
	 */
	private String getDirFromViewForlder(String viewFolder) {
		if(viewFolder.charAt(0) != JBoltConst.SLASH) {
			viewFolder = JBoltConst.SLASH_STR + viewFolder;
		}
		return projectPath+"/src/main/webapp"+viewFolder;
	}
	
	private Filter<Method> commonGetMethodFilter=new Filter<Method>() {
		
		@Override
		public boolean accept(Method method) {
			String name=method.getName();
			boolean isBoolean=method.getReturnType()==Boolean.class;
			boolean isSortRank=method.getReturnType()==Integer.class&&method.getName().equalsIgnoreCase("getsortrank");
			boolean isPid=method.getName().equalsIgnoreCase("getpid")||method.getName().equalsIgnoreCase("getparentid");
			return name.startsWith("get")&&name.equalsIgnoreCase("getboolean")==false&&(isBoolean||isSortRank||isPid);
		}
	};
	
	/**
	 * 处理针对一个ModelClass在Service层要生成的方法
	 * @param clazz
	 * @param needPaginate 
	 * @return
	 */
	private List<JBoltGenMethod> processServiceGenMethods(JBoltMainLogicBean mainLogicBean) {
		//先得到通用的，然后判断有不需要的就删掉，有特殊的就新增
		List<JBoltGenMethod> genMehtods=getCommonServiceMethods(mainLogicBean);
		List<Method> getMethods=ClassUtil.getPublicMethods(mainLogicBean.getMainModelClass().getSuperclass(), commonGetMethodFilter);
		if(getMethods!=null&&getMethods.size()>0) {
			for(Method method:getMethods) {
				//返回值是boolean的 生成toogleXXX
				boolean isBoolean=method.getReturnType()==Boolean.class;
				if(isBoolean && mainLogicBean.isCrudType()) {
					genMehtods.add(new JBoltGenMethod(method.getName().replace("get", "toggle"),method.getAnnotation(JBoltField.class)));
					continue;
				}
				//getSortRank有的话 需要生成对应的上下 move 和初始化
				boolean isSortRank=method.getReturnType()==Integer.class&&method.getName().equalsIgnoreCase("getsortrank");
				if(isSortRank && mainLogicBean.isCrudType()) {
					genMehtods.add(new JBoltGenMethod("up"));
					genMehtods.add(new JBoltGenMethod("down"));
					genMehtods.add(new JBoltGenMethod("move"));
					genMehtods.add(new JBoltGenMethod("initRank"));
					mainLogicBean.setNeedSort(true);
					continue;
				}
				//如果有pid字段的getPid 说明表结构是父子tree关系结构
				boolean isPid= method.getName().equalsIgnoreCase("getpid")||method.getName().equalsIgnoreCase("getparentid");
				if(isPid) {
					genMehtods.add(new JBoltGenMethod("tree"));
					mainLogicBean.setHasPid(true);
					continue;
				}
			}
			
		}
		return genMehtods;
	}
	
	private Filter<Method> commonIndexHtmlGetMethodFilter=new Filter<Method>() {
		
		@Override
		public boolean accept(Method method) {
			String name=method.getName();
			boolean isPid=method.getName().equalsIgnoreCase("getpid")||method.getName().equalsIgnoreCase("getparentid");
			boolean isId=method.getName().equalsIgnoreCase("getid");
			boolean isPassword=method.getReturnType()==String.class&&(method.getName().equalsIgnoreCase("getpassword")||(method.getName().startsWith("get")&&method.getName().toLowerCase().endsWith("password")));
			return name.startsWith("get")&&!isId&&!isPassword&&!isPid&&name.equalsIgnoreCase("getboolean")==false;
		}
	};
	
	private Filter<Method> commonFormHtmlGetMethodFilter=new Filter<Method>() {
		
		@Override
		public boolean accept(Method method) {
			String name=method.getName();
			boolean isPid=method.getName().equalsIgnoreCase("getpid") || method.getName().equalsIgnoreCase("getparentid");
			boolean isId=method.getName().equalsIgnoreCase("getid");
			boolean isUserId=(
					method.getName().equalsIgnoreCase("getuserid")
					||
					method.getName().equalsIgnoreCase("getcreateuserid")
					||
					method.getName().equalsIgnoreCase("getupdateuserid")
					);
			boolean isCreateUpdateTime=
					method.getName().equalsIgnoreCase("getcreatetime")
					||
					method.getName().equalsIgnoreCase("getupdatetime")
					;
			boolean isPassword=method.getReturnType()==String.class&&(method.getName().equalsIgnoreCase("getpassword")||(method.getName().startsWith("get")&&method.getName().toLowerCase().endsWith("password")));
			return name.startsWith("get")&&!isUserId&&!isId&&!isPassword&&!isPid&&!isCreateUpdateTime&&name.equalsIgnoreCase("getboolean")==false;
		}
	};
	
	/**
	 * 处理针对一个ModelClass在Controller层要生成的方法
	 * @param clazz
	 * @param primaryKeyAttr 
	 * @param datas 
	 * @return
	 */
	
	private List<JBoltFieldBean> processIndexHtmlJboltField(Class<?> clazz, String primaryKeyAttr, Kv datas) {
		List<JBoltFieldBean> jboltFieldBeans=new ArrayList<JBoltFieldBean>();
		List<Method> getMethods=ClassUtil.getPublicMethods(clazz.getSuperclass(), commonIndexHtmlGetMethodFilter);
		boolean needSort=false;
		if(getMethods!=null&&getMethods.size()>0) {
			JBoltField field;
			for(Method method:getMethods) {
				field=method.getAnnotation(JBoltField.class);
				if(field==null) {
					continue;
				}
				if(field.name().equalsIgnoreCase("sortrank")) {
					needSort=true;
					continue;
				}
				if(field.name().equalsIgnoreCase("pwd_salt")) {
					needSort=true;
					continue;
				}
				if(field.name().equalsIgnoreCase("password")) {
					continue;
				}
				if(field.name().equalsIgnoreCase(primaryKeyAttr)) {
					continue;
				}
				jboltFieldBeans.add(new JBoltFieldBean(field));
			}
			//按照order
			jboltFieldBeans=CollUtil.sortByProperty(jboltFieldBeans,"order");
		}
		datas.set("needSort",needSort);
		datas.set("ths",jboltFieldBeans);
		return jboltFieldBeans;
	}
	
	/**
	 * 处理针对一个ModelClass在Form里的字段元素
	 * @param clazz
	 * @param primaryKeyAttr
	 * @param datas 
	 * @return
	 */
	private List<JBoltFieldBean> processFormHtmlJboltField(Class<?> clazz,String primaryKeyAttr, Kv datas) {
		List<JBoltFieldBean> jboltFieldBeans=new ArrayList<JBoltFieldBean>();
		List<Method> getMethods=ClassUtil.getPublicMethods(clazz.getSuperclass(), commonFormHtmlGetMethodFilter);
		if(getMethods!=null&&getMethods.size()>0) {
			JBoltField field;
			String name;
			JBoltFieldBean bean;
			for(Method method:getMethods) {
				field=method.getAnnotation(JBoltField.class);
				if(field==null) {
					continue;
				}
				name = field.name().toLowerCase();
				bean = new JBoltFieldBean(field);
				if(name.equalsIgnoreCase("sortrank")) {
					continue;
				}
				if(name.equalsIgnoreCase("enable")) {
					continue;
				}
				if(name.equalsIgnoreCase("pwd_salt")) {
					continue;
				}
				if(name.equalsIgnoreCase(primaryKeyAttr)) {
					continue;
				}
				if(bean.getType().equalsIgnoreCase("boolean")) {
					bean.setUiType(JBoltFieldBean.TYPE_RADIO_BOOLEAN);
				}else if(bean.getType().equalsIgnoreCase("Integer") || bean.getType().equalsIgnoreCase("BigDecimal")) {
					if(bean.getType().equalsIgnoreCase("BigDecimal")&&bean.getFixed()>0) {
						bean.setUiType(JBoltFieldBean.TYPE_INPUT_PNUMBER);
					}
					if(name.indexOf("year")!=-1) {
						bean.setUiType(JBoltFieldBean.TYPE_YEAR);
					}else if(name.equalsIgnoreCase("age")||name.indexOf("_age")!=-1) {
						if(bean.getType().equalsIgnoreCase("Integer") && bean.getMaxLength()>3) {
							bean.setUiType(JBoltFieldBean.TYPE_INPUT_PINT);
							bean.setMaxLength(3);
							bean.setFixed(0);
						}
					}else if(name.endsWith("_category") || name.indexOf("category_id")!=-1){
						bean.setUiType(JBoltFieldBean.TYPE_SELECT);
					}else if(name.endsWith("_type") || name.indexOf("type_id")!=-1){
						bean.setUiType(JBoltFieldBean.TYPE_SELECT);
					}else if(name.indexOf("state")!=-1 || name.indexOf("states")!=-1 || name.indexOf("status")!=-1){
						bean.setUiType(JBoltFieldBean.TYPE_SELECT);
					}else if(name.endsWith("_id")){
						bean.setUiType(JBoltFieldBean.TYPE_AUTOCOMPLETE);
					}else {
						bean.setUiType(JBoltFieldBean.TYPE_INPUT_PNUMBER);
					}
				}else if(bean.getType().equalsIgnoreCase("Date")) {
					bean.setUiType(JBoltFieldBean.TYPE_DATE);
				}else if(bean.getType().equalsIgnoreCase("Time")) {
					bean.setUiType(JBoltFieldBean.TYPE_TIME);
				}else {
					if(name.endsWith("_pwd") || name.endsWith("password") || name.indexOf("password")!=-1) {
						bean.setUiType(JBoltFieldBean.TYPE_INPUT_PASSWORD);
					}else if(name.endsWith("_ids")) {
						bean.setUiType(JBoltFieldBean.TYPE_CHECKBOX);
					}else if(name.endsWith("_type") || name.indexOf("type_id")!=-1){
						bean.setUiType(JBoltFieldBean.TYPE_SELECT);
					}else if(name.endsWith("_category") || name.indexOf("category_id")!=-1){
						bean.setUiType(JBoltFieldBean.TYPE_SELECT);
					}else if(name.indexOf("state")!=-1 || name.indexOf("states")!=-1 || name.indexOf("status")!=-1){
						bean.setUiType(JBoltFieldBean.TYPE_SELECT);
					}else if(name.endsWith("_id")) {
						bean.setUiType(JBoltFieldBean.TYPE_AUTOCOMPLETE);
					}else if(name.indexOf("week")!=-1) {
						bean.setUiType(JBoltFieldBean.TYPE_WEEK);
					}else if(name.indexOf("month")!=-1) {
						bean.setUiType(JBoltFieldBean.TYPE_MONTH);
					}else if(name.indexOf("year")!=-1) {
						bean.setUiType(JBoltFieldBean.TYPE_YEAR);
					}
				}
				jboltFieldBeans.add(bean);
			}
			//按照order
			jboltFieldBeans=CollUtil.sortByProperty(jboltFieldBeans,"order");
		}
		datas.set("cols",jboltFieldBeans);
		return jboltFieldBeans;
	}
	
	/**
	 * 处理针对一个ModelClass在Controller层要生成的方法
	 * @param mainLogicBean
	 * @return
	 */
	private List<String> processContrllerGenMethods(JBoltMainLogicBean mainLogicBean) {
		//先得到通用的，然后判断有不需要的就删掉，有特殊的就新增
		List<String> genMehtods=getCommonControllerMethods(mainLogicBean);
		//得到model所有public getter method
		List<Method> getMethods=ClassUtil.getPublicMethods(mainLogicBean.getMainModelClass().getSuperclass(), commonGetMethodFilter);
		if(getMethods!=null&&getMethods.size()>0) {
			//每个getter 循环处理
			for(Method method:getMethods) {
				//boolean类型特殊处理 需要提供toggleXXX方法
				boolean isBoolean=method.getReturnType()==Boolean.class;
				if(isBoolean && mainLogicBean.isCrudType()) {
					genMehtods.add(method.getName().replace("get", "toggle"));
					continue;
				}
				//如果是存在sort_rank字段的getSortRank方法 那就需要加排序了
				boolean isSortRank=method.getReturnType()==Integer.class&&method.getName().equalsIgnoreCase("getsortrank");
				if(isSortRank && mainLogicBean.isCrudType()) {
					//排序三兄弟 上下和初始化 后面还得加动态move
					genMehtods.add("up");
					genMehtods.add("down");
					genMehtods.add("move");
					genMehtods.add("initRank");
					continue;
				}
				//如果有pid字段的getPid 说明表结构是父子tree关系结构
				boolean isPid=method.getReturnType()==Integer.class&&method.getName().equalsIgnoreCase("getpid");
				if(isPid) {
					genMehtods.add("tree");
					continue;
				}
			}
		}
		return genMehtods;
	}

	/**
	  *写入文件 如果存在 不进行任何操作 不覆盖
	 * @param targetOutputDir
	 * @param fileName
	 * @param content
	 * @throws IOException
	 */
	private void writeToFile(String targetOutputDir,String fileName,String content){
		if(FileUtil.exist(targetOutputDir)==false) {
			FileUtil.mkdir(targetOutputDir);
		}
		
		String target = targetOutputDir + File.separator +fileName;
		if(FileUtil.exist(target)==false) {
			FileUtil.writeUtf8String(content, target);
		}
		
	}
	
	/**
	 * 添加到生成队列中
	 * @param mainLogicBean
	 */
	public void addGenBean(JBoltMainLogicBean mainLogicBean) {
		genLogicBeans.add(mainLogicBean);
	}
	

	/**
	 * 项目路径
	 */
	protected static final String projectPath=System.getProperty("user.dir");
	private static final String controllerTemplate = "/gentpl/controller_template.jf";
	private static final String controllerCommonTemplate = "/gentpl/controller_common_template.jf";
	private static final String serviceCommonTemplate = "/gentpl/service_common_template.jf";
	private static final String serviceTemplate = "/gentpl/service_template.jf";
	private static final String indexHtmlTemplate = "/gentpl/index_html_template.jf";
	private static final String addHtmlTemplate = "/gentpl/add_html_template.jf";
	private static final String editHtmlTemplate = "/gentpl/edit_html_template.jf";
	private static final String formHtmlTemplate = "/gentpl/form_html_template.jf";
	//生成的配置队列
	private List<JBoltMainLogicBean> genLogicBeans=new ArrayList<JBoltMainLogicBean>();
	//模板引擎实例
	private Engine engine;

}






