package cn.jbolt.common.gen;

import java.util.Set;

import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
/**
 * 用于生成postgresql里的对应序列
 * @ClassName:  OracleInitSqlGen   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年12月15日   
 */
public class JBoltPostgresqlSequenceSqlGen {
	private static final String SCHEME="public";
	private static final String OWNER="postgres";
	private static final String packageName="cn.jbolt.common.model.base";
	public static void genSequenceSql(Set<Class<?>> classes) {
		String seqName;
		for(Class<?> clazz:classes) {
			seqName=StrUtil.toSymbolCase(clazz.getSimpleName().replace("Base", "jb"), '_')+"_idsq";
			System.out.println(String.format("DROP SEQUENCE IF EXISTS %s;\n"+ 
					"CREATE SEQUENCE %s.%s\n"+
					"    INCREMENT 1\n" + 
					"    START 100001\n" + 
					"    MINVALUE 100001\n" + 
					"    MAXVALUE 9223372036854775807\n" + 
					"    CACHE 50;\n"+ 
					"ALTER SEQUENCE %s.%s OWNER TO %s;",seqName,SCHEME,seqName,SCHEME,seqName,OWNER));
		}
	}
	 
	public static void gen() {
		Set<Class<?>> classes=ClassUtil.scanPackage(packageName);
		genSequenceSql(classes);
	}
	public static void main(String[] args) {
		JBoltPostgresqlSequenceSqlGen.gen();
	}
}
