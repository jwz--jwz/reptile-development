package cn.jbolt.common.gen;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.jbolt.base.JBoltIDGenMode;
import cn.jbolt.common.db.sql.DBType;
import cn.jbolt.common.util.JBoltConsoleUtil;
/**
 * model上的TableBind注解自动生成器
 * @ClassName:  ModelTableBindGen   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年6月19日   
 */
public class ModelTableBindGen {
	public static final String SEPARATOR=File.separator;
	
	public static void main(String[] args) {
		//项目目录 默认动态获取
		String projectPath=System.getProperty("user.dir");
		//生成用的配置 构建
		List<Kv> kvs=new ArrayList<Kv>();
		//添加一个数据源和model package配置 主数据源用main 固定 后面的package填写具体model存在的包
		kvs.add(Kv.by("configName", "main")
				.set("dbType",DBType.MYSQL)
				.set("idGenMode",JBoltIDGenMode.AUTO)
				.set("package","cn.jbolt.common.model")
				.set("tablePrefix","jb_")//注意 如果你的model生成时已经带着前缀生成的就不要设置了
				);
		//执行生成注解
		ModelTableBindGen.me.gen(projectPath,kvs,true);
	}
	
	
	
	
	
	public static final ModelTableBindGen me=new ModelTableBindGen();
	private ModelTableBindGen() {
	}
	
	public void gen(String projectPath,List<Kv> kvs, boolean overrideIfExist) {
		kvs.forEach(kv->processOneDataSource(projectPath, kv,overrideIfExist));
	}

	private static void processOneDataSource(String projectPath,Kv kv,boolean overrideIfExist) {
		String configName=kv.getStr("configName");
		String tablePrefix=kv.getStr("tablePrefix");
		String packageName=kv.getStr("package");
		String dbType=kv.getStr("dbType");
		String idGenMode=kv.getStr("idGenMode");
		List<File> files = FileUtil.loopFiles(projectPath + SEPARATOR +"src"+SEPARATOR+"main"+SEPARATOR+"java"+SEPARATOR+packageName.replace(".", SEPARATOR),
				new FileFilter() {
					@Override
					public boolean accept(File file) {
						return FileUtil.isFile(file) && file.getName().endsWith(".java")
								&& file.getName().indexOf("_MappingKit") == -1
								&& file.getPath().indexOf(SEPARATOR+"base"+SEPARATOR) == -1;
					}
				});
		if (files == null || files.size() == 0) {
			return;
		}
		files.forEach(file -> {
			String path=file.getAbsolutePath();
			int sepIndex=path.lastIndexOf(SEPARATOR);
			String baseFilePath=path.substring(0,sepIndex)+SEPARATOR+"base"+SEPARATOR+"Base"+path.substring(sepIndex+1);
			String primaryKey=getPrimaryKey(baseFilePath);
			JBoltConsoleUtil.printMessageWithDate(primaryKey);
			genTableBind(configName,dbType,tablePrefix,primaryKey,idGenMode,file,overrideIfExist);
		});
	}
	
	private static String getPrimaryKey(String baseFilePath) {
		if(FileUtil.exist(baseFilePath)==false) {return null;}
		List<String> codeLines = FileUtil.readLines(baseFilePath, CharsetUtil.UTF_8);
		if (codeLines == null || codeLines.size() == 0) {
			return null;
		}
		String primaryKey=null;
		List<String> arrs;
		for(String codeLine:codeLines) {
			if(StrKit.notBlank(codeLine)&&codeLine.trim().indexOf("JBoltField")!=-1&&codeLine.trim().indexOf("columnName")!=-1) {
				arrs=StrUtil.splitTrim(codeLine.trim().replace("@JBoltField(", "").replace(")", "")	, ",");
				primaryKey=arrs.get(1).replace("columnName=", "").replace("\"", "");
				break;
			}
		}
		return primaryKey;
	}

	/**
	 * 生成
	 * @param configName
	 * @param dbType
	 * @param tablePrefix
	 * @param primaryKey
	 * @param file
	 * @param overrideIfExist
	 */
	private static void genTableBind(String configName,String dbType,String tablePrefix,String primaryKey,String idGenMode,File file,boolean overrideIfExist) {
		System.out.println("正在处理:"+configName+":"+file.getName());
		List<String> codeLines = FileUtil.readLines(file, CharsetUtil.UTF_8);
		if (codeLines == null || codeLines.size() == 0) {
			return;
		}
		if(StrKit.isBlank(dbType)) {
			dbType = DBType.MYSQL;
		}
		if(StrKit.isBlank(idGenMode)) {
			switch (dbType) {
				case DBType.MYSQL:
					idGenMode=JBoltIDGenMode.AUTO;
					break;
				case DBType.SQLSERVER:
					idGenMode=JBoltIDGenMode.AUTO;
					break;
				case DBType.DM:
					idGenMode=JBoltIDGenMode.AUTO;
					break;
				case DBType.ORACLE:
					idGenMode=JBoltIDGenMode.SNOWFLAKE;
					break;
				case DBType.POSTGRESQL:
					idGenMode=JBoltIDGenMode.SNOWFLAKE;
					break;
				default:
					idGenMode=JBoltIDGenMode.AUTO;
					break;
			}
		}
		int tableBindLine = 0;
		int importLine = 0;
		int lineSize = codeLines.size();
		boolean skip = false;
		boolean hasOverrideIfExist=false;
		boolean importExist=false;
		for (int i = 0; i < lineSize; i++) {
			if(codeLines.get(i).trim().indexOf("cn.jbolt.common.config.TableBind")!=-1) {
				importExist=true;
			}
			if (codeLines.get(i).trim().indexOf("@TableBind") != -1) {
				if(overrideIfExist) {
					String className = StrUtil.toUnderlineCase(file.getName().replace(".java", ""));
					if(StrKit.isBlank(primaryKey)) {
						primaryKey="id";
					}
					codeLines.set(i, "@TableBind(dataSource = \"" + configName + "\" , table = \""+(StrKit.isBlank(tablePrefix)?"":tablePrefix) + className + "\" , primaryKey = \""+primaryKey+"\" , idGenMode = \""+idGenMode+"\")");
					hasOverrideIfExist=true;
				}else {
					skip = true;
				}
				break;
			}
			if (importLine == 0 && codeLines.get(i).trim().toLowerCase().indexOf("import ") != -1) {
				importLine = i;
			}
			if (codeLines.get(i).trim().toLowerCase().indexOf("public class") != -1) {
				tableBindLine = i;
				break;
			}
		}
		if (skip == false) {
			if (tableBindLine > 0&&hasOverrideIfExist==false) {
				String className = StrUtil.toUnderlineCase(file.getName().replace(".java", ""));
				if(StrKit.isBlank(primaryKey)) {
					primaryKey="id";
				}
				codeLines.add(tableBindLine,"@TableBind(dataSource = \"" + configName + "\" , table = \""+(StrKit.isBlank(tablePrefix)?"":tablePrefix) + className + "\" , primaryKey = \""+primaryKey+"\" , idGenMode = \""+idGenMode+"\")");
			}
			if(importLine>0&&importExist==false) {
				codeLines.add(importLine, "import cn.jbolt.common.config.TableBind;");
			}
			FileUtil.writeLines(codeLines, file, CharsetUtil.UTF_8);
		}
	}
}
