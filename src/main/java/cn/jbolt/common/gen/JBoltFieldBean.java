package cn.jbolt.common.gen;

public class JBoltFieldBean {
	public final static String TYPE_INPUT="input";
	public final static String TYPE_INPUT_PASSWORD="password";
	public final static String TYPE_INPUT_PNUMBER="input_pnumber";
	public final static String TYPE_INPUT_PINT="input_pint";
	public final static String TYPE_INPUT_PZINT="input_pzint";
	public final static String TYPE_INPUT_INT="input_int";
	public final static String TYPE_INPUT_PZNUMBER="input_pznumber";
	public final static String TYPE_INPUT_NUMBER="input_number";
	public final static String TYPE_HIDDEN="hidden";
	public final static String TYPE_SELECT="select";
	public final static String TYPE_SELECT2="select2";
	public final static String TYPE_AUTOCOMPLETE="autocomplete";
	public final static String TYPE_CHECKBOX="checkbox";
	public final static String TYPE_RADIO="radio";
	public final static String TYPE_RADIO_BOOLEAN="radio_boolean";
	public final static String TYPE_TEXTAREA="textarea";
	public final static String TYPE_IMGUPLOADER="imguploader";
	public final static String TYPE_FILEUPLOADER="fileuploader";
	public final static String TYPE_NEDITOR="neditor";
	public final static String TYPE_SUMMERNOTE="summernote";
	public final static String TYPE_DATETIME="datetime";
	public final static String TYPE_DATE="date";
	public final static String TYPE_TIME="time";
	public final static String TYPE_YEAR="year";
	public final static String TYPE_MONTH="month";
	public final static String TYPE_WEEK="week";
	
	private String name;
	private String columnName;
	private String type;
	private String remark;
	private String uiType;//组件UI类型
	private boolean required;
	private int maxLength;
	private int fixed;
	private int order;//顺序
	public JBoltFieldBean(JBoltField jBoltField) {
		this(jBoltField.name(), jBoltField.columnName(), jBoltField.type(), jBoltField.remark(),jBoltField.required(),jBoltField.maxLength(),jBoltField.fixed(),jBoltField.order());
	}
	public JBoltFieldBean(String name,String columnName,String type,String remark,boolean required,int maxLength,int fixed,int order) {
		this(name, columnName, type, remark,TYPE_INPUT,required,maxLength,fixed,order);
	}
	public JBoltFieldBean(String name,String columnName,String type,String remark,String uiType,boolean required,int maxLength,int fixed, int order) {
		this.name=name;
		this.columnName=columnName;
		this.type=type;
		this.remark=remark;
		this.uiType=uiType;
		this.required = required;
		this.maxLength = maxLength;
		this.fixed = fixed;
		this.order=order;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getUiType() {
		return uiType;
	}
	public void setUiType(String uiType) {
		this.uiType = uiType;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public boolean isRequired() {
		return required;
	}
	public boolean getRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public int getMaxLength() {
		return maxLength;
	}
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}
	public int getFixed() {
		return fixed;
	}
	public void setFixed(int fixed) {
		this.fixed = fixed;
	}
}
