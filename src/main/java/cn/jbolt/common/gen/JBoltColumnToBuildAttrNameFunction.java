package cn.jbolt.common.gen;
/**
 *  JBolt平台生成器里用来处理数据库字段名转为Java属性名和驼峰名的处理函数
 * @ClassName:  JBoltColumnToBuildAttrNameFunction   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月22日   
 *    
 */
public interface JBoltColumnToBuildAttrNameFunction {
	public String build(String column);
}
