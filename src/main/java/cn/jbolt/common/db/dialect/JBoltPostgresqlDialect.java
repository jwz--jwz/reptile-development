package cn.jbolt.common.db.dialect;

import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.Table;
import com.jfinal.plugin.activerecord.dialect.PostgreSqlDialect;
/**
 * JBolt Postgresql方言 继承自JFinal内置 
 * @ClassName:  JBoltPostgresqlDialect   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年9月17日   
 *    
 * 注意：本内容仅限于JFinal学院 JBolt平台VIP成员内部传阅，请尊重开发者劳动成果，不要外泄出去用于其它商业目的
 */
public class JBoltPostgresqlDialect extends PostgreSqlDialect {
	@Override
	public void forDbSave(String tableName, String[] pKeys, Record record, StringBuilder sql, List<Object> paras) {
		tableName = tableName.trim();
		trimPrimaryKeys(pKeys);
		
		sql.append("insert into \"");
		sql.append(tableName).append("\"(");
		StringBuilder temp = new StringBuilder();
		temp.append(") values(");
		
		int count = 0;
		for (Entry<String, Object> e: record.getColumns().entrySet()) {
			String colName = e.getKey();
			if (count++ > 0) {
				sql.append(", ");
				temp.append(", ");
			}
			sql.append('\"').append(colName).append('\"');
			
			Object value = e.getValue();
			if (value instanceof String && isPrimaryKey(colName, pKeys) && ((String)value).indexOf("nextval('")!=-1) {
			    temp.append(value);
			} else {
				temp.append('?');
				paras.add(value);
			}
		}
		sql.append(temp.toString()).append(')');
	}
	@Override
	public void forModelSave(Table table, Map<String, Object> attrs, StringBuilder sql, List<Object> paras) {
		sql.append("insert into ").append(table.getName()).append('(');
		StringBuilder temp = new StringBuilder(") values(");
		String[] pKeys = table.getPrimaryKey();
		int count = 0;
		for (Entry<String, Object> e: attrs.entrySet()) {
			String colName = e.getKey();
			if (table.hasColumnLabel(colName)) {
				if (count++ > 0) {
					sql.append(", ");
					temp.append(", ");
				}
				sql.append(colName);
				Object value = e.getValue();
				if (value instanceof String && isPrimaryKey(colName, pKeys) && ((String)value).indexOf("nextval('")!=-1) {
				    temp.append(value);
				} else {
				    temp.append('?');
				    paras.add(value);
				}
			}
		}
		sql.append(temp.toString()).append(')');
	}
	
	/**
	 * 解决 PostgreSql 获取自增主键时 rs.getObject(1) 总是返回第一个字段的值，而非返回了 id 值
	 * issue: https://www.oschina.net/question/2312705_2243354
	 * 
	 * 相对于 Dialect 中的默认实现，仅将 rs.getXxx(1) 改成了 rs.getXxx(pKey)
	 */
	public void getModelGeneratedKey(Model<?> model, PreparedStatement pst, Table table) throws SQLException {
		String[] pKeys = table.getPrimaryKey();
		ResultSet rs = pst.getGeneratedKeys();
		for (String pKey : pKeys) {
			if (model.get(pKey) == null || model.get(pKey).toString().indexOf("nextval('")!=-1) {
				if (rs.next()) {
					Class<?> colType = table.getColumnType(pKey);
					if (colType != null) {
						if (colType == Integer.class || colType == int.class) {
							model.set(pKey, rs.getInt(pKey));
						} else if (colType == Long.class || colType == long.class) {
							model.set(pKey, rs.getLong(pKey));
						} else if (colType == BigInteger.class) {
							processGeneratedBigIntegerKey(model, pKey, rs.getObject(pKey));
						} else {
							model.set(pKey, rs.getObject(pKey));
						}
					}
				}
			}
		}
		rs.close();
	}
	
	/**
	 * 解决 PostgreSql 获取自增主键时 rs.getObject(1) 总是返回第一个字段的值，而非返回了 id 值
	 * issue: https://www.oschina.net/question/2312705_2243354
	 * 
	 * 相对于 Dialect 中的默认实现，仅将 rs.getXxx(1) 改成了 rs.getXxx(pKey)
	 */
	public void getRecordGeneratedKey(PreparedStatement pst, Record record, String[] pKeys) throws SQLException {
		ResultSet rs = pst.getGeneratedKeys();
		for (String pKey : pKeys) {
			if (record.get(pKey) == null || record.get(pKey).toString().indexOf("nextval('")!=-1) {
				if (rs.next()) {
					record.set(pKey, rs.getObject(pKey));
				}
			}
		}
		rs.close();
	}
}
