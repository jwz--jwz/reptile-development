package cn.jbolt.common.db.sql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.jfinal.kit.LogKit;
import com.jfinal.kit.StrKit;

/**
 * Sql语句拼接工具 面向对象方式
 * 
 * @ClassName: Sql
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2019年9月5日
 */
@SuppressWarnings("serial")
public class Sql implements Serializable, Cloneable{
	/**
	 * 内置常量 可用于其他数据库标识boolean true
	 */
	public static final String TRUE="1";
	/**
	 * 内置常量 可用于其他数据库标识boolean false
	 */
	public static final String FALSE="0";
	/**
	 * 数据库默认类型
	 */
	protected String dbType = DBType.MYSQL;
	/**
	 * 查询搜索 select
	 */
	public static final int TYPE_SELECT = 1;
	/**
	 * 插入数据 sigle
	 */
	public static final int TYPE_INSERT = 2;
	/**
	 * 插入数据 batch
	 */
	public static final int TYPE_INSERT_BATCH = 3;
	/**
	 * 更新数据 update
	 */
	public static final int TYPE_UPDATE = 4;
	/**
	 * 删除数据 delete
	 */
	public static final int TYPE_DELETE = 5;

	/************** 数据库关键字 *****************/

	public static final String KEY_SELECT = "select ";
	public static final String KEY_STAR = "*";
	public static final String KEY_QUESTION_MARK = "?";
	public static final String KEY_INSERT = "insert into ";
	public static final String KEY_UPDATE = "update ";
	public static final String KEY_DELETE = "delete ";
	public static final String KEY_LIMIT = " limit ";
	public static final String KEY_OFFSET = " offset ";
	public static final String KEY_ROWNUM_ORACLE = " rownum ";
	public static final String KEY_FROM = " from ";
	public static final String KEY_SET = " set ";
	public static final String KEY_WHERE = " where ";
	public static final String KEY_ORDERBY = " order by ";
	public static final String KEY_GROUPBY = " group by ";
	public static final String KEY_WHITESPACE = " ";
	public static final String KEY_COMMA = ",";
	public static final String KEY_AS = " as ";
	public static final String KEY_ON = " on ";
	public static final String KEY_ASC = " asc";
	public static final String KEY_DESC = " desc";
	public static final String KEY_COUNT_START = "count(*)";
	public static final String KEY_COUNT_COLUMN = "count(%s)";
	public static final String KEY_DISTINCT = "distinct ";
	public static final String KEY_COUNT_DISTINCT = "count(distinct %s)";
	public static final String KEY_INSERT_VALUES = " values(%s) ";
	public static final String KEY_NULLSTRING = "";
	public static final String KEY_RAND = "rand()";
	public static final String KEY_AND = " and ";
	public static final String KEY_LEFT_JOIN = " left join ";
	public static final String KEY_LEFT_OUTER_JOIN = " left outer join ";
	public static final String KEY_RIGHT_JOIN = " right join ";
	public static final String KEY_RIGHT_OUTER_JOIN = " right outer join ";
	public static final String KEY_INNER_JOIN = " inner join ";
	public static final String KEY_FULL_JOIN = " full join ";
	public static final String KEY_ID = "id";
	public static final String KEY_PID = "pid";
	public static final String KEY_BRACKET_LEFT="(";
	public static final String KEY_BRACKET_RIGHT=")";
	public static final String KEY_UNION = " union ";
	public static final String KEY_UNION_ALL = " union all ";

	// 语句操作类型
	private int type;
	// 表名
	private String table;
	// 搜索返回字段
	private String returnColumns;
	// 查询单列列表
	private boolean queryColumnList;
	// 查询数量
	private boolean queryCount;
	// 查询最大值
	private boolean queryMax;
	// 查询distinct数量
	private boolean queryDistinctCount;
	// where后的条件
	private List<Condition> conditions;
	// insert values
	private Object[] insertValues;
	// 需要更新的字段map
	private Map<String, Object> updateColumnsMap;
	// 分组查询
	private String groupBy;
	// 排序查询
	private String orderBy;
	// 关联查询语句
	private List<String> joinSqls;
	// 查询数量 字段
	private String countColumns;
	// prepared sql需要的值
	private List<Object> whereValues;
	// 是否倒叙
	private boolean desc;
	// 是否支持问号占位方式
	private boolean prepared;
	// 是否启用分页
	private boolean hasPage;
	// 分页参数 第几页
	private int pageNumber;
	// 分页参数 每页几个
	private int pageSize;
	//union的sql对象
	private List<UnionSql> unionSqls;
	//主表的asName
	private String mainTableAsName;

	/**
	 * 只能从这里获得Sql 指定数据类型
	 * 
	 * @return
	 */
	public static Sql me(String dbType) {
		return new Sql().setDbType(dbType);
	}

	/**
	 * 支持问号占位sql
	 * 
	 * @return
	 */
	public Sql prepared() {
		this.prepared = true;
		return this;
	}
	/**
	 * 首页
	 * @param pageSize
	 * @return
	 */
	public Sql firstPage(int pageSize) {
		return page(1, pageSize);
	}
	/**
	 * 限制了查询一个
	 * @return
	 */
	public boolean isLimitOne() {
		return this.pageSize==1;
	}
	/**
	 * 分页查询
	 * 
	 * @return
	 */
	public Sql page(int pageNumber, int pageSize) {
		if(pageNumber<=0) {
			throw new RuntimeException("Sql pageNumber must >0");
		}
		if(pageSize<=0) {
			throw new RuntimeException("Sql PageSize must >0");
		}
		this.hasPage = true;
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		if(pageNumber==1&&pageSize==1&&DBType.SQLSERVER.equalsIgnoreCase(dbType)) {
			this.returnColumns="top 1 "+returnColumns;
		}
		return this;
	}

	/**
	 * 分页查询 符合条件第一个
	 * 
	 * @return
	 */
	public Sql first() {
		return page(1, 1);
	}

	/**
	 * 随机一个
	 * 
	 * @return
	 */
	public Sql randomOne() {
		orderBy(KEY_RAND);
		return page(1, 1);
	}

	/**
	 * 构造函数
	 */
	private Sql() {
		this.returnColumns = KEY_STAR;
		this.conditions = new ArrayList<>();
	}

	/**
	 * 查询表
	 * 
	 * @param table
	 * @param asName
	 * @return
	 */
	public Sql from(String table, String asName) {
		this.mainTableAsName = asName;
		this.table = table + KEY_WHITESPACE + asName;
		return this;
	}
	/**
	 * 查询 sql返回值
	 * 
	 * @param table
	 * @param asName
	 * @return
	 */
	public Sql from(Sql sql, String asName) {
		if(StrKit.isBlank(asName)) {
			asName="this";
		}
		this.mainTableAsName = asName;
		this.table = KEY_BRACKET_LEFT + sql.toSql() + KEY_BRACKET_RIGHT + KEY_WHITESPACE + asName;
		return this;
	}
	/**
	 * 查询 sql返回值
	 * 
	 * @param table
	 * @return
	 */
	public Sql from(Sql sql) {
		String asName="this";
		if(StrKit.notBlank(sql.table)) {
			asName=sql.table+"_tmp";
		}
		return from(sql, asName);
	}
	/**
	 * 判断是Mysql
	 * @return
	 */
	public boolean isMysql() {
		return DBType.isMysql(dbType);
	}
	/**
	 * 判断是达梦
	 * @return
	 */
	public boolean isDM() {
		return DBType.isDM(dbType);
	}
	/**
	 * 判断是Postgresql
	 * @return
	 */
	public boolean isPostgresql() {
		return DBType.isPostgresql(dbType);
	}
	/**
	 * 判断是Oracle
	 * @return
	 */
	public boolean isOracle() {
		return DBType.isOracle(dbType);
	}
	/**
	 * 判断是SqlServer
	 * @return
	 */
	public boolean isSqlServer() {
		return DBType.isSqlServer(dbType);
	}

	

	/**
	 * 查询表
	 * 
	 * @param table
	 * @return
	 */
	public Sql from(String table) {
		this.table = table;
		return this;
	}
	
	/**
	 * 查询表
	 * @param table
	 * @return
	 */
	public Sql fromIfBlank(String table) {
		if(StrKit.isBlank(this.table)) {
			return from(table);
		}
		return this;
	}
	
	/**
	 * columnName is null
	 * 
	 * @return
	 */
	public Sql isNull(String columnName) {
		processAnd();
		conditions.add(new Condition().isNull(processPrependMainTableAsName(columnName)));
		return this;
	}

	/**
	 * columnName is not null
	 * 
	 * @return
	 */
	public Sql isNotNull(String columnName) {
		processAnd();
		conditions.add(new Condition().isNotNull(processPrependMainTableAsName(columnName)));
		return this;
	}

	/**
	 * 查询表
	 * 
	 * @param Sql
	 * @return
	 */
	public Sql fromSql(Sql sql) {
		this.table = Condition.BRACKET_LEFT + sql.toSql() + Condition.BRACKET_RIGHT;
		return this;
	}

	/**
	 * 查询表
	 * 
	 * @param Sql
	 * @param as
	 * @return
	 */
	public Sql fromSql(Sql sql, String as) {
		this.table = Condition.BRACKET_LEFT + sql.toSql() + Condition.BRACKET_RIGHT + KEY_WHITESPACE + as
				+ KEY_WHITESPACE;
		return this;
	}

	/**
	 * 设置
	 * 
	 * @return
	 */
	public Sql values(Object... values) {
		this.insertValues = values;
		return this;
	}

	/**
	 * 更新表
	 * 
	 * @param table
	 * @return
	 */
	public Sql update(String table) {
		this.table = table;
		this.type = TYPE_UPDATE;
		return this;
	}

	
	/**
	 * 插入数据
	 * 
	 * @param tableAndColumns
	 * @return
	 */
	public Sql insert(String tableAndColumns) {
		this.table = tableAndColumns;
		this.type = TYPE_INSERT;
		return this;
	}
	/**
	 * 插入数据
	 * 
	 * @param table
	 * @param insertColumns
	 * @return
	 */
	public Sql insert(String table,String insertColumns) {
		this.table = table+"("+insertColumns+")";
		this.type = TYPE_INSERT;
		return this;
	}
	/**
	 * 插入数据
	 * 
	 * @param table
	 * @param insertColumns
	 * @param values
	 * @return
	 */
	public Sql insert(String table,String insertColumns,Object... values) {
		this.table = table+"("+insertColumns+")";
		this.type = TYPE_INSERT;
		values(values);
		return this;
	}

	/**
	 * 删除表数据
	 * 
	 * @param table
	 * @return
	 */
	public Sql delete() {
		this.type = TYPE_DELETE;
		return this;
	}
	/**
	 * 查询ID
	 * @return
	 */
	public Sql selectId() {
		return select(KEY_ID);
	}

	/**
	 * 搜索表中的指定列
	 * 
	 * @param table
	 * @param columns
	 * @return
	 */
	public Sql select(String... columns) {
		this.type = TYPE_SELECT;
		// 如果是null 直接按照* 查询
		if (columns == null || (columns.length==1 && columns[0]==null)) {
			this.queryColumnList = false;
			returnColumns = KEY_STAR;
			return this;
		}
		// 判断查询columns个数
		int size = columns.length;
		if (size == 0) {
			// 没有就按照星
			this.queryColumnList = false;
			returnColumns = KEY_STAR;
		} else if (size == 1) {
			// 一个column 可能是单个列查询列表 如果带着表达式 可能是查询表达式结果
			this.queryColumnList = true;
			if (columns[0].indexOf("max(") != -1) {
				this.queryMax = true;
			}
			returnColumns = columns[0];
		} else {
			this.queryColumnList = false;
			// 多个肯定就是分开了
			returnColumns = "";
			for (int i = 0; i < size; i++) {
				returnColumns += columns[i];
				if (i != size - 1) {
					returnColumns += ",";
				}
			}
		}

		return this;
	}

	/**
	 * 条件 or
	 * 
	 * @return
	 */
	public Sql or() {
		conditions.add(new Condition().or());
		return this;
	}

	/**
	 * 条件 and
	 * 
	 * @return
	 */
	public Sql and() {
		conditions.add(new Condition().and());
		return this;
	}
	
	/**
	 * 条件 in(数组值) list
	 * 
	 * @param columnName
	 * @param inValues
	 * @return
	 */
	public Sql inList(String columnName, List<?> inValues) {
		processAnd();
		conditions.add(new Condition().in(processPrependMainTableAsName(columnName), inValues.toArray(new Object[inValues.size()])));
		return this;
	}
	/**
	 * 条件 in(数组值)
	 * 
	 * @param columnName
	 * @param inValues
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Sql in(String columnName, Object... inValues) {
		if(inValues!=null&&inValues.length>0) {
			if(inValues.length==1&&inValues[0] instanceof List) {
				return inList(columnName, (List)inValues[0]);
			}
			processAnd();
			conditions.add(new Condition().in(processPrependMainTableAsName(columnName), inValues));
		}
		return this;
	}
	/**
	 * 条件 not in(数组值)
	 * 
	 * @param columnName
	 * @param notInValues
	 * @return
	 */
	public Sql notIn(String columnName, Object... notInValues) {
		processAnd();
		conditions.add(new Condition().notIn(processPrependMainTableAsName(columnName), notInValues));
		return this;
	}
	
	/**
	 * 条件 in(数组字符串)
	 * 
	 * @param columnName
	 * @param inValues
	 * @return
	 */
	public Sql in(String columnName, String inValues) {
		if(StrKit.hasBlank(columnName,inValues)) {return this;}
		processAnd();
		conditions.add(new Condition().in(processPrependMainTableAsName(columnName), inValues));
		return this;
	}
	
	/**
	 * 条件 instr查询
	 * @param str
	 * @param substr
	 * @param strIsTableColumn
	 * @return
	 */
	public Sql instr(String str, String substr, boolean strIsTableColumn) {
		if(StrKit.hasBlank(str,substr)) {return this;}
		processAnd();
		conditions.add(new Condition().instr(strIsTableColumn?processPrependMainTableAsName(str):str, substr, strIsTableColumn));
		return this;
	}
	/**
	 * 条件 notinstr查询
	 * @param str
	 * @param substr
	 * @param strIsTableColumn
	 * @return
	 */
	public Sql notInstr(String str, String substr, boolean strIsTableColumn) {
		if(StrKit.hasBlank(str,substr)) {return this;}
		processAnd();
		conditions.add(new Condition().notInstr(strIsTableColumn?processPrependMainTableAsName(str):str, substr, strIsTableColumn));
		return this;
	}
	
	/**
	 * 条件 not in(数组字符串)
	 * 
	 * @param columnName
	 * @param notInValues
	 * @return
	 */
	public Sql notIn(String columnName, String notInValues) {
		if(StrKit.hasBlank(columnName,notInValues)) {return this;}
		processAnd();
		conditions.add(new Condition().notIn(processPrependMainTableAsName(columnName), notInValues));
		return this;
	}

	/**
	 * 条件 find_in_set
	 * 
	 * @param value
	 * @param values 
	 * @param valuesIsTableColumn values参数是否是数据库表中的列
	 * @return
	 */
	public Sql findInSet(Object value, String values, boolean valuesIsTableColumn) {
		if(StrKit.isBlank(values) || value == null) {return this;}
		if(isOracle()) {
			return like("(',' || "+(valuesIsTableColumn?processPrependMainTableAsName(values):"'"+values+"'")+" ||',')", ","+value+",");
		}
		if(isPostgresql()) {
			return like("(',' || "+(valuesIsTableColumn?processPrependMainTableAsName(values):"'"+values+"'")+" ||',')", ","+value+",");
		}
		if(isSqlServer()) {
			return like("(',' + "+(valuesIsTableColumn?processPrependMainTableAsName(values):"'"+values+"'")+" + ',')", ","+value+",");
		}
		
		if(isMysql()) {
			processAnd();
			conditions.add(new Condition().findInSet(value, valuesIsTableColumn?processPrependMainTableAsName(values):values, valuesIsTableColumn));
		}
		return this;
	}

	/**
	 * 条件 in(返回数组值的sql对象)
	 * 
	 * @param columnName
	 * @param sql
	 * @return
	 */
	public Sql inSql(String columnName, Sql sql) {
		return inSql(columnName, sql.toSql());
	}
	/**
	 * 条件 not in(返回数组值的sql对象)
	 * 
	 * @param columnName
	 * @param sql
	 * @return
	 */
	public Sql notInSql(String columnName, Sql sql) {
		return notInSql(columnName, sql.toSql());
	}

	/**
	 * 条件 in(返回数组值的sql语句)
	 * 
	 * @param columnName
	 * @param sql
	 * @return
	 */
	public Sql inSql(String columnName, String sql) {
		if(StrKit.hasBlank(columnName,sql)) {return this;}
		processAnd();
		conditions.add(new Condition().inSql(processPrependMainTableAsName(columnName), sql));
		return this;
	}
	/**
	 * 条件 not in(返回数组值的sql语句)
	 * 
	 * @param columnName
	 * @param sql
	 * @return
	 */
	public Sql notInSql(String columnName, String sql) {
		if(StrKit.hasBlank(columnName,sql)) {return this;}
		processAnd();
		conditions.add(new Condition().notInSql(processPrependMainTableAsName(columnName), sql));
		return this;
	}

	/**
	 * 条件 columnName=value
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql eq(String columnName, Object value) {
		if(StrKit.isBlank(columnName) || value == null) {return this;}
		processEq(columnName,value);
		return this;
	}
	/**
	 * 条件 columnName=value
	 * int数据不能小于等于0
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql eqIfOk(String columnName, Integer value) {
		if(StrKit.isBlank(columnName) || value==null || value<=0) {return this;}
		processEq(columnName,value);
		return this;
	}
	/**
	 * 底层处理eq
	 * @param columnName
	 * @param value
	 */
	private void processEq(String columnName,Object value) {
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.EQ));
	}
	/**
	 * 条件 columnName=value
	 * String数据不能为null或者""空字符串
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql eqIfNotBlank(String columnName, String value) {
		if(StrKit.isBlank(columnName) || StrKit.isBlank(value)) {return this;}
		processEq(columnName,value);
		return this;
	}
	/**
	 * 条件(columnName=value1 or  columnName=value2)
	 * 
	 * @param columnName
	 * @param values
	 * @return
	 */
	public Sql eqOr(String columnName, Object... values) {
		if(StrKit.isBlank(columnName) || values==null || values.length==0) {return this;}
		processAnd();
		bracketLeft();
		int len=values.length;
		for(int i=0;i<len;i++) {
			conditions.add(new Condition(processPrependMainTableAsName(columnName), values[i], Condition.EQ));
			if(i<len-1) {
				conditions.add(new Condition().or());
			}
		}
	
		bracketRight();
		return this;
	}
	/**
	 * 条件 id=value
	 * @param value
	 * @return
	 */
	public Sql eqId(Object value) {
		return eq(KEY_ID, value);
	}
	/**
	 * 条件 pid=value
	 * @param value
	 * @return
	 */
	public Sql eqPid(Object value) {
		return eq(KEY_PID, value);
	}
	/**
	 * 条件 key=? 可批量处理
	 * 
	 * @param columnNames
	 * @return
	 */
	public Sql eqQM(String... keys) {
		if(keys!=null&&keys.length>0) {
			for(String columnName:keys) {
				eq(columnName, KEY_QUESTION_MARK);
			}
		}
		return this;
	}
	/**
	 * 条件 id=?
	 * @return
	 */
	public Sql idEqQM() {
		return eqQM(KEY_ID);
	}
	/**
	 * 条件 pid=?
	 * @return
	 */
	public Sql pidEqQM() {
		return eqQM(KEY_PID);
	}
	/**
	 * 条件 id=?
	 * 指定id名字
	 * @param idName
	 * @return
	 */
	public Sql idEqQM(String idName) {
		return eqQM(idName);
	}
	/**
	 * 条件 pid=?
	 * 指定pid名字
	 * @param pidName
	 * @return
	 */
	public Sql pidEqQM(String pidName) {
		return eqQM(pidName);
	}

	/**
	 * 条件 key like value
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql like(String columnName, String value) {
		if(StrKit.isBlank(columnName) || StrKit.isBlank(value)) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.LIKE));
		return this;
	}
	
	/**
	 * 多字段like
	 * @param likeValue
	 * @param columns
	 * @return
	 */
	public Sql likeMulti(String likeValue, String... columns) {
		if(StrKit.notBlank(likeValue)&&columns!=null&&columns.length>0) {
			bracketLeft();
			int len=columns.length;
			for(int i=0;i<len;i++) {
				like(columns[i], likeValue);
				if(i!=len-1) {
					or();
				}
			}
			bracketRight();
		}
		return this;
	}
	
	/**
	 * 多字段 not like
	 * @param likeValue
	 * @param columns
	 * @return
	 */
	public Sql notLikeMulti(String likeValue, String... columns) {
		 if(StrKit.notBlank(likeValue)&&columns!=null&&columns.length>0) {
			 bracketLeft();
			 int len=columns.length;
			 for(int i=0;i<len;i++) {
				 notLike(columns[i], likeValue);
				 if(i!=len-1) {
					 and();
				 }
			 }
			 bracketRight();
		 }
		return this;
	}

	/**
	 * 条件 key not like value
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql notLike(String columnName, String value) {
		if(StrKit.isBlank(columnName) || StrKit.isBlank(value)) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.NOTLIKE));
		return this;
	}

	/**
	 * 条件 value以key开头模糊查询
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql startWith(String columnName, String value) {
		if(StrKit.isBlank(columnName) || StrKit.isBlank(value)) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.STARTWITH));
		return this;
	}

	/**
	 * 条件 value不以key开头模糊查询
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql notStartWith(String columnName, String value) {
		if(StrKit.isBlank(columnName) || StrKit.isBlank(value)) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.NOT_STARTWITH));
		return this;
	}

	/**
	 * 条件 value以key结尾模糊查询
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql endWith(String columnName, String value) {
		if(StrKit.isBlank(columnName) || StrKit.isBlank(value)) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.ENDWITH));
		return this;
	}
	
	/**
	 * 条件 value不以key结尾模糊查询
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql notEndWith(String columnName, String value) {
		if(StrKit.isBlank(columnName) || StrKit.isBlank(value)) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.NOT_ENDWITH));
		return this;
	}

	/**
	 * 分组查询
	 * 
	 * @param columnName
	 * @return
	 */
	public Sql groupBy(String columnName) {
		this.groupBy = columnName;
		return this;
	}

	/**
	 * 排序查询
	 * 
	 * @param columnName
	 * @param orderType
	 * @return
	 */
	public Sql orderBy(String columnName, String orderType) {
		return orderBy(columnName, "desc".equalsIgnoreCase(orderType));
	}
	/**
	 * 排序查询
	 * 
	 * @param columnName
	 * @param desc
	 * @return
	 */
	public Sql orderBy(String columnName, boolean desc) {
		this.orderBy = processPrependMainTableAsName(columnName);
		this.desc = desc;
		return this;
	}
	
	/**
	 * 清掉order by
	 */
	public Sql clearOrderBy() {
		this.orderBy = null;
		this.desc = false;
		return this;
	}

	/**
	 * 排序查询 默认正序
	 * 
	 * @param columnName
	 * @return
	 */
	public Sql orderBy(String columnName) {
		return orderBy(columnName, false);
	}
	/**
	 * 按照ID排序 默认正序
	 * @return
	 */
	public Sql orderById() {
		return orderById(false);
	}
	/**
	 * 按照ID排序 指定是否倒序
	 * @return
	 */
	public Sql orderById(boolean desc) {
		return orderBy("id", desc);
	}
	/**
	 * 按照sort_rank字段排序
	 * @return
	 */
	public Sql orderBySortRank() {
		return orderBy("sort_rank");
	}

	/**
	 * 处理条件前加and
	 */
	private void processAnd() {
		if (conditions.size() > 0) {
			Condition condition = conditions.get(conditions.size() - 1);
			if (condition.getType() != Condition.TYPE_LINK || (condition.getType() == Condition.TYPE_LINK
					&& condition.getValue1().equals(Condition.BRACKET_RIGHT))) {
				and();
			}
		}
	}

	/**
	 * 关联查询 左联接
	 * 
	 * @param joinSqls
	 * @return
	 */
	public Sql leftJoin(String... joinSqls) {
		addJoinSql(KEY_LEFT_JOIN,joinSqls);
		return this;
	}
	/**
	 * 关联查询 左外联接
	 * 
	 * @param joinSqls
	 * @return
	 */
	public Sql leftOuterJoin(String... joinSqls) {
		addJoinSql(KEY_LEFT_OUTER_JOIN,joinSqls);
		return this;
	}
	/**
	 * 关联查询 右外联接
	 * 
	 * @param joinSqls
	 * @return
	 */
	public Sql rightOuterJoin(String... joinSqls) {
		addJoinSql(KEY_RIGHT_OUTER_JOIN,joinSqls);
		return this;
	}
	/**
	 * 关联查询 全联接
	 * 
	 * @param joinSqls
	 * @return
	 */
	public Sql fullJoin(String... joinSqls) {
		addJoinSql(KEY_FULL_JOIN,joinSqls);
		return this;
	}
	/**
	 * 底层处理joinSql 添加到joinSqls里
	 * @param dir
	 * @param joinSqls
	 */
	private void addJoinSql(String dir,String... joinSqls) {
		if(joinSqls==null||joinSqls.length==0) {return;}
		for(String joinSql:joinSqls) {
			if(this.joinSqls==null) {
				this.joinSqls=new ArrayList<String>();
			}
			this.joinSqls.add(dir + joinSql + KEY_WHITESPACE);
		}
	}
	/**
	 * 关联查询 左联接
	 * 
	 * @param joinTableName
	 * @param asName
	 * @param onSql
	 * @return
	 */
	public Sql leftJoin(String joinTableName,String asName,String onSql) {
		if(isOracle()) {
			addJoinSql(KEY_LEFT_JOIN , joinTableName+KEY_WHITESPACE+asName+KEY_ON+onSql);
		}else {
			addJoinSql(KEY_LEFT_JOIN , joinTableName+KEY_AS+asName+KEY_ON+onSql);
		}
		return this;
	}

	/**
	 * 关联查询 右联接
	 * 
	 * @param joinSqls
	 * @return
	 */
	public Sql rightJoin(String... joinSqls) {
		addJoinSql(KEY_RIGHT_JOIN , joinSqls);
		return this;
	}
	
	
	/**
	 * 关联查询 右联接
	 * 
	 * @param joinTableName
	 * @param asName
	 * @param onSql
	 * @return
	 */
	public Sql rightJoin(String joinTableName,String asName,String onSql) {
		if(isOracle()) {
			addJoinSql(KEY_RIGHT_JOIN , joinTableName+KEY_WHITESPACE+asName+KEY_ON+onSql);
		}else {
			addJoinSql(KEY_RIGHT_JOIN , joinTableName+KEY_AS+asName+KEY_ON+onSql);
		}
		return this;
	}
	/**
	 * 连接其它Sql对象 去重
	 * @param sql
	 * @return
	 */
	public Sql union(Sql sql) {
		if(this.unionSqls == null) {
			this.unionSqls = new ArrayList<UnionSql>();
		}
		this.unionSqls.add(new UnionSql(sql, false));
		return this;
	}
	
	/**
	 * 连接其它Sql对象 返回所有数据 包括重复
	 * @param sql
	 * @return
	 */
	public Sql unionAll(Sql sql) {
		if(this.unionSqls == null) {
			this.unionSqls = new ArrayList<UnionSql>();
		}
		this.unionSqls.add(new UnionSql(sql, true));
		return this;
	}
	/**
	 * 检测是否包含union
	 * @return
	 */
	public boolean hasUnion() {
		return unionSqls!=null&&unionSqls.size()>0;
	}

	/**
	 * 关联查询 内联接
	 * 
	 * @param joinSqls
	 * @return
	 */
	public Sql innerJoin(String... joinSqls) {
		addJoinSql(KEY_INNER_JOIN , joinSqls);
		return this;
	}
	
	/**
	 * 关联查询 左联接
	 * 
	 * @param joinTableName
	 * @param asName
	 * @param onSql
	 * @return
	 */
	public Sql innerJoin(String joinTableName,String asName,String onSql) {
		if(isOracle()) {
			addJoinSql(KEY_INNER_JOIN , joinTableName+KEY_WHITESPACE+asName+KEY_ON+onSql);
		}else {
			addJoinSql(KEY_INNER_JOIN , joinTableName+KEY_AS+asName+KEY_ON+onSql);
		}
		return this;
	}
	
	/**
	 * 关联查询 左外联接
	 * 
	 * @param joinTableName
	 * @param asName
	 * @param onSql
	 * @return
	 */
	public Sql leftOuterJoin(String joinTableName,String asName,String onSql) {
		return leftOuterJoin(joinTableName+KEY_WHITESPACE+asName+KEY_ON+onSql);
	}
	/**
	 * 关联查询 右外联接
	 * 
	 * @param joinTableName
	 * @param asName
	 * @param onSql
	 * @return
	 */
	public Sql rightOuterJoin(String joinTableName,String asName,String onSql) {
		return rightOuterJoin(joinTableName+KEY_WHITESPACE+asName+KEY_ON+onSql);
	}
	/**
	 * 关联查询 全联接
	 * 
	 * @param joinTableName
	 * @param asName
	 * @param onSql
	 * @return
	 */
	public Sql fullJoin(String joinTableName,String asName,String onSql) {
		return fullJoin(joinTableName+KEY_WHITESPACE+asName+KEY_ON+onSql);
	}

	/**
	 * 左括号
	 * 
	 * @return
	 */
	public Sql bracketLeft() {
		processAnd();
		conditions.add(new Condition().bracketLeft());
		return this;
	}

	/**
	 * 右括号
	 * 
	 * @return
	 */
	public Sql bracketRight() {
		conditions.add(new Condition().bracketRight());
		return this;
	}

	/**
	 * 大于 比较 key>value
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql gt(String columnName, Object value) {
		if(StrKit.isBlank(columnName) || value == null) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.GT));
		return this;
	}
	/**
	 * 大于 比较 key>? 可多个
	 * 
	 * @param columnNames
	 * @return
	 */
	public Sql gtQM(String... columnNames) {
		if(columnNames!=null&&columnNames.length>0) {
			for(String columnName:columnNames) {
				gt(columnName, KEY_QUESTION_MARK);
			}
		}
		return this;
	}

	/**
	 * 小于 比较 key<value
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql lt(String columnName, Object value) {
		if(StrKit.isBlank(columnName) || value == null) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.LT));
		return this;
	}
	/**
	 * 小于 比较 key<? 可多个
	 * 
	 * @param columnNames
	 * @return
	 */
	public Sql ltQM(String... columnNames) {
		if(columnNames!=null&&columnNames.length>0) {
			for(String columnName:columnNames) {
				lt(columnName, KEY_QUESTION_MARK);
			}
		}
		return this;
	}

	/**
	 * 大于等于 比较 key>=value
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql ge(String columnName, Object value) {
		if(StrKit.isBlank(columnName) || value == null) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.GE));
		return this;
	}
	/**
	 * 大于等于 比较 key>=? 可多个
	 * 
	 * @param columnNames
	 * @return
	 */
	public Sql geQM(String... columnNames) {
		if(columnNames!=null&&columnNames.length>0) {
			for(String columnName:columnNames) {
				ge(columnName, KEY_QUESTION_MARK);
			}
		}
		return this;
	}

	/**
	 * 小于等于 比较 key<=value
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql le(String columnName, Object value) {
		if(StrKit.isBlank(columnName) || value == null) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.LE));
		return this;
	}
	/**
	 * 小于等于 比较 key<=? 可多个
	 * 
	 * @param columnNames
	 * @return
	 */
	public Sql leQM(String... columnNames) {
		if(columnNames!=null&&columnNames.length>0) {
			for(String columnName:columnNames) {
				le(columnName, KEY_QUESTION_MARK);
			}
		}
		return this;
	}

	/**
	 * 不等于 比较 key!=value
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql noteq(String columnName, Object value) {
		if(StrKit.isBlank(columnName) || value == null) {return this;}
		processAnd();
		conditions.add(new Condition(processPrependMainTableAsName(columnName), value, Condition.NOT_EQ));
		return this;
	}
	/**
	 * 不等于 比较 id!=value
	 * @param value
	 * @return
	 */
	public Sql noteqId(Object value) {
		return noteq(KEY_ID, value);
	}
	
	/**
	 * 不等于 比较  key!=?
	 * 
	 * @param columnNames
	 * @return
	 */
	public Sql noteqQM(String... columnNames) {
		if(columnNames!=null&&columnNames.length>0) {
			for(String columnName:columnNames) {
				noteq(columnName, KEY_QUESTION_MARK);
			}
		}
		return this;
	}
	/**
	 * 不等于比较 id!=? 
	 * @return
	 */
	public Sql idNoteqQM() {
		return noteqQM(KEY_ID);
	}
	/**
	 * 不等于比较 pid!=? 
	 * @return
	 */
	public Sql pidNoteqQM() {
		return noteqQM(KEY_PID);
	}
	/**
	 * 不等于比较 id!=? 
	 * 指定主键名
	 * @param idName
	 * @return
	 */
	public Sql idNoteqQM(String idName) {
		return noteqQM(idName);
	}
	/**
	 * 不等于比较 pid!=? 
	 * 指定父主键名
	 * @param pidName
	 * @return
	 */
	public Sql pidNoteqQM(String pidName) {
		return noteqQM(pidName);
	}

	/**
	 * 更新sql专用 set字段值
	 * 
	 * @param columnName
	 * @param value
	 * @return
	 */
	public Sql set(String columnName, Object value) {
		if (updateColumnsMap == null) {
			updateColumnsMap = new HashMap<String, Object>();
		}
		updateColumnsMap.put(columnName, value);
		return this;
	}
	

	/**
	 * 将对象转sql
	 * 
	 * @return
	 */
	public String toSql() {
		return toSql(new StringBuilder());
	}
	
	/**
	 * 将对象转sql
	 * @param sql
	 * @return
	 */
	public String toSql(StringBuilder sql) {
		switch (type) {
		case TYPE_SELECT:
			sql.append(KEY_SELECT);
			break;
		case TYPE_INSERT:
			sql.append(KEY_INSERT);
			break;
		case TYPE_INSERT_BATCH:
			sql.append(KEY_INSERT);
			break;
		case TYPE_UPDATE:
			sql.append(KEY_UPDATE);
			break;
		case TYPE_DELETE:
			sql.append(KEY_DELETE).append(KEY_FROM);
			break;
		}
		builderSql(sql, false);
		return sql.toString();
	}

	/**
	 * 内部底层函数 将对象属性判断拼接 转为最终sql
	 * 
	 * @param sql
	 * @param withoutSelect 是否生成不带select的sql
	 */
	private void builderSql(StringBuilder sql, boolean withoutSelect) {
		if (prepared) {
			whereValues = new ArrayList<Object>();
		}
		// 首先判断是不是查询类型语句 是的话 加 from关键词
		if (type == TYPE_SELECT) {
			if (withoutSelect == false) {
				sql.append(returnColumns);
			}
			sql.append(KEY_FROM);
		}
		// 拼接tableName
		sql.append(table);
		if (type == TYPE_SELECT && joinSqls != null && joinSqls.size()>0) {
			joinSqls.forEach(joinSql->sql.append(joinSql));
		}
		// 判断更新类型的语句 更新字段是否存在设置 如果存在 拼接set
		else if (type == TYPE_UPDATE && updateColumnsMap != null) {
			int size = updateColumnsMap.size();
			int index = 0;
			sql.append(KEY_SET);
			Object v;
			for (Entry<String, Object> e : updateColumnsMap.entrySet()) {
				sql.append(e.getKey()).append("=");
				v=e.getValue();
				if (v instanceof String) {
					sql.append("'").append(v).append("'");
				} else if (v instanceof Boolean) {
					sql.append(SqlUtil.boolToChar((Boolean)v));
				} else if (v instanceof SqlExpress) {
					sql.append(v);
				} else {
					sql.append(v);
				}
				if (index != size - 1) {
					sql.append(",");
				}
				index++;
			}
		} else if (this.type == TYPE_INSERT) {
			// 插入语句设置
			sql.append(KEY_WHITESPACE).append(String.format(KEY_INSERT_VALUES, processInsertValuesToString()));
		}
		// 判断吃否存在where过滤条件 拼接where后面的查询条件
		if (conditions.size() > 0) {
			sql.append(KEY_WHERE);
			// 循环查询条件
			Condition condition = null;
			for (int i = 0; i < conditions.size(); i++) {
				condition = conditions.get(i);
				sql.append(condition.toSql(dbType, prepared));
				// 判断当前设置是否使用问号占位参数
				if (prepared && condition.getType() == Condition.TYPE_COMPARE
						&& condition.getCompareState().indexOf("in") == -1) {
					whereValues.add(SqlUtil.processBooleanValue(condition.getValue1()));
				}
			}

		}
		
		// 判断分组查询
		if (StrKit.notBlank(groupBy)) {
			sql.append(KEY_GROUPBY);
			sql.append(groupBy);
		}
		
		// 判断排序
		if (StrKit.notBlank(orderBy)) {
			sql.append(KEY_ORDERBY).append(orderBy);
			if (desc) {
				sql.append(KEY_DESC);
			}else {
				sql.append(KEY_ASC);
			}
		}
		// 判断limit
		if (hasPage) {
			sql.append(KEY_WHITESPACE);
			switch (dbType) {
			case DBType.MYSQL:
				processMysqlPage(sql);
				break;
			case DBType.ORACLE:
				processOraclePage(sql);
				break;
			case DBType.SQLSERVER:
				processSqlserverPage(sql);
				break;
			case DBType.POSTGRESQL:
				processPostgresqlPage(sql);
				break;
			case DBType.DM:
				processDMPage(sql);
				break;
			}
		}
		//判断是否存在Union
		if(hasUnion()) {
			processUnion(sql);
		}
	}

	/**
	 * 处理Union sql
	 * @param sql
	 */
	private void processUnion(StringBuilder sql) {
		for(UnionSql unionSql:unionSqls) {
			unionSql.appendToSql(sql);
		}
	}

	/**
	 * 处理sqlServer分页
	 * 
	 * @param sql
	 */
	private void processSqlserverPage(StringBuilder sql) {
		if(pageNumber==1&&pageSize==1) {
			return;
		}
		int end = pageNumber * pageSize;
		if (end <= 0) {
			end = pageSize;
		}
		int begin = (pageNumber - 1) * pageSize;
		if (begin < 0) {
			begin = 0;
		}
		String findSql = sql.toString();
		sql.setLength(0);
		sql.append("SELECT *  FROM ( SELECT row_number() over (order by tempcolumn) temprownumber, * FROM ");
		sql.append(" ( SELECT TOP ").append(end).append(" tempcolumn=0,");
		sql.append(findSql.replaceFirst("(?i)select", ""));
		sql.append(")vip)mvp where temprownumber>").append(begin);
	}

	/**
	 * 处理oracle分页
	 * 
	 * @param sql
	 */
	private void processOraclePage(StringBuilder sql) {
		int start = (pageNumber - 1) * pageSize;
		int end = pageNumber * pageSize;
		String findSql = sql.toString();
		sql.setLength(0);
		sql.append("select ").append(returnColumns).append(" from ( select row_.*, rownum rownum_ from (  ");
		sql.append(findSql);
		sql.append(" ) row_ where rownum <= ").append(end).append(") table_alias");
		sql.append(" where table_alias.rownum_ > ").append(start);
	}

	/**
	 * 处理mysql分页
	 */
	private void processMysqlPage(StringBuilder sql) {
		int offset = (pageNumber - 1) * pageSize;
		sql.append(KEY_LIMIT).append(offset).append(KEY_COMMA).append(pageSize).append(KEY_WHITESPACE);
	}
	/**
	 * 处理DM分页
	 */
	private void processDMPage(StringBuilder sql) {
		int offset = (pageNumber - 1) * pageSize;
		sql.append(KEY_LIMIT).append(offset).append(KEY_COMMA).append(pageSize).append(KEY_WHITESPACE);
	}
	/**
	 * 处理postgresql分页
	 */
	private void processPostgresqlPage(StringBuilder sql) {
		int offset = (pageNumber - 1) * pageSize;
		sql.append(KEY_LIMIT).append(pageSize).append(KEY_OFFSET).append(offset).append(KEY_WHITESPACE);
	}

	/**
	 * 将insertValues转为string 字符串
	 * 
	 * @return
	 */
	private String processInsertValuesToString() {
		if (insertValues == null || insertValues.length == 0) {
			return KEY_NULLSTRING;
		}
		StringBuilder sb = new StringBuilder();
		int len = insertValues.length;
		Object value;
		for (int i = 0; i < len; i++) {
			value = insertValues[i];
			if (value instanceof String) {
				sb.append("'").append(value).append("'");
			} else {
				sb.append(value);
			}
			if (i < len - 1) {
				sb.append(",");
			}
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return toSql();
	}

	/**
	 * 计数 count(*)
	 * 
	 * @return
	 */
	public Sql count() {
		return count(KEY_STAR);
	}
	

	/**
	 * 对指定列计数
	 * 
	 * @param column
	 * @return
	 */
	public Sql count(String column) {
		this.queryCount = true;
		processReturnColumnsBySelectCount(column);
		this.select(returnColumns);
		return this;
	}

	private void processReturnColumnsBySelectCount(String column) {
		if(null==column) {
			column = KEY_STAR;
		}
		if(this.queryDistinctCount) {
			returnColumns = returnColumns+","+String.format(KEY_COUNT_COLUMN, column);
		}else {
			returnColumns = String.format(KEY_COUNT_COLUMN, column);
		}
		
	}

	public Sql distinct(String column) {
		returnColumns = KEY_DISTINCT + column;
		this.select(returnColumns);
		return this;
	}

	/**
	 * 不重复列技术
	 * 
	 * @param column
	 * @return
	 */
	public Sql distinctCount(String column) {
		this.queryDistinctCount = true;
		processReturnColumnsBySelectDistinctCount(column);
		this.select(returnColumns);
		return this;
	}

	private void processReturnColumnsBySelectDistinctCount(String column) {
		if(this.queryCount) {
			returnColumns=returnColumns+","+String.format(KEY_COUNT_DISTINCT, column);
		}else {
			returnColumns=String.format(KEY_COUNT_DISTINCT, column);
		}
	}

	public static void main(String[] args) {
		Sql leftSql=Sql.mysql().select().from("jb_user","u1").leftJoin("jb_user", "t1", "t1.id=u1.id");
		Sql rightSql=Sql.mysql().select().from("jb_user","u2").rightJoin("jb_user", "t2", "t2.id=u2.id");
		leftSql.union(rightSql);
		leftSql.unionAll(rightSql);
		leftSql.eq("name", 123);
		leftSql.notInstr("username", "adm",true);
		leftSql.gt("func(t.col)",0);
		System.out.println(leftSql);
	}

	public String getCountColumns() {

		return countColumns;
	}

	public String getReturnColumns() {

		return returnColumns;
	}

	public void setCountColumns(String countColumns) {
		this.countColumns = countColumns;
	}

	public boolean hasTable() {
		return table != null && table.trim() != "";
	}

	/**
	 * 得到参数集合
	 * 
	 * @return
	 */
	public Object[] getWhereValues() {
		if (prepared && whereValues != null && whereValues.size() > 0) {
			return whereValues.toArray();
		}
		return Collections.EMPTY_LIST.toArray();
	}

	public boolean isPrepared() {
		return prepared;
	}

	public boolean isQueryColumnList() {
		return queryColumnList;
	}

	public boolean isQueryCount() {
		return queryCount;
	}

	public boolean isQueryMax() {
		return queryMax;
	}

	public boolean isDelete() {
		return type == TYPE_DELETE;
	}

	public boolean isQuery() {
		return type == TYPE_SELECT;
	}

	public boolean isUpdate() {
		return type == TYPE_UPDATE;
	}

	public boolean isQueryDistinctCount() {
		return queryDistinctCount;
	}

	/**
	 * 生成不带select的sql
	 * 
	 * @return
	 */
	public String toSqlExceptSelect() {
		StringBuilder sql = new StringBuilder();
		builderSql(sql, true);
		return sql.toString();
	}

	/**
	 * 得到select部分
	 * 
	 * @return
	 */
	public String getSelect() {
		StringBuilder select = new StringBuilder();
		select.append(KEY_SELECT).append(KEY_WHITESPACE).append(returnColumns);
		return select.toString();
	}

	/**
	 * 切换dbType为mysql
	 * 
	 * @return
	 */
	public static Sql mysql() {
		return me(DBType.MYSQL);
	}
	/**
	 * 切换dbType为postgresql
	 * 
	 * @return
	 */
	public static Sql postgresql() {
		return me(DBType.POSTGRESQL);
	}
	/**
	 * 切换dbType为DM
	 * 
	 * @return
	 */
	public static Sql dm() {
		return me(DBType.DM);
	}

	/**
	 * 切换dbType为oracle
	 * 
	 * @return
	 */
	public static Sql oracle() {
		return me(DBType.ORACLE);
	}

	/**
	 * 切换dbType为sqlserver
	 * 
	 * @return
	 */
	public static Sql sqlserver() {
		return me(DBType.SQLSERVER);
	}

	/**
	 * 设置数据库类型
	 * 
	 * @param dbType
	 * @return
	 */
	public Sql setDbType(String dbType) {
		if(StrKit.isBlank(dbType)) {
			String msg = "Sql.java 初始化 必须指定dbType";
			LogKit.error(msg);
			throw new RuntimeException(msg);
		}
		this.dbType = dbType;
		return this;
	}

	public Object[] getInsertValues() {
		return insertValues;
	}

	public void setInsertValues(Object[] insertValues) {
		this.insertValues = insertValues;
	}
	/**
	 * 如果是pgsql 处理id正序排序
	 * @return
	 */
	public Sql orderByIdAscIfPgSql() {
		if(isPostgresql()) {
			orderById();
		}
		return this;
	}
	/**
	 * 如果是pgsql 处理id倒序排序
	 * @return
	 */
	public Sql orderByIdDescIfPgSql() {
		if(isPostgresql()) {
			orderById(true);
		}
		return this;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}
	public boolean hasPage() {
		return hasPage;
	}
	
	public void clearPage() {
		this.hasPage=false;
		this.pageNumber = 0;
		this.pageSize = 0;
	}

	public String getMainTableAsName() {
		return mainTableAsName;
	}
	/**
	 * 处理主表列名添加表的asName
	 * @param columnName
	 */
	private String processPrependMainTableAsName(String columnName) {
		if(StrKit.isBlank(mainTableAsName) || columnName.indexOf(".")!=-1) {
			return columnName;
		}
		return mainTableAsName + "." + columnName;
	}
}