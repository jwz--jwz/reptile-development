package cn.jbolt.common.db.sql;

/**
 * 数据库类型
 * @ClassName:  DBType   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年6月9日   
 */
public class DBType {
	public static final String MYSQL="mysql";
	public static final String ORACLE="oracle";
	public static final String POSTGRESQL="postgresql";
	public static final String SQLSERVER="sqlserver";
	public static final String SQLITE="sqlite";
	public static final String H2="h2";
	public static final String DB2="db2";
	public static final String DM="dm";
	
	public static boolean isMysql(String dbType) {
		return MYSQL.equalsIgnoreCase(dbType);
	}
	public static boolean isOracle(String dbType) {
		return ORACLE.equalsIgnoreCase(dbType);
	}
	public static boolean isPostgresql(String dbType) {
		return POSTGRESQL.equalsIgnoreCase(dbType);
	}
	public static boolean isSqlServer(String dbType) {
		return SQLSERVER.equalsIgnoreCase(dbType);
	}
	public static boolean isSqlite(String dbType) {
		return SQLITE.equalsIgnoreCase(dbType);
	}
	public static boolean isH2(String dbType) {
		return H2.equalsIgnoreCase(dbType);
	}
	public static boolean isDB2(String dbType) {
		return DB2.equalsIgnoreCase(dbType);
	}
	public static boolean isDM(String dbType) {
		return DM.equalsIgnoreCase(dbType);
	}
}
