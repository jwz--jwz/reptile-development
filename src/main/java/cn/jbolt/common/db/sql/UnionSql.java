package cn.jbolt.common.db.sql;
/**
 * union 连接查询
 * @ClassName:  UnionSql   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年4月7日   
 */
public class UnionSql {
	private Sql sql;
	private boolean unionAll;
	public UnionSql(Sql sql,boolean unionAll) {
		this.sql = sql;
		this.unionAll = unionAll;
	}
	public Sql getSql() {
		return sql;
	}
	public void setSql(Sql sql) {
		this.sql = sql;
	}
	public boolean isUnionAll() {
		return unionAll;
	}
	public void setUnionAll(boolean unionAll) {
		this.unionAll = unionAll;
	}
	
	public String toString() {
		return unionAll?Sql.KEY_UNION_ALL:Sql.KEY_UNION + sql.toSql()+Sql.KEY_WHITESPACE;
	}
	
	public void appendToSql(StringBuilder sqlBuilder) {
		sqlBuilder.append(unionAll?Sql.KEY_UNION_ALL:Sql.KEY_UNION);
		sql.toSql(sqlBuilder);
		sqlBuilder.append(Sql.KEY_WHITESPACE);
	}
}
