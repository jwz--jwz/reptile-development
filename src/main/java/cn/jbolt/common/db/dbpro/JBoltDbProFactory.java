package cn.jbolt.common.db.dbpro;

import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.IDbProFactory;

public class JBoltDbProFactory implements IDbProFactory {

	@Override
	public DbPro getDbPro(String configName) {
		return new JBoltDbPro(configName);
	}

}
