package cn.jbolt.common.inteceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.log.Log;

import cn.jbolt.base.JBoltControllerKit;
/**
 * JBolt 异常处理全局拦截器
 * @ClassName:  JBoltExceptionGlobalInterceptor   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年6月17日   
 */
public class JBoltExceptionGlobalInterceptor implements Interceptor {
	protected static final Log LOG = Log.getLog(JBoltExceptionGlobalInterceptor.class);
	@Override
	public void intercept(Invocation inv) {
		try {
			inv.invoke();
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e.getMessage());
			JBoltControllerKit.renderFail(inv.getController(), e.getMessage());
		}
	}
}
