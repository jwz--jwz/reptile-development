package cn.jbolt.common.bean;
/**
 * 排序信息
 * @ClassName:  SortInfo   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年6月28日   
 */
public class SortInfo {
	private String key;
	private String sortColumn;
	private String sortType;
	public SortInfo(String sortColumn,String sortType) {
		this.sortColumn = sortColumn;
		this.sortType = sortType;
	}
	public SortInfo(String key,String sortColumn,String sortType) {
		this(sortColumn,sortType);
		this.key = key;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
}
