package cn.jbolt.common.bean;
/**
 * jstree封装
 * @author mmm
 *
 */
public class JsTreeBean {
	private String id;//主键
	private String parent;//父节点
	private String text;//显示文本
	private String icon;//图标
	private JsTreeStateBean state;//状态
	private String type;//类型
	private Object data;//原始数据 
	private Boolean children;
	public JsTreeBean(Object id,Object pid,String text,boolean hasRoot) {
		this.state=new JsTreeStateBean();
		init(id, pid, text,null,null,hasRoot);
	}
	public JsTreeBean(Long id,Long pid,String text,String type,boolean hasRoot) {
		this.state=new JsTreeStateBean();
		init(id, pid, text,type,null,hasRoot);
	}
	public JsTreeBean(Object id,Object pid,String text,String type,String icon,boolean hasRoot) {
		this.state=new JsTreeStateBean();
		init(id, pid, text,type,icon,hasRoot);
	}
	private void init(Object id, Object pid, String text,String type,String icon,boolean hasRoot) {
		this.id=id+"";
		if(hasRoot) {
			if((id==null||id.toString().equals("0")) && (pid==null||pid.toString().equals("0"))) {
				this.parent="#";
			}else {
				this.parent=pid+"";
			}
		}else {
			if(pid==null||pid.toString().equals("0")) {
				this.parent="#";
			}else {
				this.parent=pid+"";
			}
		}
		 
		this.text=text;
		if(hasRoot==false &&  this.parent.equals("#")) {
			if(this.state!=null && this.state.isOpened()) {
				this.type="root_opened";
			}else {
				this.type="root";
			}
		}else {
			if(type!=null){
				this.type=type;
			}else{
				this.type="default";
			}
		}
		if(icon!=null) {
			this.icon=icon;
		}
		
		
	}
	public JsTreeBean(Object id,Object pid,String text,boolean opened,String type,boolean hasRoot) {
		this.state=new JsTreeStateBean(opened);
		init(id, pid, text,type,null,hasRoot);
	}
	public JsTreeBean(Object id,Object pid,String text,boolean opened,boolean selected,String type,boolean hasRoot) {
		this.state=new JsTreeStateBean(opened,selected);
		init(id, pid, text,type,null,hasRoot);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public JsTreeStateBean getState() {
		return state;
	}
	public void setState(JsTreeStateBean state) {
		this.state = state;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public Object getData() {
		return data;
	}
	public JsTreeBean setData(Object data) {
		this.data = data;
		return this;
	}
	public Boolean getChildren() {
		return children;
	}
	public void setChildren(Boolean children) {
		this.children = children;
	}
	
}
