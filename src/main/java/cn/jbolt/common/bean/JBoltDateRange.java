package cn.jbolt.common.bean;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;

public class JBoltDateRange {
	public static final String TYPE_YEAR="year";
	public static final String TYPE_MONTH="month";
	public static final String TYPE_DATE="date";
	public static final String TYPE_DATETIME="datetime";
	public static final String TYPE_TIME="time";
	public static final String DEFAULT_PARA_NAME="dateRange";
	/**
	 * 请求参数名
	 */
	private String paraName;
	/**
	 * 原解析前数据
	 */
	private String originDateRange;
	/**
	 * 开始范围值
	 */
	private String start;
	/**
	 * 结束范围值
	 */
	private String end;
	/**
	 * 分隔符
	 */
	private String splitChar;
	/**
	 * 类型 datetime date time month week
	 */
	private String type;
	/**
	 * 格式
	 */
	private String format;
	public JBoltDateRange(String dateRange,String type) {
		this(DEFAULT_PARA_NAME,dateRange,type, "~");
	}
	public JBoltDateRange(String paraName,String dateRange,String type) {
		this(paraName,dateRange,type, "~");
	}
	public JBoltDateRange(String paraName,String dateRange,String type,String splitChar) {
		List<String> list = StrUtil.splitTrim(dateRange, splitChar);
		if(list!=null&&list.size()==2) {
			this.start=list.get(0);
			this.end=list.get(1);
			this.type=type;
			this.splitChar=splitChar;
			this.format=cn.jbolt.common.util.DateUtil.YMD;
		}
		this.paraName=paraName;
		this.originDateRange=dateRange;
	}
	
//	private JBoltDateRange(String start,String end,String type,String splitChar) {
//		this(start,end, splitChar,type,cn.jbolt.common.util.DateUtil.YMD);
//	}
//	
//	private JBoltDateRange(String start,String end,String type,String splitChar,String format) {
//		this.start=start;
//		this.end=end;
//		this.type=type;
//		this.splitChar=splitChar;
//		this.format=format;
//	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getSplitChar() {
		return splitChar;
	}
	public void setSplitChar(String splitChar) {
		this.splitChar = splitChar;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getStartYear() {
		if(StrUtil.isBlankIfStr(start)) {
			return 1970;
		}
		start=normalize(start);
		return Integer.parseInt(start.trim());
	}
	public int getEndYear() {
		if(StrUtil.isBlankIfStr(end)) {
			return 1970;
		}
		end=normalize(end);
		return Integer.parseInt(end.trim());
	}
	
	public String getStartYearMonth() {
		return normalize(start);
	}
	
	public String getEndYearMonth() {
		return normalize(end);
	}
	
	
	public Date getStartDate() {
		return start==null?null:DateUtil.parseDate(start);
	}
	public Date getStartDate(Date defaultValue) {
		return start==null?defaultValue:DateUtil.parseDate(start);
	}
	public Date getEndDate() {
		return end==null?null:DateUtil.parseDate(end);
	}
	public Date getEndDate(Date defaultValue) {
		return end==null?defaultValue:DateUtil.parseDate(end);
	}
	public String getStartDateStr() {
		return start==null?null:DateUtil.parseDate(start).toDateStr();
	}
	public String getStartDateStr(String defaultValue) {
		return start==null?defaultValue:DateUtil.parseDate(start).toDateStr();
	}
	public String getEndDateStr() {
		return end==null?null:DateUtil.parseDate(end).toDateStr();
	}
	public String getEndDateStr(String defaultValue) {
		return end==null?defaultValue:DateUtil.parseDate(end).toDateStr();
	}
	
	public Date getStartDateTime() {
		return start==null?null:DateUtil.parseDateTime(start);
	}
	public Date getStartDateTime(Date defaultValue) {
		return start==null?defaultValue:DateUtil.parseDateTime(start);
	}
	public Date getEndDateTime() {
		return end==null?null:DateUtil.parseDateTime(end);
	}
	public Date getEndDateTime(Date defaultValue) {
		return end==null?defaultValue:DateUtil.parseDateTime(end);
	}
	public String getStartDateTimeStr() {
		return start!=null?DateUtil.parseDateTime(start).toDateStr():null;
	}
	public String getStartDateTimeStr(String defaultValue) {
		return start==null?defaultValue:DateUtil.parseDateTime(start).toDateStr();
	}
	public String getEndDateTimeStr() {
		return end==null?null:DateUtil.parseDateTime(end).toDateStr();
	}
	public String getEndDateTimeStr(String defaultValue) {
		return end==null?defaultValue:DateUtil.parseDateTime(end).toDateStr();
	}
	
	public Timestamp getStartTimestamp() {
		return start==null?null:DateUtil.parseTime(start).toTimestamp();
	}
	public Timestamp getStartTimestamp(Timestamp defaultValue) {
		return start==null?defaultValue:DateUtil.parseTime(start).toTimestamp();
	}
	public Timestamp getEndTimestamp() {
		return end==null?null:DateUtil.parseTime(end).toTimestamp();
	}
	public Timestamp getEndTimestamp(Timestamp defaultValue) {
		return end==null?defaultValue:DateUtil.parseTime(end).toTimestamp();
	}
	public String getStartTimeStr() {
		return start==null?null:DateUtil.parseTime(start).toTimeStr();
	}
	public String getStartTimeStr(String defaultValue) {
		return start==null?defaultValue:DateUtil.parseTime(start).toTimeStr();
	}
	public String getEndTimeStr() {
		return end==null?null:DateUtil.parseTime(end).toTimeStr();
	}
	public String getEndTimeStr(String defaultValue) {
		return end==null?defaultValue:DateUtil.parseTime(end).toTimeStr();
	}
	
	private String normalize(String dateStr){
		if (StrUtil.isBlank(dateStr)) {
			return StrUtil.str(dateStr);
		}

		// 日期时间分开处理
		final List<String> dateAndTime = StrUtil.splitTrim(dateStr, ' ');
		final int size = dateAndTime.size();
		if (size < 1 || size > 2) {
			// 非可被标准处理的格式
			return StrUtil.str(dateStr);
		}

		final StringBuilder builder = StrUtil.builder();

		// 日期部分（"\"、"/"、"."、"年"、"月"都替换为"-"）
		String datePart = dateAndTime.get(0).replaceAll("[/.年月]", "-");
		datePart = StrUtil.removeSuffix(datePart, "日");
		builder.append(datePart);

		// 时间部分
		if (size == 2) {
			builder.append(' ');
			String timePart = dateAndTime.get(1).replaceAll("[时分秒]", ":");
			timePart = StrUtil.removeSuffix(timePart, ":");
			//将ISO8601中的逗号替换为.
			timePart = timePart.replace(',', '.');
			builder.append(timePart);
		}

		return builder.toString();
	}
	public String getOriginDateRange() {
		return originDateRange;
	}
	public void setOriginDateRange(String originDateRange) {
		this.originDateRange = originDateRange;
	}
	public String getParaName() {
		return paraName;
	}
	public void setParaName(String paraName) {
		this.paraName = paraName;
	}
}
