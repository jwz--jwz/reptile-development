package cn.jbolt.common.bean;

import cn.hutool.core.util.StrUtil;

public class IpRegion {
	/**
	 * IP地址
	 */
	private String ip;
	/**
	 * 国家
	 */
	private String country;
	/**
	 * 省份
	 */
	private String province;
	/**
	 * 城市
	 */
	private String city;
	/**
	 *  城市代码
	 */
	private String cityCode;
	/**
	 * 服务商
	 */
	private String isp;
	/**
	 * 横坐标
	 */
	private String latitude;
	/**
	 * 纵坐标
	 */
	private String longitude;
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getIsp() {
		return isp;
	}
	public void setIsp(String isp) {
		this.isp = isp;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getAddress() {
		if(StrUtil.isBlank(country)) {
			return null;
		}
		return country+("0".equals(province)?"":province)+("0".equals(city)?"":city);
	}
}
