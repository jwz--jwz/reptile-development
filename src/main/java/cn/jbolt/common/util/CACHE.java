package cn.jbolt.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import com.jfinal.aop.Aop;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.IDataLoader;
import com.jfinal.weixin.sdk.msg.out.OutMsg;

import cn.hutool.core.io.FileUtil;
import cn.jbolt._admin.dept.DeptService;
import cn.jbolt._admin.dictionary.DictionaryService;
import cn.jbolt._admin.dictionary.DictionaryTypeService;
import cn.jbolt._admin.globalconfig.GlobalConfigService;
import cn.jbolt._admin.globalconfig.GlobalConfigTypeService;
import cn.jbolt._admin.onlineuser.OnlineUserService;
import cn.jbolt._admin.permission.PermissionService;
import cn.jbolt._admin.post.PostService;
import cn.jbolt._admin.role.RoleService;
import cn.jbolt._admin.rolepermission.RolePermissionService;
import cn.jbolt._admin.topnav.TopnavMenuService;
import cn.jbolt._admin.topnav.TopnavService;
import cn.jbolt._admin.user.UserService;
import cn.jbolt._admin.userconfig.UserConfigService;
import cn.jbolt.admin.appdevcenter.ApplicationService;
import cn.jbolt.admin.mall.goodscategory.back.GoodsBackCategoryService;
import cn.jbolt.admin.mall.goodstype.GoodsTypeService;
import cn.jbolt.admin.wechat.autoreply.WechatReplyContentService;
import cn.jbolt.admin.wechat.config.WechatConfigKey;
import cn.jbolt.admin.wechat.config.WechatConfigService;
import cn.jbolt.admin.wechat.mpinfo.WechatMpinfoService;
import cn.jbolt.admin.wechat.user.WechatUserService;
import cn.jbolt.base.JBoltConst;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.config.GlobalConfigKey;
import cn.jbolt.common.model.Application;
import cn.jbolt.common.model.Dept;
import cn.jbolt.common.model.Dictionary;
import cn.jbolt.common.model.DictionaryType;
import cn.jbolt.common.model.GlobalConfig;
import cn.jbolt.common.model.GlobalConfigType;
import cn.jbolt.common.model.GoodsBackCategory;
import cn.jbolt.common.model.GoodsType;
import cn.jbolt.common.model.OnlineUser;
import cn.jbolt.common.model.Permission;
import cn.jbolt.common.model.Post;
import cn.jbolt.common.model.Role;
import cn.jbolt.common.model.Topnav;
import cn.jbolt.common.model.TopnavMenu;
import cn.jbolt.common.model.User;
import cn.jbolt.common.model.UserConfig;
import cn.jbolt.common.model.WechatConfig;
import cn.jbolt.common.model.WechatMpinfo;
import cn.jbolt.common.model.WechatUser;

/**
 * 全局CACHE操作工具类
 * 
 * @ClassName: CACHE
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2019年11月13日
 */
public class CACHE extends CacheParaValidator {
	public static final CACHE me = new CACHE();
	public static final String JBOLT_CACHE_NAME = "jbolt_cache";
	public static final String JBOLT_CACHE_DEFAULT_PREFIX = "jbc_";
	public static final String JBOLT_WECAHT_KEYWORDS_CACHE_NAME = "jbolt_cache_wechat_keywords";
	private DictionaryService dictionaryService = Aop.get(DictionaryService.class);
	private DictionaryTypeService dictionaryTypeService = Aop.get(DictionaryTypeService.class);
	private RolePermissionService rolePermissionService = Aop.get(RolePermissionService.class);
	private UserService userService = Aop.get(UserService.class);
	private PermissionService permissionService = Aop.get(PermissionService.class);
	private GlobalConfigService globalConfigService = Aop.get(GlobalConfigService.class);
	private UserConfigService userConfigService = Aop.get(UserConfigService.class);
	private WechatUserService wechatUserService = Aop.get(WechatUserService.class);
	private ApplicationService applicationService = Aop.get(ApplicationService.class);
	private WechatConfigService wechatConfigService = Aop.get(WechatConfigService.class);
	private WechatMpinfoService wechatMpinfoService = Aop.get(WechatMpinfoService.class);
	private WechatReplyContentService wechatReplyContentService = Aop.get(WechatReplyContentService.class);
	private GoodsBackCategoryService goodsBackCategoryService = Aop.get(GoodsBackCategoryService.class);
	private RoleService roleService = Aop.get(RoleService.class);
	private GoodsTypeService goodsTypeService = Aop.get(GoodsTypeService.class);
	private TopnavService topnavService = Aop.get(TopnavService.class);
	private TopnavMenuService topnavMenuService = Aop.get(TopnavMenuService.class);
	private GlobalConfigTypeService globalConfigTypeService = Aop.get(GlobalConfigTypeService.class);
	private DeptService deptService = Aop.get(DeptService.class);
	private PostService postService = Aop.get(PostService.class);
	private OnlineUserService onlineUserService = Aop.get(OnlineUserService.class);
	 
	private String buildCacheKey(String pre,Object value) {
		return JBOLT_CACHE_DEFAULT_PREFIX+pre+value.toString();
	}
	/**
	 * 缓存通过ID获得GoodsBackCategory数据
	 * 
	 * @param id
	 * @return
	 */
	public GoodsBackCategory getGoodsBackCategory(Object id) {
		return goodsBackCategoryService.findById(id);
	}

	/**
	 * cache中获取商品后台分类的名称
	 * 
	 * @param id
	 * @return
	 */
	public String getGoodsBackCategoryName(Object id) {
		GoodsBackCategory goodsBackCategory = getGoodsBackCategory(id);
		return goodsBackCategory == null ? "" : goodsBackCategory.getName();
	}

	/**
	 * cache中获取商品后台分类的唯一标识KEY
	 * 
	 * @param id
	 * @return
	 */
	public String getGoodsBackCategoryKey(Object id) {
		GoodsBackCategory goodsBackCategory = getGoodsBackCategory(id);
		return goodsBackCategory == null ? null : goodsBackCategory.getCategoryKey();
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public String getGoodsBackCategoryFullName(Object id) {
		GoodsBackCategory goodsCategory = getGoodsBackCategory(id);
		if (goodsCategory == null) {
			return "";
		}
		String keys = goodsCategory.getCategoryKey();
		if (StrKit.isBlank(keys)) {
			return "";
		}
		if (keys.indexOf("_") == -1) {
			return goodsCategory.getName();
		}
		String[] ids = ArrayUtil.from3(keys, "_");
		if (ids == null || ids.length == 0) {
			return "";
		}
		String fullName = "";
		for (int i = 0; i < ids.length; i++) {
			String name = getGoodsBackCategoryName(ids[i]);
			if (StrKit.notBlank(name)) {
				if (i == 0) {
					fullName = name;
				} else {
					fullName = fullName + " > " + name;
				}
			}
		}
		return fullName;
	}

	/**
	 * 缓存通过ID获得角色Role数据
	 * 
	 * @param id
	 * @return
	 */
	public Role getRole(Object id) {
		return roleService.findById(id);
	}
	/**
	 * 缓存通过ID获得数据的Name
	 * 
	 * @param id
	 * @return
	 */
	public String getRoleName(Object id) {
		Role role = getRole(id);
		return role == null ? "未分配" : role.getName();
	}
	/**
	 * 缓存通过SN获得角色Role数据
	 * 
	 * @param sn
	 * @return
	 */
	public Role getRoleBySn(String sn) {
		return roleService.getCacheByKey(sn);
	}
	
	/**
	 * 缓存通过sn获得数据的Name
	 * 
	 * @param sn
	 * @return
	 */
	public String getRoleNameBySn(String sn) {
		Role role = getRoleBySn(sn);
		return role == null ? "未分配" : role.getName();
	}
	/**
	 * 从缓存获取多个角色
	 * 
	 * @param ids
	 * @return
	 */
	public List<Role> getRoles(String roles) {
		if (StrKit.isBlank(roles)) {
			return Collections.emptyList();
		}
		String[] roleIds = ArrayUtil.from3(roles, ",");
		if (roleIds == null || roleIds.length == 0) {
			return Collections.emptyList();
		}
		List<Role> roleList = new ArrayList<Role>();
		Role role = null;
		for (String roleId : roleIds) {
			role = getRole(roleId);
			if (role != null) {
				roleList.add(role);
			}
		}
		
		return roleList;
	}
	/**
	 * 从缓存获取多个岗位
	 * 
	 * @param ids
	 * @return
	 */
	public List<Post> getPosts(String posts) {
		if (StrKit.isBlank(posts)) {
			return Collections.emptyList();
		}
		String[] postIds = ArrayUtil.from3(posts, ",");
		if (postIds == null || postIds.length == 0) {
			return Collections.emptyList();
		}
		List<Post> postList = new ArrayList<Post>();
		Post post = null;
		for (String postId : postIds) {
			post = getPost(postId);
			if (post != null) {
				postList.add(post);
			}
		}

		return postList;
	}

	/**
	 * 通过ID获得字典数据类型
	 * 
	 * @return
	 */
	public DictionaryType getDictionaryType(Object id) {
		return dictionaryTypeService.findById(id);
	}
	/**
	 * 通过typeKey获得字典数据类型
	 * 
	 * @return
	 */
	public DictionaryType getDictionaryTypeByKey(String typeKey) {
		return dictionaryTypeService.getCacheByKey(typeKey);
	}

	/**
	 * 获得字典数据类型的标识Key
	 * 
	 * @param id
	 * @return
	 */
	public String getDictionaryTypeName(Object id) {
		DictionaryType dictionaryType = getDictionaryType(id);
		return dictionaryType == null ? "" : dictionaryType.getName();
	}

	/**
	 * 获得字典数据类型的标识Key
	 * 
	 * @param id
	 * @return
	 */
	public String getDictionaryTypeKey(Object id) {
		DictionaryType dictionaryType = getDictionaryType(id);
		return dictionaryType == null ? "" : dictionaryType.getTypeKey();
	}

	/**
	 * 获得字典数据类型的ModelLevel 层级模式
	 * 
	 * @param id
	 * @return
	 */
	public int getDictionaryTypeModeLevel(Object id) {
		DictionaryType dictionaryType = getDictionaryType(id);
		return dictionaryType == null ? 0 : dictionaryType.getModeLevel();
	}

	/**
	 * 通过Sn获得字典数据
	 * @param typeKey
	 * @param sn
	 * @return
	 */
	public Dictionary getDictionaryBySn(String typeKey,String sn) {
		return dictionaryService.getCacheByKey(sn, typeKey);
	}
	/**
	 * 通过ID获得字典数据
	 * 
	 * @return
	 */
	public Dictionary getDictionary(Object id) {
		return dictionaryService.findById(id);
	}

	/**
	 * 获得字典数据名称
	 * 
	 * @param id
	 * @return
	 */
	public String getDictionaryName(Object id) {
		Dictionary dictionary = getDictionary(id);
		return dictionary == null ? "" : dictionary.getName();
	}
	/**
	 * 获得字典数据名称 根据类型key和编号sn查询cache
	 * @param typeKey
	 * @param sn
	 * @return
	 */
	public String getDictionaryNameBySn(String typeKey , String sn) {
		Dictionary dictionary = getDictionaryBySn(typeKey, sn);
		return dictionary == null ? "" : dictionary.getName();
	}

	/**
	 * 获得用户姓名
	 * 
	 * @param id
	 * @return
	 */
	public String getUserName(Object id) {
		User user = getUser(id);
		return user == null ? "" : user.getName();
	}
	/**
	 * 获得用户用户名
	 * 
	 * @param id
	 * @return
	 */
	public String getUserUsername(Object id) {
		User user = getUser(id);
		return user == null ? "" : user.getUsername();
	}

	/**
	 * 获得用户头像
	 * 
	 * @param id
	 * @return
	 */
	public String getUserAvatar(Object id) {
		User user = getUser(id);
		return user == null ? "" : user.getAvatar();
	}

	/**
	 * 获得用户头像
	 * 
	 * @param id
	 * @return
	 */
	public String getUserRealAvatar(Object id) {
		User user = getUser(id);
		return user == null ? "" : RealUrlUtil.getImage(user.getAvatar(), "assets/img/avatar.jpg");
	}

	/**
	 * 通过ID获得User
	 * 
	 * @return
	 */
	public User getUser(Object id) {
		return userService.findById(id);
	}
	/**
	 * 获得当前线程User
	 * 
	 * @return
	 */
	public User getCurrentAdminUser() {
		return getUser(JBoltUserKit.getUserId());
	}

	/**
	 * 缓存通过ID获得角色Role的permissionsKeySet
	 * 
	 * @param roleIds
	 * @return
	 */
	public Set<String> getRolePermissionKeySet(String roleIds) {
		if (StrKit.isBlank(roleIds)) {
			return null;
		}
		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("roles_permission_keyset_" , roleIds), new IDataLoader() {
			@Override
			public Object load() {
				return rolePermissionService.getPermissionsKeySetByRoles(roleIds);
			}
		});
	}
	
	/**
	 * 缓存通过User ID获得角色Role的permissions
	 * 
	 * @return
	 */
	public List<Permission> getCurrentUserMenus() {
		return getUserMenus(JBoltUserKit.getUserId());
	}

	/**
	 * 缓存通过User ID获得角色Role的permissions
	 * 
	 * @param userId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Permission> getUserMenus(Object userId) {
		User user = getUser(userId);
		if (user == null) {
			return Collections.emptyList();
		}
		List<Permission> menus=null;
		if (user.getIsSystemAdmin()) {
			menus=getRoleMenusWithSystemAdminDefault(user.getRoles());
		}else {
			menus=getRoleMenus(user.getRoles());
		}
		if(isOk(menus)) {
			//处理绑定顶部菜单问题
			List<TopnavMenu> topnavMenus=topnavMenuService.findAll();
			if(isOk(topnavMenus)) {
				Kv kv=Kv.create();
				for(TopnavMenu menu:topnavMenus) {
					kv.set("m_"+menu.getPermissionId(),menu.getTopnavId());
				}
				for(Permission menu:menus) {
					menu.put("topnavId",kv.getOrDefault("m_"+menu.getId(),0));
				}
			}else {
				for(Permission menu:menus) {
					menu.put("topnavId",0);
				}
			}
			
		}
		
		return menus;
	}

	public List<Permission> getRoleMenus(String roleIds) {
		if (StrKit.isBlank(roleIds)) {
			return null;
		}
		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("roles_menus_" , roleIds), new IDataLoader() {
			@Override
			public Object load() {
				return permissionService.getMenusByRoles(roleIds);
			}
		});

	}

	public List<Permission> getRoleMenusWithSystemAdminDefault(String roleIds) {
		return CacheKit.get(JBOLT_CACHE_NAME,
				buildCacheKey("roles_menus_with_sadmindefault_" , (StrKit.isBlank(roleIds) ? "null" : roleIds)),
				new IDataLoader() {
					@Override
					public Object load() {
						return permissionService.getMenusByRolesWithSystemAdminDefault(roleIds);
					}
				});

	}

	/**
	 * 通过ID获得Permissions
	 * 
	 * @return
	 */
	public Permission getPermission(Object id) {
		return permissionService.findById(id);
	}

	/**
	 * 通过permissionKey获得Permissions
	 * 
	 * @return
	 */
	public Permission getPermissionByKey(String permissionKey) {
		return permissionService.getCacheByKey(permissionKey);
	}

	public void removeMenusAndPermissionsByRoleGroups() {
		List<String> roleGroups = userService.getAllRoleGroups();
		if (roleGroups.size() > 0) {
			roleGroups.forEach(new Consumer<String>() {
				@Override
				public void accept(String roleIds) {
					removeRolesPermissionKeySet(roleIds);
					removeRolesMenus(roleIds);
					removeRolesMenusWithSystemAdminDefault(roleIds);
				}
			});
		}
	}

	
	/**
	 * 删除removeRolesPermissionKeySet
	 * 
	 * @param roleIds
	 */
	public void removeRolesPermissionKeySet(String roleIds) {
		if (StrKit.notBlank(roleIds)) {
			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("roles_permission_keyset_",roleIds));
		}
	}


	/**
	 * 删除removeRoleMenus
	 * 
	 * @param roleIds
	 */
	public void removeRolesMenus(String roleIds) {
		if (StrKit.notBlank(roleIds)) {
			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("roles_menus_" ,roleIds));
		}
	}

	/**
	 * 删除removeRolesMenusWithSystemAdminDefault
	 * 
	 * @param roleIds
	 */
	public void removeRolesMenusWithSystemAdminDefault(String roleIds) {
		CacheKit.remove(JBOLT_CACHE_NAME,
				buildCacheKey("roles_menus_with_sadmindefault_" , (StrKit.isBlank(roleIds) ? "null" : roleIds)));
	}

	/**
	 * put
	 * @param key
	 * @param value
	 */
	public void put(String key, Object value) {
		CacheKit.put(JBOLT_CACHE_NAME, key, value);
	}
	/**
	 * get
	 * @param <T>
	 * @param key
	 * @return
	 */
	public <T> T get(String key) {
		return CacheKit.get(JBOLT_CACHE_NAME, key);
	}
	/**
	 * 得到全局配置JFinalActionReport输出位置
	 * @return
	 */
	public String getJFinalActionReportWriter() {
		String value=getGlobalConfigValue(GlobalConfigKey.JBOLT_ACTION_REPORT_WRITER);
		return StrKit.isBlank(value)?"sysout":value;
	}
	/**
	 * 得到全局配置Jbolt auto cache log 是否启用
	 * @return
	 */
	public boolean getJBoltAutoCacheLog() {
		String value=getGlobalConfigValue(GlobalConfigKey.JBOLT_AUTO_CACHE_LOG);
		if(StrKit.isBlank(value)) {return false;}
		return Boolean.parseBoolean(value);
	}
	/**
	 * 得到全局配置assets version
	 * @return
	 */
	public String getAssetsVersion() {
		String value=getGlobalConfigValue(GlobalConfigKey.ASSETS_VERSION);
		if(StrKit.isBlank(value)) {
			globalConfigService.changeAssetsVersion();
			value=getGlobalConfigValue(GlobalConfigKey.ASSETS_VERSION);
		}
		return value;
	}
	/**
	 * 得到全局配置SYSTEM_ADMIN_H50
	 * @return
	 */
	public boolean getSystemAdminH50() {
		String value=getGlobalConfigValue(GlobalConfigKey.SYSTEM_ADMIN_H50);
		if(StrKit.isBlank(value)) {return false;}
		return Boolean.parseBoolean(value);
	}
	/**
	 * 得到全局配置SYSTEM_ADMIN_NAV_MENU_DEFAULT_ICON
	 * @return
	 */
	public String getSystemAdminNavMenuDefaultIcon() {
		String value=getGlobalConfigValue(GlobalConfigKey.SYSTEM_ADMIN_NAV_MENU_DEFAULT_ICON);
		return StrKit.isBlank(value)?"":value;
	}
	/**
	 * 得到全局配置SYSTEM_ADMIN_GLOBAL_SEARCH_SHOW
	 * @return
	 */
	public boolean getSystemAdminGlobalSearchShow() {
		String value=getGlobalConfigValue(GlobalConfigKey.SYSTEM_ADMIN_GLOBAL_SEARCH_SHOW);
		if(StrKit.isBlank(value)) {return false;}
		return Boolean.parseBoolean(value);
	}
	/**
	 * 得到全局配置SYSTEM_ADMIN_GLOBAL_SEARCH_SHOW
	 * @return
	 */
	public int getSystemAdminLeftNavWidth() {
		String value=getGlobalConfigValue(GlobalConfigKey.SYSTEM_ADMIN_LEFT_NAV_WIDTH);
		if(StrKit.isBlank(value)) {return 220;}
		return Integer.parseInt(value);
	}
	/**
	 * 缓存通过key获取全局配置
	 * 
	 * @param configKey
	 * @return
	 */
	public GlobalConfig getGlobalConfig(String configKey) {
		return globalConfigService.getCacheByKey(configKey);
	}

	/**
	 * 缓存通过key获取配置的值
	 * 
	 * @param key
	 * @return
	 */
	public String getGlobalConfigValue(String configKey) {
		GlobalConfig globalConfig = getGlobalConfig(configKey);
		if(globalConfig==null) {
			globalConfigService.checkAndInitConfig(configKey);
			globalConfig = getGlobalConfig(configKey);
		}
		return globalConfig == null ? "" : globalConfig.getConfigValue();
	}

	/**
	 * 获取微信公众号服务器配置根地址URL
	 * 
	 * @return
	 */
	public String getWechatMpServerDomainRootUrl() {
		return getGlobalConfigValue(GlobalConfigKey.WECHAT_MP_SERVER_DOMAIN);
	}

	/**
	 * 获取微信小程序服务器配置根地址URL
	 * 
	 * @return
	 */
	public String getWechatWxaServerDomainRootUrl() {
		return getGlobalConfigValue(GlobalConfigKey.WECHAT_WXA_SERVER_DOMAIN);
	}

	/**
	 * 获取登录是否启用验证码
	 * 
	 * @return
	 */
	public boolean isJBoltLoginUseCapture() {
		GlobalConfig config = getGlobalConfig(GlobalConfigKey.JBOLT_LOGIN_USE_CAPTURE);
		if (config == null) {
			return true;
		}
		return Boolean.parseBoolean(config.getConfigValue());
	}

	/**
	 * 通过id获得goodsType
	 * 
	 * @return
	 */
	public GoodsType getGoodsType(Object id) {
		return goodsTypeService.findById(id);
	}

	/**
	 * 缓存通过mpId获得微信公众平台WechatConfig configValue
	 * @param mpId
	 * @param type
	 * @param configKey
	 * @return
	 */
	public String getWechatConfigValue(Object mpId,int type,String configKey) {
		if (mpId == null ||StrKit.isBlank(mpId.toString()) || "0".equals(mpId.toString())) {
			return null;
		}
		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("mpId_"+type+"_"+configKey+"_" , mpId), new IDataLoader() {
			@Override
			public Object load() {
				return wechatConfigService.getWechatConfigValue(mpId, type, configKey);
			}
		});
	}
	
	/**
	 * 删除微信公众平台的基础配置wechatConfig configValue
	 * @param mpId
	 * @param type
	 * @param configKey
	 */
	public void removeWechatConfigValue(Object mpId,int type,String configKey) {
		if (mpId != null && "0".equals(mpId.toString())==false) {
			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("mpId_"+type+"_"+configKey+"_"  , mpId));
		}
	}
	
	/**
	 * 缓存通过mpId获得微信公众平台WechatConfig AppId
	 * 
	 * @param mpId
	 * @return
	 */
	public String getWechatConfigAppId(Object mpId) {
		return getWechatConfigValue(mpId, WechatConfig.TYPE_BASE, WechatConfigKey.APP_ID);
	}
	
	/**
	 * 缓存通过mpId获得微信公众平台WechatConfig mchId
	 * 
	 * @param mpId
	 * @return
	 */
	public String getWechatConfigMchId(Object mpId) {
		return getWechatConfigValue(mpId, WechatConfig.TYPE_PAY, WechatConfigKey.MCH_ID);
	}
	
	/**
	 * 缓存通过mpId获得微信公众平台WechatConfig paternerKey
	 * 
	 * @param mpId
	 * @return
	 */
	public String getWechatConfigPaternerKey(Object mpId) {
		return getWechatConfigValue(mpId, WechatConfig.TYPE_PAY, WechatConfigKey.PATERNERKEY);
	}
	
 
	/**
	 * 缓存通过mpId获得微信公众平台WechatConfig AppSecret
	 * @param mpId
	 * @return
	 */
	public String getWechatConfigAppSecret(Object mpId) {
		return getWechatConfigValue(mpId, WechatConfig.TYPE_BASE, WechatConfigKey.APP_SECRET);
	}
	 
	/**
	 * 从cache获取到公众平台默认回复
	 * 
	 * @return
	 */
	public OutMsg getWechcatDefaultOutMsg(String appId, String openId) {
		if (StrKit.isBlank(appId)) {
			return null;
		}
		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("mpaureply_defaultmsg_" , appId), new IDataLoader() {
			@Override
			public Object load() {
				return wechatReplyContentService.getWechcatDefaultOutMsg(appId, openId);
			}
		});
	}


	/**
	 * 从cache获取到公众平台关注回复
	 * 
	 * @param appId
	 * @param openId
	 * @return
	 */
	public OutMsg getWechcatSubscribeOutMsg(String appId, String openId) {
		if (StrKit.isBlank(appId)) {
			return null;
		}
		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("mpaureply_subscribemsg_" , appId), new IDataLoader() {
			@Override
			public Object load() {
				return wechatReplyContentService.getWechcatSubscribeOutMsg(appId, openId);
			}
		});
	}

	/**
	 * 删除微信公众平台默认自动回复消息
	 * 
	 * @param mpId
	 */
	public void removeWechcatDefaultOutMsg(Object mpId) {
		removeWechcatDefaultOutMsg(getWechatConfigAppId(mpId));
	}

	/**
	 * 删除微信公众平台关注自动回复消息
	 * 
	 * @param mpId
	 */
	public void removeWechcatSubscribeOutMsg(Object mpId) {
		removeWechcatSubscribeOutMsg(getWechatConfigAppId(mpId));
	}

	/**
	 * 删除微信公众平台关注自动回复消息
	 * 
	 * @param mpId
	 */
	public void removeWechcatSubscribeOutMsg(String appId) {
		if (StrKit.notBlank(appId)) {
			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("mpaureply_subscribemsg_" , appId));
		}
	}

	/**
	 * 删除微信公众平台默认自动回复消息
	 * 
	 * @param appId
	 */
	public void removeWechcatDefaultOutMsg(String appId) {
		if (StrKit.notBlank(appId)) {
			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("mpaureply_defaultmsg_" , appId));
		}
	}

	/**
	 * 获取Jbolt style的样式设置
	 * 
	 * @param userId
	 * @return
	 */
	public String getUserJboltStyle(Object userId) {
		if(notOk(userId)) {
			return getJboltStyle();
		}
		String jboltStyle = getUserConfigValue(userId, GlobalConfigKey.JBOLT_ADMIN_STYLE);
		if (StrKit.isBlank(jboltStyle)) {
			return getJboltStyle();
		}
		return jboltStyle;
	}
	/**
	 * 获取Jbolt style的样式设置
	 * 
	 * @param userId
	 * @return
	 */
	public String getCurrentUserJboltStyle() {
		return getUserJboltStyle(JBoltUserKit.getUserId());
	}

	/**
	 * 获取Jbolt style的样式设置
	 * 
	 * @return
	 */
	public String getJboltStyle() {
		String jboltStyle = getGlobalConfigValue(GlobalConfigKey.JBOLT_ADMIN_STYLE);
		if (StrKit.isBlank(jboltStyle)) {
			jboltStyle = "default";
		}
		return jboltStyle;
	}
	
	/**
	 * 获取Jbolt 登录背景图配置
	 * 
	 * @return
	 */
	public String getJboltLoginBgimg() {
		String jboltLoginBgimg = getGlobalConfigValue(GlobalConfigKey.JBOLT_LOGIN_BGIMG);
		if (StrKit.isBlank(jboltLoginBgimg)) {
			jboltLoginBgimg = "assets/css/img/login_bg.jpg";
		}else if(jboltLoginBgimg.charAt(0)=='/'){
			jboltLoginBgimg=jboltLoginBgimg.substring(1);
		}
		return jboltLoginBgimg;
	}
	/**
	 * 获取Jbolt 登录背景类型
	 * 
	 * @return
	 */
	public String getJboltLoginBgType() {
		String jboltLoginBgimg = getGlobalConfigValue(GlobalConfigKey.JBOLT_LOGIN_BGIMG);
		if (StrKit.isBlank(jboltLoginBgimg)) {
			jboltLoginBgimg = "assets/css/img/login_bg.jpg";
		}else if(jboltLoginBgimg.charAt(0)=='/'){
			jboltLoginBgimg=jboltLoginBgimg.substring(1);
		}
		String extname=FileUtil.extName(jboltLoginBgimg);
		String type="img";
		switch (extname) {
			case "jpg":
				type="img";
				break;
			case "png":
				type="img";
				break;
			case "gif":
				type="img";
				break;
			case "webp":
				type="img";
				break;
			case "mp4":
				type="video";
				break;
			case "webm":
				type="video";
				break;
		}
		return type;
	}

	/**
	 * 获取Jbolt 登录线条特效配置
	 * 
	 * @return
	 */
	public boolean getJboltLoginNest() {
		String value = getGlobalConfigValue(GlobalConfigKey.JBOLT_LOGIN_NEST);
		return  StrKit.notBlank(value) && value.toLowerCase().trim().equals("true");
	}

	/**
	 * 根据AppId获得属于哪个MpId
	 * 
	 * @param appId
	 * @return
	 */
	public Object getWechatMpidByAppId(String appId) {
		if (StrKit.isBlank(appId)) {
			return null;
		}
		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("mpid_by_appid_" , appId), new IDataLoader() {
			@Override
			public Object load() {
				return wechatConfigService.getWechatMpidByAppId(appId);
			}
		});
	}
	/**
	 * 通过ID获取wechatMpInfo
	 * @param id
	 * @return
	 */
	public WechatMpinfo getWechatMpInfo(Object id) {
		return wechatMpinfoService.findById(id);
	}

	/**
	 * 删除appId对应mpId的缓存数据
	 * 
	 * @param appId
	 */
	public void removeMpidByAppId(String appId) {
		if (StrKit.notBlank(appId)) {
			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("mpid_by_appid_" , appId));
		}
	}

	/**
	 * 得到Jbolt默认后台是不是多选项卡风格 全局配置
	 * 
	 * @return
	 */
	public boolean getJBoltAdminWithTabs() {
		String value = getGlobalConfigValue(GlobalConfigKey.JBOLT_ADMIN_WITH_TABS);
		return StrKit.notBlank(value) && value.toLowerCase().trim().equals("true");
	}

	/**
	 * 得到当前User是否启用多选项卡风格 用户配置
	 * 
	 * @return
	 */
	public boolean getCurrentUserJBoltAdminWithTabs() {
		return getUserJBoltAdminWithTabs(JBoltUserKit.getUserId());
	}
	/**
	 * 得到user是否启用多选项卡风格 用户配置
	 * 
	 * @return
	 */
	public boolean getUserJBoltAdminWithTabs(Object userId) {
		if(notOk(userId)) {
			return getJBoltAdminWithTabs();
		}
		String value = getUserConfigValue(userId, GlobalConfigKey.JBOLT_ADMIN_WITH_TABS);
		if (StrKit.isBlank(value)) {
			return getJBoltAdminWithTabs();
		}
		return value.toLowerCase().trim().equals("true");
	}

	/**
	 * 得到Jbolt登录页面是否启用透明玻璃风格 全局配置
	 * 
	 * @return
	 */
	public boolean getJBoltLoginFormStyleGlass() {
		String value = getGlobalConfigValue(GlobalConfigKey.JBOLT_LOGIN_FORM_STYLE_GLASS);
		return StrKit.notBlank(value) && value.toLowerCase().trim().equals("true");
	}

	/**
	 * 得到Jbolt登录页面是否启用透明玻璃风格 用户配置
	 * 
	 * @return
	 */
	public boolean getUserJBoltLoginFormStyleGlass(Object userId) {
		if(notOk(userId)) {
			return getJBoltLoginFormStyleGlass();
		}
		String value = getUserConfigValue(userId, GlobalConfigKey.JBOLT_LOGIN_FORM_STYLE_GLASS);
		if (StrKit.isBlank(value)) {
			return getJBoltLoginFormStyleGlass();
		}
		return value.toLowerCase().trim().equals("true");
	}

	/**
	 * 得到Jbolt登录页面是否启用背景图模糊 全局配置
	 * 
	 * @return
	 */
	public boolean getJBoltLoginBgimgBlur() {
		String value = getGlobalConfigValue(GlobalConfigKey.JBOLT_LOGIN_BGIMG_BLUR);
		return StrKit.notBlank(value) && value.toLowerCase().trim().equals("true");
	}

	/**
	 * 得到Jbolt登录页面是否启用线条特效 用户配置
	 * 
	 * @return
	 */
	public boolean getUserJBoltLoginNest(Object userId) {
		if(notOk(userId)) {
			return getJboltLoginNest();
		}
		String value = getUserConfigValue(userId, GlobalConfigKey.JBOLT_LOGIN_NEST);
		if (StrKit.isBlank(value)) {
			return getJboltLoginNest();
		}
		return value.toLowerCase().trim().equals("true");
	}
	
	/**
	 * 得到Jbolt登录页面是否启用背景图模糊 用户配置
	 * 
	 * @return
	 */
	public boolean getUserJBoltLoginBgimgBlur(Object userId) {
		if(notOk(userId)) {
			return getJBoltLoginBgimgBlur();
		}
		String value = getUserConfigValue(userId, GlobalConfigKey.JBOLT_LOGIN_BGIMG_BLUR);
		if (StrKit.isBlank(value)) {
			return getJBoltLoginBgimgBlur();
		}
		return value.toLowerCase().trim().equals("true");
	}

	/**
	 * 通过AppId 获取Application
	 * 
	 * @param appId
	 * @return
	 */
	public Application getApplicationByAppId(String appId) {
		return applicationService.getCacheByKey(appId);
	}
	/**
	 * 通过AppId 获取Application调用接口的Application
	 * 有一定阉割 只需要部分字段的
	 * @param appId
	 * @return
	 */
	public Application getApiCallApplication(String appId) {
		if (StrKit.isBlank(appId)) {
			return null;
		}
		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("api_application_appid_" , appId), new IDataLoader() {
			@Override
			public Object load() {
				return applicationService.getApiApplicationByAppId(appId);
			}
		});
	}
	/**
	 * 根据APPID删除Api专用Application的换缓存
	 * @param appId
	 */
	public void removeApiCallApplication(String appId) {
		if (StrKit.notBlank(appId)) {
			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("api_application_appid_" , appId));
		}
	}
	
	/**
	 * 通过id 获取Application
	 * 
	 * @param id
	 * @return
	 */
	public Application getApplication(Object id) {
		return applicationService.findById(id);
	}
	
	/**
	 * 通过id 获取Application attr
	 * 
	 * @param id
	 * @return
	 */
	public <T> T getApplicationAttr(Object id,String attrName) {
		Application application=getApplication(id);
		return application==null?null:application.get(attrName);
	}
	
	/**
	 * 通过id 获取Application name
	 * @param id
	 * @return
	 */
	public String getApplicationName(Object id) {
		return getApplicationAttr(id, "name");
	}


	/**
	 * 缓存通过key获取用户配置
	 * 
	 * @param key
	 * @return
	 */
	public UserConfig getUserConfig(Object userId, String configKey) {
		return userConfigService.getCacheByKey(configKey, userId);
	}

	
	/**
	 * 缓存通过key获取用户个性化配置的值
	 * 
	 * @param key
	 * @return
	 */
	public String getUserConfigValue(Object userId, String configKey) {
		UserConfig userConfig = getUserConfig(userId, configKey);
		return userConfig == null ? "" : userConfig.getConfigValue();
	}
	
	/**
	 * 根据mpid和openId拿到wechat用户信息 api专用
	 * @param mpId
	 * @param openId
	 * @return
	 */
	public WechatUser getApiWechatUserByMpOpenId(Object mpId, String openId) {
		if (mpId==null||StrKit.isBlank(openId)) {
			return null;
		}
		return CacheKit.get(JBOLT_CACHE_NAME, buildCacheKey("api_wechat_user_openid_" ,mpId.toString()+"_"+openId), new IDataLoader() {
			@Override
			public Object load() {
				return wechatUserService.getApiWechatUserByOpenId(mpId,openId);
			}
		});
	}
	
	/**
	 * 根据mpid和openId删除wechat用户信息 api专用
	 * @param mpId
	 * @param appId
	 */
	public void removeApiWechatUserByMpOpenId(Object mpId, String openId) {
		if (mpId!=null&&StrKit.notBlank(openId)) {
			CacheKit.remove(JBOLT_CACHE_NAME, buildCacheKey("api_wechat_user_openid_" ,mpId.toString()+"_"+openId));
		}
	}
	
	/**
	 * 得到全局配置SYSTEM_NAME
	 * @return
	 */
	public String getSystemName(String defaultName) {
		String value=getGlobalConfigValue(GlobalConfigKey.SYSTEM_NAME);
		if(StrKit.isBlank(value)) {return StrKit.isBlank(defaultName)?"JBolt极速开发平台":defaultName;}
		if(value.indexOf(":")!=-1) {
			String[] nameArr=value.split(":");
			return (nameArr!=null&&nameArr.length==2)?nameArr[0]:(StrKit.isBlank(defaultName)?"JBolt极速开发平台":defaultName);
		}
		return value;
	}
	/**
	 * 得到全局配置SYSTEM_NAME的color值
	 * @return
	 */
	public String getSystemNameColor(String defaultColor) {
		String value=getGlobalConfigValue(GlobalConfigKey.SYSTEM_NAME);
		if(StrKit.isBlank(value)) {return StrKit.isBlank(defaultColor)?"#FFFFFF":defaultColor;}
		if(value.indexOf(":")!=-1) {
			String[] nameArr=value.split(":");
			return (nameArr!=null&&nameArr.length==2)?nameArr[1]:(StrKit.isBlank(defaultColor)?"#FFFFFF":defaultColor);
		}
		return StrKit.isBlank(defaultColor)?"#FFFFFF":defaultColor;
	}
	/**
	 * 得到全局配置SYSTEM_COPYRIGHT_COMPANY
	 * @return
	 */
	public String getSystemCopyrightCompany(String defaultName) {
		String value=getGlobalConfigValue(GlobalConfigKey.SYSTEM_COPYRIGHT_COMPANY);
		if(StrKit.isBlank(value)) {return StrKit.isBlank(defaultName)?"©JBolt(JBOLT.CN)":defaultName;}
		if(value.indexOf(":")!=-1) {
			String[] nameArr=value.split(":");
			return (nameArr!=null&&nameArr.length==2)?nameArr[0]:(StrKit.isBlank(defaultName)?"©JBolt(JBOLT.CN)":defaultName);
		}
		return value;
	}
	/**
	 * 得到全局配置SYSTEM_COPYRIGHT_COMPANY的color值
	 * @return
	 */
	public String getSystemCopyrightColor(String defaultColor) {
		String value=getGlobalConfigValue(GlobalConfigKey.SYSTEM_COPYRIGHT_COMPANY);
		if(StrKit.isBlank(value)) {return StrKit.isBlank(defaultColor)?"#FFFFFF":defaultColor;}
		if(value.indexOf(":")!=-1) {
			String[] copyrightArr=value.split(":");
			return (copyrightArr!=null&&copyrightArr.length==2)?copyrightArr[1]:(StrKit.isBlank(defaultColor)?"#FFFFFF":defaultColor);
		}
		return StrKit.isBlank(defaultColor)?"#FFFFFF":defaultColor;
	}
	/**
	 * 通过用户名 拿到用户信息
	 * @param username
	 * @return
	 */
	public User getUserByUsername(String username) {
		User user=userService.getCacheByKey(username);
		if(user!=null) {
			user.removeNullValueAttrs().remove("password");
		}
		return user;
	}
	/**
	 * 用过用户名拿到用户ID
	 * @param username
	 * @return
	 */
	public Object getUserIdByUsername(String username) {
		User user=getUserByUsername(username);
		return user==null?null:user.getId();
	}
	
	/**
	 * 获取顶部导航数据
	 * @return
	 */
	public List<Topnav> getEnableTopNavs(){
		return CacheKit.get(JBOLT_CACHE_NAME,"jbolt_enable_top_navs", new IDataLoader() {
			@Override
			public Object load() {
				return topnavService.getEnableList();
			}
		});
	}
	/**
	 * 获取顶部导航 网页上指令直接调用
	 * @return
	 */
	public List<Topnav> getTopNavs(){
		return getCurrentUserTopNavs();
	}
	/**
	 * 获取顶部当前用户可看顶部导航数据
	 * @return
	 */
	public List<Topnav> getCurrentUserTopNavs(){
		List<Permission> userMenus = getCurrentUserMenus();
		if(isOk(userMenus)) {
			List<Integer> ids=ArrayUtil.getDistinctIntegerList(userMenus, "topnavId");
			if(isOk(ids)) {
				return topnavService.getEnableListByIds(ids);
			}
		}
		return Collections.emptyList();
	}
	
	/**
	 * 删除顶部导航cache数据
	 */
	public void removeTopNavs() {
		CacheKit.remove(JBOLT_CACHE_NAME, "jbolt_top_navs");
	}
	/**
	 * 获取topnav
	 * @param id
	 * @return
	 */
	public Topnav getTopnav(Object id) {
		return topnavService.findById(id);
	}
	/**
	 * 获取topnav name
	 * @param topnavId
	 * @return
	 */
	public String getTopMenuName(Object topnavId) {
		Topnav nav=getTopnav(topnavId);
		if(nav==null) {return null;}
		return nav.getName();
	}
	/**
	 * 获取当前是否存在开启的topnav
	 * @return
	 */
	public boolean getJBoltEnableTopnav() {
		return CacheKit.get(JBOLT_CACHE_NAME,"jbolt_enable_topnav", new IDataLoader() {
			@Override
			public Object load() {
				return topnavService.checkHasEnableTopnav();
			}
		});
		
	}
	/**
	 * 删除topnav开启 cache
	 */
	public void removeJBoltEnableTopnav() {
		CacheKit.remove(JBOLT_CACHE_NAME, "jbolt_enable_topnav");
	}
	
	/**
	 * 获取GlobalConfigType
	 * @param id
	 * @return
	 */
	public GlobalConfigType getGlobalConfigType(Object id) {
		return globalConfigTypeService.findById(id);
	}
	
	/**
	 * 获取GlobalConfigType
	 * @param key
	 * @return
	 */
	public GlobalConfigType getGlobalConfigTypeByKey(String key) {
		return globalConfigTypeService.getCacheByKey(key);
	}
	
	/**
	 * 获取GlobalConfigType-name
	 * @param typeId
	 * @return
	 */
	public String getGlobalConfigTypeName(Object typeId) {
		GlobalConfigType type=getGlobalConfigType(typeId);
		if(type==null) {return null;}
		return type.getName();
	}
	/**
	 * 获取岗位信息
	 * @param id
	 * @return
	 */
	public Post getPost(Object id) {
		return postService.findById(id);
	} 
	/**
	 * 根据SN编码获取岗位信息
	 * @param sn
	 * @return
	 */
	public Post getPostBySn(String sn) {
		return postService.getCacheByKey(sn);
	} 
	
	/**
	 * 获取岗位Name
	 * @param id
	 * @return
	 */
	public String getPostName(Object id) {
		Post post=getPost(id);
		return post==null?"":post.getName();
	}
	/**
	 * 获取岗位Sn+Name
	 * @param id
	 * @return
	 */
	public String getPostSnAndName(Object id) {
		Post post=getPost(id);
		return post==null?"":("("+post.getSn()+")"+post.getName());
	}
	/**
	 * 获取部门信息
	 * @param id
	 * @return
	 */
	public Dept getDept(Object id) {
		return deptService.findById(id);
	} 
	/**
	 * 使用SN编号获取部门信息
	 * @param sn
	 * @return
	 */
	public Dept getDeptBySn(String sn) {
		return deptService.getCacheByKey(sn);
	} 
	/**
	 * 获取部门Name
	 * @param deptId
	 * @return
	 */
	public String getDeptName(Object id) {
		Dept dept=getDept(id);
		return dept==null?"":dept.getName();
	}
	/**
	 * 获取部门FullName
	 * @param deptId
	 * @return
	 */
	public String getDeptFullName(Object id) {
		Dept dept=getDept(id);
		return dept==null?"":dept.getFullName();
	}
	/**
	 * 获取部门Sn and Name
	 * @param deptId
	 * @return
	 */
	public String getDeptSnAndName(Object id) {
		Dept dept=getDept(id);
		return dept==null?"":("("+dept.getSn()+")"+dept.getName());
	}
	/**
	 * 根据sessionId 获取在线用户
	 * @param sessionId
	 * @return
	 */
	public OnlineUser getOnlineUserBySessionId(String sessionId) {
		return onlineUserService.getCacheByKey(sessionId);
	}
	/**
	 * 根据sessionId 获取系统用户
	 * @param sessionId
	 * @return
	 */
	public User getUserBySessionId(String sessionId) {
		OnlineUser onlineUser = getOnlineUserBySessionId(sessionId);
		if(onlineUser==null) {return null;}
		return getUser(onlineUser.getUserId());
	}
	
	/**
	 * 根据sessionId 获取系统用户userId
	 * @param sessionId
	 * @return
	 */
	public Object getUserIdBySessionId(String sessionId) {
		User user = getUserBySessionId(sessionId);
		return user==null?null:user.getId();
	}
	
	/**
	 * 获取后台用户登录保持登录的时间长短值
	 * @return
	 */
	public int getGlobalConfigKeepLoginSeconds() {
		String configValue = getGlobalConfigValue(GlobalConfigKey.JBOLT_ADMIN_USER_KEEPLOGIN_SECONDS);
		if (StrKit.isBlank(configValue)) {
			//默认7天 如果还没有配置的话
			return JBoltConst.JBOLT_KEEPLOGIN_DEFAULT_SECONDS;
		}
		int value = Integer.parseInt(configValue);
		//如果没有配置正确值默认7天
		return value<=0?JBoltConst.JBOLT_KEEPLOGIN_DEFAULT_SECONDS:value;
	}
	
	/**
	 * 获取后台登录页面配置的文件
	 * @return
	 */
	public String getGlobalConfigLoginFile() {
		String configValue = getGlobalConfigValue(GlobalConfigKey.JBOLT_ADMIN_LOGIN_HTML_FILE);
		if (StrKit.isBlank(configValue)) {
			return JBoltConst.JBOLT_ADMIN_LOGIN_DEFAULT_FILE;
		}
		return configValue;
	}
	/**
	 * 获取后台用户登录不保持登录的时间长短值
	 * @return
	 */
	public int getGlobalConfigNotKeepLoginSeconds() {
		String configValue = getGlobalConfigValue(GlobalConfigKey.JBOLT_ADMIN_USER_NOT_KEEPLOGIN_SECONDS);
		if (StrKit.isBlank(configValue)) {
			//默认8小时 如果还没有配置的话
			return JBoltConst.JBOLT_NOT_KEEPLOGIN_DEFAULT_SECONDS;
		}
		int value = Integer.parseInt(configValue);
		//如果没有配置正确值默认8小时
		return value<=0?JBoltConst.JBOLT_NOT_KEEPLOGIN_DEFAULT_SECONDS:value;
	}
	/**
	 * 获取多长时间不操作 自动锁屏时间
	 * @return
	 */
	public int getJBoltAutoLockScreenSeconds() {
		String configValue = getGlobalConfigValue(GlobalConfigKey.JBOLT_AUTO_LOCKSCREEN_SECONDS);
		if (StrKit.isBlank(configValue)) {
			return 0;
		}
		int value = Integer.parseInt(configValue);
		//如果没有配置正确值默认30分钟
		return value;
	}
	
	/**
	 * 是否只允许一个终端登录在线 其他终端登录 在线终端被迫下线
	 * 默认可以多终端同时使用一个账号登录
	 * @return
	 */
	public boolean isLoginTerminalOnlyOne() {
		String value = getGlobalConfigValue(GlobalConfigKey.JBOLT_LOGIN_TERMINAL_ONLYONE);
		if (StrKit.isBlank(value)) {
			return false;
		}
		return value.toLowerCase().trim().equals("true");
	}
	/**
	 * 从缓存里获取wechatUser
	 * @param _id
	 * @param id
	 * @return
	 */
	public WechatUser getApiWechatUserByApiUserId(Object mpId, Object id) {
		return CacheKit.get(JBOLT_CACHE_NAME,"wecaht_user_"+mpId+"_"+id, new IDataLoader() {
			@Override
			public Object load() {
				WechatUser wechatUser = wechatUserService.findByIdToWechatUser(mpId,id);
				if(wechatUser!=null) {
					wechatUser.removeNullValueAttrs().remove("last_auth_time","first_auth_time","first_login_time","last_login_time","checked_time","remark","group_id","tag_ids","subscribe_scene","qr_scene","qr_scene_str","check_code","session_key","update_time");
				}
				return wechatUser;
			}
		});
	}
	
	/**
	 * 删除wechatUser
	 */
	public void removeApiWechatUser(Object mpId, Object id) {
		CacheKit.remove(JBOLT_CACHE_NAME, "wecaht_user_"+mpId+"_"+id);
	}
	/**
	 * 获取当前是否存在部门数据
	 * @return
	 */
	public boolean checkHasDept() {
		return CacheKit.get(JBOLT_CACHE_NAME,"sys_has_dept", new IDataLoader() {
			@Override
			public Object load() {
				return deptService.notEmpty();
			}
		});
	}
	
	/**
	 * 删除checkHasDept
	 */
	public void removeCheckHasDept() {
		CacheKit.remove(JBOLT_CACHE_NAME, "sys_has_dept");
	}
	
	/**
	 * 获取当前是否存在岗位
	 * @return
	 */
	public boolean checkHasPost() {
		return CacheKit.get(JBOLT_CACHE_NAME,"sys_has_post", new IDataLoader() {
			@Override
			public Object load() {
				return postService.notEmpty();
			}
		});
	}
	
	/**
	 * 删除checkHasDept
	 */
	public void removeCheckHasPost() {
		CacheKit.remove(JBOLT_CACHE_NAME, "sys_has_post");
	}
	
	/**
	 * 获取全局配置是否启用部门
	 * @return
	 */
	public boolean deptEnable() {
		String configValue = getGlobalConfigValue(GlobalConfigKey.SYSTEM_DEPT_ENABLE);
		if (StrKit.isBlank(configValue)) {
			return true;
		}
		return Boolean.parseBoolean(configValue);
	}
	
	/**
	 * 获取全局配置是否启用岗位
	 * @return
	 */
	public boolean postEnable() {
		String configValue = getGlobalConfigValue(GlobalConfigKey.SYSTEM_POST_ENABLE);
		if (StrKit.isBlank(configValue)) {
			return true;
		}
		return Boolean.parseBoolean(configValue);
	}
}
