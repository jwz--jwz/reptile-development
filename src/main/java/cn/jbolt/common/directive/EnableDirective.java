package cn.jbolt.common.directive;

/**
 * Boolean值转字符串指令 boolToStr
 * @ClassName:  BooleanToStrDirective   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月16日17:41:48  
 */
public class EnableDirective extends BooleanToStrDirective {
	public EnableDirective() {
		super("enable");
	}
}
