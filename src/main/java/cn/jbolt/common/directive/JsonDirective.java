package cn.jbolt.common.directive;

import java.io.IOException;
import java.util.Set;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.jfinal.kit.StrKit;
import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.expr.ast.Expr;
import com.jfinal.template.expr.ast.ExprList;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.ParseException;
import com.jfinal.template.stat.Scope;

import cn.jbolt.common.util.ArrayUtil;

/**
 * 专门处理数据转JSON的指令
 * 
 * @ClassName: JsonDirective
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2021年01月07日
 */
public class JsonDirective extends Directive {

	private Expr valueExpr;
	private Expr filterColumnExpr;
	private int paraNum;

	public void setExprList(ExprList exprList) {
		this.paraNum = exprList.length();
		if(paraNum==0) {
			throw new ParseException("Wrong number parameter of #json directive, one parameters allowed at most", location);
		}else if (paraNum > 2) {
			throw new ParseException("Wrong number parameter of #json directive, two parameters allowed at most",
					location);
		}

		if (paraNum == 1) {
			this.valueExpr = exprList.getExpr(0);
			this.filterColumnExpr = null;
		} else if (paraNum == 2) {
			this.valueExpr = exprList.getExpr(0);
			this.filterColumnExpr = exprList.getExpr(1);
		}
		
		if(this.valueExpr==null) {
			throw new ParseException("the first parameter of #json directive cannot be null", location);
		}
	}

	public void exec(Env env, Scope scope, Writer writer) {
		Object jsonObj=this.valueExpr.eval(scope);
		if(jsonObj!=null) {
			try {
				if(this.filterColumnExpr==null) {
					writer.write(objectToJson(jsonObj));
				}else{
					Object filterColumn=this.filterColumnExpr.eval(scope);
					if(filterColumn==null) {
						writer.write(objectToJson(jsonObj));
					}else {
						String fcs=filterColumn.toString();
						if(StrKit.isBlank(fcs)) {
							writer.write(objectToJson(jsonObj));
						}else {
							String[] columns=ArrayUtil.from2(fcs, ",");
							if(columns!=null&&columns.length>0) {
								writer.write(objectToJson(jsonObj,columns));
							}
						}
						
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public String objectToJson(Object obj) {
		return JSON.toJSONString(obj,SerializerFeature.DisableCircularReferenceDetect);
	}
	
	public String objectToJson(Object obj, String[] columns) {
		   //属性过滤器对象
		   SimplePropertyPreFilter filter = new SimplePropertyPreFilter();
		   
		   //属性排斥集合,强调某些属性不需要或者一定不能被序列化
		   Set<String> includes = filter.getIncludes();
		   
		   //排除不需序列化的属性
		   for (String column : columns) {
			   includes.add(column);
		   }
		   
		   //调用fastJson的方法,对象转json,
		   //参数一:需要被序列化的对象
		   //参数二:用于过滤属性的过滤器
		   //参数三:关闭循环引用,若不加这个,页面无法展示重复的属性值
		   return JSON.toJSONString(obj, filter,SerializerFeature.DisableCircularReferenceDetect);
		}
 
}
