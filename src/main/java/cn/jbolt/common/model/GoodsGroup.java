package cn.jbolt.common.model;

import cn.jbolt.common.config.TableBind;
import cn.jbolt.common.model.base.BaseGoodsGroup;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
@TableBind(dataSource = "main" , table = "jb_goods_group" , primaryKey = "id" , idGenMode = "auto")
public class GoodsGroup extends BaseGoodsGroup<GoodsGroup> {
	
}
