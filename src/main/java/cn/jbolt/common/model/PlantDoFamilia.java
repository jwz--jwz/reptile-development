package cn.jbolt.common.model;

import cn.jbolt.common.model.base.BasePlantDoFamilia;
import cn.jbolt.common.config.TableBind;
import cn.jbolt.base.JBoltIDGenMode;

/**
 * 存储多肉的科类表
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
@TableBind(dataSource = "main" , table = "plant_do_familia" , primaryKey = "id" , idGenMode = JBoltIDGenMode.AUTO)
public class PlantDoFamilia extends BasePlantDoFamilia<PlantDoFamilia> {

}

