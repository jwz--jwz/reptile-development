package cn.jbolt.common.model;

import cn.jbolt.common.model.base.BaseChangeLog;
import cn.jbolt.common.config.TableBind;

/**
 * changeLog
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
@TableBind(dataSource = "main" , table = "jb_change_log" , primaryKey = "id" , idGenMode = "auto")
public class ChangeLog extends BaseChangeLog<ChangeLog> {

}
