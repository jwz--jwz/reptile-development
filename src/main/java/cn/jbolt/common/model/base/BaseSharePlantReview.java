package cn.jbolt.common.model.base;
import cn.jbolt.base.JBoltBaseModel;
import cn.jbolt.common.gen.JBoltField;

/**
 * 审核表
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseSharePlantReview<M extends BaseSharePlantReview<M>> extends JBoltBaseModel<M>{

	/**
	 * 上传图片审核组
	 */
	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * 上传图片审核组
	 */
	@JBoltField(name="id" ,columnName="id",type="Integer", remark="上传图片审核组", required=true, maxLength=10, fixed=0, order=1)
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 关联植物 id
	 */
	public M setPlantId(java.lang.Integer plantId) {
		set("plant_id", plantId);
		return (M)this;
	}
	
	/**
	 * 关联植物 id
	 */
	@JBoltField(name="plantId" ,columnName="plant_id",type="Integer", remark="关联植物 id", required=true, maxLength=10, fixed=0, order=2)
	public java.lang.Integer getPlantId() {
		return getInt("plant_id");
	}

	/**
	 * 用户id
	 */
	public M setUserId(java.lang.Integer userId) {
		set("user_id", userId);
		return (M)this;
	}
	
	/**
	 * 用户id
	 */
	@JBoltField(name="userId" ,columnName="user_id",type="Integer", remark="用户id", required=true, maxLength=10, fixed=0, order=3)
	public java.lang.Integer getUserId() {
		return getInt("user_id");
	}

	/**
	 * 上传时间
	 */
	public M setUplodeTime(java.util.Date uplodeTime) {
		set("uplode_time", uplodeTime);
		return (M)this;
	}
	
	/**
	 * 上传时间
	 */
	@JBoltField(name="uplodeTime" ,columnName="uplode_time",type="Date", remark="上传时间", required=false, maxLength=19, fixed=0, order=4)
	public java.util.Date getUplodeTime() {
		return getDate("uplode_time");
	}

	/**
	 * 上传地址
	 */
	public M setUplodeAddress(java.lang.String uplodeAddress) {
		set("uplode_address", uplodeAddress);
		return (M)this;
	}
	
	/**
	 * 上传地址
	 */
	@JBoltField(name="uplodeAddress" ,columnName="uplode_address",type="String", remark="上传地址", required=false, maxLength=255, fixed=0, order=5)
	public java.lang.String getUplodeAddress() {
		return getStr("uplode_address");
	}

	/**
	 * 备注
	 */
	public M setRemark(java.lang.String remark) {
		set("remark", remark);
		return (M)this;
	}
	
	/**
	 * 备注
	 */
	@JBoltField(name="remark" ,columnName="remark",type="String", remark="备注", required=false, maxLength=255, fixed=0, order=6)
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	/**
	 * 关联审核人员
	 */
	public M setReviewerId(java.lang.Integer reviewerId) {
		set("reviewer_id", reviewerId);
		return (M)this;
	}
	
	/**
	 * 关联审核人员
	 */
	@JBoltField(name="reviewerId" ,columnName="reviewer_id",type="Integer", remark="关联审核人员", required=false, maxLength=10, fixed=0, order=7)
	public java.lang.Integer getReviewerId() {
		return getInt("reviewer_id");
	}

	/**
	 * 审核状态  0未审核  1 已经审核  2  退回 默认为0
	 */
	public M setReviewState(java.lang.Integer reviewState) {
		set("review_state", reviewState);
		return (M)this;
	}
	
	/**
	 * 审核状态  0未审核  1 已经审核  2  退回 默认为0
	 */
	@JBoltField(name="reviewState" ,columnName="review_state",type="Integer", remark="审核状态  0未审核  1 已经审核  2  退回 默认为0", required=false, maxLength=10, fixed=0, order=8)
	public java.lang.Integer getReviewState() {
		return getInt("review_state");
	}

	/**
	 * 审核时间
	 */
	public M setReviewTime(java.util.Date reviewTime) {
		set("review_time", reviewTime);
		return (M)this;
	}
	
	/**
	 * 审核时间
	 */
	@JBoltField(name="reviewTime" ,columnName="review_time",type="Date", remark="审核时间", required=false, maxLength=19, fixed=0, order=9)
	public java.util.Date getReviewTime() {
		return getDate("review_time");
	}

	/**
	 * 是否删除 1未删除   0已删除  默认为1
	 */
	public M setIsDel(java.lang.Integer isDel) {
		set("is_del", isDel);
		return (M)this;
	}
	
	/**
	 * 是否删除 1未删除   0已删除  默认为1
	 */
	@JBoltField(name="isDel" ,columnName="is_del",type="Integer", remark="是否删除 1未删除   0已删除  默认为1", required=false, maxLength=10, fixed=0, order=10)
	public java.lang.Integer getIsDel() {
		return getInt("is_del");
	}

}

