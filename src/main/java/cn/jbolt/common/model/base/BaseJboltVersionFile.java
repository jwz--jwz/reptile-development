package cn.jbolt.common.model.base;
import cn.jbolt.base.JBoltBaseModel;
import cn.jbolt.common.gen.JBoltField;

/**
 * JBolt下载版本管理
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseJboltVersionFile<M extends BaseJboltVersionFile<M>> extends JBoltBaseModel<M>{

	/**
	 * 主键ID
	 */
	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * 主键ID
	 */
	@JBoltField(name="id" ,columnName="id",type="Integer", remark="主键ID", required=true, maxLength=10, fixed=0, order=1)
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public M setUrl(java.lang.String url) {
		set("url", url);
		return (M)this;
	}
	
	@JBoltField(name="url" ,columnName="url",type="String", remark="URL", required=false, maxLength=255, fixed=0, order=2)
	public java.lang.String getUrl() {
		return getStr("url");
	}

	/**
	 * jbolt版本ID
	 */
	public M setJboltVersionId(java.lang.Integer jboltVersionId) {
		set("jbolt_version_id", jboltVersionId);
		return (M)this;
	}
	
	/**
	 * jbolt版本ID
	 */
	@JBoltField(name="jboltVersionId" ,columnName="jbolt_version_id",type="Integer", remark="jbolt版本ID", required=false, maxLength=10, fixed=0, order=3)
	public java.lang.Integer getJboltVersionId() {
		return getInt("jbolt_version_id");
	}

}

