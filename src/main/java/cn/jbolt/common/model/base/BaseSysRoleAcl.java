package cn.jbolt.common.model.base;
import cn.jbolt.base.JBoltBaseModel;
import cn.jbolt.common.gen.JBoltField;

/**
 * 角色用户表
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseSysRoleAcl<M extends BaseSysRoleAcl<M>> extends JBoltBaseModel<M>{

	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	@JBoltField(name="id" ,columnName="id",type="Integer", remark="ID", required=true, maxLength=10, fixed=0, order=1)
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public M setRoleId(java.lang.Integer roleId) {
		set("role_id", roleId);
		return (M)this;
	}
	
	@JBoltField(name="roleId" ,columnName="role_id",type="Integer", remark="ROLEID", required=true, maxLength=10, fixed=0, order=2)
	public java.lang.Integer getRoleId() {
		return getInt("role_id");
	}

	public M setAclId(java.lang.Integer aclId) {
		set("acl_id", aclId);
		return (M)this;
	}
	
	@JBoltField(name="aclId" ,columnName="acl_id",type="Integer", remark="ACLID", required=true, maxLength=10, fixed=0, order=3)
	public java.lang.Integer getAclId() {
		return getInt("acl_id");
	}

	public M setOperator(java.lang.String operator) {
		set("operator", operator);
		return (M)this;
	}
	
	@JBoltField(name="operator" ,columnName="operator",type="String", remark="OPERATOR", required=true, maxLength=20, fixed=0, order=4)
	public java.lang.String getOperator() {
		return getStr("operator");
	}

	public M setOperatorTime(java.util.Date operatorTime) {
		set("operator_time", operatorTime);
		return (M)this;
	}
	
	@JBoltField(name="operatorTime" ,columnName="operator_time",type="Date", remark="OPERATORTIME", required=true, maxLength=19, fixed=0, order=5)
	public java.util.Date getOperatorTime() {
		return getDate("operator_time");
	}

	public M setOperatorIp(java.lang.String operatorIp) {
		set("operator_ip", operatorIp);
		return (M)this;
	}
	
	@JBoltField(name="operatorIp" ,columnName="operator_ip",type="String", remark="OPERATORIP", required=true, maxLength=200, fixed=0, order=6)
	public java.lang.String getOperatorIp() {
		return getStr("operator_ip");
	}

}

