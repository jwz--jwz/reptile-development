package cn.jbolt.common.model.base;
import cn.jbolt.base.JBoltBaseModel;
import cn.jbolt.common.gen.JBoltField;

/**
 * 在线用户
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseOnlineUser<M extends BaseOnlineUser<M>> extends JBoltBaseModel<M>{

	/**
	 * 主键ID
	 */
	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * 主键ID
	 */
	@JBoltField(name="id" ,columnName="id",type="Integer", remark="主键ID", required=true, maxLength=10, fixed=0, order=1)
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 会话ID
	 */
	public M setSessionId(java.lang.String sessionId) {
		set("session_id", sessionId);
		return (M)this;
	}
	
	/**
	 * 会话ID
	 */
	@JBoltField(name="sessionId" ,columnName="session_id",type="String", remark="会话ID", required=true, maxLength=255, fixed=0, order=2)
	public java.lang.String getSessionId() {
		return getStr("session_id");
	}

	/**
	 * 用户ID
	 */
	public M setUserId(java.lang.Integer userId) {
		set("user_id", userId);
		return (M)this;
	}
	
	/**
	 * 用户ID
	 */
	@JBoltField(name="userId" ,columnName="user_id",type="Integer", remark="用户ID", required=true, maxLength=10, fixed=0, order=3)
	public java.lang.Integer getUserId() {
		return getInt("user_id");
	}

	/**
	 * 登录日志ID
	 */
	public M setLoginLogId(java.lang.Integer loginLogId) {
		set("login_log_id", loginLogId);
		return (M)this;
	}
	
	/**
	 * 登录日志ID
	 */
	@JBoltField(name="loginLogId" ,columnName="login_log_id",type="Integer", remark="登录日志ID", required=true, maxLength=10, fixed=0, order=4)
	public java.lang.Integer getLoginLogId() {
		return getInt("login_log_id");
	}

	/**
	 * 登录时间
	 */
	public M setLoginTime(java.util.Date loginTime) {
		set("login_time", loginTime);
		return (M)this;
	}
	
	/**
	 * 登录时间
	 */
	@JBoltField(name="loginTime" ,columnName="login_time",type="Date", remark="登录时间", required=false, maxLength=19, fixed=0, order=5)
	public java.util.Date getLoginTime() {
		return getDate("login_time");
	}

	/**
	 * 更新时间
	 */
	public M setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
		return (M)this;
	}
	
	/**
	 * 更新时间
	 */
	@JBoltField(name="updateTime" ,columnName="update_time",type="Date", remark="更新时间", required=false, maxLength=19, fixed=0, order=6)
	public java.util.Date getUpdateTime() {
		return getDate("update_time");
	}

	/**
	 * 过期时间
	 */
	public M setExpirationTime(java.util.Date expirationTime) {
		set("expiration_time", expirationTime);
		return (M)this;
	}
	
	/**
	 * 过期时间
	 */
	@JBoltField(name="expirationTime" ,columnName="expiration_time",type="Date", remark="过期时间", required=true, maxLength=19, fixed=0, order=7)
	public java.util.Date getExpirationTime() {
		return getDate("expiration_time");
	}

	/**
	 * 是否锁屏
	 */
	public M setScreenLocked(java.lang.Boolean screenLocked) {
		set("screen_locked", screenLocked);
		return (M)this;
	}
	
	/**
	 * 是否锁屏
	 */
	@JBoltField(name="screenLocked" ,columnName="screen_locked",type="Boolean", remark="是否锁屏", required=true, maxLength=1, fixed=0, order=8)
	public java.lang.Boolean getScreenLocked() {
		return getBoolean("screen_locked");
	}

	/**
	 * 在线状态
	 */
	public M setOnlineState(java.lang.Integer onlineState) {
		set("online_state", onlineState);
		return (M)this;
	}
	
	/**
	 * 在线状态
	 */
	@JBoltField(name="onlineState" ,columnName="online_state",type="Integer", remark="在线状态", required=true, maxLength=10, fixed=0, order=9)
	public java.lang.Integer getOnlineState() {
		return getInt("online_state");
	}

	/**
	 * 离线时间
	 */
	public M setOfflineTime(java.util.Date offlineTime) {
		set("offline_time", offlineTime);
		return (M)this;
	}
	
	/**
	 * 离线时间
	 */
	@JBoltField(name="offlineTime" ,columnName="offline_time",type="Date", remark="离线时间", required=false, maxLength=19, fixed=0, order=10)
	public java.util.Date getOfflineTime() {
		return getDate("offline_time");
	}

}

