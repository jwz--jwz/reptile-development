package cn.jbolt.common.model.base;
import cn.jbolt.base.JBoltBaseModel;
import cn.jbolt.common.gen.JBoltField;

/**
 * 内部私信
 * Generated by JBolt, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BasePrivateMessage<M extends BasePrivateMessage<M>> extends JBoltBaseModel<M>{

	/**
	 * 主键ID
	 */
	public M setId(java.lang.Integer id) {
		set("id", id);
		return (M)this;
	}
	
	/**
	 * 主键ID
	 */
	@JBoltField(name="id" ,columnName="id",type="Integer", remark="主键ID", required=true, maxLength=10, fixed=0, order=1)
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 私信内容
	 */
	public M setContent(java.lang.String content) {
		set("content", content);
		return (M)this;
	}
	
	/**
	 * 私信内容
	 */
	@JBoltField(name="content" ,columnName="content",type="String", remark="私信内容", required=true, maxLength=65535, fixed=0, order=2)
	public java.lang.String getContent() {
		return getStr("content");
	}

	/**
	 * 发送时间
	 */
	public M setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
		return (M)this;
	}
	
	/**
	 * 发送时间
	 */
	@JBoltField(name="createTime" ,columnName="create_time",type="Date", remark="发送时间", required=true, maxLength=19, fixed=0, order=3)
	public java.util.Date getCreateTime() {
		return getDate("create_time");
	}

	/**
	 * 发信人
	 */
	public M setFromUserId(java.lang.Integer fromUserId) {
		set("from_user_id", fromUserId);
		return (M)this;
	}
	
	/**
	 * 发信人
	 */
	@JBoltField(name="fromUserId" ,columnName="from_user_id",type="Integer", remark="发信人", required=true, maxLength=10, fixed=0, order=4)
	public java.lang.Integer getFromUserId() {
		return getInt("from_user_id");
	}

	/**
	 * 收信人
	 */
	public M setToUserId(java.lang.Integer toUserId) {
		set("to_user_id", toUserId);
		return (M)this;
	}
	
	/**
	 * 收信人
	 */
	@JBoltField(name="toUserId" ,columnName="to_user_id",type="Integer", remark="收信人", required=true, maxLength=10, fixed=0, order=5)
	public java.lang.Integer getToUserId() {
		return getInt("to_user_id");
	}

}

