package cn.jbolt.common.model;

import cn.jbolt.common.model.base.BasePrivateMessage;
import cn.jbolt.common.config.TableBind;
import cn.jbolt.base.JBoltIDGenMode;

/**
 * 内部私信
 * Generated by JBolt.
 */
@SuppressWarnings("serial")
@TableBind(dataSource = "main" , table = "jb_private_message" , primaryKey = "id" , idGenMode = JBoltIDGenMode.AUTO)
public class PrivateMessage extends BasePrivateMessage<PrivateMessage> {

}
