package cn.jbolt.common.config;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import cn.jbolt.base.JBoltConst;
import cn.jbolt.base.JBoltIDGenMode;

@Retention(RUNTIME)
@Target({ TYPE })
public @interface TableBind {
	/**
	 * 数据源
	 * @return
	 */
	String dataSource() default "main";

	/**
	 * 绑定表
	 * @return
	 */
	String table();
	
	/**
	 * 主键
	 * @return
	 */
	String primaryKey() default JBoltConst.DEFAULT_ID_NAME;
	
	/**
	 * 主键生成策略 默认雪花 可以 auto|snowflake|sequence
	 * @return
	 */
	String idGenMode() default JBoltIDGenMode.AUTO;
	
	/**
	 * 主键生成策略设置为sequence的时候 对应的序列名称
	 * @return
	 */
	String idSequence() default JBoltConst.JBOLT_ID_SEQUENCE_DEFAULT;
}
