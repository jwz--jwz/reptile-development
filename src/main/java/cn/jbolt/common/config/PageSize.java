package cn.jbolt.common.config;

/**
 * 配置默认常用的PageSize
 * @ClassName:  PageSize   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年1月23日   
 */
public interface PageSize {
	int PAGESIZE_ADMIN_LIST=10;
	int PAGESIZE_ADMIN_LIST_15=15;
	int PAGESIZE_ADMIN_LIST_20=20;
	int PAGESIZE_ADMIN_LIST_25=25;
	int PAGESIZE_ADMIN_LIST_30=30;
	int PAGESIZE_ADMIN_LIST_35=35;
	int PAGESIZE_ADMIN_LIST_40=40;
	int PAGESIZE_ADMIN_LIST_45=45;
	int PAGESIZE_ADMIN_LIST_50=50;
	
	int PAGESIZE_API_LIST_5=5;
	int PAGESIZE_API_LIST_6=6;
	int PAGESIZE_API_LIST_10=10;
}
