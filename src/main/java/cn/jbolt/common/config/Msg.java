package cn.jbolt.common.config;
/**   
 * 常用返回信息工具类
 * @ClassName:  Msg   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年3月26日 下午2:14:29   
 */
public interface Msg {
	String PARAM_ERROR="参数异常";
	String SUCCESS="操作成功";
	String FAIL="操作失败";
	String NOPERMISSION="无权访问";
	String FAIL_NEED_CATEGORY="添加商品前需指定一个商品类目";
	String FAIL_INIT_GOODS="新商品信息初始化失败";
	String TABLE_NOT_EXIST="数据表不存在，分表尚未初始化";
	String DATA_NOT_EXIST="数据记录不存在";
	String DATA_SAME_NAME_EXIST="存在重名数据，请更换";
	String DATA_NAME_SN_COUNT_NOT_EQUALS="名称和编号个数不匹配";
	String DATA_SAME_SN_EXIST="存在重复编码数据，请更换";
	String DATA_INUSE="已经被关联使用";
	String DEMO_MODE_CAN_NOT_DELETE="Demo演示模式下，不能删除此数据";
	String DEMO_MODE_LIMIT_MSG="Demo演示模式下，不能执行此操作";
	String DATA_IMPORT_FAIL="数据导入异常";
	String DATA_IMPORT_FAIL_EMPTY="数据导入异常，导入文件无数据";
	String NOT_ALLOWED_JBOLT_API="非法Api调用请求";
	String ADMIN_NO_LOGIN_INFO="尚未登录,请<a data-dialogbtn class='text-danger' data-title='请重新登录系统'  data-handler='refreshPjaxContainer' data-area='500,400' data-url='admin/relogin'>点此</a>重新登录";
	String ADMIN_TERMINAL_OFFLINE_INFO="当前用户已在其它终端登录，本机已下线,可<a data-dialogbtn class='text-danger' data-title='请重新登录系统'  data-handler='refreshPjaxContainer' data-area='500,400' data-url='admin/relogin'>点此</a>重新登录";
	String ADMIN_TERMINAL_OFFLINE_PAGE_MSG="当前用户已在其它终端登录，本机已下线，可<a class='text-danger' href='admin/login'>点此</a>重新登录";
	String ADMIN_FORCED_OFFLINE_PAGE_MSG="当前用户被平台强退下线，如有疑问，请联系平台管理";
	String ADMIN_AJAXPORTAL_NO_LOGIN_INFO="尚未登录,请<a data-dialogbtn class='text-danger' data-title='请重新登录系统'  data-handler='refreshCurrentAjaxPortal' data-area='500,400' data-url='admin/relogin'>点此</a>重新登录";
	String ADMIN_AJAXPORTAL_TERMINAL_OFFLINE_INFO="当前用户已在其它终端登录，本机已下线，可<a data-dialogbtn class='text-danger' data-title='请重新登录系统'  data-handler='refreshCurrentAjaxPortal' data-area='500,400' data-url='admin/relogin'>点此</a>重新登录";
	String ERROR_404_NOTFOUND="404,您访问的资源不存在!";
	String JBOLT_SYSTEM_LOCKED="jbolt_system_locked";
	String EXIST_SON = "存在下级数据";
	String DEPT_USER_USE = "此部门已经在用户中绑定使用";
	String DEPT_SAME_FULLNAME_EXIST="同级存在部门全称重复数据，请更换";
	String DEPT_SAME_SN_EXIST = "存在重复部门代码，请更换";
	String PARENT_CAN_NOT_SAME_SELF = "上级不能是自身，请换一个";
	String JBOLTTABLE_IS_BLANK = "表格提交数据不能为空";
	
	/****拦截器里用*****/
	String INTERCEPTOR_MUST_EXTEND_JBOLT_BASE_CONTROLLER="控制器需要继承 JBoltBaseController";
	String INTERCEPTOR_NO_PERMISSIONKEY="无权访问,当前访问资源尚未设置任何可校验权限";
	String INTERCEPTOR_NO_AUTH_ASSIGN="无权访问，当前用户所属角色下尚未分配任何权限";
	String INTERCEPTOR_CHECK_NO_AUTH="无权访问";
}
