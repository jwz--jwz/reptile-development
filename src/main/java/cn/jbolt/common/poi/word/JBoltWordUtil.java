package cn.jbolt.common.poi.word;

import java.io.File;

import com.jfinal.template.Engine;
import com.jfinal.template.Template;
import com.jfinal.template.ext.directive.NowDirective;

import cn.hutool.core.io.FileUtil;
import cn.jbolt.common.directive.BooleanToStrDirective;
import cn.jbolt.common.directive.DateTimeDirective;
import cn.jbolt.common.directive.EnableDirective;
import cn.jbolt.common.directive.ImageUrlToBase64Directive;
import cn.jbolt.common.directive.SexDirective;

/**
 * JBoltWord工具类
 * @ClassName:  JBoltWordUtil   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月14日   
 */
public class JBoltWordUtil {
	private static Engine engine;
	static {
		engine=Engine.create("jboltwordtpl");
		engine.setDevMode(true);
		engine.addDirective("now", NowDirective.class, true);
		engine.addDirective("imgUrlToBase64", ImageUrlToBase64Directive.class);
		engine.addDirective("sex", SexDirective.class);
		engine.addDirective("boolToStr", BooleanToStrDirective.class);	
		engine.addDirective("enable", EnableDirective.class);	
		engine.addDirective("datetime", DateTimeDirective.class);
	}
	/**
	 * 获取byte[]数据
	 * @param jBoltWord
	 * @return
	 */
	public static byte[] getWordBytes(JBoltWord jBoltWord) {
		String xmlTplContent=FileUtil.readUtf8String(jBoltWord.getTplFile());
		Template tpl=engine.setEncoding("utf-8").getTemplateByString(xmlTplContent);
		String result = tpl.renderToString(jBoltWord.getDatas());
		return result.getBytes();
	}
	
	/**
	 * 保存到文件
	 * @param jBoltWord
	 * @param saveFilePath
	 * @return
	 */
	public static File saveFile(JBoltWord jBoltWord, String saveFilePath) {
		return FileUtil.writeBytes(jBoltWord.toByteArray(), saveFilePath);
	}
}
