package cn.jbolt.common.poi.excel;
import java.util.List;
/**
 * 直接读取到Excel的数据 用这个装起来
 * @ClassName:  JBoltExcelDataMap   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年12月2日   
 */
public class JBoltExcelDataMapper {
	private List<List<Object>> datas;
	private JBoltExcelDataMapper(List<List<Object>> datas) {
		this.datas=datas;
	}
	public static JBoltExcelDataMapper create(List<List<Object>> datas) {
		return new JBoltExcelDataMapper(datas);
	}
	/**
	 * 获取一行数据
	 * @param row
	 * @return
	 */
	public List<Object> getRowData(int row) {
		boolean hasDatas=hasDatas();
		return hasDatas?datas.get(row-1):null;
	}
	/**
	 * 获取指定单元格数据 泛型强转
	 * @param <T>
	 * @param row 行 从1开始
	 * @param col 列 从1开始
	 * @return
	 */
	public <T> T getAs(int row,int col) {
		Object value = get(row, col);
		return value==null?null:(T)value;
	}
	
	/**
	 * 获取指定单元格数据 Object
	 * @param row 行 从1开始
	 * @param col 列 从1开始
	 * @return
	 */
	public Object get(int row,int col) {
		List<Object> datas= getRowData(row);
		if(datas==null||datas.size()==0) {
			return null;
		}
		return datas.get(col-1);
	}
	
	/**
	 * 获取指定单元格数据 转String类型
	 * @param row 行 从1开始
	 * @param col 列 从1开始
	 * @return
	 */
	public String getStr(int row,int col) {
		Object value = get(row, col);
		return value==null?null:value.toString();
	}
	
	/**
	 * 获取指定单元格数据 转Boolean类型
	 * @param row 行 从1开始
	 * @param col 列 从1开始
	 * @return
	 */
	public Boolean getBoolean(int row,int col) {
		return getAs(row, col);
	}
	
	/**
	 * 获取指定单元格数据 转Integer类型
	 * @param row 行 从1开始
	 * @param col 列 从1开始
	 * @return
	 */
	public Integer getInt(int row,int col) {
		Number n = getNumber(row, col);
		return n != null ? n.intValue() : null;
	}
	
	/**
	 * 获取指定单元格数据 转Long类型
	 * @param row 行 从1开始
	 * @param col 列 从1开始
	 * @return
	 */
	public Long getLong(int row,int col) {
		Number n = getNumber(row, col);
		return n != null ? n.longValue() : null;
	}
	
	/**
	 * 获取指定单元格数据 转Number类型
	 * @param row 行 从1开始
	 * @param col 列 从1开始
	 * @return
	 */
	public Number getNumber(int row,int col) {
		return getAs(row, col);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如E5或者E:5 
	 * Object类型
	 * @param position 坐标 例E5 或 E:5
	 * @return
	 */
	public Object get(String position) {
		 int[] pos=JBoltExcelUtil.positionStrSplit(position);
		 return get(pos[0], pos[1]);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如E5或者E:5 
	 * Integer类型
	 * @param position 坐标 例E5 或 E:5
	 * @return
	 */
	public Integer getInt(String position) {
		int[] pos=JBoltExcelUtil.positionStrSplit(position);
		return getInt(pos[0], pos[1]);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如E5或者E:5 
	 * Long类型
	 * @param position 坐标 例E5 或 E:5
	 * @return
	 */
	public Long getLong(String position) {
		int[] pos=JBoltExcelUtil.positionStrSplit(position);
		return getLong(pos[0], pos[1]);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如E5或者E:5 
	 * Long类型
	 * @param position 坐标 例E5 或 E:5
	 * @return
	 */
	public String getStr(String position) {
		int[] pos=JBoltExcelUtil.positionStrSplit(position);
		return getStr(pos[0], pos[1]);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如E5或者E:5 
	 * Number类型
	 * @param position 坐标 例E5 或 E:5
	 * @return
	 */
	public Number getNumber(String position) {
		int[] pos=JBoltExcelUtil.positionStrSplit(position);
		return getNumber(pos[0], pos[1]);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如E5或者E:5 
	 * Boolean类型
	 * @param position 坐标 例E5 或 E:5
	 * @return
	 */
	public Boolean getBoolean(String position) {
		int[] pos=JBoltExcelUtil.positionStrSplit(position);
		return getBoolean(pos[0], pos[1]);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如E5或者E:5 
	 * Long类型
	 * @param position 坐标 例E5 或 E:5
	 * @return
	 */
	public <T> T getAs(String position) {
		 int[] pos=JBoltExcelUtil.positionStrSplit(position);
		 return getAs(pos[0], pos[1]);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如getAs("E",5)
	 * Long类型
	 * @param colStr 列 从A开始
	 * @param row 行 从1开始
	 * @return
	 */
	public <T> T getAs(String colStr,int row) {
		 int col=JBoltExcelUtil.colStrToNum(colStr);
		 return getAs(row, col);
	}
	
	/**
	 * 根据excel自有坐标获取值 例如get("E",5)
	 * Object类型
	 * @param colStr 列 从A开始
	 * @param row 行 从1开始
	 * @return
	 */
	public Object get(String colStr, int row) {
		 return get(row, JBoltExcelUtil.colStrToNum(colStr));
	}
	
	/**
	 * 根据excel自有坐标获取值 例如getInt("E",5) 
	 * Integer类型
	 * @param colStr 列 从A开始
	 * @param row 行 从1开始
	 * @return
	 */
	public Integer getInt(String colStr,int row) {
		return getInt(row, JBoltExcelUtil.colStrToNum(colStr));
	}
	
	/**
	 * 根据excel自有坐标获取值 例如getLong("E",5) 
	 * Long类型
	 * @param colStr 列 从A开始
	 * @param row 行 从1开始
	 * @return
	 */
	public Long getLong(String colStr, int row) {
		return getLong(row, JBoltExcelUtil.colStrToNum(colStr));
	}
	
	/**
	 * 根据excel自有坐标获取值 例如getStr("E",5)  
	 * Long类型
	 * @param colStr 列 从A开始
	 * @param row 行 从1开始
	 * @return
	 */
	public String getStr(String colStr, int row) {
		return getStr(row, JBoltExcelUtil.colStrToNum(colStr));
	}
	
	/**
	 * 根据excel自有坐标获取值 例如getNumber("E",5) 
	 * Number类型
	 * @param colStr 列 从A开始
	 * @param row 行 从1开始
	 * @return
	 */
	public Number getNumber(String colStr, int row) {
		return getNumber(row, JBoltExcelUtil.colStrToNum(colStr));
	}
	
	/**
	 * 根据excel自有坐标获取值 例如getBoolean("E",5) 
	 * Boolean类型
	 * @param colStr 列 从A开始
	 * @param row 行 从1开始
	 * @return
	 */
	public Boolean getBoolean(String colStr,int row) {
		return getBoolean(row, JBoltExcelUtil.colStrToNum(colStr));
	}
	public boolean hasDatas() {
		return datas!=null&&datas.size()>0;
	}
	
	public List<List<Object>> getDatas() {
		return datas;
	}
	public void setDatas(List<List<Object>> datas) {
		this.datas = datas;
	}
}
