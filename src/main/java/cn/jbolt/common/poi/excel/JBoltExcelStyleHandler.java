package cn.jbolt.common.poi.excel;

import cn.hutool.poi.excel.ExcelWriter;

/**
 * excel样式处理器
 * @ClassName:  JBoltExcelDataChangeHandler   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年3月13日   
 */
public interface JBoltExcelStyleHandler {
	public void process(ExcelWriter excelWriter);
}
