package cn.jbolt.common.poi.excel;
/**
 * jboltexcel中使用模板填充用的数据 根据坐标定位
 * @ClassName:  JBoltExcelTplData   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2020年11月5日   
 */
public class JBoltExcelPositionData {
 private int row;
 private int col;
 private Object value;
 private boolean valueIsFormula;
 private JBoltExcelPositionData(int row,int col,Object value) {
	 this(row, col, value, false);
 }
 private JBoltExcelPositionData(int row,int col,Object value,boolean valueIsFormula) {
	 this.row=row;
	 this.col=col;
	 this.value=value;
	 this.valueIsFormula=valueIsFormula;
 }
 private JBoltExcelPositionData(int row,String colStr,Object value) {
	 this(row, colStr, value,false);
 }
 private JBoltExcelPositionData(int row,String colStr,Object value,boolean valueIsFormula) {
	 this(row, JBoltExcelUtil.colStrToNum(colStr), value,valueIsFormula);
 }
 private JBoltExcelPositionData(String colStr,int row,Object value) {
	 this(colStr, row, value,false);
 }
 private JBoltExcelPositionData(String colStr,int row,Object value,boolean valueIsFormula) {
	 this(row, JBoltExcelUtil.colStrToNum(colStr), value,valueIsFormula);
 }
 private JBoltExcelPositionData(String position,Object value) {
	 this(position, value, false);
 }
 private JBoltExcelPositionData(String position,Object value,boolean valueIsFormula) {
	 int[] pos=JBoltExcelUtil.positionStrSplit(position);
	 this.row=pos[0];
	 this.col=pos[1];
	 this.value=value;
	 this.valueIsFormula=valueIsFormula;
 }
 /**
  * 通过row和col定位单元格设置数据 从1开始
  * @param row 行 从1开始
  * @param col 列 从1开始
  * @param value
  * @return
  */
 public static JBoltExcelPositionData create(int row, int col, Object value) {
	 return new JBoltExcelPositionData(row, col, value);
 }
 /**
  * 通过row和col定位单元格设置数据 从1开始
  * @param row 行 从1开始
  * @param col 列 从1开始
  * @param value
  * @param valueIsFormula
  * @return
  */
 public static JBoltExcelPositionData create(int row, int col, Object value,boolean valueIsFormula) {
	 return new JBoltExcelPositionData(row, col, value,valueIsFormula);
 }
 /**
  * 定位列用字符串 例如 create(5,"E","值")
  * @param row 行 从1开始
  * @param colStr 列字符串
  * @param value
  * @return
  */
 public static JBoltExcelPositionData create(int row, String colStr, Object value) {
	 return new JBoltExcelPositionData(row, colStr, value);
 }
 /**
  * 定位列用字符串 例如 create(5,"E","值")
  * @param row 行 从1开始
  * @param colStr 列字符串
  * @param value
  * @param valueIsFormula
  * @return
  */
 public static JBoltExcelPositionData create(int row, String colStr, Object value,boolean valueIsFormula) {
	 return new JBoltExcelPositionData(row, colStr, value,valueIsFormula);
 }
 /**
  * 定位列用字符串 例如 create("E",5,"值")
  * @param colStr 列字符串
  * @param row 行 从1开始
  * @param value
  * @return
  */
 public static JBoltExcelPositionData create(String colStr, int row, Object value) {
	 return new JBoltExcelPositionData(colStr, row, value);
 }
 /**
  * 定位列用字符串 例如 create("E",5,"值")
  * @param colStr 列字符串
  * @param row 行 从1开始
  * @param value
  * @param valueIsFormula
  * @return
  */
 public static JBoltExcelPositionData create(String colStr, int row, Object value,boolean valueIsFormula) {
	 return new JBoltExcelPositionData(colStr, row, value,valueIsFormula);
 }
 /**
  * 定位字符串 例 E5或者E:5 create("E5","值")或者create("E:5","值")
  * @param position 定位字符串 E5或者E:5
  * @param value
  * @return
  */
 public static JBoltExcelPositionData create(String position, Object value) {
	 return new JBoltExcelPositionData(position, value);
 }
 /**
  * 定位字符串 例 E5或者E:5 create("E5","值")或者create("E:5","值")
  * @param position 定位字符串 E5或者E:5
  * @param value
  * @param valueIsFormula
  * @return
  */
 public static JBoltExcelPositionData create(String position, Object value,boolean valueIsFormula) {
	 return new JBoltExcelPositionData(position, value,valueIsFormula);
 }
public int getRow() {
	return row;
}
public void setRow(int row) {
	this.row = row;
}
public Object getValue() {
	return value;
}
public void setValue(Object value) {
	this.value = value;
}
public int getCol() {
	return col;
}
public void setCol(int col) {
	this.col = col;
}
public boolean isValueIsFormula() {
	return valueIsFormula;
}
public void setValueIsFormula(boolean valueIsFormula) {
	this.valueIsFormula = valueIsFormula;
}
}
