package cn.jbolt.common.poi.excel;

/**
 * JBoltExcel中设置模板里原有数据位置转移使用配置
 * @ClassName:  JBoltExcelPositionChange   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年2月26日   
 */
public class JBoltExcelPositionChange {
	/**
	 * 移动区域首行
	 */
	private int originFirstRow;
	/**
	 * 移动区域尾行
	 */
	private int originLastRow;
	/**
	 * 移动区域首列
	 */
	private int originFirstCol;
	/**
	 * 移动区域尾列
	 */
	private int originLastCol;
	/**
	 * 移动区域行加数
	 */
	private int rowIncrease;
	/**
	 * 移动区域列加数
	 */
	private int colIncrease;
	/**
	 * 构造函数
	 * @param originFirstRow
	 * @param originLastRow
	 * @param originFirstCol
	 * @param originLastCol
	 * @param rowIncrease
	 * @param colIncrease
	 */
	private JBoltExcelPositionChange(int originFirstRow,int originLastRow,int originFirstCol,int originLastCol,int rowIncrease,int colIncrease) {
		this.originFirstRow = originFirstRow;
		this.originLastRow = originLastRow;
		this.originFirstCol = originFirstCol;
		this.originLastCol = originLastCol;
		this.rowIncrease = rowIncrease;
		this.colIncrease = colIncrease;
	}
	
	/**
	 * 构造函数
	 * @param originFirstRow
	 * @param originLastRow
	 * @param originFirstCol
	 * @param originLastCol
	 */
	private JBoltExcelPositionChange(int originFirstRow,int originLastRow,int originFirstCol,int originLastCol) {
		this(originFirstRow, originLastRow, originFirstCol, originLastCol, 0, 0);
	}
	
	/**
	 * 创建待变更区域
	 * @param originFirstRow
	 * @param originLastRow
	 * @param originFirstCol
	 * @param originLastCol
	 * @return
	 */
	public static JBoltExcelPositionChange create(int originFirstRow,int originLastRow,int originFirstCol,int originLastCol) {
		return  new JBoltExcelPositionChange(originFirstRow, originLastRow, originFirstCol, originLastCol);
	}
	
	/**
	 * 创建待变更区域
	 * @param originFirstRow
	 * @param originLastRow
	 * @param originFirstCol
	 * @param originLastCol
	 * @param rowIncrease
	 * @param colIncrease
	 * @return
	 */
	public static JBoltExcelPositionChange create(int originFirstRow,int originLastRow,int originFirstCol,int originLastCol,int rowIncrease,int colIncrease) {
		return  new JBoltExcelPositionChange(originFirstRow, originLastRow, originFirstCol, originLastCol,rowIncrease,colIncrease);
	}
	/**
	 * 创建待变更区域
	 * @param originFirstRow
	 * @param originLastRow
	 * @param originFirstCol
	 * @param originLastCol
	 * @param rowIncrease
	 * @return
	 */
	public static JBoltExcelPositionChange create(int originFirstRow,int originLastRow,int originFirstCol,int originLastCol,int rowIncrease) {
		return  new JBoltExcelPositionChange(originFirstRow, originLastRow, originFirstCol, originLastCol,rowIncrease,0);
	}
	
	/**
	 * 判断是否需要变更
	 * @return
	 */
	public boolean isNeedChange() {
		return rowIncrease>0||colIncrease>0;
	}
	
	/**
	 * 行增加指定值
	 * @param rowIncrease
	 * @return
	 */
	public JBoltExcelPositionChange increaseRow(int rowIncrease) {
		this.rowIncrease += rowIncrease;
		return this;
	}
	/**
	 * 行增加指定值
	 * @param rowIncrease
	 * @return
	 */
	public JBoltExcelPositionChange increaseCol(int colIncrease) {
		this.rowIncrease += rowIncrease;
		return this;
	}
	public int getOriginFirstRow() {
		return originFirstRow;
	}
	public void setOriginFirstRow(int originFirstRow) {
		this.originFirstRow = originFirstRow;
	}
	public int getOriginLastRow() {
		return originLastRow;
	}
	public void setOriginLastRow(int originLastRow) {
		this.originLastRow = originLastRow;
	}
	public int getOriginFirstCol() {
		return originFirstCol;
	}
	public void setOriginFirstCol(int originFirstCol) {
		this.originFirstCol = originFirstCol;
	}
	public int getOriginLastCol() {
		return originLastCol;
	}
	public void setOriginLastCol(int originLastCol) {
		this.originLastCol = originLastCol;
	}
	public int getRowIncrease() {
		return rowIncrease;
	}
	public void setRowIncrease(int rowIncrease) {
		this.rowIncrease = rowIncrease;
	}
	public int getColIncrease() {
		return colIncrease;
	}
	public void setColIncrease(int colIncrease) {
		this.colIncrease = colIncrease;
	}
}
