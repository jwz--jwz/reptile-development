package cn.jbolt.admin.wechat.autoreply;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Ret;
import com.jfinal.upload.UploadFile;

import cn.jbolt._admin.permission.UnCheck;
import cn.jbolt.admin.wechat.mpinfo.WechatMpinfoService;
import cn.jbolt.base.JBoltBaseController;
import cn.jbolt.base.JBoltUserKit;
import cn.jbolt.common.bean.Option;
import cn.jbolt.common.bean.OptionBean;
import cn.jbolt.common.config.JBoltUploadFolder;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.WechatAutoreply;
import cn.jbolt.common.model.WechatMpinfo;
import cn.jbolt.common.model.WechatReplyContent;

/**
 * 微信公众平台自动回复内容设置管理
 * 
 * @ClassName: WechatAutoReplyAdminController
 * @author: JFinal学院-小木 QQ：909854136
 * @date: 2019年6月20日
 */
public class WechatReplyContentAdminController extends JBoltBaseController {
	@Inject
	private WechatAutoReplyService wechatAutoReplyService;
	@Inject
	private WechatReplyContentService service;
	@Inject
	private WechatMpinfoService wechatMpinfoService;
	
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void index() {
		Integer autoReplyId=getInt(0);
		Ret ret=service.checkPermission(JBoltUserKit.getUserIdAs(),autoReplyId);
		if(ret.isFail()) {renderDialogFailRet(ret);return;}
		set("datas", service.getListByAutoReplyId(autoReplyId));
		set("autoReplyId", autoReplyId);
		render("index.html");
	}
	


	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void add() {
		Integer autoReplyId=getInt(0);
		Ret ret=service.checkPermission(JBoltUserKit.getUserIdAs(),autoReplyId);
		if(ret.isFail()) {renderDialogFail(ret.getStr("msg"));return;}
		
		WechatAutoreply wechatAutoreply=ret.getAs("data");
		Integer mpId=wechatAutoreply.getMpId();
		WechatMpinfo wechatMpinfo=wechatMpinfoService.findById(mpId);
		if(wechatMpinfo==null) {renderDialogFail("关联微信公众平台不存在");return;}
		
		set("mpId", mpId);
		set("autoReplyId", autoReplyId);
		render("add.html");
	}
	
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void edit() {
		Integer autoReplyId=getInt(0);
		Integer id=getInt(1);
		//根据TYPE 判断是否有权限
		Ret ret=service.checkPermission(JBoltUserKit.getUserIdAs(),autoReplyId);
		if(ret.isFail()) {renderDialogFail(ret.getStr("msg"));return;}
		WechatAutoreply wechatAutoreply=ret.getAs("data");
		
		Integer mpId=wechatAutoreply.getMpId();
		WechatMpinfo wechatMpinfo=wechatMpinfoService.findById(mpId);
		if(wechatMpinfo==null) {renderDialogFail("关联微信公众平台不存在");return;}
		
		set("mpId", mpId);
		set("autoReplyId", autoReplyId);
		WechatReplyContent wechatReplyContent=service.findById(id);
		if(wechatReplyContent==null) {
			renderDialogFail(Msg.DATA_NOT_EXIST);
			return;
		}
		if(wechatReplyContent.getMpId().intValue()!=wechatAutoreply.getMpId().intValue()) {renderDialogFail("参数异常:公众平台mpId");return;}
		if(wechatReplyContent.getAutoReplyId().intValue()!=autoReplyId.intValue()) {renderDialogFail("参数异常:所属规则 autoReplyId");return;}
		set("wechatReplyContent", wechatReplyContent);
		render("edit.html");
	}
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void up(){
		renderJson(service.up(getInt(0),getInt(1)));
	}
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void down(){
		renderJson(service.down(getInt(0),getInt(1)));
	}
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void initRank(){
		renderJson(service.doInitRank(getInt(0)));
	}
	
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void delete() {
		renderJson(service.delete(getInt(0),getInt(1)));
	}
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void toggleEnable() {
		renderJson(service.toggleEnable(getInt(0),getInt(1)));
	}
	@UnCheck
	public void types() {
		List<Option> options=new ArrayList<Option>();
		options.add(new OptionBean("图文",WechatReplyContent.TYPE_NEWS));
		options.add(new OptionBean("文本",WechatReplyContent.TYPE_TEXT));
		options.add(new OptionBean("视频",WechatReplyContent.TYPE_VIDEO));
		options.add(new OptionBean("图片",WechatReplyContent.TYPE_IMG));
		options.add(new OptionBean("语音",WechatReplyContent.TYPE_VOICE));
		options.add(new OptionBean("音乐",WechatReplyContent.TYPE_MUSIC));
		renderJsonData(options);
	}
	
	/**
	 * 上传公众平台相关图片
	 */
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void uploadImage(){
		Integer autoReplyId=getInt(0);
		Ret ret=service.checkPermission(JBoltUserKit.getUserIdAs(),autoReplyId);
		if(ret.isFail()) {renderDialogFail(ret.getStr("msg"));return;}
		//上传到今天的文件夹下
		String todayFolder=JBoltUploadFolder.todayFolder();
		String uploadPath=JBoltUploadFolder.WECHAT_AUTOREPLY_REPLYCONTENT+"/"+todayFolder;
		UploadFile file=getFile("img",uploadPath);
		if(notImage(file)){
			renderJsonFail("请上传图片类型文件");
			return;
		}
		renderJsonData(JFinal.me().getConstants().getBaseUploadPath()+"/"+uploadPath+"/"+file.getFileName());
	}
	
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void save() {
		renderJson(service.save(getInt(0),getModel(WechatReplyContent.class,"wechatReplyContent")));
	}
	@UnCheck
	@Before(WechatReplyContentMgrValidator.class)
	public void update() {
		renderJson(service.update(getInt(0),getModel(WechatReplyContent.class,"wechatReplyContent")));
	}

}
