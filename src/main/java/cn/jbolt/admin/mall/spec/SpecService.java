package cn.jbolt.admin.mall.spec;

import com.jfinal.kit.Kv;
import com.jfinal.kit.Ret;

import cn.jbolt.base.JBoltBaseService;
import cn.jbolt.common.config.Msg;
import cn.jbolt.common.model.Spec;

/**   
 * 系统商品规格管理Service
 * @ClassName:  GoodsTypeService   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2019年3月26日 下午1:01:46   
 */
public class SpecService extends JBoltBaseService<Spec> {
	private Spec dao = new Spec().dao();
	@Override
	protected Spec dao() {
		return dao;
	}
	/**
	 * 保存
	 * @param Spec
	 * @return
	 */
	public Ret save(Spec Spec) {
		if(Spec==null||isOk(Spec.getId())||notOk(Spec.getName())){
			return fail(Msg.PARAM_ERROR);
		}
		if(existsName(Spec.getName())){
			return fail(Msg.DATA_SAME_NAME_EXIST);
		}
		boolean success=Spec.save();
		if(success){
			//TODO 处理添加日志
		}
		return ret(success);
	}
	
	/**
	 * 修改
	 * @param spec
	 * @return
	 */
	public Ret update(Spec spec) {
		if(spec==null||notOk(spec.getId())||notOk(spec.getName())){
			return fail(Msg.PARAM_ERROR);
		}
		if(existsName(spec.getName(),spec.getId())){
			return fail(Msg.DATA_SAME_NAME_EXIST);
		}
		boolean success=spec.update();
		if(success){
			//TODO 处理添加日志
		}
		return ret(success);
	}
	/**
	 * 修改
	 * @param goodsType
	 * @return
	 */
	public Ret delete(Integer id) {
		Ret ret=deleteById(id, true);
		return ret;
	}
	
	@Override
	protected String afterDelete(Spec spec, Kv kv) {
		return null;
	}
	
	@Override
	public String checkCanDelete(Spec spec,Kv kv) {
		return checkInUse(spec,kv);
	}

	@Override
	public String checkInUse(Spec spec,Kv kv) {
		return null;
	}
	

}
