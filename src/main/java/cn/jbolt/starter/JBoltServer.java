package cn.jbolt.starter;

import com.jfinal.config.JFinalConfig;
import com.jfinal.core.Const;
import com.jfinal.server.undertow.UndertowConfig;
import com.jfinal.server.undertow.UndertowServer;

import cn.jbolt.base.JBoltConst;
import cn.jbolt.common.util.JBoltConsoleUtil;
import io.undertow.Version;
/**
 * JBolt Undertow服务器封装
 * @ClassName:  JBoltServer   
 * @author: JFinal学院-小木 QQ：909854136 
 * @date:   2021年8月7日   
 *    
 */
public class JBoltServer extends UndertowServer {
	protected JBoltServer(UndertowConfig undertowConfig) {
		super(undertowConfig);
	}
	
	public static JBoltServer create(Class<? extends JFinalConfig> jfinalConfigClass, String undertowConfig) {
		return new JBoltServer(new UndertowConfig(jfinalConfigClass, undertowConfig));
	}
	
	public static JBoltServer create(String jfinalConfigClass, String undertowConfig) {
		return new JBoltServer(new UndertowConfig(jfinalConfigClass, undertowConfig));
	}
	
	@Override
	public synchronized void start() {

		if (configConsumer != null) {
			configConsumer.accept(config);
			configConsumer = null;			// 配置在整个生命周期只能调用一次
		}
		
		loadCommandLineParameter();
		
		try {
			JBoltConsoleUtil.printJboltcn();
			System.out.println("-----------------------------------");
			System.out.println("Starting JBolt Plateform "+JBoltConst.JBOLT_VERSION+"...");
			System.out.println("JVM             : " + System.getProperty("java.version"));
			System.out.println("JFinal          : " + Const.JFINAL_VERSION);
			System.out.println("Undertow Server : " + Version.getVersionString());
			System.out.println("JFinal-Undertow : " + version );
			String url=config.getHost() + ":" + config.getPort() + getContextPathInfo();
			System.out.println("Http URL        : http://" + url);
			if (config.isSslEnable()) {
				System.out.println("Https URL       : https://" + url);
			}
			System.out.println("-----------------------------------");
			long start = System.currentTimeMillis();
			doStart();
			System.out.println("-----------------------------------");
			System.out.println("JBolt Plateform Startup Success in " + getTimeSpent(start) + " seconds(^_^)");
			System.out.println("-----------------------------------");
			/**
			 * 使用 kill pid 命令或者 ctrl + c 关闭 JVM 时，调用 UndertowServer.stop() 方法，
			 * 以便触发 JFinalConfig.onStop();
			 * 
			 * 注意：下方代码严格测试过，只支持 kill pid 不支持 kill -9 pid
			 */
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					JBoltServer.this.stop();
				}
			});
			
		} catch (Exception e) {
			e.printStackTrace();
			stopSilently();
			
			// 支持在 doStart() 中抛出异常后退出 JVM，例如端口被占用，否则在 linux 控制台 JVM 并不会退出 
			System.exit(1);
		}
	
	}
	
	@Override
	public synchronized void stop() {
		if (started) {
			started = false;	
		} else {
			return ;
		}
		System.out.println("-----------------------------------");
		System.out.println("Shutdown JBolt Server ......");
		System.out.println("-----------------------------------");
		long start = System.currentTimeMillis();
		try {
			if (hotSwapWatcher != null) {
				hotSwapWatcher.exit();
			}
			
			doStop();
			
		} catch (Exception e) {
			e.printStackTrace();
			stopSilently();
		} finally {
			System.out.println("-----------------------------------");
			System.out.println("Shutdown Complete in " + getTimeSpent(start) + " seconds. See you later (^_^)");
			System.out.println("-----------------------------------");
		}
	}

}
